from core.MultiApp import App

smalls={'built on': 'built on: BUILDDATE',
        'git repo.': "https://gitlab.cern.ch/wraight/itk-web-apps",
        'current commit': ' - built with: COMMITCODE',
        'API info.': "https://uuos9.plus4u.net/uu-dockitg01-main/78462435-41f76117152c4c6e947f498339998055/book/page?code=uuSubAppMainUuCmdList",
        'other webApps': "https://pointer-webapp.web.cern.ch",
        'contact': "📩 shellesuATcernDOTch"}

myapp = App("Common Electronics multiApp", "Common Electronics multi-theme webApp", smalls)

myapp.main()
