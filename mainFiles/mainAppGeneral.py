from core.MultiApp import App

smalls={'built on': 'built on: BUILDDATE',
        'git repo.':"https://gitlab.cern.ch/wraight/itk-web-apps",
        'current commit': ' - built with: COMMITCODE',
        # 'docker repo.':"https://hub.docker.com/repository/docker/kwraight/general-multi-app",
        'itk-docs': "https://itk-pdb-webapps.docs.cern.ch/generalApps/",
        'API info.': "https://uuos9.plus4u.net/uu-dockitg01-main/78462435-41f76117152c4c6e947f498339998055/book/page?code=uuSubAppMainUuCmdList",
        'other webApps': "https://pointer-webapp.web.cern.ch",
        'contact': "📩 wraightATcernDOTch"}

myapp = App("General multiApp", "General multi-theme webApp", smalls)

myapp.main()
