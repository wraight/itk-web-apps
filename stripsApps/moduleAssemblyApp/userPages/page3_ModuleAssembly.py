### standard
import streamlit as st
from core.Page import Page
### custom
import os
import pandas as pd
import ast
import csv
import json
import plotly.graph_objects as go
from io import BytesIO
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
from .CommonCode import CheckComponent
from .CommonCode import ColourCells
from .CommonCode import SlotFinder

#####################
### useful functions
#####################
def to_excel(df,shtNm):
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine='xlsxwriter')
    df.to_excel(writer, index=False, sheet_name=shtNm)
    workbook = writer.book
    worksheet = writer.sheets[shtNm]
    format1 = workbook.add_format({'num_format': '0.00'})
    worksheet.set_column('A:A', None, format1)
    writer.save()
    processed_data = output.getvalue()
    return processed_data


infoList=["  * upload sensor, HV tab and test fram serialNumbers in _xlsx_ data file",
        "  * review information",
        "  * reset if required",
        "  * register modules (one per sensor)",
        "  * assemble sensor to modules",
        "  * assemble module to test frames, set to 'SENSOR_ASSEMBLED' stage"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("Module Assembly", ":microscope: Strips Module Assemble", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["xls","xlsx"])
        if st.session_state.debug: st.write(pageDict['file'])

        sheetName='Sheet1'
        if pageDict['file'] is None:
            st.write("No data file set")
            filePath=os.path.realpath(__file__)
            exampleFileName="moduleAssembly_test.xlsx"
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
                st.write(os.listdir(filePath[:filePath.rfind('/')]))
            df_test=pd.read_excel(filePath[:filePath.rfind('/')]+"/"+exampleFileName, sheet_name=sheetName)
            df_xlsx = to_excel(df_test,sheetName)
            #st.dataframe(df_test)
            st.download_button(label="Download example", data=df_xlsx, file_name=exampleFileName)
            #st.write(pageDict.keys()-['file'])
            for k in pageDict.keys()-['file']:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            st.stop()


        st.write("## Read Data")

        ### read in excel file
        df_xl=pd.read_excel(pageDict['file'], sheet_name=sheetName, index_col=0)
        #df_xl.reset_index(level=0, inplace=True)
        df_xl=df_xl.reset_index()
        # if st.session_state.debug:
        #     st.write("**DEBUG** Data from file")
        #     st.dataframe(df_xl.head(10))

        # ### get header
        # splitRow=5
        # df_head=df_xl[0:splitRow].copy(deep=True)
        # df_head.columns = df_head.columns.str.replace(' ', '_')
        # df_head=df_head.iloc[:, [0, 1, 2]]
        # df_head

        ### get data
        # df_data= df_xl[splitRow+1::].copy(deep=True)
        df_data= df_xl.copy(deep=True)
        # df_data.columns = list(df_xl.iloc[splitRow].values)
        df_data.columns = df_data.columns.astype(str)
        df_data.columns = df_data.columns.str.strip()
        df_data.columns = df_data.columns.str.replace(' ', '_')
        df_data.columns = df_data.columns.str.replace('\s+', '_')
        df_data.columns = df_data.columns.str.replace('__', '_')
        df_data.columns = df_data.columns.str.replace('.', '')
        df_data.columns = df_data.columns.str.replace(' ', '_')
        try:
            df_data = df_data.drop(columns=['nan']).reset_index(drop=True)
        except KeyError:
            pass
        for c in df_data.columns:
            df_data[c]=df_data[c].astype(str)
            df_data[c]=df_data[c].str.replace(' ', '')
        st.dataframe(df_data)

        withTestFrames=False
        if len([col for col in df_data.columns if "testframe" in col.lower()])>0:
            st.write("found test frames")
            withTestFrames=True
        withHVtabs=False
        if len([col for col in df_data.columns if "hvtab" in col.lower()])>0:
            st.write("found HV tabs")
            withHVtabs=True
        withModules=False
        if len([col for col in df_data.columns if "module" in col.lower()])>0:
            st.write("found modules")
            withModules=True

        ### input stats
        # get number hybrids === number sensors
        st.write("### Preliminary stats")

        for x in df_data.columns:
            st.write("unique "+x+":",len(df_data[x].unique()))

        st.write("---")
        st.write("## Glean Data")

        ### COMPILE COMPONENTS FOR ASSEMBLY
        ### loop over components to make modules
        if "regList" not in pageDict.keys() or st.button("Read Data"):
            pageDict['regList']=[]
            st.write("### looping over data...")
            for sensor in df_data['SensorSN'].unique():
                ### set up sensor dictionary
                pageDict['regList'].append({'sensorSN':None, 'moduleSN':None,'assemblyMod':False, 'comment':None})
                ### check out SENSOR_TILE
                st.write("working on sensor:",sensor)
                pageDict['regList'][-1]['sensorSN']=sensor
                ### catch manSN for BM ASN definition
                if withTestFrames:
                     pageDict['regList'][-1]['testframeSN']=df_data.query('SensorSN=="'+sensor+'"').iloc[0]['TestFrameSN']
                     pageDict['regList'][-1]['assemblyTF']=False
                if withHVtabs:
                     pageDict['regList'][-1]['hvtabSN']=df_data.query('SensorSN=="'+sensor+'"').iloc[0]['HVTabSN']
                     pageDict['regList'][-1]['checkHVt']=False
                if withModules:
                     pageDict['regList'][-1]['moduleSN']=df_data.query('SensorSN=="'+sensor+'"').iloc[0]['ModuleSN']

        st.write("### Gleaned collections:")
        st.write(pageDict['regList'])

        snCols=['sensorSN']
        if withTestFrames:
            snCols.append('testframeSN')
        if withModules:
            snCols.append('moduleSN')
        if withHVtabs:
            snCols.append('hvtabSN')
        

        if "gleanList" not in pageDict.keys() or st.button("reset list"):
            pageDict['gleanList']=[]
            for c in pageDict['regList']:
                # get the serial numbers
                for d in [c[snc] for snc in snCols]:
                    if d==None:
                        continue
                    try:
                        pageDict['gleanList'].append(st.session_state.myClient.get('getComponent', json={'component':d} ))
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: get component **Unsuccessful** for",d)
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

        try:
            df_gleanList=pd.json_normalize(pageDict['gleanList'], sep = "_")
        except AttributeError:
            st.write("No matching registered components found in PDB.")
            st.stop()

        st.write("### Components found in Production Database")
        st.write("(colour--> _",'componentType_code',"_)")
        colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','currentLocation_code', 'assembled']
        st.dataframe(df_gleanList[colz].style.apply(ColourCells, df=df_gleanList[colz], colName='componentType_code', flip=True, axis=1))

        ####################
        ### modules part
        ####################
        st.write("---")
        st.write("## MODULE registration")
        if withModules==False:

            if "compSchema" not in pageDict.keys():
                pageDict['compSchema']=st.session_state.myClient.get('generateComponentTypeDtoSample', json={'code':"MODULE",'project':"S",'requiredOnly':True})

            if st.session_state.debug:
                st.write("**DEBUG** component schema",pageDict['compSchema'])

            compSchema_update={**pageDict['compSchema'], "institution" : st.session_state.Authenticate['inst']['code'],
                        "subproject" : "SB"}

            if st.session_state.debug:
                st.write("**DEBUG** UPDATED component schema",compSchema_update)

            count=0
            ### REGISTER BARE_MODULES
            if st.button("Register"):
                for rl in pageDict['regList']:
                    if rl['moduleSN']!=None:
                        st.write("Module SN already set. skip")
                        continue
                    if rl['sensorSN']==None:
                        st.write("Sensor issue: no sensor found. skip")
                        continue
                    #20USBSL0000052
                    modType = rl['sensorSN'][6]
                    wafNum = rl['sensorSN'][9:]
                    st.write("Try register MODULE")
                    try:
                        regVal=st.session_state.myClient.post('registerComponent', json={**compSchema_update, 'type':"BARREL_"+modType+"S_MODULE",
                                                                                            'properties': {
                                                                                                'LOCALNAME': 'QMUL_PPA_'+modType+'S_MODULE_'+wafNum,
                                                                                                'HV_TAB_ASSEMBLY_JIG': 'HV-Tab Bonding Jig #002'
                                                                                            }} )
                        st.write("### **Successful Registration**:",regVal['component']['serialNumber'])
                        rl['moduleSN']=regVal['component']['serialNumber']
                        count+=1
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: registration **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

            myRegs=[x['moduleSN'] for x in pageDict['regList'] if x['moduleSN']!=None]
            st.write("### Registered components:",len(myRegs))
            for k,rl in enumerate(pageDict['regList']):
                if rl['moduleSN']==None: continue
                infra.ToggleButton(pageDict,'togReg_'+str(k),"R"+str(k)+": "+rl['moduleSN'])
                #st.write("Component:",rl['moduleSN'])
                if pageDict['togReg_'+str(k)]:
                    if st.button("delete",key=k):
                        try:
                            delVal=st.session_state.myClient.post('deleteComponent', json={'component':rl['moduleSN']} )
                            st.write("**Successful Deletion**:",rl['moduleSN'])
                            rl['moduleSN']=None
                        except itkX.BadRequest as b:
                            st.write(":no_entry_sign: deletion **Unsuccessful**")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        else:
            st.write("__Module column found in input so registration process is skipped.__")


        st.write("---")
        st.write("## MODULE assembly (sensor to module)")

        ### ASSEMBLE PARTS
        if st.button("Assemble",key="mods"):
            for rl in pageDict['regList']:
                st.write("working on MODULE:",rl['moduleSN'])
                compBM=st.session_state.myClient.get('getComponent', json={"component":rl['moduleSN']} )
                #st.write(compBM)
                assCheck=True
                try:
                    st.write("assemble sensor:",rl['sensorSN'])
                    if rl['hvtabSN']!=None:
                        st.write(f"assembling with HVTab info. {rl['hvtabSN']}")
                        assVal=st.session_state.myClient.post('assembleComponent', json={"parent" : rl['moduleSN'], "child" : rl['sensorSN'], 'properties':{'HV_TAB_SHEET':rl['hvtabSN']} } )
                    else:
                        assVal=st.session_state.myClient.post('assembleComponent', json={"parent" : rl['moduleSN'], "child" : rl['sensorSN']} )
                    st.write("### **Successful sensor assembly**:",assVal['id'])
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: assembly **Unsuccessful**")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                    assCheck=False
                if assCheck: rl['assemblyMod']=True

        myAsss=[x['assemblyMod'] for x in pageDict['regList'] if x['assemblyMod']==True]
        st.write("### Assembled components:",len(myAsss))
        if not withTestFrames:
            df_asss=pd.DataFrame([ x for x in pageDict['regList']if x['assemblyMod']==True])
            st.dataframe(df_asss)
            st.download_button(label="download csv", data=df_asss.to_csv(index=False),file_name="assembled_info.csv")
        for k,rl in enumerate(pageDict['regList']):
            if rl['assemblyMod']==False: continue
            infra.ToggleButton(pageDict,'togAss_'+str(k),"A"+str(k)+": "+rl['moduleSN'])
            #st.write("Component:",rl['BM'])
            if pageDict['togAss_'+str(k)]:
                modComp=st.session_state.myClient.get('getComponent', json={"component" : rl['moduleSN']} )
                if st.button("disassemble",key=k+1000):
                    for part in [rl['sensorSN']]:
                        st.write("part:",part)
                        partComp=st.session_state.myClient.get('getComponent', json={"component" : part} )
                        try:
                            disVal=st.session_state.myClient.post('disassembleComponent', json={"parent" : modComp['id'],
                                                                                         "child" : partComp['id']} )
                            st.write("**Successful disassembly**:",disVal['id'])
                            rl['assemblyMod']=False
                        except itkX.BadRequest as b:
                            st.write(":no_entry_sign: deletion **Unsuccessful**")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

        ####################
        ### test frames part
        ####################
        if withTestFrames:
            st.write("---")
            st.write("## Test Frame assembly (module to test frame)")

            ### ASSEMBLE PARTS
            if st.button("Assemble",key="tfs"):
                for rl in pageDict['regList']:
                    st.write("working on TESTFRAME:",rl['testframeSN'])
                    compBM=st.session_state.myClient.get('getComponent', json={"component":rl['testframeSN']} )
                    #st.write(compBM)
                    assCheck=True
                    try:
                        st.write("assemble module:",rl['moduleSN'])
                        assVal=st.session_state.myClient.post('assembleComponent', json={"parent" : rl['testframeSN'], "child" : rl['moduleSN']} )
                        st.write("### **Successful module assembly**:",assVal['id'])

                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: assembly **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                        assCheck=False
                    if assCheck: rl['assemblyTF']=True

            for rl in pageDict['regList']:
                if rl['assemblyTF']==False: continue
                ### set stage
                TFstage="SENSOR_ASSEMBLED"
                try:
                    pageDict['setVal']=st.session_state.myClient.post('setComponentStage', json={'component':rl['testframeSN'], 'stage':TFstage})
                    st.write("### **Successful stage set** (",TFstage,") for:",pageDict['setVal']['serialNumber'])
                    # df_set=pd.json_normalize(pageDict['setVal'], sep = "_")
                    # st.dataframe(df_set)
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: Stage (",TFstage,")setting **Unsuccessful**")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

            st.write("---")
            myAsss=[x['assemblyTF'] for x in pageDict['regList'] if x['assemblyTF']==True]
            st.write("### Assembled components with TestFrame:",len(myAsss))
            df_asss=pd.DataFrame(pageDict['regList'])
            st.dataframe(df_asss)
            st.download_button(label="download csv", data=df_asss.to_csv(index=False),file_name="assembled_info.csv")

            for k,rl in enumerate(pageDict['regList']):
                if rl['assemblyTF']==False: continue
                infra.ToggleButton(pageDict,'togAss_'+str(k),"A"+str(k)+": "+rl['moduleSN'])
                #st.write("Component:",rl['BM'])
                if pageDict['togAss_'+str(k)]:
                    tfComp=st.session_state.myClient.get('getComponent', json={"component" : rl['testframeSN']} )
                    if st.button("disassemble",key=k+1000):
                        for part in [rl['moduleSN']]:
                            st.write("part:",part)
                            partComp=st.session_state.myClient.get('getComponent', json={"component" : part} )
                            try:
                                disVal=st.session_state.myClient.post('disassembleComponent', json={"parent" : tfComp['id'],
                                                                                             "child" : partComp['id']} )
                                st.write("**Successful disassembly**:",disVal['id'])
                                rl['assemblyTF']=False
                            except itkX.BadRequest as b:
                                st.write(":no_entry_sign: deletion **Unsuccessful**")
                                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        
        ####################
        ### HV tabs part
        ####################
        if withHVtabs:
            st.write("---")
            st.write("## HV tabs update")

            hvDict={}
            for rl in pageDict['regList']:
                if "hvtabSN" in rl.keys():
                    try:
                        hvDict[rl['hvtabSN']]+=1
                    except KeyError:
                        hvDict[rl['hvtabSN']]=1

            st.write("### HV tabs information")
            st.write(hvDict)


            ### ASSEMBLE PARTS
            if st.button("HVtab update",key="hvt"):
                for k,v in hvDict.items():
                    st.write("working on HV tab:",k)
                    compHVT=st.session_state.myClient.get('getComponent', json={"component":k} )
                    #st.write(compBM)
                    st.write(f"{k} properties:")
                    st.dataframe(compHVT['properties'])
                    tabCode="NUMMBER_TABS_FOR_MODULES"
                    modTabs=next((p['value'] for p in compHVT['properties'] if p['code']==tabCode), "None")
                    st.write("Module tabs read (before update):", modTabs )

                    # hack for unset
                    if modTabs==None:
                        modTabs=0
                    
                    prop_json={
                        'component': compHVT['code'],
                        'code': tabCode,
                        'value': modTabs+v,
                    }
                    
                    try:
                        upVal=st.session_state.myClient.post('setComponentProperty', json=prop_json )
                        st.write("### **Successful update**:",upVal)
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: update **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
