### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

infoList=["  * Compile components registered at institiute"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Inventory", ":microscope: Components at Institute", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        st.write("### Check componentTypes in PDB")
        if st.button("reset list") or "compList" not in list(pageDict.keys()): # check list
            st.write("**Getting** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            pageDict['compList']=st.session_state.myClient.get('listComponents',json={'institution':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] })
            if type(pageDict['compList'])!=type([]):
                pageDict['compList']=pageDict['compList'].data
        else:
            st.write("**Got** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
        df_compList=pd.json_normalize(pageDict['compList'], sep = "_")
        df_compTypes=df_compList[['componentType_name','componentType_code']].drop_duplicates()
        st.dataframe(df_compTypes)

        infra.SelectBoxDf(pageDict,'compType',df_compTypes,'Select a componentType', 'componentType_code')
        if st.session_state.debug:
            st.write("**DEBUG** *componentType* id")
            st.write(pageDict['compType'])

        st.write("### List of *"+pageDict['compType']['componentType_name'].values[0]+"* components")
        df_comps=df_compList.query('componentType_code=="'+pageDict['compType']['componentType_code'].values[0]+'"')
        st.dataframe(df_comps[['componentType_name','componentType_code','currentLocation_code']])

        if st.button("Write inventory"):
            st.write("**To Do**: write inventory")
