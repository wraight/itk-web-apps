### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

infoList=["  * Load and register a MODULE in PDB"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Assemble", ":microscope: Check component in PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ## get ring component
        ## check constituents
        ## show fileds per component, pre-filled if constituent exists
        ## add checks for components

        # parts
        st.write("### Constiuent selection")

        infra.TextBox(pageDict,'hxCode',"Input hybridX ID:")
        if st.button("hybridX check"):
            compCheck=True
            pageDict['hxComp']=st.session_state.myClient.get('getComponent', json={'component':pageDict['hxCode']})
            if st.session_state.debug: st.write(pageDict['hxComp'])
            try:
                if pageDict['hxComp']['componentType']['code']!="HYBRID":
                    st.write("Problem with component type:",pageDict['sComp']['componentType']['code'])
                    compCheck=False
            except TypeError:
                st.write("Problem with database return")
                compCheck=False

        infra.TextBox(pageDict,'hyCode',"Input hybridY ID:")
        if st.button("hybridY check"):
            compCheck=True
            pageDict['hyComp']=st.session_state.myClient.get('getComponent', json={'component':pageDict['hyCode']})
            if st.session_state.debug: st.write(pageDict['hyComp'])
            try:
                if pageDict['hyComp']['componentType']['code']!="HYBRID":
                    st.write("Problem with component type:",pageDict['sComp']['componentType']['code'])
                    compCheck=False
            except TypeError:
                st.write("Problem with database return")
                compCheck=False

        infra.TextBox(pageDict,'sCode',"Input sensor ID:")
        if st.button("sensor check"):
            compCheck=True
            pageDict['sComp']=st.session_state.myClient.get('getComponent', json={'component':pageDict['sCode']})
            if st.session_state.debug: st.write(pageDict['hxComp'])
            try:
                if pageDict['sComp']['componentType']['code']!="SENSOR":
                    st.write("Problem with component type:",pageDict['sComp']['componentType']['code'])
                    compCheck=False
            except TypeError:
                st.write("Problem with database return")
                compCheck=False

        infra.TextBox(pageDict,'pCode',"Input powerboard ID:")
        if st.button("powerboard check"):
            compCheck=True
            pageDict['pComp']=st.session_state.myClient.get('getComponent', json={'component':pageDict['pCode']})
            if st.session_state.debug: st.write(pageDict['hxComp'])
            try:
                if pageDict['pComp']['componentType']['code']!="POWERBOARD":
                    st.write("Problem with component type:",pageDict['pComp']['componentType']['code'])
                    compCheck=False
            except TypeError:
                st.write("Problem with database return")
                compCheck=False

        if st.button("Assemble!"):
            st.balloons()
            st.write("**To Do** assemble")
