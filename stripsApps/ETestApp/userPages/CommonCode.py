###### Author: Dengfeng Zhang(dengfeng.zhang@cern.ch)
### standard
import streamlit as st
### custom
import os
import pandas as pd
import ast
import csv
import json
from datetime import datetime
### PDB stuff
import itkdb.exceptions as itkX

#####################
### useful functions
#####################
def ListComponentsByType(comTypeList, stageList):

  if len(comTypeList)==0 or len(stageList)==0: 
    return None
  comList = []

  # get the user
  try:
    user = st.session_state.myClient.get('getUser', json={'userIdentity': st.session_state.myClient.user.identity})
  except Exception:
    st.write("Wrong accessCode!!!!")
    return None

  currentLocation = user['institutions'][0]['code']
  # Get all components at your institute
  componentList = st.session_state.myClient.get('listComponents', json={'currentLocation': currentLocation}) # for current location
  
  for component in componentList:
    skip=False
    for k in ['serialNumber','dummy']:
      if k in component.keys():
          try:
            if component[k] == None: # missing data
              skip=True
          except TypeError: # missing structure
            skip=True
    if skip:
      continue
    for k in ['componentType','currentStage']:
      if k in component.keys():
        if type(component[k])!=type({}) or "code" not in component[k].keys(): # missing structure
          skip=True
        else:
          if component[k]['code']==None: # missing structure
            skip=True
    if skip:
      continue
    # st.write(f"{component['serialNumber']}: {component['currentStage']['code']}")
    if component['componentType']['code'] in comTypeList:
      if component["currentStage"]['code'] in stageList:
        comList.append(component["serialNumber"])
  
  if len(comList)==0:
    return None
  return comList

def listChildComponent(serialNumber):

  parentComponent = st.session_state.myClient.get('getComponent', json={'component': serialNumber}) # for current location
  if parentComponent["children"]==None:
    return None

  comList = []
  for childComponent in parentComponent["children"]:
    if childComponent["component"]==None: continue
    if childComponent["component"]["serialNumber"]==None: continue
    #print("  ", child["componentType"]["name"], child["component"]["serialNumber"])
    if childComponent["componentType"]["code"] == "HYBRID_ASSEMBLY":
      comList.append(childComponent["component"]["serialNumber"])

  if len(comList)==0:
    return None

  return comList

## List electrical test json files
def listJsonFiles(inPath, serialNumberList):
  fileMaps = {}

  fileList = os.listdir(inPath)
  for serialNumber in serialNumberList:
    if serialNumber not in fileMaps.keys():
      fileMaps[serialNumber] = {}
    for filename in fileList:
      if not filename.endswith(".json"):
        continue
      inFile = open(inPath+"/"+filename, "r")
      data = json.load(inFile)
      inFile.close()
      if data["component"] != serialNumber:
        continue
      if data["testType"] not in fileMaps[serialNumber].keys(): 
        fileMaps[serialNumber][data["testType"]] = []
        fileMaps[serialNumber][data["testType"]].append(filename)
  if len(fileMaps)==0:
    return None
  return fileMaps

## match electrical test json files
def readJsonFiles(inFiles, serialNumberList):
  fileMaps = {}

  # st.write("serialNumberList:",serialNumberList)
  # st.write("inFiles:",inFiles)
  for sn in serialNumberList:
    # add to dict if missing
    if sn not in fileMaps.keys():
      fileMaps[sn] = {}
    # add file data
    for inFile in inFiles:
      if sn in inFile.name:
        ### get data
        data = json.loads(inFile.getvalue().decode("utf-8"))
        if data["testType"] not in fileMaps[sn].keys(): 
          fileMaps[sn][data["testType"]] = {}
        fileMaps[sn][data["testType"]][inFile.name] = inFile

  # st.write("fileMaps",fileMaps)

  # fileList = []
  # for inFile in inFiles:
  #    if inFile.name.split("_")[1] in serialNumberList:
  #      fileList.append(inFile.name)

  # for serialNumber in serialNumberList:
  #   if serialNumber not in fileMaps.keys():
  #     fileMaps[serialNumber] = {}
  #   for inFile in inFiles:
  #     if inFile.name not in fileList: continue
  #     if inFile.name.split("_")[1] != serialNumber: continue
  #     data = json.loads(inFile.getvalue().decode("utf-8"))
  #     if data["testType"] not in fileMaps[serialNumber].keys(): 
  #       fileMaps[serialNumber][data["testType"]] = {}
  #     fileMaps[serialNumber][data["testType"]][inFile.name] = inFile

  if len(fileMaps)==0:
    return None
  return fileMaps

