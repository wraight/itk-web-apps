### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
### local stuff
from .CommonCode import EditJson, SelectCheck
from .CommonCode import testerOptions
from .CommonCode import GetModuleWireBondList, GetGleanListEDMS
import commonCode.codeChunks as chnx
import commonCode.PDBTricks as pdbTrx

from .module_wirebond import module_wirebond

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _csv_ module wire bonding file",
        " * review retrieved data & visualisation",
        " * review analysis checks",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("Module Wire Bond Upload", ":microscope: Upload Module Wire Bonding", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULE"
        if "testType" not in pageDict.keys():
            pageDict['testType']="MODULE_WIRE_BONDING"
        if "stage" not in pageDict.keys():
            pageDict['stage']="BONDED"
        if "project" not in pageDict.keys():
            pageDict['project']="S"

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_S.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testType']+'"').reset_index(drop=True)
        st.write("### componentTypes and stages for testType=="+pageDict['testType'])
        st.dataframe(df_stageTest)

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["csv"])
        if st.session_state.debug: st.write(pageDict['file'])

        if pageDict['file'] is not None:
            headerList, dataList = GetModuleWireBondList(pageDict['file'])
            try:
                df_header = pd.DataFrame(headerList, columns=['key','value',' ','  '])
                df_data = pd.DataFrame(dataList, columns=['Section','Channel','Comment',''])
            except ValueError:
                st.write("Check your data format!")
                pageDict['file'].seek(0)
                df_header = None
                df_data = None
        else:
            st.write("No data file set")
            filePath=os.path.realpath(__file__)
            exampleFileName="Module_Wire_Bonding_Example.csv"
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
            st.download_button(label="Download example", data=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+exampleFileName).to_csv(index=False),file_name=exampleFileName)
            st.write("See [EDMS document](https://edms.cern.ch/ui/file/2228437/3.4/Bonding-Procedures_Modules.pdf) for details on module wire bonding and data format.")
            for k in ['testSchema', 'toggleEdit', 'toggleText']:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            st.stop()

        ### US date format toggle
        #infra.ToggleButton(pageDict,'toggleUS','adapt US date format? (%m/%d/%Y->%d/%m/%Y)')
        if st.session_state.debug:
            st.write("Gleaned lists")
            st.write(headerList)
            st.write(dataList)

        for i, col in enumerate(df_header.columns):
            df_header.iloc[:, i] = df_header.iloc[:, i].str.replace('"', '')
        for i, col in enumerate(df_data.columns):
            df_data.iloc[:, i] = df_data.iloc[:, i].str.replace('"', '')

        # show gleaned information in dataframes
        st.write("### header info.")
        st.dataframe(df_header)
        st.write("### data info.")
        st.dataframe(df_data)

        if "testSchema" not in pageDict.keys() or st.button("re-read file"):
          pageDict['testSchema'] = module_wirebond(st.session_state.myClient, headerList+dataList)

        ### Analysis checks
        st.write("### Analysis checks")
        failAna = False
        if not pageDict['testSchema']:
          failAna = True
        if failAna:
            st.write("### :broken_heart: **Analysis failed**")
        else:
            st.write("### :green_heart: **Analysis passed**")
            st.write(pageDict['testSchema'])
            st.write("##########MODULE WIRE BONDING##########")
            if pageDict['testSchema']['passed']:
              st.write("            Passed Test")
            else:
              st.write("            Failed Test")
            st.write("#######################################")

        ## Save file
        #infra.ToggleButton(pageDict,'toggleText','Convert values to text?')
        #if pageDict['toggleText']:
        #    pageDict['testSchema'] = json.loads(json.dumps(pageDict['testSchema']), parse_int=str)
        if st.session_state.debug:
            st.write("### **DEBUG** (final!) Test Schema")
            st.write(pageDict['testSchema'])

        st.write("---")
        st.write("## Test Registration")

        # check existing component tests
        chnx.StageCheck(pageDict)

        # upload(!) test schema: change get --> post
        chnx.RegChunk(pageDict, "TestRun", "testSchema")
