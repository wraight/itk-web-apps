# Strips Metrology webApp

Tools to upload metrology test information for Strips Hybrids and Modules.

---

## Pages
  * upload module/hybrid metrology via formatted dat file, based on PDB schema
  * analysis thanks to _Ian Dyckes_, _Theo Zorbas_ and _Mitch Norfolk_. Code [here](https://gitlab.cern.ch/gdyckes/metrologyanalysis/-/tree/master/)

### multiUpload  (TBC)
  * upload multiple component information
