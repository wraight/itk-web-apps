### standard
import streamlit as st
### custom
import os
import pandas as pd
import numpy as np
import ast
import csv
import json
import base64
from datetime import datetime
### PDB stuff
import itkdb.exceptions as itkX
import randomname

#####################
### useful functions
#####################

def GetGleanList(fileObj, compType="MODULE"):
    header=[]
    data=[]
    change = False

    ### tool to check delimiter
    sniffer = csv.Sniffer()

    for raw_line in fileObj.readlines():
        #st.write("raw line:",line)
        line=raw_line.decode("ISO-8859-1")
        # st.write(line)
        if not line:
            break

        if change==False:
            if "sens" in compType.lower():
                if "mm" in line:
                    change=True
                    continue
            else:
                if "Position" in line or "Bow" in line:
                    change=True
                    continue
            appList=[np.NaN for x in range(0,6,1)]
            for i,x in enumerate(line.split(': ')):
                appList[i]=x.strip()
            # header.append([x.strip() for x in line.split(': ')])
            header.append(appList)
        elif change==True:
            if "#" in line: continue
            appList=[np.NaN for x in range(0,6,1)]
            delim=sniffer.sniff(line).delimiter
            splitArr=[x for x in line.split(delim) if len(x)>0]
            for i,x in enumerate(splitArr):
                if len(x)<1:
                    continue
                appList[i]=x.strip()
            data.append(appList)
    return header, data

def GetAnaObjs(fileObj):

    #Read file into list.  Each elemenet is a line (string)
    content = []

    fileObj.seek(0, 0)
    for raw_line in fileObj.readlines():
        #st.write("raw line:",line)
        line=raw_line.decode("ISO-8859-1")
        content.append(line)
        # st.write(line)
        if not line:
            break

    # You may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    # Remove empty lines
    content = [x for x in content if not len(x)==0]

    metaDict = {}
    for line in content:
        if line.startswith("#") and "HEADER" in line.upper(): #skip the #---Header line.  Try to be robust to e.g. '# header' vs. '#---Header:'
            continue
        if line.startswith("#"): # start of position data.
            break
        field = line.split(":")[0]
        value = ":".join(line.split(":")[1:]).lstrip().rstrip()
        metaDict[field]=value

    return content, metaDict

def MakeUniqueName():
    # random bit
    name=randomname.get_name(adj=('physics',), noun=('cats'))
    # with date
    name=name+"_"+datetime.now().strftime('%Y_%m_%d')
    return name

def DisplayPDF(file):
    # Opening file from file path
    with open(file, "rb") as f:
        base64_pdf = base64.b64encode(f.read()).decode('utf-8')

    # Embedding PDF in HTML
    pdf_display = F'<iframe src="data:application/pdf;base64,{base64_pdf}" width="700" height="550" type="application/pdf"></iframe>'
    # Displaying File
    st.markdown(pdf_display, unsafe_allow_html=True)
