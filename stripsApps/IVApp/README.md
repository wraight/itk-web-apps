# Strips IV webApp

Tools to help Strips Sensor, Module and Powerboard IV measurements. 

---

## Pages

### Sensor IV Upload
  * upload module test via formatted dat file, based on PDB schema
  * using code developed by David Rousso, Dominic Jones and Pavla Federicova from [here](https://gitlab.cern.ch/cambridge-ITK/production_database_scripts/-/blob/sensor_updates/strips/sensors/sensor_tests_interfacing_functions/CommonConfig.py)

### Module IV Upload
  * upload module test via formatted dat file, based on PDB schema
  * using code developed by David Rousso, Dominic Jones and Pavla Federicova from [here](https://gitlab.cern.ch/cambridge-ITK/production_database_scripts/-/blob/sensor_updates/strips/sensors/sensor_tests_interfacing_functions/CommonConfig.py)

### multiUpload (TBC)
  * upload multiple component information
