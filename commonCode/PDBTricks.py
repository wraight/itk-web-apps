### standard
import ast
### custom
import os
from datetime import datetime

### PDB stuff
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
import streamlit as st
import pandas as pd

### common stuff
import commonCode.StreamlitTricks as stTrx


#####################
### useful functions
#####################
### pull value out of json
def search_nested_json(json_obj, key):
    if isinstance(json_obj, dict):
        if key in json_obj:
            return json_obj[key]
        for k, v in json_obj.items():
            result = search_nested_json(v, key)
            if result is not None:
                return result
    elif isinstance(json_obj, list):
        for item in json_obj:
            result = search_nested_json(item, key)
            if result is not None:
                return result
    return None

### format PDB error message
def DigestMessage(err_json):
    ### initial message
    try:
        err_val=next((err_json[e] for e in err_json.keys() if "ucl-itkpd-main" in e),None)
        st.write(f"Error Message:")
        st.write(f"\t__{err_val['message']}__")
    except KeyError:
        # st.write("This error is not digestible...")
        st.write(err_json)
        return None
    ### errors with keys: missing/extra
    gotKeyIssue=False
    # st.write("Checking json keys...")
    for k in ['unsupportedKeyList','missingKeyMap']: #'invalidValueKeyMap']: #:
        search_key=search_nested_json(err_val['paramMap'], k)
        if search_key!=None:
            st.write(f" - issue: {k}")
            if k=="unsupportedKeyList":
                for l in search_key:
                    print(f"\t - {l.replace('$.','')}")
            if k=="missingKeyMap":
                for l,w in search_key.items():
                    print(f"\t - {l.replace('$.','')}: {w}")
            gotKeyIssue=True
    if gotKeyIssue:
        st.write("Please update keys")
        return None
    # else:
    #     st.write(" - error keys seems ok")
    ### errors with invalid stuff
    gotValidityIssue=False
    # st.write("Checking validity...")
    for k in ['invalidValueKeyMap']: #:
        search_key=search_nested_json(err_val['paramMap'], k)
        if search_key==None:
            # st.write("Cannot digest error :(")
            break
        for l in search_key.keys():
            if l=="$":
                continue
            st.write(f" - issue: {l.replace('$.','')}")
            st.write("\t -",list(search_key[l].values())[0])
            gotValidityIssue=True
    if gotValidityIssue:
        st.write("Please update values.")
        return None
    # else:
    #     st.write(" - error validity seems ok")
    if gotKeyIssue!=True and gotValidityIssue!=True:
        # st.write("Cannot digest error :(")
        st.write("Parameter map...")
        st.write(err_val['paramMap'])
    else:
        st.write("done!")

def SetCompStage(compCode, stageCode):
    try:
        setVal=st.session_state.myClient.post('setComponentStage', json={'component':compCode, 'stage':stageCode})
        st.write(f"### **Successful stage set** ({stageCode}) for:",FindKey(setVal,'id'))
        return True
    except itkX.BadRequest as b:
        st.write(f"### :no_entry_sign: Stage ({stageCode}) setting **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        return False

def UploadData(upCmd,upSchema):

    ### add reason to delete schema
    if "delete" in upCmd.lower():
        try:
            upSchema['reason']+=" -> deleting from webApp"
        except KeyError:
            upSchema['reason']="deleting from webApp"


    try:
        upVal=st.session_state.myClient.post( upCmd, json=upSchema)
        stTrx.Flourish()
        #st.write(upVal)
        if "component" in upCmd.lower():
            st.write("### **Successful "+upCmd+" **:",FindKey(upVal))
        elif "batch" in upCmd.lower():
            st.write("### **Successful "+upCmd+" **:",FindKey(upVal,'id'))
        elif "testrun" in upCmd.lower():
            st.write("### **Successful "+upCmd+" **:",FindKey(upVal['testRun'],'id'))
        else:
            st.write("### **Successful "+upCmd+" **")
        return upVal
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: "+upCmd+" **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
    except TypeError:
        st.write(upCmd+": Don't have return value :(")
    return None

def UploadFile(pageDict,upID,fileKey,title=None):
    try:
        ### faff to create file for binary upload - reading direct from buffer loses name(?!)
        tmpPath="/tmp"
        fnew=open(tmpPath+"/"+pageDict[fileKey].name, "w")
        ### haxk to use latin-1 encoding: https://stackoverflow.com/questions/5552555/unicodedecodeerror-invalid-continuation-byte
        fnew.write(pageDict[fileKey].getvalue().decode('latin-1'))
        fnew.close()
        if st.session_state.debug:
            st.write([f for f in os.listdir(tmpPath) if os.path.isfile(os.path.join(tmpPath, f))])
            st.write("use:",tmpPath+"/"+pageDict[fileKey].name)
        # f = open(tmpPath+"/"+pageDict[fileKey].name, "rb")
        with open(tmpPath+"/"+pageDict[fileKey].name, "rb") as f: 
            ### set attachment name
            fileTitle="resultsFile"
            if title!=None:
                fileTitle=title
            attVal=st.session_state.myClient.post('createTestRunAttachment', data=dict(testRun=upID,title=fileTitle,type="file"), files=dict(data=f))
            st.write("### **Successful File Upload**:",attVal['filename'])
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: File Upload **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

def DeleteData(delCmd,delSchema):
    try:
        delVal=st.session_state.myClient.post(delCmd, json=delSchema)
        #st.write("delVal:",delVal)
        st.write("### **Successful "+delCmd+" **")#":",FindKey(delVal,id))
        return delVal
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: "+delCmd+" **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
    except TypeError:
        st.write(delCmd+": Don't have return value :(")
    return None

def CheckComponent(sn, compType=None, curLoc=None, debug=False):
    ### mostly Carl's comp check code

    compCheck=""
    if debug: st.write("getComponent")
    try:
        retVal=st.session_state.myClient.get('getComponent', json={'component':sn})
        if debug: st.write("found:",sn,"("+retVal['componentType']['code']+")")
    except itkX.BadRequest:
        if debug: st.write("No component found")
        return "False: No component found "

    # Check type of component is as expected
    if debug: st.write("check type")
    try:
        if retVal['componentType']['code'] != compType:
            if debug: st.write("Unexpected component type for "+compType+":", retVal['componentType']['code'])
            compCheck+="False: Wrong componentType "
    except TypeError:
        if debug: st.write("Problem with database return")
        compCheck+="False: return issue "

    # Check at correct institution
    if debug: st.write("check inst.")
    try:
        if retVal['currentLocation']['code'] != curLoc:
            if debug: st.write("Component not "+curLoc+", it\'s:", retVal['currentLocation']['code'])
            compCheck="False: Wrong currentLocation "
    except TypeError:
        st.write("Problem with database return")
        compCheck="False: return issue "

    # Check not already attached to a bare module
    try:
        if retVal["parents"] is None:
                used = []
        else:
            used = [c["componentType"]["id"] for c in retVal["parents"] if c["componentType"]["code"] == "BARE_MODULE"]
        if len(used)>0:
            if debug: st.write("Component already attached to bare module:", used[0])
            compCheck="False: attach issue "
    except TypeError:
        if debug: st.write("Problem with database return")
        compCheck="False: return issue "

    # Check at correct institution
    if debug: st.write("check state")
    try:
        if retVal['state'] != "ready":
            if debug: st.write("Component not ready:", retVal['state'])
            compCheck="False: Wrong state "
    except TypeError:
        st.write("Problem with database return")
        compCheck="False: return issue "

    return compCheck


def GetDataType(inDict, inVal, inKey=None):
    ### define map dataType
    matchMap={'int':int,'integer':int,'float':float,'list':list,'dict':dict,'str':str,'string':str,'bool':bool}
    
    if inDict['dataType'] in ["string","integer","float"]:
        if inVal!=None:
            retVal=st.text_input(inDict['name']+" :", value=inVal, key=inKey)
        else:
            retVal=st.text_input(inDict['name']+" :", key=inKey)
        
    elif inDict['dataType']=="bool":
        if inVal!=None:
            retVal=st.radio(inDict['name']+" :", [True, False], index=["True", "False"].index(str(inVal)), key=inKey)
        else:
            retVal=st.radio(inDict['name']+" :", [True, False], key=inKey)

    elif inDict['dataType']=="codeTable":
        # st.write("found codeTable")
        # st.write(inDict['codeTable'])
        if inVal!=None:
            try:
                inIdx=[ct['code'] for ct in inDict['codeTable']].index(inVal)
            except ValueError:
                inIdx=0
            val=st.selectbox("code :", [ct['value'] for ct in inDict['codeTable']], index=inIdx, key=inKey)
        else:
            val=st.selectbox("code :", [ct['value'] for ct in inDict['codeTable']], key=inKey)
        retVal= next((item['code'] for item in inDict['codeTable'] if item['value'] == val), None)

    else:
        st.write("No match to dataType:",inDict['dataType'])
        return retVal

    ### map value to dataType (unless codeTable!)
    if inDict['dataType']!="codeTable":
        retVal= matchMap[inDict['dataType']](retVal)
    # st.write("returning:",retVal)
    return retVal

def InputType(inDict, inVal, inKey=None):

    retVal=None
    if inDict['valueType']=="single":
        retVal=GetDataType(inDict, inVal, inKey=None)

    elif inDict['valueType']=="array":
        st.write(f"Please select {inDict['code']} array size")
        max=10
        if len(inVal)>max:
            max=len(inVal)
        numList=list(range(1,max+1,1))
        retSize=st.selectbox("Size :", numList, index=numList.index(len(inVal)), key=inKey)

        retVal=[None for x in range(0,int(retSize),1)]
        for e,x in enumerate(retVal):
            st.write(f"- {inDict['code']} array entry {e+1}")
            # st.write("entry",e)
            try:
                retVal[e]=GetDataType(inDict, inVal[e], inKey=e+500)
            except IndexError:
                retVal[e]=GetDataType(inDict, None, inKey=e+500)

    else:
        if inDict['dataType']=="codeTable":
            retVal=GetDataType(inDict, inVal, inKey=None)
        else:
            st.write("No match to valueType:",inDict['valueType'])
    
    # st.write("more return:",retVal) 
    return retVal


def SelectCheck(k,v,inJson, inKey=None):
    try:
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v), key=inKey)
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, type='default', key=inKey)
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    except AttributeError: # non string types will not have lower() attribute
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v), key=inKey)
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, type='default', key=inKey)
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    inVal=stTrx.Tryeval(val)
    inVal=stTrx.MatchType(v,inVal)
    return inVal

def EditJson(inJson):
    for k,v in inJson.items():
        if type(v)==type({}):
            st.write("**"+str(k)+"**")
            for l,w in v.items():
                # inJson[k][l]=SelectCheck(l,w)
                inJson[k][l]=SelectCheck(l,w,inJson)
        else:
            # inJson[k]=SelectCheck(k,v)
            inJson[k]=SelectCheck(k,v,inJson)
    return inJson


# evaluate input string (try to catch lists)
    for k,v in inJson.items():

        if type(v)==type({}):
            st.write("**"+str(k)+"**")
            for l,w in v.items():
                inJson[k][l]=SelectCheck(l,w)
        else:
            inJson[k]=SelectCheck(k,v)
    return inJson

def FindKey(inDict, myKey='serialNumber'):

    return search_nested_json(inDict, myKey)

    # try:
    #     return inDict[myKey]
    # except KeyError:
    #     for k,v in inDict.items():
    #         if type(v)==type({}):
    #             # st.write("looking in ",k)
    #             val=FindKey(v, myKey)
    #             if val!=None:
    #                 return val
    #             else:
    #                 return None
    
    # return None

def DeleteCoTeSt(pageDict,dictKey,delCmd,ident):
    infra.ToggleButton(pageDict,dictKey,'Delete?')
    # st.write(ident)
    if pageDict[dictKey]:
        if st.button("Are you sure?",key=dictKey+delCmd):
            try:
                idType=delCmd.replace('delete','').lower()
                ### hack to recover camelCase
                if idType=="testrun":
                    idType="testRun"
                delVal=st.session_state.myClient.post(delCmd, json={idType:ident, 'reason':"webApp delete"} )
                st.write("**Successful "+delCmd+"**:",ident)
            except itkX.BadRequest as b:
                st.write(":no_entry_sign: "+delCmd+" **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

def SelectManyDf(pageDict,df,colName,dictKey=None):
    if dictKey==None:
        tmpKey=colName+"_many"
    else:
        tmpKey=dictKey
    infra.SelectBox(pageDict,tmpKey,df[colName].unique(),'Select '+colName+':')
    return df.query(colName+'=="'+pageDict[tmpKey]+'"')

def SelectOneDf(pageDict,df,colName,dictKey=None,altID=False):
    if dictKey==None:
        tmpKey=colName+"_one"
    else:
        tmpKey=dictKey
    df_many=SelectManyDf(pageDict,df,colName,dictKey)
    ident='serialNumber'
    if altID: ident='alternativeIdentifier'
    infra.SelectBoxDf(pageDict,tmpKey,df_many,'Select individual:',ident)
    return

def GetComponentByIdentifier(ident,type="SN"):
    comp=None
    if type=="SN" or type=="serialNumber":
        try:
            comp=st.session_state.myClient.get('getComponent', json={'component':ident})
        except KeyError:
            st.write("- no ASN found.")
        except itkX.BadRequest as err:
            DigestMessage(err.response.json()['uuAppErrorMap'])
    elif type=="alternativeIdentifier" or type=="altID":
        try:
            comp=st.session_state.myClient.get('getComponent', json={'component':ident,'alternativeIdentifier':True})
        except KeyError:
            st.write("- no ASN found.")
        except itkX.BadRequest as err:
            DigestMessage(err.response.json()['uuAppErrorMap'])
        
    return comp

### Get slot id from component of child (of compType) in slot position pos (order run 0...)
def SlotFinder(compObj,compType,pos):
    # loop over children
    for child in compObj['children']:
        # debug output
        stTrx.DebugOutput("checking children "+child['componentType']['code']+": ",child['order'])
        # if child is correct component type and in correct position
        if child['componentType']['code']==compType and str(child['order'])==pos:
            stTrx.DebugOutput("Found "+compType+" slot: ",pos)
            return child['id']
    ### failure output...
    st.write("No matching slot found")
    # positions check
    if pos not in [str(child['order']) for child in compObj['children']]:
        st.write("No such child position available")
    # componentType check
    if compType not in [child['componentType']['code'] for child in compObj['children']]:
        st.write("No such child componentType available")
    # if fail then return None
    return None

### convert input timestamp using pattern to standard output format
### default to now if input string "now"
def TimeStampConverter(inStr,inPat):
    timeObj=None
    # empty input shortcut for getting _now_ info.
    if inStr=="now":
        timeObj=datetime.now()
    # use input
    else:
        timeObj=datetime.strptime(inStr, inPat)
    # return standard formatted timestamp

    if timeObj.date() > datetime.now().date():
        st.write(f"🛑 __Date Issue__: This date looks to be in the _future_ (local date: {datetime.now().date()}).")
        st.write(f" - Please check format: {inPat}")
        st.stop()
    return timeObj.strftime("%Y-%m-%dT%H:%MZ")


def GetSNFromString(pageDict, someStr):
    ### Get serialNumber from string (e.g. file name) if possible - offer user input if not
    # check file name
    if "compSN" not in pageDict.keys() or st.button("re-check SN"):
        serialNumber=None
        st.write(f"Try extracting serialNumber from: {someStr}")
        # drop extension if exists
        dropExt=os.path.splitext(someStr)[0]
        # st.write(f" - drop ext: {dropExt}")
        # loop over parts
        for x in dropExt.split('_'):
            for y in x.split('-'):
                snIdx=y.find('20U')
                # st.write(f" - check sub: {y} - ind: {snIdx}")
                if snIdx>-1:
                    serialNumber=y[snIdx:snIdx+14]
                    break
            if serialNumber!=None:
                break
        # 
        if serialNumber!=None:
            st.write(f" - extracted serialNumber: {serialNumber}")
            pageDict['compSN']=serialNumber
        else:
            st.write(" - cannot find suitable string")
    else:
        st.write(f"Using component serial number: {pageDict['compSN']}")
    # manually set
    if "compSN" not in pageDict.keys() or st.checkbox(f"Set serialNumber?"):
        pageDict['compSN']=st.text_input(f"Enter module serialNumber:")
        st.write(f" - {pageDict['compSN']} updated")
    return pageDict['compSN']

#####################################
### component stage/test checklist
#####################################
### get tests per stage from componentType
def GetCheckList(compInfo):
    compTypeInfo=st.session_state.myClient.get('getComponentTypeByCode', json={'code':compInfo['componentType']['code'],'project':compInfo['project']['code']}) 
    # st.write(compTypeInfo)
    ### stages
    df_stageList=pd.DataFrame(compTypeInfo['stages'])
    df_stageList=df_stageList.rename(columns={k:'stage_'+k for k in ['code','name','order','alternative','initial','final']})
    df_stageList['stage_order']=df_stageList['stage_order'].apply(lambda x: int(x) if not pd.isnull(x) else pd.NA)
#     df_stageList
    ### then tests
    df_checkList=df_stageList.explode('testTypes').reset_index(drop=True)
    df_checkList.iloc[0]['testTypes']
    for col in ['order','nextStage','receptionTest','receptionTestOnly']:
        df_checkList['test_'+col]=df_checkList['testTypes'].apply(lambda x: x[col] if type(x)==type({}) and col in x.keys() else None)
    df_checkList['test_order']=df_checkList['test_order'].apply(lambda x: int(x) if not pd.isnull(x) else pd.NA)
    df_checkList['test_code']=df_checkList['testTypes'].apply(lambda x: x['testType']['code'] if type(x)==type({}) and "testType" in x.keys() and type(x['testType'])==type({}) and "code" in x['testType'].keys() else None)
#     df_checkList

    ### drop receptionOnly tests
    df_checkList=df_checkList.query('test_receptionTestOnly!=True').reset_index(drop=True)

    return df_checkList

### get component tests
def GetTestInfo(compInfo):
    
    if "tests" not in compInfo.keys() or len(compInfo['tests'])==0:
        st.write("__No test information or component__")
        return pd.DataFrame()
    # st.write(compInfo['tests'])
    df_tests=pd.DataFrame(compInfo['tests'])
    df_tests=df_tests.explode('testRuns').reset_index(drop=True)
    for col in ['id','state','runNumber','passed','problems','date','testedAtStage']:
        df_tests['testRun_'+col]=df_tests['testRuns'].apply(lambda x: x[col] if type(x)==type({}) and col in x.keys() else None)
    df_tests['testRun_inst']=df_tests['testRuns'].apply(lambda x: x['institution']['code'] if type(x)==type({}) and "institution" in x.keys() and type(x['institution'])==type({}) and "code" in x['institution'].keys() else None)
    ### remove deleted
    df_tests=df_tests.query('testRun_state=="ready"')
    # st.write(df_tests)
    ### format date and ignore non-standard formats
    df_tests['testRun_date']=pd.to_datetime(df_tests['testRun_date'], format='%Y-%m-%dT%H:%M:%S.%fZ', errors="ignore")
    df_tests=df_tests.sort_values(by=["code","testRun_passed","testRun_date"])
    ### consolidate
    df_tests=df_tests.groupby(by=["code","testRun_passed"]).last().reset_index()
    return df_tests

### highlighting
def HighlightTests(s):
    if s['passed'] and "UPLOADED" in s['present']:
        if "currentStage" in s['present']:
            return ['background-color: green'] * len(s)
        return ['background-color: lightgreen'] * len(s)
    elif not s['passed'] and "UPLOADED" in s['present']:
        if not s['passed'] and "currentStage" in s['present']:
            return ['background-color: red'] * len(s)
        return ['background-color: pink'] * len(s)
    elif "MISSING" in s['present']:
        if "currentStage" in s['present']:
            return ['background-color: grey'] * len(s)
        return ['background-color: lightgrey'] * len(s)
    else:
        return ['text-color: black'] * len(s)

### populate checklist
def FillCheckList(compInfo):
    
    df_tests=GetTestInfo(compInfo)
    # st.write(df_tests)
    if df_tests.empty:
        st.write("Cannot check previous uploads")
        return pd.DataFrame()
    df_checkList=GetCheckList(compInfo)
    
    # df_checkList['present']="MISSING"
    df_checkList['present']=df_checkList.apply(lambda x: "OPTIONAL" if x['test_nextStage']==False else "MISSING",axis=1)
    # st.write(df_checkList.columns)
    df_checkList['passed']=False
    for index,row in df_checkList.iterrows():
        tc=row['test_code']
        sc=row['stage_code']
        # find test
        if not df_tests.query(f'code=="{tc}" & testRun_testedAtStage=="{sc}"').empty:
            df_checkList.at[index, 'present']="UPLOADED"
        # find test and passed
        if not df_tests.query(f'code=="{tc}" & testRun_testedAtStage=="{sc}" & testRun_passed==True').empty:
            df_checkList.at[index, 'passed']=True
        # find current test
        if row['stage_code']==compInfo['currentStage']['code']:
            df_checkList.at[index, 'present']+="_currentStage"
    
    # st.write(df_checkList)
    return df_checkList

### get component tests
def GetStageInfo(compInfo):
    df_tests=pd.DataFrame(compInfo['tests'])
    df_tests=df_tests.explode('testRuns').reset_index(drop=True)
    for col in ['id','state','runNumber','passed','problems','date']:
        df_tests['testRun_'+col]=df_tests['testRuns'].apply(lambda x: x[col] if type(x)==type({}) and col in x.keys() else None)
    df_tests['testRun_inst']=df_tests['testRuns'].apply(lambda x: x['institution']['code'] if type(x)==type({}) and "institution" in x.keys() and type(x['institution'])==type({}) and "code" in x['institution'].keys() else None)
    ### remove deleted
    df_tests=df_tests.query('testRun_state=="ready"')
    df_tests['testRun_date']=pd.to_datetime(df_tests['testRun_date'], format='%Y-%m-%dT%H:%M:%S.%fZ')
    df_tests=df_tests.sort_values(by=["code","testRun_passed","testRun_date"])
    ### consolidate
    df_tests=df_tests.groupby(by=["code","testRun_passed"]).last().reset_index()
    return df_tests


#####################################
### component stage coherency (pixels)
#####################################
### get most complex relative
def GetMostParent(myClient, compId, altIdFlag=False):
    
    compInfo=myClient.get('getComponent', json={'component':compId, 'alternativeIdentifier':altIdFlag}) 
    
    while type(compInfo['parents'])==type([]):
        st.write(f"Get parent for {compInfo['serialNumber']} ({compInfo['componentType']['code']})")
        
        parList=[x for x in compInfo['parents'] if type(x)==type({}) and "component" in x.keys() and type(x['component'])==type({}) and "serialNumber" in x['component'].keys() and x['component']['serialNumber']!=compId]

        if len(parList)>1:
            st.write("Warning: more than one (_reasonbable_) parent found!.",[x['componentType']['code'] for x in compInfo['parents']])
        elif len(parList)==0:
            st.write("No (_reasonbable_) parent found. Skipping.")
            break
        else:
            for par in parList:
                try:
                    st.write(f"using parent: {par['component']['serialNumber']} ({par['componentType']['code']})")
                    compInfo=myClient.get('getComponent', json={'component':par['component']['serialNumber']})
                except TypeError:
                    continue
                except KeyError:
                    continue
    
    return compInfo


def BuildChildList(myClient, compInfo, childList):

    ### in case module (i.e. grandparent)
    # st.write(f"Check children for {compInfo['serialNumber']} ({compInfo['componentType']['code']})")
    if type(compInfo)==type({}) and "children" in compInfo.keys():
        # st.write(f"- add to list ({len(compInfo['children'])})")
        childList.extend(compInfo['children'])
        # df_sc=df_sc.dropna(subset=['component'], inplace=False)
        for cc in compInfo['children']:
            try:
                # st.write(f" - trying {cc['component']['code']}")
                newComp=myClient.get('getComponent', json={'component':cc['component']['code']})
                BuildChildList(myClient, newComp, childList)
            # handle missing code info.
            except KeyError:
                pass
            # handle missing data structure - no component assembled
            except TypeError:
                pass


### Get children df
def GetChildrenDF(myClient, compInfo, colSet=['serialNumber','componentType','currentStage']):

    ### recursively get children
    childList=[]
    BuildChildList(myClient, compInfo, childList)
    # make df
    df_sc=pd.DataFrame(childList)
    # tidy missing comps
    df_sc=df_sc.dropna(subset=['component'], inplace=False)

    if df_sc.empty:
        st.write("The child dataframe is empty.")
        return df_sc
    else:
        # standard info. (SN & code) from parent
        df_sc['serialNumber']=df_sc['component'].apply(lambda x: x['serialNumber'] if "serialNumber" in x.keys() else x)
        df_sc['code']=df_sc['component'].apply(lambda x: x['code'] if "code" in x.keys() else x)
        for col in df_sc.columns:
            df_sc[col]=df_sc[col].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
        # get specific component info.
        df_sc['getComponent']=df_sc['component'].apply(lambda x: myClient.get('getComponent', json={'component':x}) if x!=None else x)
        # user defined info.
        for col in colSet:
            if col=="code" or col=="serialNumber": continue
            df_sc[col]=df_sc['getComponent'].apply(lambda x: x[col]['code'] if type(x)==type({}) and col in x.keys() and type(x[col])==type({}) and x[col].keys() else x)
        
        return df_sc[colSet] 

### check children 
def CheckChildCoherence(df_children, col="currentStage"):
    
    ### check coherency of child stages
    coherentCheck=True
    if len(df_children[col].unique())==1:
        st.write(f"✔ child _{col}_ agree: {df_children[col].unique()}")
    else:
        st.write(f"❌ child _{col}_ do not agree: {df_children[col].unique()}")
        coherentCheck=False
    
    return coherentCheck

### Check coherence w.r.t parent
def CheckParentCoherence(compInfo, df_children, col="currentStage"):
    
    # match child stage to component (parent) by default
    coherentCheck=True
    st.write(f"Checking _{col} coherence_ w.r.t {compInfo['componentType']['code']}")
    st.write(" - Re-run page for parent component if necessary")
    parValue=compInfo[col]['code'] #GetCoherentStage(compInfo)

    st.write(f"Expect children to match {col}: **{parValue}**")

    for i,row in df_children.iterrows():
        if row[col]==parValue:
            st.write(f"✔ {row['serialNumber']} ({row['componentType']}) @ {row[col]} is in _coherent_ {col}")
        else:
            st.write(f"❌ {row['serialNumber']} ({row['componentType']}) @ {row[col]} is **not** in _coherent_ {col}")
            coherentCheck=False
    
    return coherentCheck

### highlight matched (by column value) rows
def Highlighter(s, col, matchVal):
    if s[col]==matchVal:
        return ['background-color: lightgreen'] * len(s)
    else:
        return ['background-color: red'] * len(s)

### force stage coherence with parent
def ForceStageCoherence(myClient, compInfo, df_children, debug=False):
    
    parStage=compInfo['currentStage']['code']
    ### loop over children and update stage
    for i,row in df_children.iterrows():
        st.write(f"working on component: {row['serialNumber']} ({row['componentType']}) @ {row['currentStage']}")
        # st.write(f"\ttry to move to: {parStage}")
        try:
            retVal=myClient.post('setComponentStage', json={'component':row['serialNumber'], 'stage':parStage})
            st.write("\t\t - __Move complete__:", retVal['component']['currentStage'])
            ### debugging
            if debug:
                st.write(f"\ttry to move back: {row['currentStage']}")
                retVal=myClient.post('setComponentStage', json={'component':row['serialNumber'], 'stage':row['currentStage']})
                st.write("\t\t - __Move complete__:", retVal['component']['currentStage'])
        except itkX.BadRequest as b:
            st.write(" - :no_entry_sign: **Unsuccessful**")
            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                
    else:
        st.write("__Done, Re-run Stage Coherency check__") # Reset component to see update__")

### combined function for stage coherence
def CheckStageCoherence(myClient, compId, debug=False):

    st.write("### :stopwatch: Checking Stage Coherency")
    ### get most complex relative
    parInfo=GetMostParent(myClient, compId)

    st.write(" - got parent")

    df_children=GetChildrenDF(myClient, parInfo)
    # st.write(df_children)    

    st.write(" - got children")

    ### check stage coherence
    scCheck= ( CheckChildCoherence(df_children) and CheckParentCoherence(parInfo, df_children) )

    st.write(" - got check value")

    ### st.write coloured dataframe
    st.write(f"Check components match: **{parInfo['currentStage']['code']}** _of {parInfo['serialNumber']} ({parInfo['componentType']['code']})_")
    st.write( df_children.style.apply(Highlighter, matchVal=parInfo['currentStage']['code'], col='currentStage', axis=1) )
    
    ### what to do
    if not scCheck:
        st.write("Components are __not__ _Stage Coherent_.")
        # if not st.checkbox("Continue anyway?"):
        #     st.write(f"Force all (grand)children components stages to match (grand)parent components stage ({parInfo['currentStage']['code']})")
        if st.button("Force Stage Coherence"):
            ForceStageCoherence(myClient, parInfo, df_children, debug)

    else:
        st.write("Components are _Stage Coherent_.")
    
    st.stop()

### bonus location coherency
def CheckLocationCoherence(myClient, compId, debug=True):

    st.write("### :stopwatch: Checking Location Coherency")
    ### get most complex relative
    parInfo=GetMostParent(myClient, compId)

    df_children=GetChildrenDF(myClient, parInfo, colSet=['serialNumber','componentType','currentLocation'])
    # st.write(df_children)    

    ### check location coherence
    scCheck= ( CheckChildCoherence(df_children, "currentLocation") and CheckParentCoherence(parInfo, df_children, "currentLocation") )

    ### st.write coloured dataframe
    st.write(f"Check components match: **{parInfo['currentLocation']['code']}** _of {parInfo['serialNumber']} ({parInfo['componentType']['code']})_")
    st.write( df_children.style.apply(Highlighter, matchVal=parInfo['currentLocation']['code'], col='currentLocation', axis=1) )
    
    ### what to do
    if not scCheck:
        st.write("Components are __not__ _Location Coherent_.")

    else:
        st.write("Components are _Location Coherent_.")
    
    st.stop()

#####################################
### 
#####################################
### check testRun data for match
def CheckTestDataMatch(myClient, matchValue, compId, testTypeCode, paraCode, subCode=False, altIdFlag=False):    
    ### get component
    compInfo=myClient.get('getComponent', json={'component':compId, 'alternativeIdentifier':altIdFlag}) 
    
    ### get matching testType (should only be one)
    df_test=pd.DataFrame([x for x in compInfo['tests'] if x['code']==testTypeCode])
    
    ### return if no tests found
    if df_test.empty:
        st.write(f"No tests found with code: {testTypeCode}")
        return False
        
    ### format and order testRuns by date
    df_test=df_test.explode('testRuns').reset_index(drop=True)
    for col in ["id","cts"]:
        df_test["testRun_"+col]=df_test['testRuns'].apply(lambda x: x[col] if type(x)==type({}) and col in x.keys() else None)
    df_test['testRun_cts']=pd.to_datetime(df_test['testRun_cts'])
    
    ### get testRun data
    df_test['testData']=df_test['testRun_id'].apply(lambda x: myClient.get('getTestRun', json={'testRun':x}) if x!=None else None)
    
    ### extract parameter value for input parameter code
    def GetParaValue(testObj, paraCode):
        ### loop over test properties and results
        paraValue=None
        for pr in ['properties','results']:
            st.write(f"- checking testRun _{pr}_")
            try:
                if testObj[pr]==None:
                    st.write("  - None found")
                    continue
                paraValue=next((para['value'] for para in testObj[pr] if para['code']==paraCode), "NOT FOUND")
                if paraValue!="NOT FOUND":
                    st.write(f"  - Found {paraCode}") # value: {paraValue}")
                    return paraValue
                    break
            except KeyError:
                st.write(f"  - No {pr} here")
            return paraValue
    
    ### extract parameter value
    st.write(f"Check {compId} _{testTypeCode}_ testRuns for {paraCode}")
    df_test['paraValue']=df_test['testData'].apply(lambda x: GetParaValue(x,paraCode) if type(x)==type({}) else None)
#     st.write(df_test)
    
    ### get parameter subCode if required
    if subCode!=None:
        st.write(f"Getting sub key code: _{subCode}_")
        df_test['paraValue']=df_test['paraValue'].apply(lambda x: x[subCode] if type(x)==type({}) and subCode in x.keys() else None)
    
    ### check match
    st.write("Test matching...")
    df_match=df_test.query(f'paraValue=="{matchValue}"')
    
    ### returns
    # - none
    if df_match.empty:
        st.write(" - no matches found :(")
        st.write(df_test)
        return False
    
    # - many
    if len(df_match)>1:
        st.write(f" - multiple matches found: {len(df_match)}")
        st.write(df_match)
        return True
    # - one
    else:
        st.write(f" - single match found: {df_match.iloc[0]['paraValue']}")
        return True
    
    ### catch-all
    st.write("Somehow got here?!")
    return False
    