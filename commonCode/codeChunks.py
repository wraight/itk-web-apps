### standard
import io
import cv2
import json
import copy
### custom
import numpy as np
from datetime import datetime

### PDB stuff
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
import pandas as pd
import requests
import streamlit as st

import commonCode.PDBTricks as pdbTrx
### common stuff
import commonCode.StreamlitTricks as stTrx

#####################
### useful functions
#####################

### return git repo. commit SHA - useful to stamp analysis
def GetCommitSHA():
    
    mainInfo=""
    with open("/code/gitRef.txt", "r") as f: 
        mainInfo=f.read()

    return "SHA:"+mainInfo[0:7]

### Manually select parameters for upload
def SelectObjectParameters(pageDict, schemaType, noQC=False):
    ### option to drop non-required information
    dropNonReq=False
    if st.checkbox("Drop all non-required parameters?"):
        dropNonReq=True

    ### get testType info
    if "schemaInfo" not in pageDict.keys() or ("test" in schemaType and pageDict['schemaInfo']['code']!=pageDict[schemaType]['testType']) or ("comp" in schemaType and pageDict['schemaInfo']['code']!=pageDict[schemaType]['componentType']):
        ### tests
        if "test" in schemaType.lower():
            pageDict['schemaInfo']=st.session_state.myClient.get('getTestTypeByCode', json={'code':pageDict[schemaType]['testType'], 'componentType':pageDict['componentType'], 'project':pageDict['project'], 'required':False})
        ### components
        elif "comp" in schemaType.lower():
            pageDict['schemaInfo']=st.session_state.myClient.get('getComponentTypeByCode', json={'code':pageDict[schemaType]['componentType'], 'project':pageDict['project'], 'required':False})
        ### others
        else:
            st.write(f"Not sure what to do with this component/test flag: {schemaType}")
            st.stop()

    ### Fill test content
    # header parameters
    st.write("#### Header Parameters")

    ### tests
    if "test" in schemaType.lower():
        st.write(f"__component__")
        pageDict[schemaType]['component']= st.text_input("Serial number (or PDB code):", value=pageDict[schemaType]['component'])
        st.write(f"__institution__")
        pageDict[schemaType]['institution']= st.text_input("Institution code:", value=pageDict[schemaType]['institution'])
        st.write(f"__runNumber__")
        pageDict[schemaType]['runNumber']= st.text_input("Index of test:")
        st.write(f"__date__")
        test_dt= datetime.strptime(pageDict[schemaType]['date'], "%Y-%m-%dT%H:%M:%S.%fZ")
        test_date= st.date_input("Date of test:", value=test_dt.date())
        test_time= st.time_input("Time of test:", value=test_dt.time())
        pageDict[schemaType]['date']=test_date.strftime("%Y-%m-%d")+"T"+test_time.strftime("%H:%M:%S")+".000Z"
        st.write(f" - date: {pageDict[schemaType]['date']}")

        if noQC==False:
            st.write(f"__passed__")
            pageDict[schemaType]['passed']=st.radio("QC passed?", options=[True,False], index=[True,False].index(pageDict[schemaType]['passed']) )
            st.write(f"__problems__")
            pageDict[schemaType]['problems']=st.radio("QC problems?", options=[True,False], index=[True,False].index(pageDict[schemaType]['problems']) )

    ### components
    elif "comp" in schemaType.lower():
        for k,v in {'subproject':'subprojects','type':'types'}.items(): # use code table in schemaInfo
            if k in pageDict[schemaType].keys(): # possibly CM components miss this
                st.write(f"__{k}__")
                rendVal=next((x for x in pageDict['schemaInfo'][v] if x['code'] == pageDict[schemaType][k]), None)
                if rendVal!=None:
                    pageDict[schemaType][k]= st.selectbox(f"{k}:", options=pageDict['schemaInfo'][v], format_func=lambda x: x['name'], index=pageDict['schemaInfo'][v].index(rendVal))['code']
                else:
                    pageDict[schemaType][k]= st.selectbox(f"{k}:", options=pageDict['schemaInfo'][v], format_func=lambda x: x['name'])['code']
        st.write(f"__institution__")
        pageDict[schemaType]['institution']= st.text_input("Institution code:", value=pageDict[schemaType]['institution'])
    
    ### others
    else:
        st.write(f"Not sure what to do with this schemaType: {schemaType}")
        st.stop()
    
    # properties & results parameters
    for pr,pp in {'properties':'properties','results':'parameters'}.items():
        if "comp" in schemaType.lower() and pr=='results':
            continue
        st.write(f"#### {pr.title()}")
        if len(pageDict[schemaType][pr].keys())<1:
            st.write(f"No {pr} information")
        for k,v in pageDict[schemaType][pr].items():
            st.write(f"{k}")
            param=next((x for x in pageDict['schemaInfo'][pp] if x['code'] == k), None)
            if param==None:
                st.write(f"No {pp} found in {schemaType} info. for: __{k}__ ({pr})")
                continue

            ### skip required
            if "required" not in param.keys() or param['required']!=True:
                if dropNonReq or st.checkbox("Drop non-required parameter?", key=f"{k}"):
                    pageDict[schemaType][pr][k]="Zaphod Beeblebrox"
                    continue
            else:
                st.write("❕ This parameter is _required_")

            titleStr=param['description']
            if titleStr==None or len(titleStr)<1:
                titleStr=param['name']

            # if code table
            if "code" in param['dataType']:
                rendVal=next((x for x in param['codeTable'] if x['code'] == pageDict[schemaType][pr][k]), None)
                if rendVal!=None:
                    pageDict[schemaType][pr][k]= st.selectbox(f"{titleStr}:", options=param['codeTable'], format_func=lambda x: x['value'], index=param['codeTable'].index(rendVal))['code']
                else:
                    pageDict[schemaType][pr][k]= st.selectbox(f"{titleStr}:", options=param['codeTable'], format_func=lambda x: x['value'])['code']
            # if numeric
            elif "int" in param['dataType']:
                if type(pageDict[schemaType][pr][k])==type(1):
                    pageDict[schemaType][pr][k]= st.number_input(f"{titleStr}:", key=f"{pr}_{k}", step=1, value=pageDict[schemaType][pr][k])
                else:
                    pageDict[schemaType][pr][k]= st.number_input(f"{titleStr}:", key=f"{pr}_{k}", step=1)
            elif "float" in param['dataType']:
                if type(pageDict[schemaType][pr][k])==type(1.0):
                    pageDict[schemaType][pr][k]= st.number_input(f"{titleStr}:", key=f"{pr}_{k}", value=pageDict[schemaType][pr][k])
                else:
                    pageDict[schemaType][pr][k]= st.number_input(f"{titleStr}:", key=f"{pr}_{k}")
            elif "bool" in param['dataType']:
                if type(pageDict[schemaType][pr][k])==type(True):
                    pageDict[schemaType][pr][k]= st.checkbox(f"{titleStr}:", key=f"{pr}_{k}", value=pageDict[schemaType][pr][k])
                else:
                    pageDict[schemaType][pr][k]= st.checkbox(f"{titleStr}:", key=f"{pr}_{k}")
            elif "date" in param['dataType']:
                if pageDict[schemaType][pr][k]==None:
                    pageDict[schemaType][pr][k]=pageDict[schemaType]['date']
                pr_dt= datetime.strptime(pageDict[schemaType][pr][k], "%Y-%m-%dT%H:%M:%S.%fZ")
                pr_date= st.date_input("Date of test:", value=pr_dt.date(), key=f"{pr}_{k}_date")
                pr_time= st.time_input("Time of test:", value=pr_dt.time(), key=f"{pr}_{k}_time")
                pageDict[schemaType][pr][k]=pr_date.strftime("%Y-%m-%d")+"T"+pr_time.strftime("%H:%M:%S")+".000Z"
                st.write(f" - date: {pageDict[schemaType][pr][k]}")
            # others
            else:
                if pageDict[schemaType][pr][k]!="Zaphod Beeblebrox" and pageDict[schemaType][pr][k]!=None:
                    pageDict[schemaType][pr][k]= st.text_input(f"{titleStr}:", key=f"{pr}_{k}", value=pageDict[schemaType][pr][k])
                else:
                    pageDict[schemaType][pr][k]= st.text_input(f"{titleStr}:", key=f"{pr}_{k}")

    return 

### Recursively remove keys with the specified value from a nested dictionary.
def RemoveKeys(inJson, value_to_remove="Zaphod Beeblebrox"):

    if isinstance(inJson, dict):
        return {k: RemoveKeys(v, value_to_remove) for k, v in inJson.items() if v != value_to_remove}
    elif isinstance(inJson, list):
        return [RemoveKeys(i, value_to_remove) for i in inJson]
    else:
        return inJson

def SelectComponentChunk(pageDict):
    infra.TextBox(pageDict,'inComp',"Enter component identifier")
    infra.Radio(pageDict,'altID',['serialNumber (or code)','alternativeIdentifier'],"Type of identifier")

    if pageDict['inComp']==None or len(pageDict['inComp'])<1:
        st.write("Enter identifier to get component information")
        pageDict['comp']=None
    else:
        # if 'comp' not in pageDict.keys() or st.button("Get component"):
        compQueryJson={'component':pageDict['inComp'],'type':"SN"}
        if pageDict['altID']=='alternativeIdentifier':
            compQueryJson['type']='altID'
        stTrx.DebugOutput("queryJson:",compQueryJson)

        pageDict['comp']=pdbTrx.GetComponentByIdentifier(compQueryJson['component'],compQueryJson['type'])

def SelectShipmentChunk(pageDict):
    infra.TextBox(pageDict,'inShip',"Enter shipment identifier")

    if pageDict['inShip']==None or len(pageDict['inShip'])<1:
        st.write("Enter identifier to get component information")
        pageDict['ship']=None
    else:
        # if 'comp' not in pageDict.keys() or st.button("Get component"):
        shipQueryJson={'shipment':pageDict['inShip']}
        stTrx.DebugOutput("queryJson:",shipQueryJson)

        try:
            pageDict['ship']=st.session_state.myClient.get('getShipment', json=shipQueryJson)
        except KeyError:
            st.write("- no ASN found.")

def DefaultDictChunk(pageDict, defDict):

    for k,v in defDict.items():
        if k not in pageDict.keys():
            pageDict[k]=v

    if st.session_state.debug:
        st.write("Change defaults")
        for k,v in defDict.items():
            pageDict[k]=pdbTrx.SelectCheck(k,v)

def ReviewAndEditChunk(pageDict, schemaName, retroOpt=True): # e.g. schemaName='testSchema'
    ### Review and edit
    st.write(f"### Review and edit _{schemaName}_ schema")
    infra.ToggleButton(pageDict,'toggleEdit',f'Update {schemaName} schema manually')        
    if pageDict['toggleEdit']:
        st.write("---")
        st.write("#### Set schema values")
        pageDict[schemaName]=pdbTrx.EditJson(pageDict[schemaName])

        ### check if schemas available to add optional parameters
        if "fullSchema" in pageDict.keys() and "origSchema" in pageDict.keys():
            if st.checkbox('Add non-required parameter?', key=f"addNotReq_{schemaName}"):
                st.write("### Add non-required parameter:")
                optKeyList=set(pageDict['fullSchema'].keys()) - set(pageDict['origSchema'].keys())
                if len(optKeyList)>0:
                    infra.SelectBox(pageDict,'setKey',list(optKeyList),"Select optional parameter:")
                    pageDict[schemaName]=pdbTrx.SelectCheck(pageDict['setKey'],None,pageDict[schemaName])
                else:
                    st.write("No optional parameters found.")

    else:
        st.write("#### Review Schema")
        st.write(pageDict[schemaName])
    infra.ToggleButton(pageDict,'toggleText',f'Convert all {schemaName} schema values to text (deprecated)?')
    if pageDict['toggleText']:
        pageDict[schemaName] = json.loads(json.dumps(pageDict[schemaName]), parse_int=str)
        for pKey in ['properties','results']:
            st.write(f"convert {pKey} values")
            try:
                pageDict[schemaName][pKey] = json.loads(json.dumps(pageDict[schemaName][pKey]), parse_float=str)
            except KeyError:
                pass

    
    if "test" in schemaName.lower() and retroOpt:
        ### retroactive?
        st.write("__Is this test retroactive?__")
        if st.checkbox("What does retroactive mean?"):
            st.info("Retroactive uploads are used when the component is no longer at the appropriate stage. \
                    \n- the test is uploaded without changing the component stage \
                    \n- the component must have previously been at the user's institution (history of component found in _locations_)")
            st.stop()
        if st.radio("Retroactive?",['No','Yes']) == "Yes":
            pageDict['testSchema']['isRetroactive']=True
        else:
            pageDict['testSchema']['isRetroactive']=False
    stTrx.DebugOutput("Final schema:",pageDict[schemaName])

    st.write("---")

def ToggleChangesChunk(pageDict, togType, filePath, csvFileName):

        stTrx.DebugOutput("looking in: "+filePath[:filePath.rfind('/')])
        infra.ToggleButton(pageDict,'toggleChanger','Change default '+togType+' value?')
        if pageDict['toggleChanger']:
            df_all=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+csvFileName)
            infra.SelectBox(pageDict,'componentType',df_all['compType'].unique(),"Select componentType code:")
            if 'test' in togType.lower():
                infra.SelectBox(pageDict,'stage',df_all.query('compType=="'+pageDict['componentType']+'"')['stage'].unique(),"Select stage code:")
                infra.SelectBox(pageDict,'code',df_all.query('stage=="'+pageDict['stage']+'"')['testType'].unique(),"Select testType code (in "+pageDict['stage']+"):")


def UploadJSONChunk(pageDict, exampleName=None, exampleDict=None, ignoreSelect=False):

    if ignoreSelect:
        pageDict['toggleUpload']=True
    else:
        infra.ToggleButton(pageDict,'toggleUpload','Upload JSON?')
    retDict=None
    if pageDict['toggleUpload']:

        infra.Radio(pageDict,'jsonInputType',["area","camera","file"],"Select input type:")

        if pageDict['jsonInputType'].lower()=="area":
        
            data_raw = st.text_area('Input json here:',None)

            if data_raw==None or data_raw=="None":
                st.write("Please input dictionary")
                st.stop()
            
            try:
                stTrx.DebugOutput("raw data:",data_raw)
                data_eval=eval(data_raw)
                # st.write("eval data:",data_eval)

                if type(data_eval)==type({}):
                    retDict=data_eval
                    # st.write("dictionary found!")
                    # df_input = pd.DataFrame([data_eval])
                    # # st.write(df_input)
                    # retDict={k:v[0] for k,v in df_input.to_dict().items() }
                    
                else:
                    st.write("not sure what to do with type:",type(data_eval))
            except TypeError:
                st.write("type error")

        elif pageDict['jsonInputType'].lower()=="camera":
            
            inPic=st.camera_input("Input picture:")

            if inPic is not None:
                bytes_data = inPic.getvalue()
                cv2_img = cv2.imdecode(np.frombuffer(bytes_data, np.uint8), cv2.IMREAD_COLOR)
                detector = cv2.QRCodeDetector()
                data_raw, bbox, straight_qrcode = detector.detectAndDecode(cv2_img)

                try:
                    stTrx.DebugOutput("raw data:",data_raw)
                    data_eval=eval(data_raw)
                    # st.write("eval data:",data_eval)

                    if type(data_eval)==type({}):
                        retDict=data_eval
                        # st.write("dictionary found!")
                        # df_input = pd.DataFrame([data_eval])
                        # # st.write(df_input)
                        # retDict={k:v[0] for k,v in df_input.to_dict().items() }
                        
                    else:
                        st.write("not sure what to do with type:",type(data_eval))
                except TypeError:
                    st.write("Type error")
                except SyntaxError:
                    st.write("Syntax error --> No QR found")
                    st.write("Check image/brightness")

        elif pageDict['jsonInputType'].lower()=="file":

            pageDict['file'] = st.file_uploader('Upload JSON file', type="json")

            if pageDict['file'] is not None:
                stTrx.DebugOutput("file:",pageDict['file'])
                retDict = json.load(pageDict['file'])

            else:
                if exampleDict!=None and exampleName!=None:
                    st.write("No file uploaded")
                    st.download_button(label="Download example JSON", data=json.dumps(
                        exampleDict, indent=2), file_name=exampleName)
                st.write("Please upload JSON file")
                st.stop()
        
        else:
            st.write("Don't understand input type?!")

        return retDict

def GetDfFromCSV(filePath, csvSep=None, headRowIdx=0):
    ### if seperator is not defined
    if csvSep==None:
        # st.write("No seperator defined")
        try:
            # if no header
            if headRowIdx>-1:
                df_input=pd.read_csv(filePath, header=headRowIdx)
            else:
                df_input=pd.read_csv(filePath, names=list('abcdefghij'))
        # catch uneven row widths
        except pd.errors.ParserError:
            st.write("work around1")
            df_input=pd.read_csv(filePath, names=list('abcdefghij'))
    # if seperator is defined
    else:
        # st.write(f"Defined seperator: {csvSep}")
        try:
            # if no header
            if headRowIdx>-1:
                df_input=pd.read_csv(filePath, sep=csvSep, header=headRowIdx)
            else:
                df_input=pd.read_csv(filePath, sep=csvSep, names=list('abcdefghij'))
        # catch uneven row widths
        except pd.errors.ParserError:
            st.write("work around2")
            df_input=pd.read_csv(filePath, sep=csvSep, names=list('abcdefghij'))
    
    # retrun without empty columns
    return df_input.dropna(how='all', axis=1)


def UploadFileChunk(pageDict,fileTypes,exampleFile,excelSheetName=None,extraText=None,csvSep=None, headRowIdx=0):

    pageDict['file']= st.file_uploader("Upload data file", type=fileTypes)
    stTrx.DebugOutput("file:",pageDict['file'])

    if pageDict['file'] is not None:
        # st.write(f"type: {pageDict['file'].type} and csvSep: {csvSep}")
        if "csv" in pageDict['file'].type or ("text" in pageDict['file'].type and csvSep!=None):
            # st.write(f"Got csv, with header row index: {headRowIdx}")
            df_input=GetDfFromCSV(pageDict['file'], csvSep, headRowIdx)

        elif "excel" in pageDict['file'].type:
            # st.write("Got excel")
            try:
                df_input=pd.read_excel(pageDict['file'], sheet_name=excelSheetName, index_col=0, header=0)
            except ValueError:
                st.write(f"__Trouble reading file.__ Please check format.\n\n\t- if using excel please check tab name is {excelSheetName}")
                st.stop()
        else:
            st.write("No reader found for file types:",fileTypes)
            st.stop()
        df_input.columns = df_input.columns.str.strip().str.replace(' ', '')
        for c in df_input.columns:
            df_input[c] = df_input[c].apply(lambda x: x.strip() if type(x)==type("str") else x)

        ### drop empty rows
        df_input=df_input.dropna(axis = 0, how = 'all').reset_index(drop=True)
        return df_input
    else:
        st.write("No data file set")
        stTrx.DebugOutput("I'll tidy this up!")
        for k in ['file','testSchema','compSchema','testSchemas','compSchemas','currentSchema','compSN']:
            try:
                pageDict.pop(k)
            except KeyError:
                pass
        if exampleFile!=None:
            stTrx.DebugOutput("looking for: "+exampleFile)
            if "csv" in fileTypes or ("txt" in fileTypes and csvSep!=None):
                ### read csv
                df_data=GetDfFromCSV(exampleFile, csvSep, headRowIdx)
                # st.write(df_data)
                ## drop fake columns abc etc.
                if "abc" in "".join(list(df_data.columns)):
                    df_data.columns = df_data.iloc[0]
                    df_data=df_data.drop(df_data.index[0])
                if csvSep==None:
                    st.download_button(label="Download example", data=df_data.to_csv(index=False),file_name=exampleFile.split('/')[-1])
                else:
                    st.download_button(label="Download example", data=df_data.to_csv(index=False, sep=csvSep),file_name=exampleFile.split('/')[-1])
            elif "xlsx" in fileTypes or "xls" in fileTypes:
                ### write excel
                buffer = io.BytesIO()
                with pd.ExcelWriter(buffer, engine='xlsxwriter') as writer:
                    pd.read_excel(exampleFile, sheet_name=excelSheetName, index_col=0).to_excel(writer, sheet_name=excelSheetName)
                    writer.save() # Close the Pandas Excel writer and output the Excel file to the buffer
                st.download_button(
                    label="Download example",
                    data=buffer,
                    file_name=exampleFile.split('/')[-1],
                    mime="application/vnd.ms-excel"
                )
            else:
                st.write("No reader found for file types:",fileTypes)
                st.stop()
        if extraText!=None:
            st.write(extraText)
        st.stop()
        ### remove data from pageDict
        
        return None


def ReadCSVChunk(df_input, pageDict, readType, schemaName):

    if schemaName not in pageDict.keys() or st.button("Re-read data"):
        pageDict[schemaName]=[]
        if "component" in readType.lower():
            st.write("Read CSV as components")
            listLen=len(df_input)
        elif "testrun" in readType.lower():
            st.write("Read CSV as testRuns")
            listLen=len(df_input)
        else:
            st.write(f"**readType: {readType} not supported :( **")
            st.stop()
        status_bar_ids=st.progress(0.0) #st.empty()
        status_text_ids=st.empty()
        for index, row in df_input.iterrows():
            status_bar_ids.progress((1.0*index+1)/listLen)
            if "component" in readType.lower():
                if "serialNumber" in df_input.columns:
                    loopText="setting up: "+row['serialNumber']+", "+str(index+1)+" of "+str(listLen)
                else: # some componentTypes automatically generate SNs so none supplied
                    loopText="setting up: "+str(index+1)+" of "+str(listLen)+" (no SN specified)"
                status_text_ids.text(loopText)
                if "currentSchema" in pageDict.keys() and pageDict['currentSchema']['componentType']==row['componentType']:
                    # st.write("same as last componentType:",pageDict['currentSchema']['componentType'])
                    readSchema=copy.deepcopy(pageDict['currentSchema'])
                else:
                    # st.write("get new componentType:",pageDict['currentSchema']['componentType'])
                    try:
                        pageDict['currentSchema']=st.session_state.myClient.get('generateComponentTypeDtoSample', json={'project':pageDict['project'], 'code':row['componentType'], 'requiredOnly' :True})
                        readSchema=copy.deepcopy(pageDict['currentSchema'])
                    except KeyError:
                        st.write("Data is missing from file. Please check componentType is defined.")
                        continue

            elif "testrun" in readType.lower():
                loopText="setting up: "+row['testType']+" for "+row['component']+", "+str(index+1)+" of "+str(listLen)
                status_text_ids.text(loopText)
                #st.write("setting up *",row['testType'],"* for",row['component'])
                if "currentSchema" in pageDict.keys() and pageDict['currentSchema']['testType']==row['testType']:
                    # st.write("same as last testType:",pageDict['currentSchema']['testType'])
                    readSchema=copy.deepcopy(pageDict['currentSchema'])
                else:
                    st.write(f"get testType schema: {row['testType']} for {row['componentType']} ({pageDict['project']})")
                    try:
                        pageDict['currentSchema']=st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':row['componentType'], 'code':row['testType'], 'requiredOnly':True})
                    except KeyError:
                        st.write("__Data is missing from file__. Please check componentType and testType are defined.")
                        continue
                    readSchema=copy.deepcopy(pageDict['currentSchema'])
                    # st.write("Something missing (componentType/testType). Will use:",{'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType']})
                    # readSchema=st.session_state.myClient.get('generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['code'], 'requiredOnly':True})

            else:
                st.write("Not sure what this is: component/testrun. exit.")
                return None
            # st.write("### schema here:",pageDict['currentSchema'])

            ### checking stage supplied if isRetroactive is used
            if "isRetroactive" in df_input.columns: 
                if "stage" not in df_input.columns:
                    st.error(f"_isRetroactive_ flag found for {row['component']} without _stage_ information.")
                    st.write(" - Please remove flag or add stage code.")
                    st.stop()
                else:
                    st.info(f"_isRetroactive_ flag found for {row['component']}.")
                    st.write(f"-  using stage {row['stage']}.")


            ### update schema with supplied values
            for x in df_input.columns: 
                stTrx.DebugOutput("column: __"+str(x)+"__")
                ### do not use value columns, or asStrings flag
                if x not in readSchema.keys() and x not in ["isRetroactive"] and "property" not in x and "result" not in x:
                    stTrx.DebugOutput(" - skip this")
                    continue
                ### skip if empty
                if x=="date":
                    stTrx.DebugOutput(" - handling _date_")
                    try:
                        dateStr=datetime.strptime(row['date'], "%d/%m/%y").strftime("%Y-%m-%dT10:10Z")
                        readSchema['date']= pdbTrx.TimeStampConverter(dateStr, "%Y-%m-%dT10:10Z")
                    except ValueError:
                        dateStr=datetime.strptime(row['date'], "%d/%m/%Y").strftime("%Y-%m-%dT10:10Z")
                        readSchema['date']= pdbTrx.TimeStampConverter(dateStr, "%Y-%m-%dT10:10Z")
                    except:
                        st.write("Issue reading date. Check format.")
                        readSchema['date']=row['date']
                    continue
                ### handle is Retroactive (or it will be ignored as not part of standard testType structure)
                if x=="isRetroactive":
                    stTrx.DebugOutput(f" - handling _isRetroactive_ ({row['isRetroactive']}), using stage: {row['stage']}")
                    readSchema['isRetroactive']=row['isRetroactive']
                    readSchema['stage']=row['stage']
                    continue
                if x=="stage":
                    stTrx.DebugOutput(f" - handling _stage_ ({row['stage']})")
                    readSchema['stage']=row['stage']
                    continue
                # st.write("this is to be intepreted")
                ### skip if empty
                if pd.isnull(row[x]):
                    stTrx.DebugOutput(" - empty cell. __skipping__")
                    continue
                else:
                    #st.write("working on:",row[x])
                    ### get corresponding value for key parameters
                    if "_key" in x:
                        # st.write(" - this is a key!",row[x])
                        try:
                            ### evaluate input string for parameter values (try to catch lists)
                            newVal=stTrx.Tryeval(str(row[x.replace('key','value')]))
                        except KeyError:
                            st.write(f" - No corresponding value found for key {row[x]}. Please check inputs.")
                            continue
                        oriVal=stTrx.GetDictVal(readSchema,row[x])
                        # st.write("read in value:",newVal)
                        if oriVal!=None:
                            stTrx.DebugOutput(f"MatchType: {oriVal} ({type(oriVal)})")
                            ### try to match new value type to original schema (try to catch float/int mismatch)
                            newVal=stTrx.MatchType(oriVal,newVal)
                        foundKey=stTrx.SetDictVal(readSchema,row[x],newVal)
                        if foundKey==False:
                            st.write(f" - No _required_ key found for '{row[x]}' for value: {newVal}. Adding anyway (hope it's optional).")
                            # st.write("properties:",readSchema['properties'])
                            # st.write("results:",readSchema['results'])

                            if "prop" in x:
                                readSchema['properties'][row[x]]=newVal
                                st.write(" - added to properties")
                            elif "res" in x:
                                readSchema['results'][row[x]]=newVal
                                st.write(" - added to results")
                            else:
                                st.write(" - not sure where to add. __skipping__.")
                                continue
                        else:
                            stTrx.DebugOutput(f" --> Found key for {row[x]} for value: {newVal}.")

                    ### or just use value for top-level
                    else:
                        # st.write(" - this is _NOT_ a key!")
                        if "_value" in x:
                            stTrx.DebugOutput(" - skip this _value_")
                            continue
                        newVal=row[x]
                        oriVal=stTrx.GetDictVal(readSchema,x)
                        #st.write("read in value:",newVal)
                        ### if value is found and not passed or problems key
                        if oriVal!=None and x not in ["passed","problems"]:
                            stTrx.DebugOutput(f"MatchType: {oriVal} ({type(oriVal)})")
                            ### try to match new value type to original schema schema (try to catch float/int mismatch)
                            newVal=stTrx.MatchType(oriVal,newVal)
                        ### update schema with new value
                        foundKey=stTrx.SetDictVal(readSchema,x,newVal)
                        if foundKey==False:
                            st.write(f" - No key found for {x} for value: {row[x]}.")
                            st.write("   - Not sure where to add. __skipping__.")
                        else:
                            stTrx.DebugOutput(f" --> Found key for {x} for value: {newVal}.")

            ### add stage key for test upload later
            if "testrun" in readType.lower():
                try:
                    readSchema['stage']=row['stage']
                except KeyError:
                    pass

            readSchema['institution']=st.session_state.Authenticate['inst']['code']

            try:
                if row['asStrings']:
                    readSchema = json.loads(json.dumps(readSchema), parse_int=str)
                    for x in ['properties','results']:
                        # st.write(x)
                        # st.write(readSchema)
                        try:
                            readSchema[x] = json.loads(json.dumps(readSchema[x]), parse_float=str)
                        except KeyError:
                            pass
            except KeyError:
                pass

            pageDict[schemaName].append(readSchema)


def ReadCSVChunkOld(df_input, pageDict, readType, schemaName):

    if schemaName not in pageDict.keys() or st.button("Re-read data"):
        pageDict[schemaName]=[]
        if "component" in readType.lower():
            listLen=len(df_input['serialNumber'].astype(str).tolist())
        elif "testrun" in readType.lower():
            listLen=len(df_input['component'].astype(str).tolist())
        else:
            st.write(f"**readType: {readType} not supported :( **")
            st.stop()
        status_bar_ids=st.progress(0.0) #st.empty()
        status_text_ids=st.empty()
        for index, row in df_input.iterrows():
            status_bar_ids.progress((1.0*index+1)/listLen)
            if "component" in readType.lower():
                loopText="setting up: "+row['serialNumber']+", "+str(index+1)+" of "+str(listLen)
                status_text_ids.text(loopText)
                readSchema=st.session_state.myClient.get('generateComponentTypeDtoSample', json={'project':pageDict['project'], 'code':row['componentType'], 'requiredOnly' :True})

                for x in ['subproject','type','serialNumber','alternativeIdentifier']:
                    if pd.isnull(row[x]):
                        continue
                    try:
                        readSchema[x]=row[x]
                    except KeyError:
                        pass

                for x in df_input.columns:
                    #st.write("column:",x)
                    if "prop" in x and "key" in x:
                        #st.write("this is a property")
                        ### skip is empty
                        if pd.isnull(row[x]):
                            #st.write("no info. skipping")
                            continue
                        else:
                            #st.write("working on:",row[x])
                            try:
                                # evaluate input string (try to catch lists)
                                #st.write("Tryeval:",str(row[x.replace('property','value')]))
                                inVal=stTrx.Tryeval(str(row[x.replace('key','value')]))
                                # match original schema (try to catch float/int mismatch)
                                #st.write("MatchType:",inVal)
                                inVal=stTrx.MatchType(readSchema['properties'][row[x]],inVal)
                                #st.write("inVal:",inVal)
                            except KeyError:
                                #st.write(f"KeyError for {row[x]} - is the _key_ column entry correct?"")
                                pass
                            foundKey=False
                            pKey="properties"
                            try:
                                if row[x] in readSchema[pKey].keys():
                                    # match original schema (try to catch float/int mismatch)
                                    inVal=stTrx.MatchType(readSchema[pKey][row[x]],inVal)
                                    readSchema[pKey][row[x]]=inVal
                                    foundKey=True
                            except KeyError:
                                # some tests don't have properties
                                st.write(f"No {pKey} key found in schema.")
                                pass
                            if foundKey==False:
                                st.write(f"No required key found in {pKey} for value: {row[x]}. Adding anyway.")
                                readSchema[pKey][row[x]]=inVal

            if "testrun" in readType.lower():
                loopText="setting up: "+row['testType']+" for "+row['component']+", "+str(index+1)+" of "+str(listLen)
                status_text_ids.text(loopText)
                #st.write("setting up *",row['testType'],"* for",row['component'])
                try:
                    readSchema=st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':row['componentType'], 'code':row['testType'], 'requiredOnly':True})
                except KeyError:
                    st.write("Data is missing from file. Please check componentType and testType are defined.")
                    continue
                    # st.write("Something missing (componentType/testType). Will use:",{'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType']})
                    # readSchema=st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['code'], 'requiredOnly':True})
                readSchema['component']=row['component']
                try:
                    dateStr=datetime.strptime(row['date'], "%d/%m/%y").strftime("%Y-%m-%dT10:10Z")
                    readSchema['date']= pdbTrx.TimeStampConverter(dateStr, "%Y-%m-%dT10:10Z")
                except ValueError:
                    dateStr=datetime.strptime(row['date'], "%d/%m/%Y").strftime("%Y-%m-%dT10:10Z")
                    readSchema['date']= pdbTrx.TimeStampConverter(dateStr, "%Y-%m-%dT10:10Z")
                except:
                    st.write("Issue reading date. Check format.")
                    readSchema['date']=row['date']

                # looping over input then setting schema (rather than looping schemas and finding input)
                for x in df_input.columns:
                    ### for testTypes, need to get properties and results
                    for para in ['prop','res']:
                        ### match to schema sub-dictionary
                        pKey="properties"
                        if para=="res":
                            pKey="results"
                        if para in x and "key" in x:
                            if pd.isnull(row[x]):
                                continue
                            else:
                                try:
                                    # evaluate input string (try to catch lists)
                                    inVal=stTrx.Tryeval(str(row[x.replace('key','value')]))
                                    inVal=stTrx.MatchType(readSchema[pKey][row[x]],inVal)
                                except KeyError:
                                    #st.write(f"KeyError for {row[x]} - is the _key_ column entry correct?")
                                    pass
                                # st.write(row[x])
                                # set value, find in results or properties
                                foundKey=False
                                try:
                                    if row[x] in readSchema[pKey].keys():
                                        # match original schema (try to catch float/int mismatch)
                                        inVal=stTrx.MatchType(readSchema[pKey][row[x]],inVal)
                                        readSchema[pKey][row[x]]=inVal
                                        foundKey=True
                                except KeyError:
                                    # some tests don't have properties
                                    st.write(f"No {pKey} key found in schema.")
                                    pass
                                if foundKey==False:
                                    st.write(f"No required key found in {pKey} for value: {row[x]}. Adding anyway.")
                                    readSchema[pKey][row[x]]=inVal

                    if "passed" in x.lower() or "problems" in x.lower():
                        # st.write("checking",x,":",row[x])
                        try:
                            readSchema[x.lower()]=row[x]
                        except KeyError:
                            pass
                    if "runNumber" in x:
                        # st.write("got run Number",row[x])
                        if not pd.isnull(row[x]):
                            try:
                                readSchema[x]=str(row[x])
                            except KeyError:
                                pass
                try:
                    readSchema['stage']=row['stage']
                except KeyError:
                    pass

            readSchema['institution']=st.session_state.Authenticate['inst']['code']

            try:
                if row['asStrings']:
                    readSchema = json.loads(json.dumps(readSchema), parse_int=str)
                    for x in ['properties','results']:
                        # st.write(x)
                        # st.write(readSchema)
                        try:
                            readSchema[x] = json.loads(json.dumps(readSchema[x]), parse_float=str)
                        except KeyError:
                            pass
            except KeyError:
                pass

            pageDict[schemaName].append(readSchema)

def DelChunk(regType, retObj):
        
    delVal=None

    # get identifier - use in button to help avoid key duplication error
    ident=None
    if "component" in regType.lower():
        ident=pdbTrx.FindKey(retObj)
    elif "batch" in regType.lower() or "testrun" in regType.lower():
        ident=pdbTrx.FindKey(retObj,'id')

    if st.button(f"Delete {ident}"):
        ### delete data
        if "component" in regType.lower():
            delVal = pdbTrx.UploadData('deleteComponent',{'component':ident})
        elif "batch" in regType.lower():
            delVal = pdbTrx.UploadData('deleteBatch',{'id':ident})
        elif "testrun" in regType.lower():
            delVal = pdbTrx.UploadData('deleteTestRun',{'testRun':ident, 'reason':"webApp delete"})

    if delVal!=None:
        st.write("Last _successful_ deletion:", pdbTrx.FindKey(delVal,'id'))
    else:
        st.write("No deletion return found")
    
    return delVal

def StageCheck(pageDict, schemaName="testSchema"):

    # st.write("In StageCheck")
    compVal=st.session_state.myClient.get('getComponent', json={'component':pageDict[schemaName]['component']})

    ### adding stage info.abs
    # collect testRun ids for bulk call
    if "tests" in compVal.keys() and type(compVal['tests'])==type([]):
        testRuns=[]
        for tt in compVal['tests']:
            if "testRuns" in tt.keys() and type(tt['testRuns'])==type([]):
                testRuns.extend( [tr['id'] for tr in tt['testRuns'] if type(tr)==type({}) and "id" in tr.keys()] )
    # bulk call
    testRuns=st.session_state.myClient.get('getTestRunBulk', json={'testRun': testRuns})
    # match to tests
    if "tests" in compVal.keys() and type(compVal['tests'])==type([]):
        for tt in compVal['tests']:
            if "testRuns" in tt.keys() and type(tt['testRuns'])==type([]):
                for x in [tr for tr in tt['testRuns'] if type(tr)==type({}) and "id" in tr.keys()]:
                    testMatch= next((tm for tm in testRuns if tm['id']==x['id']), None)
                    if testMatch==None:
                        x['testedAtStage']=None
                        continue
                    comp= next((c for c in testMatch['components'] if c['code']==compVal['code']), None)
                    if comp==None:
                        x['testedAtStage']=None
                        continue
                    x['testedAtStage']=comp['testedAtStage']['code']
    
    # st.write("got comp:",compVal)
    df_cl=pdbTrx.FillCheckList(compVal)
    if df_cl.empty:
        st.write("Cannot generate Test Upload Summary :(")
        return None
    sum_cols=['stage_order','stage_code','test_code','present','passed']
    st.write("Test Upload Summary (green: PASSED, red: FAILED, grey: MISSING)")
    if st.checkbox("Show only uploaded tests?", value=True, key=f"upload_{pageDict[schemaName]['component']}"):
        st.write(df_cl[sum_cols].query('present.str.contains("UPLOAD")', engine='python').style.apply(pdbTrx.HighlightTests, axis=1))
    else:
        st.write(df_cl[sum_cols].style.apply(pdbTrx.HighlightTests, axis=1))

    ### check for missing/failed tests in current stage
    # get current stage
    csc=df_cl.query('present.str.contains("current")', engine='python')['stage_code'].values[0]
    # st.write(csc)
    testcheck=True
    # check for missing tests
    if not df_cl.query(f'present.str.contains("current") & present.str.contains("MISSING")', engine='python').empty:
        st.write("__There are missing tests at currentStage__")
        st.write()
        testCheck=False
    # if not df_cl.query(f'stage_code=="{csc}" & passed==False', engine='python').empty:
    #     st.write(f"__Unpassed tests remain at current stage ({csc})__")
    #     testCheck=False
    # user option to proceed
    if not testcheck:
        if st.checkbox("Select to proceed with missing tests"):
            st.write(f"Stage will be _temporarily_ changed to {pageDict['stage']} (from {csc})")
        else:
            st.stop()
    else:
        st.write(f"No *required* tests missing at current stage: __{csc}__ (current location: __{compVal['currentLocation']['code']}__)")   

    return df_cl

def RegChunk(pageDict, regType, schemaName, fileKey=None, checkStage=True):
    # upload(!)

    retStage=False
    if "testrun" in regType.lower():
        if st.checkbox("Return components to original stage after test upload?", key=schemaName):
            st.info("Components will __return to original stage__ after test upload.\
                    \n-  Not applicable to retroactive uploads.")
            retStage=True
        else:
            st.info("Components will __remain in upload stage__ after test upload.")
            retStage=False

    # adding key hack - not perfect but hopefully enough to use schemaName
    # ### test check summary (for all)
    # if "testrun" in regType.lower() and checkStage:
    #     StageCheck(pageDict)

    ### key to distinguish widgets
    keyStr=schemaName
    # use some input data
    for k in ['serialNumber','component','batchNumber','date']:
        # st.write(f"try: {k}")
        try:
            keyStr=keyStr+"_"+pageDict[schemaName][k]
            break
        except KeyError:
            pass

    # st.write(f"__key {keyStr}__")

    if st.button("Upload "+regType, key=keyStr):
        ### upload data
        if "component" in regType.lower():
            pageDict['upVal']=pdbTrx.UploadData('registerComponent',pageDict[schemaName])

        elif "batch" in regType.lower():
            pageDict['upVal']=pdbTrx.UploadData('createBatch',pageDict[schemaName])

        elif "testrun" in regType.lower():
            ### get current stage of component
            st.write("checking code:",pageDict[schemaName]['component'])
            compVal=st.session_state.myClient.get('getComponent', json={'component':pageDict[schemaName]['component']})
            putBack=False
            ### check current stage (default on)
            if checkStage:

                ### check for retroactive upload
                if "isRetroactive" not in pageDict[schemaName] or pageDict[schemaName]['isRetroactive']==False:
                    st.write("- upload is __not__ _retroactive_")
                    ### if not in correct stage set it
                    try:
                        if compVal['currentStage']['code']!=pageDict['stage']:
                            ### set stage first!
                            st.write(f"Changing to approriate stage: {pageDict['stage']} (from {compVal['currentStage']['code']})")
                            putBack=pdbTrx.SetCompStage(compVal['code'], pageDict['stage'])
                        else:
                            st.write(f"Already in appropriate stage: {pageDict['stage']}")
                    except TypeError:
                        st.write("No stage previously exists. Setting...")
                        pdbTrx.SetCompStage(compVal['code'], pageDict['stage'])
                else:
                    st.write("- upload __is__ _retroactive_ (no stage change)")
                    pageDict[schemaName]['stage']=pageDict['stage']
                
                ### upload data
                pageDict['upVal']=pdbTrx.UploadData('uploadTestRunResults',pageDict[schemaName])

                ### file(s) upload if data upload successful and fileKey set
                if pageDict['upVal']!=None and fileKey!=None:
                    if type(pageDict[fileKey])==type([]):
                        st.write("Upload file array:",fileKey)
                        for fk in pageDict[fileKey]:
                            pdbTrx.UploadFile(fk,pdbTrx.FindKey(pageDict['upVal']['testRun'],'id'),'obj',fk['name'])
                    elif type(pageDict[fileKey])==type({}):
                        st.write("Upload file dictionary:",fileKey)
                        pdbTrx.UploadFile(pageDict[fileKey],pdbTrx.FindKey(pageDict['upVal']['testRun'],'id'),'obj',pageDict[fileKey]['name'])
                    else:
                        st.write("Upload single file:",fileKey)
                        pdbTrx.UploadFile(pageDict,pdbTrx.FindKey(pageDict['upVal']['testRun'],'id'),fileKey)

                ### check for retroactive upload
                if "isRetroactive" not in pageDict[schemaName] or pageDict[schemaName]['isRetroactive']==False:
                    st.write("- upload is __not__ _retroactive_")
                    ### set component back to original stage, if moved
                    if retStage and putBack:
                        st.write(f"Returning to stage: {compVal['currentStage']['code']} (from {pageDict['stage']})")
                        pdbTrx.SetCompStage(compVal['code'], compVal['currentStage']['code'])
                else:
                    st.write("- upload __is__ _retroactive_ (no stage)")

        # stTrx.Flourish()
        # st.write("Upload confirmation:", pageDict['upVal'])

    if 'upVal' in pageDict.keys():
        if pageDict['upVal']==None:
            st.write("No Previous upload data")
        else:
            st.write("Last _successful_ upload:",pdbTrx.FindKey(pageDict['upVal'],'id'))

            if st.checkbox("Add comment?", key=keyStr+"_addCom"):
                ### text input
                comStr=st.text_input("Comment string:")
                if st.button("Upload Comment"):
                    if "component" in regType.lower():
                        pageDict['comVal'] = pdbTrx.UploadData('createComponentComment',{'component':pdbTrx.FindKey(pageDict['upVal']),'comments':[comStr]})
                    elif "testrun" in regType.lower():
                        pageDict['comVal'] = pdbTrx.UploadData('createTestRunComment',{'testRun':pdbTrx.FindKey(pageDict['upVal'],'id'),'comments':[comStr]})

            if st.button("Delete", key=keyStr+"delete"):
                ### delete data
                if "component" in regType.lower():
                    # pageDict['delVal'] = st.session_state.myClient.post('deleteComponent', json={'component':pdbTrx.FindKey(pageDict['upVal'])})
                    pageDict['delVal'] = pdbTrx.UploadData('deleteComponent',{'component':pdbTrx.FindKey(pageDict['upVal']), 'reason':"webApp delete"})
                elif "batch" in regType.lower():
                    # pageDict['delVal'] = st.session_state.myClient.post('deleteComponent', {'component':pdbTrx.FindKey(pageDict['upVal'])})
                    pageDict['delVal'] = pdbTrx.UploadData('deleteBatch',{'id':pdbTrx.FindKey(pageDict['upVal'],'id')})
                elif "testrun" in regType.lower():
                    # pageDict['delVal'] = st.session_state.myClient.post('deleteTestRun', {'testRun':pdbTrx.FindKey(pageDict['upVal'],'id')})
                    pageDict['delVal'] = pdbTrx.UploadData('deleteTestRun',{'testRun':pdbTrx.FindKey(pageDict['upVal'],'id'), 'reason':"webApp delete"})

    if 'delVal' in pageDict.keys():
        try:
            st.write("Last _successful_ deletion:", pdbTrx.FindKey(pageDict['delVal'],'id'))
        except KeyError:
            pass


### update later to check currentStage before test upload
def RegMultiChunk(pageDict, regType, schemaName, checkStage=True):
    # upload(!)

    ### do uploads
    retStage=False
    if "testrun" in regType.lower():
        if  st.checkbox("Return components to original stage after test upload?"):
            st.info("Components will __return to original stage__ after test upload.\
                    \n-  Not applicable to retroactive uploads.")
            retStage=True
        else:
            st.info("Components will __remain in upload stage__ after test upload.")
            retStage=False

    if st.button(regType):
        if "upIds" not in pageDict.keys():
            pageDict['upIds']=[]
        listLen=len(pageDict[schemaName])
        status_bar_ids=st.progress(0.0) #st.empty()
        status_text_ids=st.empty()
        for e,cs in enumerate(pageDict[schemaName]):
            ### upload data
            try:
                if 'component' in regType.lower():
                    loopText="working on:"
                    if "serialNumber" in cs.keys():
                        loopText+=f" *{cs['serialNumber']}*"
                    loopText+=f" {str(e+1)} of {str(listLen)}"
                    status_text_ids.text(loopText)
                    status_bar_ids.progress((1.0*e+1)/listLen)
                    #st.write("Working on *",cs['serialNumber'],"*")
                    try:
                        upVal=st.session_state.myClient.post('registerComponent', json=cs)
                        if listLen<100:
                            st.write(f"#### **Successful {regType} Registration**:",pdbTrx.FindKey(upVal))
                            stTrx.Flourish()
                        else:
                            st.write("#### 🎈🎈🎈 **Successful "+regType+" Registration**:",pdbTrx.FindKey(upVal))
                        pageDict['upIds'].append(pdbTrx.FindKey(upVal))                        
                    except itkX.BadRequest as b:
                        st.write(f"#### :no_entry_sign: {regType} Registration **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                elif "testrun" in regType.lower():
                    loopText="working on: *"+cs['component']+"*, "+str(e+1)+" of "+str(listLen)
                    status_text_ids.text(loopText)
                    status_bar_ids.progress((1.0*e+1)/listLen)
                    # st.write("Working on *",cs['component'],"*")
                    # st.write("schema:",cs)

                    oriStage=None
                    if retStage:
                        oriStage=st.session_state.myClient.get('getComponent', json={'component':cs['component']})['currentStage']['code']
                    ### set stage first!
                    ### check retroactive
                    if "isRetroactive" in cs.keys() and cs['isRetroactive']==True:
                        st.write("No stage moves for retroactive uploads")
                    else:
                        try:
                            try:
                                setVal=st.session_state.myClient.post('setComponentStage', json={'component':cs['component'], 'stage':cs['stage']})
                                st.write(f"#### **Successful stage set** ({cs['stage']}) for:",pdbTrx.FindKey(setVal,'id'))
                            except KeyError:
                                st.write("Something funky (stage?). Will use:",{'stage':pageDict['stage']})
                                setVal=st.session_state.myClient.post('setComponentStage', json={'component':cs['component'], 'stage':pageDict['stage']})
                                st.write(f"#### **Successful stage set** ({pageDict['stage']}) for:",pdbTrx.FindKey(setVal,'id'))
                        except itkX.BadRequest as b:
                            st.write(f"#### :no_entry_sign: Stage ({pageDict['stage']})setting **Unsuccessful**")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                        ### remove stage flag 
                        cs.pop('stage', None)

                    # [st.write(k,type(v)) for k,v in cs['results'].items()]
                    try:
                        upVal=st.session_state.myClient.post( 'uploadTestRunResults', json=cs)
                        if listLen<100:
                            st.write(f"#### **Successful {regType} Registration**:",pdbTrx.FindKey(upVal,'id'))
                            stTrx.Flourish()
                        else:
                            st.write(f"#### 🎈🎈🎈 **Successful {regType} Registration**:",pdbTrx.FindKey(upVal,'id'))
                        pageDict['upIds'].append(pdbTrx.FindKey(upVal,'id'))
                    except (requests.exceptions.InvalidJSONError, TypeError):
                        st.write("#### :exclamation: Json Error: please check schema:",cs)
                        st.write("__Are any parameters missing?__")
                    except itkX.BadRequest as b:
                        st.write(f"#### :no_entry_sign: {regType} Registration **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                    
                    ### return to original stage if appropriate
                    if retStage:
                        ### check retroactive
                        if "isRetroactive" in cs.keys() and cs['isRetroactive']==True:
                            st.write("No stage moves for retroactive uploads")
                        else:
                            st.write(f"Returning component to {oriStage} stage")
                            try:
                                setVal=st.session_state.myClient.post('setComponentStage', json={'component':cs['component'], 'stage':oriStage})
                                st.write(f"#### **Successful stage set** ({oriStage}) for:",pdbTrx.FindKey(setVal,'id'))
                            except itkX.BadRequest as b:
                                st.write(f"#### :no_entry_sign: Stage ({oriStage})setting **Unsuccessful**")
                                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

            except itkX.BadRequest as b:
                st.write("#### :no_entry_sign: Update **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
            except TypeError:
                st.write("Don't have return value :(")

    if "upIds" not in pageDict.keys():
        st.stop()

    st.write("---")
    st.write("### uploaded "+regType+" IDs")
    st.dataframe(pageDict['upIds'])

    if "upIds" in pageDict.keys():
        if st.button("Delete "+regType+"s"):
            for upId in list(pageDict['upIds']):
                ### delete data
                try:
                    if 'component' in regType.lower():
                        delVal= st.session_state.myClient.post('deleteComponent', json={'component':upId, 'reason':"webApp delete"})
                    elif "testrun" in regType.lower():
                        delVal= st.session_state.myClient.post('deleteTestRun', json={'testRun':upId, 'reason':"webApp delete"})
                    st.write("#### **Successful Deletion**:",upId)
                    pageDict['upIds'].remove(upId)
                except itkX.BadRequest as b:
                    st.write("#### :no_entry_sign: Deletion unsuccessful")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                except TypeError:
                    st.write("Don't have return value :(")

### chunk of code to assemble component based on input dictionary - with option to use slot ids
def AssembleChunk(assDict,findSlot=False):
    # st.write(assDict['comb'])
    # st.write(assDict['parts'])
    ### inital check target component SN defined
    try:
        if assDict['comb']['SN']==None:
            st.write("No comb component serialNumber defined")
            return False
    except KeyError:
        st.write("Cannot find comb component serialNumber")
        return False
    ### get target component
    st.write("working on comb:",assDict['comb'])
    compComb=st.session_state.myClient.get('getComponent', json={"component":assDict['comb']['SN']} )
    # st.write(compComb)
    ### begin with assembly flag True
    assCheck=True
    ### loop over parts
    for part in assDict['parts']:
        st.write("assembling part:",part)
        ### skip if no serialNumber
        if part['SN']==None:
            st.write("No serialNumber defined")
            continue
        ### find slot, if flagged
        if findSlot and "slot" in part.keys():
            ### give target comp, child compType and expected slot (use: 0...)
            tempPos=pdbTrx.SlotFinder(compComb,part['compType'],str(part['slot'])) #str(int(part['pos'])+1)
            #st.write("pos id:",tempPos)
            # if nothing found skip
            if tempPos==None:
                st.write("No matching slot found. Assembly skipped.")
                continue
            # if found, try assembly with slot
            try:
                assVal=st.session_state.myClient.post('assembleComponentBySlot', json={"parent":assDict['comb']['SN'], 'slot':tempPos, "child":part['SN']} )
                st.write("#### **Successful "+part['compType']+" assembly**:",assVal['id'])
                assCheck=True
            except itkX.BadRequest as b:
                st.write("#### :no_entry_sign: assembly **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                assCheck=False
            except TypeError:
                st.write("Cannot assemble",part['SN'])
                assCheck=False
        # try assembly without slot
        else:
            try:
                assVal=st.session_state.myClient.post('assembleComponent', json={"parent":assDict['comb']['SN'], "child":part['SN']} )
                assCheck=True
            except itkX.BadRequest as b:
                st.write("#### :no_entry_sign: assembly **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                assCheck=False
            except TypeError:
                st.write("Cannot assemble",part['SN'])
    ### return if unsuccessful
    if assCheck==False:
        return False
    ### check reworks information to add comments
    else:
        # assDict['assembly']=True # do this via return value
        # if find reworks information...
        if "reworks" in assDict.keys():
            st.write("Reworks found.")
            # loop over for comments
            for rw in assDict['reworks']:
                if "comment" in rw.keys():
                    try:
                        comVal=st.session_state.myClient.post('createComponentComment', json={"component" : assDict['comb']['SN'],
                                                                                 "comments" : [rw['comment']]} )
                        st.write("#### **Successful createComponentComment**:",comVal['component']['id'])
                        st.write('"',rw['comment'],'"')
                    except itkX.BadRequest as b:
                        st.write("#### :no_entry_sign: createComponentComment **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        ### finish
        return True


def DisassembleChunk(compObj):
    # if st.button("disassemble",key=k+1000):

    st.write(f"parent: {compObj['serialNumber']} ({compObj['componentType']['code']})")
    for child in compObj['children']:
        try:
            if child['component']==None:
                st.write("No child component in slot.")
                continue
            st.write(f"child: {child['component']['serialNumber']} ({child['componentType']['code']})")
        except TypeError:
            st.write("No child found")
            continue
        #partComp=st.session_state.myClient.get('getComponent', json={"component" : part} )
        try:
            disVal=st.session_state.myClient.post('disassembleComponent', json={"parent" : compObj['code'],
                                                                         "child" : child['component']['code']} )
            st.write("#### **Successful disassembly**:",disVal['id'])
            # return True
            # assDict['assembly']=False
        except itkX.BadRequest as b:
            st.write("#### :no_entry_sign: disassembly **Unsuccessful**")
            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        except TypeError:
            st.write("Cannot disassemble",child['component']['serialNumber'])
    return False

############
### new upload functions 31/01/23
############

matchMap={'int':int,'integer':int,'float':float,'str':str,'string':str,'bool':bool}

def FillJson(inJson):

    outJson={}
    for k,v in inJson.items():
        st.write("__"+k+"__")
        if k in ['project','componentType','institution','testType']:
            outJson[k]=v
            st.write("got it:",v)
            continue
        if type(v)==type({}):
            outJson[k]=FillJson(inJson[k])
        elif type(v)==type([]):
            val=st.selectbox('Select '+k,v)
            try:
                outJson[k]=val['value']
                st.write("use value:",val['value'])
            except KeyError:
                try:
                    outJson[k]=val['code']
                    st.write("use code:",val['code'])
                except KeyError:
                    outJson[k]=val
        else:
            if type(v)==type(True):
                outJson[k]= st.radio('Set '+k, [True, False])
            elif v=="object":
                val=st.text_area('Input '+k+' ('+v+')',v)
                st.write("Will try to evaluate. Please check final object")
                outJson[k]=stTrx.Tryeval(val)
            elif v=="image":
                st.write("Images supported")
            else:
                try:
                    val=st.text_input('Input '+k+' ('+v+')',v)
                except TypeError:
                    continue
                if v=="date":
                    try:
                        outJson[k]= pdbTrx.TimeStampConverter(val, "%Y-%m-%dT%H:%MZ")
                    except ValueError:
                        st.write("format: YYYY-mm-ddTHH:MMZ")
                else:
                    try:
                        outJson[k]=matchMap[v](val)
                    except ValueError:
                        pass

    return outJson


def CustomJson(pageDict, cmdType, comJson, subSet=None):

    ### check reequiredOnly included
    if "requiredOnly" not in comJson.keys():
        comJson['requiredOnly']=False
    else:
        if comJson['requiredOnly']:
            comJson['requiredOnly']=False
            subSet="required"

    ### get PDB info.
    retInfo, retJson = None, None
    if "component" in cmdType:
        retInfo=st.session_state.myClient.get( 'getComponentTypeByCode',json=comJson)
        retJson=st.session_state.myClient.get( 'generateComponentTypeDtoSample',json=comJson)
    elif "test" in cmdType:
        retInfo=st.session_state.myClient.get( 'getTestTypeByCode',json=comJson)
        retJson=st.session_state.myClient.get( 'generateTestTypeDtoSample',json=comJson)
    else:
        st.write(f"Don't undetstand {cmdType}.")
        st.write(f"Check {comJson}. Try component/type.")
        return None
    
    ### build json - top level keys
    basicJson={ k:None for k in retJson.keys()}
    basicJson['institution']=st.session_state.Authenticate['inst']['code']
    ### for component
    if "component" in cmdType:
        basicJson['project']=comJson['project']
        basicJson['componentType']=comJson['code']
        if "serialNumber" in basicJson.keys():
            basicJson['serialNumber']="string"
        ### sub-project
        # infra.SelectBox(pageDict,'subSel',retInfo['subprojects'],'Select sub-project:',"name")
        if "subprojects" in retInfo.keys():
            basicJson['subproject']=retInfo['subprojects']
        ### sub-type
        # infra.SelectBox(pageDict,'typeSel',retInfo['types'],'Select sub-type:',"name")
        basicJson['type']=retInfo['types']
    ### for component
    elif "test" in cmdType:
        if "component" in basicJson.keys():
            basicJson['component']="string"
        basicJson['testType']=comJson['code']
        basicJson['runNumber']="string"
        basicJson['date']="date"
        basicJson['passed']=True
        basicJson['problems']=False

    ### build json - properties (comp & test) and results (test)
    for pp in ["properties","parameters"]:
        try:
            ### get selected keys
            df_info=pd.DataFrame(retInfo[pp])
            if subSet!=None:
                if type(subSet)==type([]):
                    ### check user columns in dataframe (splits properties and parameters)
                    dfCodes=[s for s in subSet if s in df_info['code'].to_list()]
                    # st.write('code==["'+'","'.join(dfCodes)+'"]')
                    if df_info.empty:
                        st.write(f" - no {pp} options found")
                        continue
                    try:
                        df_info=df_info.query('code==["'+'","'.join(dfCodes)+'"]')
                    except KeyError:
                        st.write("One of the columns is not present.")
                        st.write("Check columns:",df_info['code'].to_list())
                else:
                    if "req" in subSet:
                        df_info=df_info.query('required==True')
            st.write(f"__{pp}__")
            if df_info.empty:
                st.write(" - none selected")
            else:
                st.write(df_info)
            
            ### get values
            rp=None
            if pp=="parameters":
                rp="results"
            else:
                rp=pp

            basicJson[rp]={}
            for i,row in df_info.iterrows():
                # st.write(f"{row['name']} ({row['code']})")
                if "code" in row['dataType']:
                    # code=st.selectbox(f"{row['name']} ({row['code']})",)
                    basicJson[rp][row['code']]=row['codeTable']
                elif row['dataType'] in matchMap.keys():
                    basicJson[rp][row['code']]=row['dataType']
                else:
                    basicJson[rp][row['code']]=row['dataType']
        except KeyError: # handle non-results (i.e. component)
            pass

    # st.write("returning:",basicJson)
    return basicJson

def SelectComponentChunk(pageDict):
    # infra.Radio(pageDict,'inputType',["Enter identifier", "QRcode", "Select from inventory"],"Select input method:")
    pageDict['inputType']= st.radio("Select input method:",["Enter identifier", "QRcode", "Select from inventory"])
    st.write("### ",pageDict['inputType'])

    if "qrcode" in pageDict['inputType'].lower():
        
        inPic=st.camera_input("Input picture:")
        pageDict['altID']= st.radio("Type of identifier:",['serialNumber','alternativeIdentifier'])

        if inPic is not None:
            bytes_data = inPic.getvalue()
            cv2_img = cv2.imdecode(np.frombuffer(bytes_data, np.uint8), cv2.IMREAD_COLOR)
            detector = cv2.QRCodeDetector()
            data_raw, bbox, straight_qrcode = detector.detectAndDecode(cv2_img)

            st.write("raw input data:",data_raw)
            try:
                stTrx.DebugOutput("raw data:",data_raw)

                if 'comp' not in pageDict.keys() or st.button("Get component"):
                    pageDict['inComp']=data_raw
                    compQueryJson={'component':pageDict['inComp'],'type':"SN"}
                    if pageDict['altID']=='alternativeIdentifier':
                        compQueryJson['type']='altID'
                    stTrx.DebugOutput("queryJson:",compQueryJson)

                    pageDict['comp']=pdbTrx.GetComponentByIdentifier(compQueryJson['component'],compQueryJson['type'])

            except SyntaxError:
                st.write("Syntax error --> No QR found")
                st.write("Check image/brightness")


    elif "identifier" in pageDict['inputType'].lower():

        infra.TextBox(pageDict,'inComp',"Enter component identifier")
        pageDict['altID']= st.radio("Type of identifier:",['serialNumber','alternativeIdentifier'])

        if pageDict['inComp']==None or len(pageDict['inComp'])<1:
            st.write("Enter identifier to get component information")
            st.stop()

        if 'comp' not in pageDict.keys() or st.button("Get component"):
            compQueryJson={'component':pageDict['inComp'],'type':"SN"}
            if pageDict['altID']=='alternativeIdentifier':
                compQueryJson['type']='altID'
            stTrx.DebugOutput("queryJson:",compQueryJson)

            pageDict['comp']=pdbTrx.GetComponentByIdentifier(compQueryJson['component'],compQueryJson['type'])

    else:
        ### select components by hand
        if st.button("reset list") or "compList" not in list(pageDict.keys()): # check list
            st.write("**Getting** "+pageDict['componentType']+" list for "+st.session_state.Authenticate['inst']['code']+" ("+pageDict['project']+")")
            pageDict['compList']=st.session_state.myClient.get('listComponents', json={'currentLocation':st.session_state.Authenticate['inst']['code'], 'project':pageDict['project'], 'componentType':pageDict['componentType']})
            if type(pageDict['compList'])!=type([]):
                pageDict['compList']=pageDict['compList'].data
        else:
            st.write("**Got** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
        df_compList=pd.json_normalize(pageDict['compList'], sep = "_")

        # st.write(df_subComps)
        infra.SelectBox(pageDict,'selSN',df_compList['serialNumber'].to_list(),'Select serialNumbers:')
        stTrx.DebugOutput('Selected serialnumber: ',pageDict['selSN'])
        pageDict['comp'] = next(item for item in pageDict['compList'] if item['serialNumber'] == pageDict['selSN'])
    
    ### stop page if not found
    if type(pageDict['comp'])!=type({}) and type(pageDict['comp'])!=type([]):
        st.write("__Cannot find component__")
        st.write("Please check inputs.")
        st.stop()

    return None
    
### write json to csv (10/02/25)
def WriteDF2CSV(inJson):
    ### header
    df_csv=pd.DataFrame([{k:v for k,v in inJson.items() if type(v)!=type({}) and type(v)!=type([])}])
    ### properties
    if "properties" in inJson.keys():
        for e,prop in enumerate(inJson['properties'].keys(),1):
            df_csv[f'property{e}_key']=prop
            df_csv[f'property{e}_value']=f"{inJson['properties'][prop]}"
    ### results
    if "results" in inJson.keys():
        for e,res in enumerate(inJson['results'].keys(),1):
            df_csv[f'result{e}_key']=res
            df_csv[f'result{e}_value']=f"{inJson['results'][res]}"
    # display(df_csv)
    # df_csv.to_csv('test.csv',index=False)
    return df_csv

### options to select strucutre to write
def SelectStructure(originalSchema, fullSchema, testSchema, offerCSV=True):

    ### add additional properties
    st.write("The required parameters will be included in the upload schema. Please select below to include others.")
    if st.checkbox("Select optional parameters to include?"):
        for pr in ['properties','results']:
            st.write(f"__{pr}__")
            if pr not in fullSchema.keys() or type(fullSchema[pr])!=type({}) or len(fullSchema[pr].keys())<1: 
                st.write(" - No parameters found")
                continue
            optList=list(set(fullSchema[pr]) - set(originalSchema[pr]))
            if len(optList)<1:
                st.write(" - No _optional_ parameters found")
                continue
            optSels=st.multiselect("add optional parameter:",optList)
            for os in optSels:
                if os not in testSchema[pr].keys():
                    testSchema[pr][os]=None

    ### option to write csv
    if offerCSV:
        if st.checkbox("Write to csv?"):
            df_csv=WriteDF2CSV(testSchema)
            st.download_button(label="Write csv", data=df_csv.to_csv(index=False),file_name=f"template_{originalSchema['testType']}.csv")

    return testSchema

### Get distribution stats - help visualisation
def MakeStatDict(df_paraData, colName="paraValue", dataType=None):
    # st.write(f"colName: {colName}")
    

    # st.write("Initial type:",df_paraData[colName].dtype)
    # characteristics
    ### if nothing to go on...
    if dataType==None:
        try:
            statDict={
                'total':len(df_paraData),
                'mean':df_paraData[colName].mean(),
                'std':df_paraData[colName].std()
            }
        except TypeError:
            # force numeric
            df_paraData[colName]=pd.to_numeric(df_paraData[colName], errors='coerce').notnull()
            statDict={
                'total':len(df_paraData),
                'mean':df_paraData[colName].mean(),
                'std':df_paraData[colName].std()
            }
    else:
        foundType=False
        if dataType in ['int', 'integer', 'float', 'bool']:
            try:
                df_paraData[colName]=df_paraData[colName].astype(float)
                statDict={
                    'total':len(df_paraData),
                    'mean':df_paraData[colName].mean(),
                    'std':df_paraData[colName].std()
                }
                foundType=True
            except ValueError:
                st.write(" - Geez this type sucks!")
        if not foundType:
            # force numeric
            df_paraData[colName]=pd.to_numeric(df_paraData[colName], errors='coerce').notnull()
            statDict={
                'total':len(df_paraData),
                'mean':df_paraData[colName].mean(),
                'std':df_paraData[colName].std()
            }


    ### range
    # if std sucks
    if any(np.isnan(val) for val in statDict.values()):
        # st.write(f"Can't calculate range from std: {statDict}.\nHack it!")
        statDict['lo']=statDict['mean']-statDict['mean']
        statDict['hi']=statDict['mean']+statDict['mean']
    # if std good
    else:
        # st.write(f"Calculating range from std: {statDict}")
        statDict['lo']=statDict['mean']-2*statDict['std']
        statDict['hi']=statDict['mean']+2*statDict['std']
    # specs
    statDict['in_range']=len(df_paraData.query(f"{colName}>={statDict['lo']} & {colName}<={statDict['hi']}"))
    statDict['under_range']=len(df_paraData.query(f"{colName}<{statDict['lo']}"))
    statDict['over_range']=len(df_paraData.query(f"{colName}>{statDict['hi']}"))

    return statDict