# Overview App

Some tools to get statistics from the ITk Production Database (PDB)

---

## Pages 

### PDB Stats

Quick PDB Overview stats:

- number componentTypes, testTypes, batchTypes, components, testRuns, intitutions, users
    - information given per project where possible 

### Project Objects

Populations of componentTypes and testTypes per project

- number of stages and testTypes per componentType per project

### Project Populations

Populations of componentTypes and testTypes per project

### Institution Inventories

Institution Inventories

- number of components of componentType per institution (for selected project)
    - stage history available
    - testuploads not currently available (useful addition!)

### Shipments

Counting shipments

### Plot Parameters

Make on-demand plots of component/testRun data.

Select:
 
 - project code: P
 - componentType code: PCB
 - Get parameter: from testType
 - testType: …
 - select parameter: …
 - select legend: institution

Plotting:

 - parameter histogram
    - option to apply (automatic 2 sigma) statistics: improve plot resolution if possible - requires good data integrity
        - option to input plot max/min manually
 - option to see upload timeline 
 
Available information:

 - download the plot using the “…” button you get when you move the mouse over the plot
 - table below the plot has the count information for the test – the id column will give the number of tests (all tests have an _id_ value)
 - button beneath the table allows download the full test info. as a _csv_ file – make your own plots and check more details information
 