import streamlit as st
from core.Page import Page
import pandas as pd
import altair as alt
import numpy as np

import itkdb
import core.stInfrastructure as infra
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

### highlight high and low values
def Highlighter(s, col="count", lo=10, hi=1000):
    if s[col]<lo:
        return ['background-color: orange'] * len(s)
    elif s[col]>hi:
        return ['background-color: red'] * len(s)
    else:
        return ['background-color: white'] * len(s)

infoList=["  * Get components at selected institution",
        "   * filter based on populations"]
#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("Inventories", "Institution Inventories", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        st.write("## Select Project")

        PROJECT_CODE= st.selectbox("Select project code:",["S","P","CE","CM"])

        ### retrieving institutions information...
        if "df_instList" not in pageDict.keys() or pageDict['projCode']!=PROJECT_CODE:
            st.write("Getting project institutions and component counts")
            pageDict['projCode']=PROJECT_CODE
            instList=st.session_state.myClient.get('listInstitutions', json={}).data  
                      
            ### wangle institution information
            df_insts=pd.DataFrame(instList)

            # df_insts=df_insts.explode("componentTypes").reset_index(drop=True)
            df_insts['projects']=df_insts['componentTypes'].apply(lambda x: [ct['code'] for ct in x if type(ct)==type({}) and "code" in ct.keys()] if type(x)==type([]) else x)
            df_insts=df_insts.explode("projects")
            df_insts=df_insts.query(f'projects=="{PROJECT_CODE}"').reset_index(drop=True)
            df_insts['compCount']=df_insts['code'].apply(lambda x: st.session_state.myClient.get('listComponents', json={'project':PROJECT_CODE, 'currentLocation':x, 'pageInfo': {'pageSize': 1}}).total if x!=None else x )
            
            # cache dataframe
            pageDict['df_instList']=df_insts
            # remove previous inst info.
            if "compList" in pageDict.keys():
                pageDict.pop('compList')

        ### stop if list empty 
        if pageDict['df_instList'].empty:
            st.write(f"__No institutions found for project {PROJECT_CODE}__")
            st.stop()

        ### visualise datatable
        st.write(f"### Institutions ({PROJECT_CODE})")
        
        if "lowLimit_ct" not in pageDict.keys():
            pageDict['lowLimit_ct']= 10
        if "highLimit_ct" not in pageDict.keys():
            pageDict['highLimit_ct']= 1000

        if st.checkbox('Set institution limits?'):
            for k in ["lowLimit_inst","highLimit_inst"]:
                try:
                    pageDict[k]= int(st.text_input(f'set {k}'))
                except TypeError:
                    st.write("Please input integer value")
                except ValueError:
                    st.write("Please input integer value")

        # st.write(pageDict['df_instList'][['code','name','compCount']])
        st.write(f"population: :orange[<{pageDict['lowLimit_ct']}], :red[>{pageDict['highLimit_ct']}]")
        df_instList=pageDict['df_instList']
        if st.checkbox("remove Vendors"):
            df_instList=pageDict['df_instList'].query('isVendor!=True')[['code','name','compCount']]
        else:
            df_instList=pageDict['df_instList'][['code','name','compCount']]
        st.write(df_instList.style.apply(Highlighter, col='compCount', lo=10, hi=1000, axis=1))
        st.write(f"__Number of {PROJECT_CODE} institutions: {len(df_instList)}__")

        st.write("---")
        st.write("### Select Institution")

        INST_CODE= st.selectbox("Select institution code:",pageDict['df_instList']['code'].to_list())

        if "compList" not in pageDict.keys() or pageDict['instCode']!=INST_CODE:
            st.write(f"Get {INST_CODE} info.")
            pageDict['instCode']=INST_CODE
            total = st.session_state.myClient.get('listComponents', json={'project':PROJECT_CODE, 'currentLocation':INST_CODE, 'pageInfo': {'pageSize': 1}}).total
            st.write(f"Components to retrieve: {total}")
            ### set up loop
            pageSize=100
            count= int(total/pageSize)
            if total%pageSize>0:
                count=count+1
            ### do loop
            loop_bar = st.progress(0)
            loop_count=1
            pageDict['compList']=[]
            for pi in range(0,count,1):
                loop_bar.progress(loop_count/count)
                pageDict['compList'].extend(st.session_state.myClient.get('listComponents', json={'project':PROJECT_CODE,'currentLocation':INST_CODE,'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize }}).data)
                loop_count+=1

        st.write(f"### Component list for {INST_CODE} ({PROJECT_CODE}): {len(pageDict['compList'])}")

        ### filter data
        codeList=["project","subproject","componentType","type","currentLocation","currentStage","institution"]
        keyList=["id","code","serialNumber"]
        dataList=[ {k:x[k] for k in keyList if k in x.keys()} | {k:x[k]['code'] for k in codeList if type(x[k])==type({}) and "code" in x[k].keys()} for x in pageDict['compList'] if type(x)==type({}) ]
        # st.write(dataList)
        df_data=pd.DataFrame(dataList)

        # stop if empty
        if df_data.empty:
            st.write(f"__No components found for institution {INST_CODE}__")
            st.stop()

        # st.write(df_data)
        st.write( df_data.groupby(by=['componentType','type']).count().reset_index().rename(columns={'code':"count"})[['componentType','type','count']] )

