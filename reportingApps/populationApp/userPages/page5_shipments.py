import streamlit as st
from core.Page import Page
import pandas as pd
import altair as alt
import numpy as np
from datetime import datetime, timedelta
# plotting
import holoviews as hv
from holoviews import opts, dim
# from bokeh.plotting import show
import streamlit.components.v1 as components

import itkdb
import core.stInfrastructure as infra
import itkdb.exceptions as itkX

from commonCode.codeChunks import MakeStatDict

#####################
### useful functions
#####################


infoList=["  * Get components at selected institution",
        "   * filter based on populations"]
#####################
### main part
#####################

class Page5(Page):
    def __init__(self):
        super().__init__("Plot Shipments", "Shipment Diagram", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        st.write("## Shipments Overview")

        if "shipList" not in pageDict.keys() or st.button("reset list"):
            st.write(f"Get shipment info.")
            total = st.session_state.myClient.get('listShipments', json={'pageInfo': {'pageSize': 1}}).total
            st.write(f"Shipments to retrieve: {total}")
            ### set up loop
            pageSize=200
            count= int(total/pageSize)
            if total%pageSize>0:
                count=count+1
            ### do loop
            loop_bar = st.progress(0)
            loop_count=1
            pageDict['shipList']=[]
            for pi in range(0,count,1):
                loop_bar.progress(loop_count/count)
                pageDict['shipList'].extend(st.session_state.myClient.get('listShipments', json={'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize }}).data)
                loop_count+=1

        st.write(f"Total items retireved: {len(pageDict['shipList'])}")

        df_shipList=pd.json_normalize(pageDict['shipList'], sep = "_")
        if st.checkbox("See all shipment data?"):
            st.write(df_shipList)
        
        df_shipList=df_shipList[['name','type','status','state','sender_code','recipient_code','stateTs','id']].sort_values(by=['status','stateTs'], ascending=False).reset_index(drop=True)
        ### use cts if stateTs problematic (isNull)
        df_shipList['dateCheck']=pd.to_datetime(df_shipList['stateTs'], format='%Y-%m-%dT%H:%M:%S.%fZ', errors='coerce')
        # st.write(df_shipList.query('dateCheck.isnull()', engine='python'))



        # st.write(df_shipList)
        df_shipList=df_shipList.sort_values(by=['status','dateCheck'], ascending=False).reset_index(drop=True)


        if 'startTime' not in pageDict.keys():
            st.write("default 100 days ago")
            # then=(datetime.now() - timedelta(days = 100)).date()
            # df_shipList[~(df_shipList['stateTs'] <  datetime.strptime(str(pageDict['startTime']), '%Y-%m-%d') ) ]
            pageDict['startTime']= (datetime.now() - timedelta(days = 100)).date()

        pageDict['startTime']=st.slider('Select date', min_value=df_shipList['dateCheck'].min().date(), value=pageDict['startTime'] ,max_value=df_shipList['dateCheck'].max().date(), format="Y-M-D")

        st.write("Check shipments from:",pd.Timestamp(pageDict['startTime'], tz="UTC") )


        # format datetime
        df_shipList['dateCheck']=df_shipList['dateCheck'].apply(lambda x: pd.Timestamp(x, tz="UTC") if "+" not in str(x) else x)
        # filter using selection
        df_shipList = df_shipList[~(df_shipList['dateCheck'] <  pd.Timestamp(pageDict['startTime'], tz="UTC") ) ].reset_index(drop=True)

        st.write(f"- shipments in date: {len(df_shipList)}")
        ### filters
        if st.checkbox("Filter shipments?"):
            filtDict={'state':None,'status':None,'type':None}
            ## select filters
            for col in filtDict.keys():
                if st.checkbox(f" - Filter by {col}?"):
                    sel_col=st.selectbox(f"Select {col}:", df_shipList[col].unique())
                    filtDict[col]=sel_col
            ## run filters
            # df_shipList=pd.DataFrame(pageDict['shipList'])
            df_shipList.groupby(by=['status'])[['status']].count()
            for col in filtDict.keys():
                if filtDict[col]==None:
                    continue
                df_shipList=df_shipList.query(f'{col}=="{filtDict[col]}"')

        else:
            df_shipList.groupby(by=['status'])[['status']].count()

        st.write("---")
        st.write("## Plotting")

        df_shipList=df_shipList.reset_index()
        st.write(df_shipList)
        st.write(f"__Items to plot: {len(df_shipList)}__")

        st.write("### Chord Diagram")
        if "df_inst" not in pageDict.keys() or st.button("Re-plot") or len(pageDict['shipList'])!=len(df_shipList):

            st.write(f"Plotting dataset: {len(df_shipList)}")
            for x in df_shipList.columns:
                df_shipList[x]=df_shipList[x].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
        
            df_plot=df_shipList.groupby(by=['sender_code','recipient_code']).count().reset_index()
            df_inst=pd.DataFrame(set( list(df_plot['sender_code'].unique()) + list(df_plot['recipient_code'].unique()) ))
            df_inst=df_inst.rename(columns={0:"inst"})
            pageDict['df_inst']=df_inst

            df_links=df_plot[['sender_code','recipient_code','id','status']].rename(columns={'sender_code':"source",'recipient_code':"target",'id':"value"})
            df_links['source']=df_links['source'].apply(lambda x: list(df_inst['inst'].unique()).index(x) )
            df_links['target']=df_links['target'].apply(lambda x: list(df_inst['inst'].unique()).index(x) )
            pageDict['df_links']=df_links

        # Plotting with hv
        hv.extension('bokeh')
        # How large should be the diagram?
        hv.output(size=300)

        # Data set
        nodes = hv.Dataset(pageDict['df_inst'], 'index')
        links = pageDict['df_links']

        # Chord diagram
        chord = hv.Chord((links, nodes)).select(value=(5, None))
        chord.opts(
            opts.Chord(cmap='Category20', edge_cmap='Category20', edge_color=dim('source').str(), 
                    labels='inst', node_color=dim('index').str()))

        # st.bokeh_chart(chord)
        # st.bokeh_chart(chord)

        hv.save(chord ,'chord.html')
        HtmlFile = open("chord.html", 'r', encoding='utf-8')
        source_code = HtmlFile.read()
        components.html(source_code, width=1800, height=1200, scrolling=True)
