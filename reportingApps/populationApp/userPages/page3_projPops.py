import streamlit as st
from core.Page import Page
import pandas as pd
import altair as alt
import numpy as np

import itkdb
import core.stInfrastructure as infra
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

### highlight high and low values
def Highlighter(s, col="count", lo=10, hi=1000):
    if s[col]<lo:
        return ['background-color: orange'] * len(s)
    elif s[col]>hi:
        return ['background-color: red'] * len(s)
    else:
        return ['background-color: white'] * len(s)

infoList=["  * Get componentTypes for selected project",
        "   * filter based on populations"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("Project Pops", "Populations of componentTypes and testTypes per project", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        st.write("## Select Project")

        PROJECT_CODE= st.selectbox("Select project code:",["S","P","CE","CM"])

        ### retrieving componentType information...
        compTypeList=st.session_state.myClient.get('listComponentTypes', json={'project':PROJECT_CODE}) 
        ### report

        df_compTypeList=pd.DataFrame(compTypeList)
        for col in df_compTypeList.columns:
            df_compTypeList[col]=df_compTypeList[col].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
            df_compTypeList[col]=df_compTypeList[col].apply(lambda x: len(x) if type(x)==type([]) else x)

        st.write("### Quick componentType summary")
        sort_sel=st.selectbox('Sort values by:',df_compTypeList.columns)
        df_ctl=df_compTypeList
        if st.checkbox("Remove non-active?"):
            df_ctl=df_ctl.query('state=="active"').sort_values(by=[sort_sel])
        else:
            df_ctl.sort_values(by=[sort_sel])
        st.write(df_ctl)
        st.write(f"__Number of {PROJECT_CODE} componentTypes: {len(df_ctl)}__")
        if st.checkbox("See stats?"):
            st.write(df_compTypeList.describe())

        ### componentType info.
        if "compTypeDict" not in pageDict.keys() or pageDict['projCode']!=PROJECT_CODE or st.button("Get componentType populations"):

            pageDict['projCode']=PROJECT_CODE
            ### Check component count per componentType
            st.write("Check component count per componentType")
            pageDict['compTypeDict']= {x['code']:0 for x in compTypeList.data if type(x)==type({}) and "code" in x}

            ### retrieving information...
            # Use listComponents; rather than getComponentCount
            ct_bar = st.progress(0)
            ctText=st.empty()

            count=1
            st.write("retrieve info.")
            for k in pageDict['compTypeDict'].keys():
                ct_bar.progress(count/compTypeList.total)
                ctText.markdown(f"Working on: {k}")
                pageDict['compTypeDict'][k]= st.session_state.myClient.get('listComponents', json={'project':PROJECT_CODE, 'componentType':k, 'pageInfo': {'pageSize': 1}}).total
                count+=1
            ### report
            # for k,v in compTypeDict.items():
            #     print(f" - Number of {k}s: {v}")

        st.write("---")
        ### build dataframe
        # st.write(f"Build table for {PROJECT_CODE} componentTypes within ({POPULATION_LIMIT_LOW}:{POPULATION_LIMIT_HIGH})")
        df_compTypes=pd.DataFrame([{'code':k,'count':v} for k,v in pageDict['compTypeDict'].items()])
        df_compTypes=df_compTypes.sort_values(by=['code']).reset_index(drop=True)
        st.write("## ComponentType Populations")
        
        if "lowLimit_ct" not in pageDict.keys():
            pageDict['lowLimit_ct']= 10
        if "highLimit_ct" not in pageDict.keys():
            pageDict['highLimit_ct']= 1000

        if st.checkbox('Set componentType limits?'):
            for k in ["lowLimit_ct","highLimit_ct"]:
                try:
                    pageDict[k]= int(st.text_input(f'set {k}'))
                except TypeError:
                    st.write("Please input integer value")
                except ValueError:
                    st.write("Please input integer value")

        st.write("total componentTypes inside limits:", len(df_compTypes.query(f"count>{pageDict['lowLimit_ct']} & count<{pageDict['highLimit_ct']}") ) )

        st.write(f"population: :orange[<{pageDict['lowLimit_ct']}], :red[>{pageDict['highLimit_ct']}]")
        st.write(df_compTypes.style.apply(Highlighter, col='count', lo=10, hi=1000, axis=1))

        ### testType info.
        st. write("---")
        st.write("## TestType Populations")

        ### select componentType
        sel_ct=st.selectbox("Select componentType:",df_compTypes['code'].to_list())

        compTypeObj=st.session_state.myClient.get('getComponentTypeByCode', json={'project':PROJECT_CODE, 'code':sel_ct})                        

        try:
            df_stages=pd.DataFrame(compTypeObj['stages'])
        except KeyError:
            st.write("No _stages_ info. found. Please select another")
            st.stop()

        if df_stages.empty:
            st.write("No _stages_ info. found. Please select another")
            st.stop()

        df_stages=df_stages.explode('testTypes').reset_index(drop=True)
        df_stages['testTypes']=df_stages['testTypes'].apply(lambda x: x['testType']['code'] if type(x)==type({}) and "testType" in x.keys() else x)
        
        if df_stages['testTypes'].empty:
            st.write("No _testType_ info. found. Please select another")
            st.stop()

        try:
            if np.isnan(df_stages['testTypes'].to_list()):
                st.write("No _testType_ info. found. Please select another")
                st.stop()
        except TypeError:
            pass

        df_stages['testType@stage']=df_stages['testTypes']+"@"+df_stages['code']

        ### testType info.
        if "testTypeDict" not in pageDict.keys() or pageDict['sel_ct']!=sel_ct or st.button("Get testType populations"):
            pageDict['sel_ct']=sel_ct

            ### Check testRun count per testType
            st.write("Check testRun count per testType")
            pageDict['testTypeDict']= {x:0 for x in df_stages['testType@stage'].unique()}

            ### retrieving information...
            # Use listTestRunsByTestType
            tt_bar = st.progress(0)
            ttText=st.empty()

            count=1
            st.write("retrieve info.")
            for k in pageDict['testTypeDict'].keys():
                tt_bar.progress(count/len(df_stages))
                ttText.markdown(f"Working on: {k}")
                if "UNKNOWN" in str(k) or "nan" in str(k):
                    st.write(f" - skipping UNKNOWN/nan: {k}")
                    count+=1
                    continue
                if type(k)==type("str") and "@" in k:
                    retObj=st.session_state.myClient.get('listTestRunsByTestType', json={'project':PROJECT_CODE, 'componentType':sel_ct, 'testType':k.split('@')[0],'stage':k.split('@')[1], 'pageInfo': {'pageSize': 1}})
                else:
                    st.write(f"skipping: {k}")
                    count+=1
                    continue
                    # retObj=st.session_state.myClient.get('listTestRunsByTestType', json={'project':PROJECT_CODE, 'componentType':str(k),'testType':str(k), 'pageInfo': {'pageSize': 1}})

                try:
                    pageDict['testTypeDict'][k]= retObj.total
                except AttributeError:
                    pageDict['testTypeDict'][k]=len(retObj)
                count+=1
            ### report
            # for k,v in compTypeDict.items():
            #     print(f" - Number of {k}s: {v}")
        
        df_testTypes=pd.DataFrame([{'code':k,'count':v} for k,v in pageDict['testTypeDict'].items()])

        if "lowLimit_tt" not in pageDict.keys():
            pageDict['lowLimit_tt']= 10
        if "highLimit_tt" not in pageDict.keys():
            pageDict['highLimit_tt']= 1000

        if st.checkbox('Set testType limits?'):
            for k in ["lowLimit_tt","highLimit_tt"]:
                try:
                    pageDict[k]= int(st.text_input(f'set {k}'))
                except TypeError:
                    st.write("Please input integer value")
                except ValueError:
                    st.write("Please input integer value")

        st.write("total testType inside limits:", len(df_testTypes.query(f"count>{pageDict['lowLimit_tt']} & count<{pageDict['highLimit_tt']}") ) )

        st.write(f"population: :orange[<{pageDict['lowLimit_tt']}], :red[>{pageDict['highLimit_tt']}]")
        st.write(df_testTypes.style.apply(Highlighter, col='count', lo=10, hi=1000, axis=1))

