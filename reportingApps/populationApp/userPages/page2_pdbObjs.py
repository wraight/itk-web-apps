import streamlit as st
from core.Page import Page
import pandas as pd
import altair as alt

import itkdb
import core.stInfrastructure as infra
import itkdb.exceptions as itkX

#####################
### useful functions
#####################
projMap={'P':"Pixels", 'S':"Strips", 'CE':"Common Electronics", 'CM':"Common Mechanics"}

def GetStatsFromCsv(projCode):

    ### read data from file
    csv_url=f"https://wraight.web.cern.ch/itk-pdb-structures/stageTestList_{projCode}.csv"
    df_data= pd.read_csv(csv_url)

    return df_data

infoList=["  * test types and stages per project component type",
        "   * using CSVs on EOS"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("PDB Objects", "Quick PDB stats per componentType", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        st.write("## Select Project")
        
        proj=st.selectbox("Select project:",projMap.values())
        projKey=list(projMap.keys())[list(projMap.values()).index(proj)]
        
        st.write("---")

        df_data=GetStatsFromCsv(projKey)

        st.write(f"### Tests and stages for {proj} componentTypes")
        st.write(df_data)

        st.write("### Plots")
        try:
            df_data['altStage']=df_data['altStage'].apply(lambda x: True if x==True or x=="True" else False)
        except KeyError:
            ### skip if no alternative stages (CM)
            pass

        ### make sure stage defined (not so for CE)
        if "stage" in list(df_data.columns):
            ### stages plot
            # group tests
            df_stages=df_data.groupby(by=['compType','stage']).first().reset_index()
            # get total stages
            df_stages['stages_in_total']=df_stages['compType'].apply(lambda x: len(df_stages[ (df_stages['compType']==x) ].index) )

            df_tests=df_data.dropna().reset_index(drop=True)
            # get total tests
            df_tests['tests_in_total']=df_tests['compType'].apply(lambda x: len(df_tests[ (df_tests['compType']==x) ].index))
            df_tests['tests_in_stage']=df_tests.apply(lambda x: len(df_tests[ (df_tests['compType']==x['compType']) & (df_tests['stage']==x['stage']) ].index), axis=1)

            st.write("__Quick Stats__")
            st.write(f"ComponentTypes \n  - total number: {len(df_data['compType'].unique())}")
            # get first instance of each CT for stats
            # stages
            df_stages_stats=df_stages.groupby(by=['compType']).first().reset_index()
            st.write(f"Stages \n  - total number: {df_stages_stats['stages_in_total'].sum()}, average: {df_stages_stats['stages_in_total'].mean()}")
            idxmax=df_stages_stats['stages_in_total'].idxmax()
            st.write(f" - max: {df_stages_stats.iloc[idxmax]['stages_in_total']} ({df_stages_stats.iloc[idxmax]['compType']})")
            # tests
            df_tests_stats=df_tests.groupby(by=['compType']).first().reset_index()
            st.write(f"TestTypes \n  - total number: {df_tests_stats['tests_in_total'].sum()}, average: {df_tests_stats['tests_in_total'].mean()}")
            idxmax=df_tests_stats['tests_in_total'].idxmax()
            st.write(f" - max: {df_tests_stats.iloc[idxmax]['tests_in_total']} ({df_tests_stats.iloc[idxmax]['compType']})")

            stage_chart=alt.Chart(df_stages).mark_bar().encode(
                y=alt.Y('compType:N'),
                x=alt.X('count(stage):Q'),
                color=alt.Color('stage:N', legend=None),
                opacity=alt.condition(alt.datum.altStage == True, alt.value(0.3), alt.value(1)),
                tooltip=['compType:N','stage:N','altStage:N','stages_in_total:Q']
            ).properties( title=f"{proj} stages per componentType" )
            
            ### tests plot
            # drop no tests

            test_chart=alt.Chart(df_tests).mark_bar().encode(
                y=alt.Y('compType:N'),
                x=alt.X('count(testType):Q'),
                color=alt.Color('stage:N', legend=None),
                detail=alt.Detail('testType:N'),
                tooltip=['compType:N','testType:N','stage:N','altStage:N','tests_in_stage:Q','tests_in_total:Q']
            ).properties( title={'text': f"{proj} tests per componentType", 'subtitle':"components & stages without tests removed"} )

            ### display
            st.write(stage_chart | test_chart)
        
        ### if no stage column defined
        else:
            st.write("__no stages defined__")
            ### tests plot
            # drop no tests
            df_tests=df_data.dropna().reset_index(drop=True)
            # get total tests
            df_tests['tests_in_total']=df_tests['compType'].apply(lambda x: len(df_tests[ (df_tests['compType']==x) ].index))

            st.write("__Quick Stats__")
            st.write(f"ComponentTypes \n  - total number: {len(df_tests['compType'].unique())}")
            # tests
            # get first instance of each CT for stats
            df_tests_stats=df_tests.groupby(by=['compType']).first().reset_index()
            st.write(f"TestTypes \n  - total number: {df_tests_stats['tests_in_total'].sum()}, average: {df_tests_stats['tests_in_total'].mean()}")
            idxmax=df_tests_stats['tests_in_total'].idxmax()
            st.write(f" - max: {df_tests_stats.iloc[idxmax]['tests_in_total']} ({df_tests_stats.iloc[idxmax]['compType']})")

            test_chart=alt.Chart(df_tests).mark_bar().encode(
                y=alt.Y('compType:N'),
                x=alt.X('count(testType):Q'),
                color=alt.Detail('Color:N', legend=None),
                tooltip=['compType:N','testType:N','tests_in_total:Q']
            ).properties( title={'text': f"{proj} tests per componentType", 'subtitle':"components without tests removed"} )

            ### display
            st.write(test_chart)