import streamlit as st
from core.Page import Page
import pandas as pd
#import numpy as np
import pycountry
import datetime

import plotly.express as px
import plotly.graph_objects as go
import plotly.io as pio

import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

from .page1_Summary_tables import db_table_access

#####################
### useful functions
#####################
def DateFormat(dt): #from Kenny
    return str("{0:02}_{1:02}_{2:04}".format(dt.day,dt.month,dt.year))  

#####################
### main part
#####################

infoList=["  * Several different summary tables",
        "   * can also choose state of component assembly"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("Detailed Tables", "ITK component overview", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")
            if st.session_state.debug:
                st.write("Attribute Error")
                st.write(AttributeError)

        ### gatekeeping
        if not doWork:
            st.stop() 
    
        st.write('The tables below give an overview of components at institute and country level with options to '
                        + 'select a smaller number of countries or a single institute.')
        st.write('Each segment within the pie charts can be clicked on to get a more detailed look at that particular section.')
        st.write('Each table can be expanded to full screen using a button on the top right of the table.')
        st.write('For documentation and usage instructions please visit: [https://itk.docs.cern.ch/pixels/reporting/reporting_tutorial/](https://itk.docs.cern.ch/pixels/reporting/reporting_tutorial/)')
        st.write('Please note that tutorial and dummy components have now been removed from the tables and plots.')
        st.write("---") 

        nowTime = datetime.datetime.now()
        time_current = DateFormat(nowTime) 
        

        ##########################################################################
        # my code
        ##########################################################################
        project_user= st.session_state.Authenticate['proj']['code']   

        project_type = {'project': project_user} 
        if "compList" not in pageDict.keys():
            pageDict['compList']=st.session_state.myClient.get('listComponentTypes', json={'project':project_user}).data

        if project_user=='P':
                
            # comp_name= st.selectbox('Please select a component', ["Selection", "Module", "Module carrier", "Bare Module", "Module PCB", "OB Wirebonding Protection Roof", "Sensor Wafer", "Sensor Tile", "FE chip"])
            comp_name= st.selectbox('Please select a component', [x['name'] for x in pageDict['compList']])

            if comp_name == "Selection":
               st.stop()

            st.write(f"Get componentType information for {comp_name}")

            compCode=next((item['code'] for item in pageDict['compList'] if type(item)==type({}) and "name" in item.keys() and "code" in item.keys() and item['name']==comp_name), None)
            st.write(f"- using code: {compCode}")
            if compCode==None:
                st.write("__Issue with mapping from name to code__")
                st.stop()

            compObj= st.session_state.myClient.get('getComponentTypeByCode', json={'project':project_user, 'code':compCode})
            if st.checkbox("see full componentType info?"):
                st.write(compObj)

            if compCode=='MODULE':
                comp_type_kw = ['module'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'carrier', 'bare', 'esq'] #lower case 

                propItem=next((item for item in compObj['properties'] if type(item)==type({}) and "code" in item.keys() and item['code']=="FECHIP_VERSION"), None)
                if type(propItem)==type({}) and "codeTable" in propItem.keys():
                    chip_type={item['value']:item['code'] for item in propItem['codeTable']}
                    # print(codeTable)
                else:
                    st.write("no codeTable found :(")
                    chip_type={'RD53A' : '0', 'ITkpix_v1' : '1', 'ITkpix_v1.1' : '2', 'ITkpix_v2' : '3', 'No chip' : '9' }

            elif compCode=='MODULE_CARRIER':
                comp_type_kw = ['carrier','hybrid'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'board'] #lower case 

            elif compCode=='BARE_MODULE':
                comp_type_kw = ['bare'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'ob', 'oec', 'is', 'esq'] #lower case 

                propItem=next((item for item in compObj['properties'] if type(item)==type({}) and "code" in item.keys() and item['code']=="FECHIP_VERSION"), None)
                if type(propItem)==type({}) and "codeTable" in propItem.keys():
                    chip_type={item['value']:item['code'] for item in propItem['codeTable']}
                    # print(codeTable)
                else:
                    st.write("no codeTable found :(")
                    chip_type={ 'RD53A' : '0', 'ITkpix_v1' : '1', 'ITkpix_v1.1' : '2', 'ITkpix_v2' : '3', 'No chip' : '9'}

            elif compCode=='PCB':
                comp_type_kw = ['pcb'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'esq'] #lower case 

                propItem=next((item for item in compObj['properties'] if type(item)==type({}) and "code" in item.keys() and item['code']=="FECHIP_VERSION"), None)
                if type(propItem)==type({}) and "codeTable" in propItem.keys():
                    chip_type={item['value']:item['code'] for item in propItem['codeTable']}
                    # print(codeTable)
                else:
                    chip_type={ 'RD53A' : '0',  'Prototype (ITkpix_v1)' : '1', 'Pre-production OS (ITkpix_v1)' : '2', 'Pre-production IS (ITkpix_v1)' : '3',
                                'Production OS (ITkpix_v2)' : '4', 'Production IS (ITkpix_v2)' : '5', 'No chip' : '9'}
                    st.write("no codeTable found :(")
            
            elif compCode=='OB_WB_PROTECTION_ROOF':
                comp_type_kw = ['wb'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'esq'] #lower case 

            elif compCode=='SENSOR_TILE':
                comp_type_kw = ['sensor'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'tile', 'esq'] #lower case 

                propItem=next((item for item in compObj['properties'] if type(item)==type({}) and "code" in item.keys() and item['code']=="VERSION"), None)
                if type(propItem)==type({}) and "codeTable" in propItem.keys():
                    production_protype={item['value']:item['code'] for item in propItem['codeTable']}
                    # print(codeTable)
                else:
                    production_protype={ 'Prototype (Market survey)': '0', 'Pre-production' : '1', 'Production' : '2' }
                    st.write("no codeTable found :(")

                propItem=next((item for item in compObj['properties'] if type(item)==type({}) and "code" in item.keys() and item['code']=="VENDOR"), None)
                if type(propItem)==type({}) and "codeTable" in propItem.keys():
                    vendor={item['value']:item['code'] for item in propItem['codeTable']}
                    # print(codeTable)
                else:
                    vendor={ 'V1 (ADVACAM)' : '0', 'V2 (HLL)' : '1', 'V3 (FBK, planar)' : '2', 'V4 (HPK)' : '3',
                         'V5 (LFoundry)' : '4', 'V6 (MICRON)' : '5', 'V7 (CNM)' : '6', 'V8 (FBK, 3D)' : '7',
                         'V9 (SINTEF)' : '8', 'Dummy' : '9' }
                    st.write("no codeTable found :(")

            elif compCode=='SENSOR_TILE':
                comp_type_kw = ['sensor'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'esq'] #lower case 

                propItem=next((item for item in compObj['properties'] if type(item)==type({}) and "code" in item.keys() and item['code']=="VERSION"), None)
                if type(propItem)==type({}) and "codeTable" in propItem.keys():
                    production_protype={item['value']:item['code'] for item in propItem['codeTable']}
                    # print(codeTable)
                else:
                    production_protype={ 'Prototype (Market survey)': '0', 'Pre-production' : '1', 'Production' : '2'} 
                    st.write("no codeTable found :(")
                
                propItem=next((item for item in compObj['properties'] if type(item)==type({}) and "code" in item.keys() and item['code']=="VENDOR"), None)
                if type(propItem)==type({}) and "codeTable" in propItem.keys():
                    vendor={item['value']:item['code'] for item in propItem['codeTable']}
                    # print(codeTable)
                else:
                    vendor={ 'V1 (ADVACAM)' : '0', 'V2 (HLL)' : '1', 'V3 (FBK, planar)' : '2', 'V4 (HPK)' : '3',
                         'V5 (LFoundry)' : '4', 'V6 (MICRON)' : '5', 'V7 (CNM)' : '6', 'V8 (FBK, 3D)' : '7',
                         'V9 (SINTEF)' : '8', 'Dummy' : '9' }
                    st.write("no codeTable found :(")
                
                propItem=next((item for item in compObj['properties'] if type(item)==type({}) and "code" in item.keys() and item['code']=="SENSOR_TYPE_OR_TEST_STRUCTURE"), None)
                if type(propItem)==type({}) and "codeTable" in propItem.keys():
                    structure={item['value']:item['code'] for item in propItem['codeTable']}
                    # print(codeTable)
                else:
                    structure={ 'RD53A test structure' : '0', 'Single' : '1', 'Double' : '2', 'Quad' : '3', 'Planar diode test structure' : '4',
                            'Strip test structure' : '5', 'Mini-sensor test structure' : '6', 'Inter pixel capacitance test structure' : '7',
                            'Biasing test structure (poli-Si resistivity or punch through)' : '8', '3D diode test structure' : '9'} 
                    st.write("no codeTable found :(")

            elif compCode=='FE_CHIP':
                comp_type_kw = ['fe'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'esq'] #lower case  

            else:
                st.write(f"Component not recognised ({compCode}). Try again")
                st.stop()

            project_name='pixels' 

        elif project_user=='S':  
            st.warning('This is a work in progress')
            st.stop() 
            comp_type_kw = st.multiselect('please select', ['module','hybrid_', 'sensor', 'pwb', 'hcc']) #, 'abc' #lower case ##the abc chips are about 90% of all components
            comp_veto_kw= ['sensor_', 'pwb_', 'abc_', 'hcc_', 'fail', 'test', 'flex_', 'jj', 'frame'] #lower c #the abc chips are about 95% of all components
            project_name='strips'   
            check= st.checkbox('I have selected my components')
            if not check:
                st.stop()
        else:
            st.write('Please choose either strips or pixels as your project')
            st.stop()
    

        with st.spinner('Please wait while component information is loaded from the ITK production database'):
            comp_df_func, stage_dict = db_table_access(project_type, comp_type_kw, comp_veto_kw, time_current) 
            if comp_df_func.empty:
                st.warning('No components to display')
                st.stop() 
        comp_df_initial=comp_df_func.copy() #stops streamlit mutation warning
        stage_dict_initial= stage_dict.copy()
        if project_user=='S':
            abc_comp= st.checkbox('Rerun to include ABC chips (Warning: this may take approximately 15 minutes to load)')
            if abc_comp:
                with st.spinner('Please wait while component information is loaded from the ITK production database'):
                    comp_df_func_abc, stage_dict_abc = db_table_access(project_type, ['abc'], comp_veto_kw, time_current)  
                    if comp_df_func_abc.empty:
                        st.warning('No components to display')
                        st.stop() 
                comp_df_abc=comp_df_func_abc.copy() #stops streamlit mutation warning
                comp_df_second= pd.concat([comp_df_initial, comp_df_abc])
                comp_df=comp_df_second
                stage_dict_initial.update(stage_dict_abc) 
                stage_dict= stage_dict_initial.copy()
            else:
                comp_df=comp_df_initial
                stage_dict= stage_dict_initial
        else:
            comp_df=comp_df_initial 
            stage_dict= stage_dict_initial  
        comp_df = comp_df[comp_df["state"] != "deleted"] 

        if project_user=='P':
            comp_df = comp_df.loc[~comp_df['serialNumber'].str[5].isin(['X', 'Y', 'Z'])] #remove dummy and tutorial components  
            if comp_df.empty:
                st.warning('No components to display')
                st.stop() 
            comp_df = comp_df.loc[~comp_df['serialNumber'].str[5:6].isin(['BR', 'BT'])] #remove dummy and tutorial components
            if comp_df.empty:
                st.warning('No components to display')
                st.stop() 
        comp_df = comp_df.loc[comp_df['type_name'].str.contains('|'.join(['tutorial', 'dummy']), case=False)==False] #remove dummy and tutorial components
        if comp_df.empty:
            st.warning('No components to display')
            st.stop() 
        

        # st.write("## debug",comp_df)
        

        if comp_name in ['Module', 'Bare Module', 'Module PCB']:
            chip_names= list(chip_type.keys())
            chip_name_selection = st.multiselect('Please select chip version', chip_names, chip_names)
            chip_sn_numbers = [chip_type[x] for x in chip_name_selection]

            comp_df = comp_df.loc[comp_df['serialNumber'].str[7].isin(chip_sn_numbers)] #select which elements match array
            if comp_df.empty:
                st.warning('No components to display')
                st.stop() 
            
        elif comp_name in ['Sensor Wafers', 'Sensor Tiles']:
            prototype_names= list(production_protype.keys())
            prototype_name_selection = st.multiselect('Please select whether to display prototype or production components', prototype_names, prototype_names)
            prototype_ad_numbers = [production_protype[x] for x in prototype_name_selection]

            comp_df = comp_df.loc[comp_df['alternativeIdentifier'].str[3].isin(prototype_ad_numbers)] #select which elements match arrayif comp_df.empty:
            if comp_df.empty:
                st.warning('No components to display')
                st.stop() 

            vendor_names= list(vendor.keys())
            vendor_name_selection = st.multiselect('Please select vendor name', vendor_names, vendor_names)
            vendor_id_numbers = [vendor[x] for x in vendor_name_selection]

            comp_df = comp_df.loc[comp_df['serialNumber'].str[7].isin(vendor_id_numbers)] #select which elements match array
            if comp_df.empty:
                st.warning('No components to display')
                st.stop() 
            
            if comp_name == 'Sensor Tiles':
                structure_names= list(structure.keys())
                structure_name_selection = st.multiselect('Please select sensor type or test structure', structure_names, structure_names)
                structure_id_numbers = [structure[x] for x in structure_name_selection]

                comp_df = comp_df.loc[comp_df['serialNumber'].str[8].isin(structure_id_numbers)] #select which elements match array
                if comp_df.empty:
                    st.warning('No components to display')
                    st.stop() 




        with st.expander("Component selection options"):

            assemble= st.selectbox('Would you like to see all components, assembled components or components not yet assembled?',
                                    ['All Components', 'Assembled Only', 'Not Assembled'])
            
            if assemble== 'Assembled Only':
                comp_df_assemble= comp_df.loc[comp_df['assembled'] == True]  
            elif assemble== 'Not Assembled':
                comp_df_assemble= comp_df.loc[comp_df['assembled'] == False]  
            else:
                comp_df_assemble= comp_df

            trashed= st.selectbox('Would you like to see all components, only good components or only trashed components?',
                                    ['All Components', 'Good Components', 'Trashed Components Only'])
            
            if trashed== 'Trashed Components Only':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['trashed'] == True]  
            elif trashed== 'Good Components':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['trashed'] == False]  
            else:
                comp_df_assemble= comp_df_assemble

            rework= st.selectbox('Would you like to see all components, only good components or only reworked components?',
                                    ['All Components', 'Good Components Only', 'Reworked Components Only'])
            
            if rework== 'Reworked Components Only':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['reworked'] == True]  
            elif rework== 'Good Components Only':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['reworked'] == False]  
            else:
                comp_df_assemble= comp_df_assemble

        if comp_df_assemble.empty:
            st.warning('No components meet this criteria, please change your selection')
            st.stop() 

        comp_df_assemble["currentStage_name"].replace(to_replace=[None], value='Undefined', inplace=True) #replace empty stages
        comp_df_assemble["currentStage_name"].fillna("Undefined", inplace = True)  #replace empty stages  
 

        comp_df_pivot_inst=pd.pivot_table(comp_df_assemble, index=['type_name', 'currentStage_name'], #,'type_name'
                                        columns=['country', 'currentLocation_code'], values=['code'], 
                                        aggfunc=['count'], margins=True, margins_name = "Total", fill_value=0)#.fillna(0)
        #st.dataframe(comp_df_pivot_inst)
    
        result = pd.concat([comp_df_pivot_inst, comp_df_pivot_inst.groupby(axis=0, level=[0]).sum().assign(name='Subtotal').
                            set_index('name',append=True)]).sort_index(level=[0], sort_remaining=False)
        
        result = result.droplevel(axis=1, level=0)    
        # Drop subtotal from total, which is last row
        ##result.drop(result.tail(1).index,inplace=True)  

        #Find all countries that have one institute
        full_columns= list(result.columns) 
        columns_no_repeat=[]
        for item in full_columns:
            columns_no_repeat.append(item[1]) 
        columns_no_repeat= [x for x in columns_no_repeat if columns_no_repeat.count(x)==1]

        # Add subtotals for columns ...    
        # Create data frame for subtotals
        subcol = result.groupby(axis = 1, level=[1]).sum() #sum(axis = 1, level = [1])  
        # Form indices 
        indices = list(zip(subcol.columns, ["Subtotal"]*len(subcol.columns)))
        ####indices[-1] = ("Total", "") #what does this do? #this seems to mess things up as the total isn't always at the end
        subcol.columns = pd.MultiIndex.from_tuples(indices, names = ["country", "currentLocation_code"])
        # Drop total 
        subcol = subcol.drop(columns_no_repeat, axis=1)    
        result = result.droplevel(axis=1, level=0) #remove 'count' level
        # Concat to main Table
        result = pd.concat([result, subcol], axis=1).sort_index(axis = 1, level = [0]).astype(int) # convert floats to ints   
 
        index_stage = result.index.to_list()
        subcoms = [x[0] for x in index_stage]
        subcoms =  list(dict.fromkeys(subcoms))
        subcoms = [x for x in subcoms if "Total" not in x]
         
        stage_index=[]
        for item in list(stage_dict.items()):
            for subitem in item[1]:
                for subcomb in subcoms:
                    tuple_type= (subcomb, subitem)
                    stage_index.append(tuple_type)
         
        missing_stages = st.checkbox('Include stages that currently have no components?')
        no_stage = pd.DataFrame()
        if missing_stages:
            for item in stage_index: 
                if result.index.isin([item]).any()==False:     
                    stage_no_comps = pd.DataFrame([[0]*result.shape[1]],columns=result.columns, index= pd.MultiIndex.from_tuples([item]))
                    no_stage = pd.concat([no_stage, stage_no_comps])
            result = pd.concat([result, no_stage])       
        
        def criteria_column(x):  
            if x[0] == "CERN":
                return ("AAAAAA", x[1]) 
            if x[0] == "Total":
                return ("zzzzzz", x[1])  
            if x[1]== "Subtotal":        
                return(x[0], 'zzzzz')#"ZZZZZZZZ")
            if x[1]== "Total":        
                return(x[0], 'zzzzzz')#"ZZZZZZZZ")
            if x[0] in stage_dict:  
                alphabet= [chr(k) for k in range(len(stage_dict[x[0]]))] #chr gives the ASCII character for the given number  
                for i, item in enumerate(stage_dict[x[0]]):
                    if x[1]==item:  
                        return(x[0], alphabet[i])   
                return(x[0], x[1])
            else:   
                return x
 
        def criteria_country_column(x):
            country_map = {}
            missing = [] 
            i=st.session_state.Authenticate['inst']
            # Specific cases that don't map easilly
            if i["code"] == "LPNHE":
                country_map[i["code"]] = pycountry.countries.search_fuzzy("France")[0].alpha_2
            elif i["code"] == "BUW":
                country_map[i["code"]] = pycountry.countries.search_fuzzy("Germany")[0].alpha_2
            elif "country" in i["address"]:
                # Those that have a country
                if i["address"]["country"] in ['UK', ' UK']:##
                    country_map[i["code"]] = pycountry.countries.search_fuzzy('United Kingdom')[0].alpha_2##
                else:##
                    country_map[i["code"]] = pycountry.countries.search_fuzzy(i["address"]["country"])[0].alpha_2
            elif "state" in i["address"] and i["address"]["state"] != '':
                # Those that have a state, with some sanitising
                try:
                    state = i["address"]["state"].replace("CA, USA", "USA")
                    country_map[i["code"]] = pycountry.countries.search_fuzzy(state)[0].alpha_2
                except LookupError:
                    missing.append(i)
            else:
                # Try to guess from name, with some sanitising
                try:
                    name = i["name"].lstrip("INFN").lstrip("University of").lstrip("College").rstrip("University").rstrip("at Dallas")
                    country_map[i["code"]] = pycountry.countries.search_fuzzy(name)[0].alpha_2
                except LookupError:
                    missing.append(i)      
            country = country_map[i['code']]#.apply(lambda i : country_map.get(i, "N/A"))
 
            if x[0] == country:
                return ("AAA", x[1]) 
            if x[0] == "CERN":
                return ("AAAAAA", x[1]) 
            if x[0] == "Total":
                return ("zzzzzz", x[1])  
            if x[1]== "Subtotal":        
                return(x[0], 'zzzzz')#"ZZZZZZZZ")
            if x[1]== "Total":        
                return(x[0], 'zzzzzz')#"ZZZZZZZZ")
            if x[0] in stage_dict:  
                alphabet= [chr(k) for k in range(len(stage_dict[x[0]]))] #chr gives the ASCII character for the given number  
                for i, item in enumerate(stage_dict[x[0]]):
                    if x[1]==item: 
                        return(x[0], alphabet[i])   
                return(x[0], x[1])
            else:   
                return x

        def criteria_row(x):   
            if x[0] == "Total":
                return ("zzzzzz", x[1])  
            if x[1]== "Subtotal":        
                return(x[0], 'zzzzz')#"ZZZZZZZZ")
            if x[1]== "Total":        
                return(x[0], 'zzzzzz')#"ZZZZZZZZ")
            else:
                comp_key = list(stage_dict.keys())
                alphabet= [chr(k) for k in range(len(stage_dict[comp_key[0]]))] #chr gives the ASCII character for the given number  
                for i, item in enumerate(stage_dict[comp_key[0]]):
                    if x[1]==item:  
                        return(x[0], alphabet[i])    
                return(x[0], x[1]) 
 
        def criteria_country_row(x): 
            if x[0] == "Total":
                return ("zzzzzz", x[1])  
            if x[1]== "Subtotal":        
                return(x[0], 'zzzzz')#"ZZZZZZZZ")
            if x[1]== "Total":        
                return(x[0], 'zzzzzz')#"ZZZZZZZZ")
            else:
                comp_key = list(stage_dict.keys())
                alphabet= [chr(k) for k in range(len(stage_dict[comp_key[0]]))] #chr gives the ASCII character for the given number  
                for i, item in enumerate(stage_dict[comp_key[0]]):
                    if x[1]==item: 
                        return(x[0], alphabet[i])   
                return(x[0], x[1]) 

        st.write('## ITK ' + project_name + ' ' + comp_name + ' overview main table') 
        inst_move=st.checkbox('Move my home country to start of table')  
        if inst_move:
            # Sort column order
            columns_orig = result.columns.tolist() 
            columns_sort = sorted(columns_orig, key=criteria_country_column) 
            result = result[columns_sort]

            # Sort row order 
            index_sorted = sorted(result.index.to_list(), key=criteria_country_row)  
            result= result.reindex(index_sorted)  
        else:
            # Sort column order
            columns_orig = result.columns.tolist() 
            columns_sort = sorted(columns_orig, key=criteria_column) 
            result = result[columns_sort]

            # Sort row order  
            index_orig= result.index.to_list()
            index_sorted = sorted(index_orig, key=criteria_row)  
            result= result.reindex(index_sorted)   
        
        result.drop(result.tail(1).index,inplace=True)  #get rid of the total subtotal row

        table_countries= result.columns.levels[0].tolist()
        for item in ['total']:
            table_countries= list(filter(lambda x: (item not in x.lower()), table_countries)) #removes test components 

        table_institutes= result.columns.levels[1].tolist()
        table_institutes= filter(None, table_institutes)
        for item in ['subtotal']:
            table_institutes= list(filter(lambda x: (item not in x.lower()), table_institutes)) #removes test components 

        #st.write(result)
        def subtotal_highlight(val): #this doesn't work with the download option
            return [('background: lightblue' if 'Subtotal' in x else 'color: black') for x in result.index]

        # for item in result.columns.levels[0].tolist():
        #     result[item]=result[item].apply(lambda val: dict((y, x) for x, y in val) if type(val)==type(()) else val)

        # for item in result.columns.levels[1].tolist():
        #     result[item]=result[item].apply(lambda val: dict((y, x) for x, y in val) if type(val)==type(()) else val)


        result_highlight = result.style.apply(subtotal_highlight,axis=0)
        st.dataframe(result_highlight) 
        @st.cache
        def convert_df(df):
            # IMPORTANT: Cache the conversion to prevent computation on every rerun
            return df.to_csv().encode('utf-8') 
        csv = convert_df(result)
        result_name= comp_name.replace(" ", "_") + '_overview_table_' + time_current + '_' + project_name +'.csv'
        
        st.download_button(label="Download overview table", data=csv, file_name=result_name, mime='text/csv' ) 
        pio.renderers.default = 'iframe'  

        if st.button('Generate overview pie chart'):
            # Group original DF (before pivot) and reset indices to columns
            df_plot = comp_df_assemble.groupby(["country", "currentLocation_code", "type_name"]).agg(count=("id", "count")).reset_index()
            #df_plot 
            # Make sunburst of country:institute:type
            fig = px.sunburst(df_plot, path = ["country", "currentLocation_code" , "type_name"], color = "count", hover_data={"count":False}, 
                            values = "count", color_continuous_scale='Aggrnyl')
            # for jupyter not needed in streamlit probbaly 
            st.plotly_chart(fig)  
        
        st.write('## Multiple country overview')
        st.write('Select below to get a smaller overview table for one or more countries')
        list_countries= st.multiselect('Please select countries', table_countries)
        if st.button('Generate table for one or more countries'):
            if len(list_countries)==0:
                st.warning('Please input at least one country')
                st.stop() 
            result_small= result[list_countries]
            if missing_stages == False:
                result_small = result_small.loc[~(result_small==0).all(axis=1)] 
            st.dataframe(result_small) 
            
            csv_result_small = convert_df(result_small)
            list_countries_join ='_'.join(list_countries)
            result_small_name= list_countries_join + '_' + comp_name.replace(" ", "_") + '_overview_table_' + time_current + '_' + project_name +'.csv'
            st.download_button(label="Download overview table", data=csv_result_small, file_name=result_small_name, mime='text/csv' )

        
        if st.button('Generate pie chart for one or more countries'):
            if len(list_countries)==0:
                st.warning('Please input at least one country')
                st.stop() 
            df_gb = comp_df_assemble[comp_df_assemble["country"].isin(list_countries)].groupby(["currentLocation_code", "type_name","currentStage_name"]).agg(count=("id", "count")).reset_index()
            fig2 = px.sunburst(df_gb, path = ["currentLocation_code", "type_name", "currentStage_name"], color = "count", hover_data={"count":False}, maxdepth = 2,
                            values = "count", color_continuous_scale='Aggrnyl', title = "")
            # for jupyter not needed in streamlit probbaly 
            st.plotly_chart(fig2) 

        st.write('## Multiple institutes overview')
        st.write('Select below to get an overview table for multiple institutes')
        if st.session_state.Authenticate['inst']['code'] in table_institutes:
            inst_index= st.session_state.Authenticate['inst']['code'] 
        else:
            inst_index= 'CERN'
        list_inst= st.multiselect('Please select institutes', table_institutes, default=inst_index)
        if st.checkbox('Generate table for multiple institutes'):
            result_institutes= result.droplevel([0], axis=1)
            result_single=result_institutes.iloc[:,~result_institutes.columns.duplicated(keep=False)] 
            #if st.session_state.Authenticate['inst']['code'] not in table_institutes:
            #    st.warning('Your chosen institute does not contain the correct component type, please select another')   
            #else:
            single_inst = result_single[list_inst]   
            if missing_stages == False:
                single_inst = single_inst.loc[~(single_inst==0).all(axis=1)]
            st.dataframe(single_inst)
            csv_single_inst = convert_df(single_inst)
            list_inst_join ='_'.join(list_inst) 
            single_inst_name= list_inst_join + '_' + comp_name.replace(" ", "_") + '_overview_table_' + time_current + '_' + project_name +'.csv'
            st.download_button(label="Download overview table", data=csv_single_inst, file_name= single_inst_name, mime='text/csv' )
            
            single_inst=single_inst.reset_index()
            df_download=pd.DataFrame()
            for tn in single_inst['type_name'].unique():
                # st.write(tn)
                for csn in single_inst.query(f'type_name=="{tn}"')['currentStage_name'].unique():
                    # st.write(csn)
                    if df_download.empty:
                        df_download=comp_df.query(f'type_name=="{tn}" and currentStage_name=="{csn}"')
                    else:
                        df_download=pd.concat([df_download,comp_df.query(f'type_name=="{tn}" and currentStage_name=="{csn}"')]).reset_index(drop=True)
            st.write(df_download)
            st.download_button(label="Download serialNumbers table", data=df_download.to_csv(), file_name= single_inst_name.replace('overview_table','components'), mime='text/csv' )




