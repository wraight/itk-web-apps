import streamlit as st
from core.Page import Page
import pandas as pd
#import numpy as np
import pycountry
import datetime

import plotly.express as px
import plotly.graph_objects as go
import plotly.io as pio

import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################
def DateFormat(dt): #from Kenny
    return str("{0:02}_{1:02}_{2:04}".format(dt.day,dt.month,dt.year)) 

# @st.cache(suppress_st_warning=True)
def db_table_access(project, comp_search_kw, comp_delete_kw, time):
    #function to access the db and obtain the main dataframe. The dataframe will need to be aggregated 
    #time is included (just year month and day should be used) so that the cache can be used but table can still update
    #cache looks to see if arguments have changed and if they haven't doesn't rerun but as the data will change it 
    #still needs to update occasionally hence use the time to only cache once per day 
    list_comp_dict =  st.session_state.myClient.get('listComponentTypes', json=project)
    if type(list_comp_dict)!=type([]):
        list_comp_dict=list_comp_dict.data
    list_comp= [comp['code'] for comp in list_comp_dict if any(item in comp['code'].lower() for item in comp_search_kw)] #list_comp filters list_comp_dict by keywords
 
    for item in comp_delete_kw:
        list_comp= list(filter(lambda x: (item not in x.lower()), list_comp)) #removes test components  
    if st.session_state.debug:
        st.write("list components")

    list_comp_name= [comp['name'] for comp in list_comp_dict if any(item == comp['code']  for item in list_comp)] 
    stages=[]
    stage_dict={}
    for comp in list_comp_name:#list_comp:
        for item in list_comp_dict:
            if item['name']==comp:
                try:
                    for i in item['stages']:
                        stages.append(i['name'])
                        #print(u)
                    stages.append('Undefined')
                except TypeError:
                    stages.append('Undefined')
            stage_dict[comp]=stages
        stages=[]
    if st.session_state.debug:
        st.write("correct stages")
        
    institutes = st.session_state.myClient.get('listInstitutions', json={})
    if type(institutes)!=type([]):
        institutes=institutes.data
    st.write(institutes)
    if st.session_state.debug:
        st.write("list institutes")
        st.dataframe(institutes)
    institutes_ctype=[]
    for item in institutes:
        for comp_type in item['componentTypes']: 
            if type(comp_type)!=type({}) or "code" not in comp_type.keys():
                continue
            if comp_type['code'] == project.get('project') and any(a['code'] in list_comp for a in [ct for ct in comp_type['itemList'] if type(ct)==type({}) and "code" in ct.keys()]):
            #conditions: be a pixel institute and contain any of the components listed above
                institutes_ctype.append(item['code'])
    #st.write(institutes_ctype ) 
    if st.session_state.debug:
        st.write("got institutes and component types")
    #comp_all_filtered = list(st.session_state.myClient.get('listComponents', json={'filterMap': {'project': project.get('project'), 'componentType':list_comp, 'institute':institutes_ctype}}))
    comp_all_filtered = st.session_state.myClient.get('listComponents', json={'filterMap': {'project': project.get('project'), 'componentType':list_comp, 'institute':institutes_ctype}})
    if type(comp_all_filtered)!=type([]): 
        comp_all_filtered=comp_all_filtered.data
    if st.session_state.debug:
        st.write("listed components")
    comp_df= pd.json_normalize(comp_all_filtered, sep='_')  
    if st.session_state.debug:
        st.write("json normalise")
    if comp_df.empty:
        st.warning('No components to display')
        st.stop() 
    comp_df = comp_df[comp_df["state"] != "deleted"]
    if st.session_state.debug:
        st.write("deleted removed")

    #Carl's institute country code
    country_map = {}
    missing = []
    stat_text=st.empty()
    prog_bar = st.progress(0)
    n_inst=len(institutes)
    for e,i in enumerate(institutes,1):
        # Specific cases that don't map easilly
        stat_text.write(f"Reading data from: {i['code']}")
        prog_bar.progress( float(e)/(n_inst))
        if i["code"] == "LPNHE":
            country_map[i["code"]] = pycountry.countries.search_fuzzy("France")[0].alpha_2
        elif i["code"] == "BUW":
            country_map[i["code"]] = pycountry.countries.search_fuzzy("Germany")[0].alpha_2
        if not i['address']: #if the institute has no address 
            try:
                name = i["name"].lstrip("INFN").lstrip("University of").lstrip("College").rstrip("University").rstrip("at Dallas")
                country_map[i["code"]] = pycountry.countries.search_fuzzy(name)[0].alpha_2
                st.write('output')
            except LookupError:
                missing.append(i) 
        elif "country" in i["address"]:
            # Those that have a country
            try:
                if i["address"]["country"] in ['UK', ' UK']:
                    country_map[i["code"]] = pycountry.countries.search_fuzzy('United Kingdom')[0].alpha_2
                elif i["address"]["country"] in ['brasil']:
                    country_map[i["code"]] = pycountry.countries.search_fuzzy('brazil')[0].alpha_2
                else:
                    country_map[i["code"]] = pycountry.countries.search_fuzzy(i["address"]["country"])[0].alpha_2
            except:
                print(f'This guy is trouble: {i["address"]["country"]}')
        elif "state" in i["address"] and i["address"]["state"] != '':
            # Those that have a state, with some sanitising
            try:
                state = i["address"]["state"].replace("CA, USA", "USA")
                country_map[i["code"]] = pycountry.countries.search_fuzzy(state)[0].alpha_2
            except LookupError:
                missing.append(i)
        else:
            # Try to guess from name, with some sanitising 
            try:
                name = i["name"].lstrip("INFN").lstrip("University of").lstrip("College").rstrip("University").rstrip("at Dallas")
                country_map[i["code"]] = pycountry.countries.search_fuzzy(name)[0].alpha_2
            except LookupError:
                missing.append(i)  
    stat_text.write(f"Data read from instiutions")

    comp_df["country"] = comp_df["currentLocation_code"].apply(lambda i : country_map.get(i, "N/A"))
    if st.session_state.debug:
        st.write("add country information")
    comp_df.loc[comp_df['currentLocation_code']=='CERN', 'country']='CERN'
    if st.session_state.debug:
        st.write("add CERN")
    #comp_df = comp_df[comp_df["state"] != "deleted"]
    comp_df_complete=comp_df.copy()
    return(comp_df_complete, stage_dict)

#####################
### main part
#####################

infoList=["  * Several different summary tables",
        "   * can also choose state of component assembly"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Summary Tables", "ITK component overview", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")
            if st.session_state.debug:
                st.write("Attribute Error")
                st.write(AttributeError)

        ### gatekeeping
        if not doWork:
            st.stop() 
    
        st.write('The tables below give an overview of components at institute and country level with options to '
                        + 'select a smaller number of countries or a single institute.')
        st.write('Each segment within the pie charts can be clicked on to get a more detailed look at that particular section.')
        st.write('Each table can be expanded to full screen using a button on the top right of the table.')
        st.write('For documentation and usage instructions please visit: [https://itk.docs.cern.ch/pixels/reporting/reporting_tutorial/](https://itk.docs.cern.ch/pixels/reporting/reporting_tutorial/)')
        st.write('Please note that tutorial and dummy components have now been removed from the tables and plots.')
        st.write("---") 

        nowTime = datetime.datetime.now()
        time_current = DateFormat(nowTime) 
        

        ##########################################################################
        # my code
        ##########################################################################
        # project_list=['P', 'S']
        # st.select_slider('Please select your project', proj_list, key='proj')

        project_user= st.session_state.Authenticate['proj']['code']   

        project_type = {'project': project_user} 
        if st.session_state.Authenticate['proj']['code']=='P':
            comp_type_kw = ['module','hybrid', 'pcb', 'wb'] #lower case #old has no pcb but front
            comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'esq'] #lower case 
            project_name='pixels' 
        elif st.session_state.Authenticate['proj']['code']=='S':  
            comp_type_kw = st.multiselect('please select', ['module','hybrid_', 'sensor', 'pwb', 'hcc']) #, 'abc' #lower case ##the abc chips are about 90% of all components
            comp_veto_kw= ['sensor_', 'pwb_', 'abc_', 'hcc_', 'fail', 'test', 'flex_', 'jj', 'frame'] #lower c #the abc chips are about 95% of all components
            project_name='strips'   
            check= st.checkbox('I have selected my components')
            if not check:
                st.stop()
        else:
            st.write('Please choose either strips or pixels as your project')
            st.stop()

        if "stage_dict" not in pageDict.keys() or pageDict["project"]!=project_user or st.button("reset component list?"):
            with st.spinner('Please wait while component information is loaded from the ITK production database'):
                comp_df_func, stage_dict = db_table_access(project_type, comp_type_kw, comp_veto_kw, time_current) 
            pageDict["comp_df_func"]=comp_df_func
            pageDict["stage_dict"]=stage_dict
            pageDict['project']=project_user

        comp_df_func=pageDict["comp_df_func"]
        stage_dict=pageDict["stage_dict"]

        comp_df_initial=comp_df_func.copy() #stops streamlit mutation warning
        if comp_df_initial.empty:
            st.warning('No components to display')
            st.stop() 
        stage_dict_initial= stage_dict.copy()
        if project_user=='S':
            abc_comp= st.checkbox('Rerun to include ABC chips (Warning: this may take approximately 15 minutes to load)')
            if abc_comp:
                with st.spinner('Please wait while component information is loaded from the ITK production database'):
                    comp_df_func_abc, stage_dict_abc = db_table_access(project_type, ['abc'], comp_veto_kw, time_current)  
                comp_df_abc=comp_df_func_abc.copy() #stops streamlit mutation warning
                comp_df_second= pd.concat([comp_df_initial, comp_df_abc])
                comp_df=comp_df_second
                stage_dict_initial.update(stage_dict_abc) 
                stage_dict= stage_dict_initial.copy()
            else:
                comp_df=comp_df_initial
                stage_dict= stage_dict_initial
        else:
            comp_df=comp_df_initial 
            stage_dict= stage_dict_initial  
        comp_df = comp_df[comp_df["state"] != "deleted"] 

        with st.expander("Component selection options"):

            assemble= st.selectbox('Would you like to see all components, assembled components or components not yet assembled?',
                                    ['All Components', 'Assembled Only', 'Not Assembled'])
            
            if assemble== 'Assembled Only':
                comp_df_assemble= comp_df.loc[comp_df['assembled'] == True]  
            elif assemble== 'Not Assembled':
                comp_df_assemble= comp_df.loc[comp_df['assembled'] == False]  
            else:
                comp_df_assemble= comp_df

            trashed= st.selectbox('Would you like to see all components, only good components or only trashed components?',
                                    ['All Components', 'Good Components', 'Trashed Components Only'])
            
            if trashed== 'Trashed Components Only':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['trashed'] == True]  
            elif trashed== 'Good Components':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['trashed'] == False]  
            else:
                comp_df_assemble= comp_df_assemble

            rework= st.selectbox('Would you like to see all components, only good components or only reworked components?',
                                    ['All Components', 'Good Components Only', 'Reworked Components Only'])
            
            if rework== 'Reworked Components Only':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['reworked'] == True]  
            elif rework== 'Good Components Only':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['reworked'] == False]  
            else:
                comp_df_assemble= comp_df_assemble

        if comp_df_assemble.empty:
            st.warning('No components meet this criteria, please change your selection')
            st.stop() 
        
        comp_df_assemble["currentStage_name"].replace(to_replace=[None], value='Undefined', inplace=True) #replace empty stages
        comp_df_assemble["currentStage_name"].fillna("Undefined", inplace = True)  #replace empty stages  

        comp_df_pivot_inst=pd.pivot_table(comp_df_assemble, index=['componentType_name', 'currentStage_name'], #,'type_name'
                                        columns=['country', 'currentLocation_code'], values=['code'], 
                                        aggfunc=['count'], margins=True, margins_name = "Total", fill_value=0)#.fillna(0)
        #st.dataframe(comp_df_pivot_inst)
    
        result = pd.concat([comp_df_pivot_inst, comp_df_pivot_inst.groupby(axis=0, level=[0]).sum().assign(name='Subtotal').
                            set_index('name',append=True)]).sort_index(level=[0], sort_remaining=False)
        
        result = result.droplevel(axis=1, level=0)    
        # Drop subtotal from total, which is last row
        result.drop(result.tail(1).index,inplace=True)  

        #Find all countries that have one institute
        full_columns= list(result.columns)
        columns_no_repeat=[]
        for item in full_columns:
            columns_no_repeat.append(item[1]) 
        columns_no_repeat= [x for x in columns_no_repeat if columns_no_repeat.count(x)==1]

        # Add subtotals for columns ...  
        
        # Create data frame for subtotals
        subcol = result.groupby(axis = 1, level=[1]).sum() #sum(axis = 1, level = [1])  
        # Form indices 
        indices = list(zip(subcol.columns, ["Subtotal"]*len(subcol.columns)))
        ####indices[-1] = ("Total", "") #what does this do?
        subcol.columns = pd.MultiIndex.from_tuples(indices, names = ["country", "currentLocation_code"])
        # Drop total
        subcol = subcol.drop(columns_no_repeat, axis=1)    
        result = result.droplevel(axis=1, level=0) #remove 'count' level
        # Concat to main Table
        result = pd.concat([result, subcol], axis=1).sort_index(axis = 1, level = [0]).astype(int) # convert floats to ints   

        #result = result.rename(columns = {'':'test', 'UNKNOWN STAGE': 'test', None: 'test'}, level=1)
        #result = result.rename(index = {'':'test', 'UNKNOWN STAGE': 'test', None: 'test'}, level=1)

        stage_index=[]
        for item in list(stage_dict.items()):
            for subitem in item[1]:
                tuple_type= (item[0], subitem)
                stage_index.append(tuple_type)
 
        missing_stages = st.checkbox('Include stages that currently have no components?')
        no_stage = pd.DataFrame()
        if missing_stages:
            for item in stage_index: 
                if result.index.isin([item]).any()==False:     
                    stage_no_comps = pd.DataFrame([[0]*result.shape[1]],columns=result.columns, index= pd.MultiIndex.from_tuples([item]))
                    no_stage = pd.concat([no_stage, stage_no_comps])
            result = pd.concat([result, no_stage])            
        
        def criteria(x):  
            if x[0] == "CERN":
                return ("AAAAAA", x[1]) 
            if x[0] == "Total":
                return ("zzzzzz", x[1])  
            if x[1]== "Subtotal":        
                return(x[0], 'zzzzz')#"ZZZZZZZZ")
            if x[1]== "Total":        
                return(x[0], 'zzzzzz')#"ZZZZZZZZ")
            if x[0] in stage_dict:  
                alphabet= [chr(k) for k in range(len(stage_dict[x[0]]))] #chr gives the ASCII character for the given number  
                for i, item in enumerate(stage_dict[x[0]]): 
                    if x[1]==item: 
                        return(x[0], alphabet[i])   
                return(x[0], x[1])
            else:   
                return x

        def criteria_country(x):
            country_map = {}
            missing = [] 
            i=st.session_state.Authenticate['inst']
            # Specific cases that don't map easilly
            if i["code"] == "LPNHE":
                country_map[i["code"]] = pycountry.countries.search_fuzzy("France")[0].alpha_2
            elif i["code"] == "BUW":
                country_map[i["code"]] = pycountry.countries.search_fuzzy("Germany")[0].alpha_2
            elif "country" in i["address"]:
                # Those that have a country
                if i["address"]["country"] in ['UK', ' UK']:##
                    country_map[i["code"]] = pycountry.countries.search_fuzzy('United Kingdom')[0].alpha_2##
                else:##
                    country_map[i["code"]] = pycountry.countries.search_fuzzy(i["address"]["country"])[0].alpha_2
            elif "state" in i["address"] and i["address"]["state"] != '':
                # Those that have a state, with some sanitising
                try:
                    state = i["address"]["state"].replace("CA, USA", "USA")
                    country_map[i["code"]] = pycountry.countries.search_fuzzy(state)[0].alpha_2
                except LookupError:
                    missing.append(i)
            else:
                # Try to guess from name, with some sanitising
                try:
                    name = i["name"].lstrip("INFN").lstrip("University of").lstrip("College").rstrip("University").rstrip("at Dallas")
                    country_map[i["code"]] = pycountry.countries.search_fuzzy(name)[0].alpha_2
                except LookupError:
                    missing.append(i)      
            country = country_map[i['code']]#.apply(lambda i : country_map.get(i, "N/A"))
 
            if x[0] == country:
                return ("AAA", x[1]) 
            if x[0] == "CERN":
                return ("AAAAAA", x[1]) 
            if x[0] == "Total":
                return ("zzzzzz", x[1])  
            if x[1]== "Subtotal":        
                return(x[0], 'zzzzz')#"ZZZZZZZZ")
            if x[1]== "Total":        
                return(x[0], 'zzzzzz')#"ZZZZZZZZ")
            if x[0] in stage_dict:  
                alphabet= [chr(k) for k in range(len(stage_dict[x[0]]))] #chr gives the ASCII character for the given number  
                for i, item in enumerate(stage_dict[x[0]]):
                    if x[1]==item: 
                        return(x[0], alphabet[i])    
                return(x[0], x[1])
            else:   
                return x
        
        st.write('## ITK ' + project_name + ' overview main table') 
        inst_move=st.checkbox('Move my home country to start of table')
        if inst_move:
            # Sort column order
            columns_orig = result.columns.tolist() 
            columns_sort = sorted(columns_orig, key=criteria_country) 
            result = result[columns_sort]

            # Sort row order 
            index_sorted = sorted(result.index.to_list(), key=criteria_country)  
            result= result.reindex(index_sorted)  
        else:
            # Sort column order
            columns_orig = result.columns.tolist() 
            columns_sort = sorted(columns_orig, key=criteria) 
            result = result[columns_sort]

            # Sort row order 
            index_sorted = sorted(result.index.to_list(), key=criteria)  
            result= result.reindex(index_sorted)   

         
        table_countries= result.columns.levels[0].tolist()
        for item in ['total']:
            table_countries= list(filter(lambda x: (item not in x.lower()), table_countries)) #removes test components 

        table_institutes= result.columns.levels[1].tolist()
        table_institutes= filter(None, table_institutes)
        for item in ['subtotal']:
            table_institutes= list(filter(lambda x: (item not in x.lower()), table_institutes)) #removes test components 

        #st.write(result)
        def subtotal_highlight(val): #this doesn't work with the download option
            return [('background: lightblue' if 'Subtotal' in x else 'color: black') for x in result.index]

        # for item in result.columns.levels[0].tolist():
        #     result[item]=result[item].apply(lambda val: dict((y, x) for x, y in val) if type(val)==type(()) else val)

        # for item in result.columns.levels[1].tolist():
        #     result[item]=result[item].apply(lambda val: dict((y, x) for x, y in val) if type(val)==type(()) else val)


        result_highlight = result.style.apply(subtotal_highlight,axis=0)
        st.dataframe(result_highlight) 
        @st.cache
        def convert_df(df):
            # IMPORTANT: Cache the conversion to prevent computation on every rerun
            return df.to_csv().encode('utf-8') 
        csv = convert_df(result)
        result_name= 'overview_table_' + time_current + '_' + project_name +'.csv'
        
        st.download_button(label="Download overview table", data=csv, file_name=result_name, mime='text/csv' ) 
        pio.renderers.default = 'iframe'  

        if st.button('Generate overview pie chart'):
            # Group original DF (before pivot) and reset indices to columns
            df_plot = comp_df_assemble.groupby(["country", "currentLocation_code", "componentType_code"]).agg(count=("id", "count")).reset_index()
            #df_plot 
            # Make sunburst of country:institute:type
            fig = px.sunburst(df_plot, path = ["country", "currentLocation_code" , "componentType_code"], color = "count", hover_data={"count":False}, 
                            values = "count", color_continuous_scale='Aggrnyl')
            # for jupyter not needed in streamlit probbaly 
            st.plotly_chart(fig)  
        
        st.write('## Multiple country overview')
        st.write('Select below to get a smaller overview table for one or more countries')
        list_countries= st.multiselect('Please select countries', table_countries)
        if st.button('Generate table for one or more countries'):
            if len(list_countries)==0:
                st.warning('Please input at least one country')
                st.stop() 
            result_small= result[list_countries]
            result_small_highlight= result_small.style.apply(subtotal_highlight,axis=0)
            st.dataframe(result_small_highlight) 
            
            csv_result_small = convert_df(result_small)
            list_countries_join ='_'.join(list_countries)
            result_small_name= list_countries_join + '_overview_table_' + time_current + '_' + project_name +'.csv'
            st.download_button(label="Download overview table", data=csv_result_small, file_name=result_small_name, mime='text/csv' )

        
        if st.button('Generate pie chart for one or more countries'):
            if len(list_countries)==0:
                st.warning('Please input at least one country')
                st.stop() 
            df_gb = comp_df_assemble[comp_df_assemble["country"].isin(list_countries)].groupby(["currentLocation_code", "componentType_code","currentStage_name"]).agg(count=("id", "count")).reset_index()
            fig2 = px.sunburst(df_gb, path = ["currentLocation_code", "componentType_code", "currentStage_name"], color = "count", hover_data={"count":False}, maxdepth = 2,
                            values = "count", color_continuous_scale='Aggrnyl', title = "")
            # for jupyter not needed in streamlit probbaly 
            st.plotly_chart(fig2) 

        st.write('## Multiple institutes overview')
        st.write('Select below to get an overview table for multiple institutes')
        if st.session_state.Authenticate['inst']['code'] in table_institutes:
            inst_index= st.session_state.Authenticate['inst']['code'] 
        else:
            inst_index= 'CERN'
        list_inst= st.multiselect('Please select institutes', table_institutes, default=inst_index)
        if st.button('Generate table for multiple institutes'):
            result_institutes= result.droplevel([0], axis=1)
            result_single=result_institutes.iloc[:,~result_institutes.columns.duplicated(keep=False)] 
            #if st.session_state.Authenticate['inst']['code'] not in table_institutes:
            #    st.warning('Your chosen institute does not contain the correct component type, please select another')   
            #else:
            single_inst=result_single[list_inst]   
            st.dataframe(single_inst)
            csv_single_inst = convert_df(single_inst)
            list_inst_join ='_'.join(list_inst)
            single_inst_name= list_inst_join + '_overview_table_' + time_current + '_' + project_name +'.csv'
            st.download_button(label="Download overview table", data=csv_single_inst, file_name= single_inst_name, mime='text/csv' )





