# Conor App

Tool for compiling population statistics

- Currently working only for _Pixels_ project - _needs expanded!_

This app was developed as part of Qualification Task by Conor McPartland (@cmcpartl). 

---

## Pages

### Summary tables
  * View component summary tables

### Summary plots
  * View component summary plots
