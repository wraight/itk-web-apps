# Report Maker App

Tool to help generate on demand reports.

- based on [itk-reports](https://itk-reports.docs.cern.ch) framework

This app was developed as part of Qualification Task by Nihad Hidic
(@nhidic). 

---

## Pages


### Documentation

Instructions on how to use app.


### Submit Report

Construct specific query of PDB to be made in real-time.

- downloadable output
