# Pixels Wirebonding webApp

### [Streamlit](https://www.streamlit.io) (python**3**) code to upload component/test schemas for registration on production database (PDB)

**NB** Powered by [itkdb](https://pypi.org/project/itkdb/)

Check requirements file for necessary libraries.

See [itk-web-apps README](https://gitlab.cern.ch/wraight/itk-web-apps/) for instructions on running via Docker.

---

## Pages

### manualTestUpload
  * upload module test manually, based on PDB schema

### multiUpload
  * upload multiple component information
