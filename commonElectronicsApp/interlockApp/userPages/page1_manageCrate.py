### standard
import streamlit as st
from core.Page import Page
import json
### custom
import os
cwd=os.path.dirname(os.path.realpath(__file__))
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx


#####################
### useful functions
#####################

#get crate
def get_crate_type(crate):
    return crate["properties"][1]["codeTable"][int(crate["properties"][1]["value"])]["value"]

def set_state(state_name, state_val):
    st.session_state[state_name] = state_val

def draw_table(crate, template):
    """render table with crate contents"""

    crate_children = get_children(crate)

    col_shape = [1,1,2,2,2]
    clms = st.columns(col_shape)
    fields = ["Position", 'Type', 'Serial Number', "Add/Replace", "Remove"]
    #create headers
    for col, field_name in zip(clms, fields):
        col.write(field_name)

    for ind, comp in template.items():
        col1, col2, col3, col4, col5 = st.columns(col_shape)
        pos = str(comp["position"])
        where=comp["where"]
        occupied = False
        col1.markdown(f'<div style="display: inline-block;" title="{where}">{pos}</div>', unsafe_allow_html=True)
        if crate_children[int(ind)]["component"]:
            occupied = True
            try:
                col2.write(crate_children[int(ind)]["history"][-1]["type"]["name"])
            except:
                col2.write(crate_children[int(ind)]["type"])
            col3.write(crate_children[int(ind)]["component"]["serialNumber"])
        else:
            col2.write("EMPTY")
        new_serial = col4.text_input("Serial Number", label_visibility="collapsed", placeholder="Serial Number", key="text_{}".format(ind))
        if new_serial:
            if occupied:
                col4.warning("Are you sure?")
                if col4.button("Yes", key="button_sure_yes_{}".format(ind)):
                    remove_module(crate, crate_children[int(ind)]["component"]["serialNumber"])
                    add_module(crate, new_serial, ind, template)
                    new_serial = None
                if col4.button("No", key="button_sure_no_{}".format(ind)):
                    new_serial = None
            else:
                add_module(crate, new_serial, ind, template)

        if occupied:
            if "confirm_remove_{}".format(ind) not in st.session_state:
                st.session_state["confirm_remove_{}".format(ind)] = False
            if not st.session_state["confirm_remove_{}".format(ind)]:
                if col5.button("Remove", key="button_remove_{}".format(ind)):
                    st.session_state["confirm_remove_{}".format(ind)] = True
            else:
                col5.warning("Are you sure?")
                if col5.button("Yes", key="button_rem_yes_{}".format(ind)):
                    remove_module(crate, crate_children[int(ind)]["component"]["serialNumber"])
                    st.session_state["confirm_remove_{}".format(ind)] = False
                    occupied = False
                if col5.button("No", key="button_rem_no_{}".format(ind)):
                    st.session_state["confirm_remove_{}".format(ind)] = False
        else:
            col5.empty()


def add_module(crate, sn, index, template):
    """Mount an interlock component to a parent crate

    a check is performed to determine if the component type is allowed in
    the slot according to the placement scheme of child components
    """

    module = DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':sn})
    mod_type = module["type"]["code"]
    if mod_type not in template[index]["allowed_types"]:
        st.write("This component type is not allowed in this slot!")
        return
    try:
        assVal=DBaccess.DbPost(st.session_state.myClient,'assembleComponentBySlot',
                               {"parent": crate["serialNumber"],
                                'slot': crate["children"][int(index)]["id"],
                                "child":sn})
        st.write("### **Successfully assembled {} to {}".format(sn,crate["serialNumber"]))
        assCheck=True
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: assembly **Unsuccessful**")
        assCheck=False
    except TypeError as e:
        st.write("Cannot assemble {} to {}".format(sn, crate["serialNumber"]))
        st.write(e)
        assCheck=False
    pass

def remove_module(crate, sn):
    try:
        disVal=DBaccess.DbPost(st.session_state.myClient,'disassembleComponent', {"parent" : crate['serialNumber'], "child" : sn})
        st.write("**Successful disassembled**:{} from {}".format(sn, crate['serialNumber']))
    except itkX.BadRequest as b:
        st.write(":no_entry_sign: disassembly **Unsuccessful**")
    except TypeError:
        st.write("Cannot disassemble {}".format(sn))


def get_children(crate):
    try:
        child_dict = crate["children"]
    except TypeError:
        st.write("type error")
        child_dict=None
    except KeyError:
        st.write("key error")
        child_dict=None
    return child_dict

class Page1(Page):
    def __init__(self):
                super().__init__("Manage Crate", ":microscope: Manage Crate")

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]


        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        pageDict['project']=st.session_state.Authenticate['proj']['code']
        pageDict['institute']=st.session_state.Authenticate['inst']['code']

        st.write("## Select component")

        infra.Radio(pageDict,'inputType',["Enter identifier","Select from inventory"],"Select input method:")
        st.write("### ",pageDict['inputType'])

        if "identifier" in pageDict['inputType']:

            infra.TextBox(pageDict,'inCrate',"Enter component identifier")

            if pageDict['inCrate']==None or len(pageDict['inCrate'])<1:
                st.write("Enter identifier to get component information")
                st.stop()

        if "inventory" in pageDict["inputType"]:
            pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'currentLocation':pageDict['institute'], 'project':pageDict['project'] },True)

            #filter out only interlock crates from list of institute components
            crates_at_inst = [comp["serialNumber"] for comp in pageDict["compList"]
                              if comp["componentType"]["code"]=="INTERLOCKCRATE"]

            #check for empty list:
            if len(crates_at_inst) == 0:
                st.write("No crates registered at {}".format(pageDict["institute"]))
                st.stop()

            # Create a dropdown menu for the crates
            pageDict['inCrate'] = st.selectbox(
                "Select a crate:", crates_at_inst
            )

        if "crate" not in pageDict.keys():
            pageDict['crate'] = pdbTrx.GetComponentByIdentifier(pageDict["inCrate"], "SN")
        crate = pageDict["crate"]

        try:
            if crate["componentType"]["code"] not in ["INTERLOCKCRATE"]:
                st.write("Wrong component type selected. This is a " + crate["componentType"]["code"])
        except KeyError:
            st.write("No componentType code key")
            st.stop()
        except TypeError:
            st.write("Unexpected object")
            st.stop()

        #display numbering scheme png
        if get_crate_type(crate) == "LISSY":
            st.image(cwd + "/lissy_numbering.png")
            with open(cwd+ "/lissy_schema.json") as sf:
                template = json.load(sf)
        #elif myCrate["properties"][1]["type"] == "MIC":
        #TODO: display MIC crate layout

        st.write("## Crate Contents")
        draw_table(crate, template)

        #todo button or something to redraw table?
