### standard
import streamlit as st
from core.Page import Page
### PDB stuff
import core.stInfrastructure as infra
### custom
import pandas as pd
import os
cwd=os.path.dirname(os.path.realpath(__file__))

#####################
### useful functions
#####################

def get_component(state, pageDict, keys = "sensor", comp_type = "SENSOR_TILE", button_name = "sensor", doAlt = False):

    keys = [keys] if isinstance(keys, str) else keys

    if st.button("Check " + button_name):
        compCheck=True
        for k in keys:
            pageDict[k+'Comp']= state.myClient.get('getComponent',
                                              json={'component':pageDict[k + 'Code'], 'alternativeIdentifier' : doAlt})
        if state.debug:
            st.write(pageDict[k+'Comp'])

        # Check type of component is as expected
        try:
            for k in keys:
                ctype = pageDict[k+'Comp']['componentType']['code']
                if ctype != comp_type:
                    st.write(":exclamation: Unexpected omponent type:", ctype)
                    compCheck=False
        except TypeError:
            st.write("Problem with database return")
            compCheck=False


        # Check at correct institue
        try:
            inst = state.authenticate['inst']['code']
            for k in keys:
                loc = pageDict[k+'Comp']['currentLocation']['code']
                if loc != inst:
                    st.write(f":exclamation: Component currently at {loc} : you need to first ship to {inst} in the DB")
                    compCheck=False
        except TypeError:
            st.write("Problem with database return")
            compCheck=False

        # Check not already attached to a bare module
        try:
            for k in keys:
                if pageDict[k+'Comp']["parents"] is None:
                    used = []
                else:
                    used = [c["componentType"]["id"] for c in pageDict[k+'Comp']["parents"] if c["componentType"]["code"] == pageDict['componentType']]
                if len(used):
                    st.write(f":exclamation: Component already attached to bare module:", used[0])
                    compCheck=False
        except TypeError:
            st.write("Problem with database return")
            compCheck=False


        #if compCheck:
        #    st.write(button_name + ": OK")

        return compCheck

CHIP_TYPE = {"RD53A" : "0", "ITkpix_v1" : "1", "ITkpix_v1.1" : "2", "ITkpix_v2" : "3", "No chip": "9"}


infoList=["  * assemble bare module",
        "   * delete test upload if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Bare Assembly", ":microscope: Assemble Bare Module", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ### Actual work

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="BARE_MODULE"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        ### hidden changer
        if st.session_state.debug:
            st.write("**DEBUG** Hidden changer")
            infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
            if pageDict['toggleChanger']:
                infra.TextBox(pageDict,'componentType',"Enter componentType code:")
                infra.TextBox(pageDict,'code',"Enter testType code:")
                infra.TextBox(pageDict,'stage',"Enter testStage _code_:")
                infra.TextBox(pageDict,'project',"Enter project code:")


        # Init
        pageDict["BARE_SN"] = ""

        st.write("### Bare module info")

        # Type
        component_types = st.session_state.myClient.get( 'listComponentTypes',
                                         json={'filterMap' :
                                          {"project" : pageDict['project'],
                                           "code" : pageDict['componentType']}})
        if type(component_types)!=type([]):
            component_types=component_types.data
        component_map = {comp['code']: i for i,comp in enumerate(component_types)}
        bare_types = [ct['code'] for ct in component_types[component_map[pageDict['componentType']]]['types']]
        infra.SelectBox(pageDict,'bareType', bare_types, 'Select a bare module componentType')

        nChips = 4
        if "SINGLE" in pageDict["bareType"]:
            nChips = 1
        elif "DUAL" in pageDict["bareType"]:
            nChips = 2

        st.write("The SENSOR_TYPE and FECHIP_VERSION are taken directly from the components below")

        st.write("### Sensor selection")

        # Sensor

        # TODO: check if sensor type consistent with module type?
        if "DIGITAL" not in pageDict["bareType"]:
            infra.TextBox(pageDict,'sensorCode',"Input sensor tile ID (SN or Alt ID)")
            infra.Radio(pageDict, 'codeType', ["SN", "Alt ID"], "")

            check = get_component(st.session_state, pageDict, "sensor", "SENSOR_TILE", "sensor", doAlt = (pageDict["codeType"] == "Alt ID"))

            if check:
                pageDict["SENSOR_TYPE"] = [s["value"] for s in pageDict["sensorComp"]["properties"] if s["code"] == "MAN_SNO"][0][1]
                st.write(":white_check_mark: Found good V" + pageDict["SENSOR_TYPE"] + " sensor")
                if st.session_state.debug:
                    st.write(pageDict["SENSOR_TYPE"])
        else:
            st.write("No sensor required for digital module")
            pageDict["SENSOR_TYPE"] = "0"

        # Chips

        st.write("### Chip selection")

        st.write("Select chips following the numbering in the diagram")


        st.image(cwd+"/RD53A_quad.png")

        for i in range(1, nChips+1):
            infra.TextBox(pageDict,f'chip{i}Code',f"Input chip {i} ID (SN)")

        check = get_component(st.session_state, pageDict, [f"chip{i}" for i in range(1,nChips+1)], "FE_CHIP", button_name = "Chips")

        if check:
            chipVers =[pageDict[f"chip{i}Comp"]["type"]["code"] for i in range(1,nChips+1)]
            if len(set(chipVers)) != 1:
                st.write(":exclamation: Inconsistent chip versions", chipVers)
            else:
                st.write(":white_check_mark: Found good " + chipVers[0] + " chips")

            pageDict["FECHIP_VERSION"] = CHIP_TYPE.get(chipVers[0], None)
            if state.session_state.debug:
                st.write(pageDict["FECHIP_VERSION"])

        # Bare module reg

        if st.button("Register bare module and Assemble"):
            st.write(pageDict["bareType"])
            template = st.session_state.myClient.get( 'generateComponentTypeDtoSample',
                                      json={'project':st.session_state.Authenticate['proj']['code'], 'code': "BARE_MODULE", 'requiredOnly':True})

            del template["serialNumber"]
            bare_info = st.session_state.myClient.post( "registerComponent",
                                   json={**template, "institution" : st.session_state.Authenticate['inst']['code'],
                                    "type" : pageDict["bareType"], "subproject" : "PG",
                                    'properties': {'SENSOR_TYPE' : pageDict["SENSOR_TYPE"], 'FECHIP_VERSION': pageDict["FECHIP_VERSION"]}})


            bare_sn = bare_info["serialNumber"]
            pageDict["BARE_SN"] = bare_sn
            st.write(f"Registered bare module: {bare_sn}")


            st.session_state.myClient.post( 'assembleComponent', json={"parent" : bare_sn, "child" : pageDict["sensorComp"]["serialNumber"]})

            bareMod = client.get('getComponent', json={'component' : bare_sn})
            chipIDs = [m["id"] for m in bareMod["children"] if m["componentType"]["code"] == "FE_CHIP"]

            for i in range(1, nChips+1):
                st.session_state.myClient.post( 'assembleComponentBySlot', json={"parent" : bare_sn, "slot" : chipIDs[i],
                                                                                 "child" : pageDict[f"chip{i}Comp"]["serialNumber"]})

            st.balloons()
