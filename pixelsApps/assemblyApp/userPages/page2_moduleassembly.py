### standard
import streamlit as st
from core.Page import Page
### PDB stuff
import core.stInfrastructure as infra
### custom

#####################
### useful functions
#####################

def get_component(state, pageDict, keys = "sensor", comp_type = "SENSOR_TILE", button_name = "sensor", doAlt = False):

    keys = [keys] if isinstance(keys, str) else keys

    if st.button("Check " + button_name):
        compCheck=True
        for k in keys:
            pageDict[k+'Comp']=state.myClient.get('getComponent',
                                              json={'component':pageDict[k + 'Code'], 'alternativeIdentifier' : doAlt})
        if state.debug:
            st.write(pageDict[k+'Comp'])

        # Check type of component is as expected
        try:
            for k in keys:
                ctype = pageDict[k+'Comp']['componentType']['code']
                if ctype != comp_type:
                    st.write(":exclamation: Unexpected omponent type:", ctype)
                    compCheck=False
        except TypeError:
            st.write("Problem with database return")
            compCheck=False


        # Check at correct institue
        try:
            inst = state.authenticate['inst']['code']
            for k in keys:
                loc = pageDict[k+'Comp']['currentLocation']['code']
                if loc != inst:
                    st.write(f":exclamation: Component currently at {loc} : you need to first ship to {inst} in the DB")
                    compCheck=False
        except TypeError:
            st.write("Problem with database return")
            compCheck=False

        # Check not already attached to a bare module
        try:
            for k in keys:
                if pageDict[k+'Comp']["parents"] is None:
                    used = []
                else:
                    used = [c["componentType"]["id"] for c in pageDict[k+'Comp']["parents"] if c["componentType"]["code"] == pageDict['componentType']]
                if len(used):
                    st.write(f":exclamation: Component already attached to bare module:", used[0])
                    compCheck=False
        except TypeError:
            st.write("Problem with database return")
            compCheck=False


        #if compCheck:
        #    st.write(button_name + ": OK")

        return compCheck

CHIP_TYPE = {"RD53A" : "0", "ITkpix_v1" : "1", "ITkpix_v1.1" : "2", "ITkpix_v2" : "3", "No chip": "9"}


infoList=["  * assemble bare module",
        "   * delete test upload if required"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Module Assembly", ":microscope: Bare Module", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ### Actual work

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULE"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        ### hidden changer
        if st.session_state.debug:
            st.write("**DEBUG** Hidden changer")
            infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
            if pageDict['toggleChanger']:
                infra.TextBox(pageDict,'componentType',"Enter componentType code:")
                infra.TextBox(pageDict,'code',"Enter testType code:")
                infra.TextBox(pageDict,'stage',"Enter testStage _code_:")
                infra.TextBox(pageDict,'project',"Enter project code:")


        # Type
        component_types = st.session_state.myClient.get( 'listComponentTypes',
                                         json={'filterMap' :
                                          {"project" : pageDict['project'],
                                           "code" : pageDict['componentType']}})
        if type(component_types)!=type([]):
            component_types=component_types.data
        component_map = {comp['code']: i for i,comp in enumerate(component_types)}

        MODULE_SN_PREFIX = {st["code"]: st["subprojects"][0]["code"]+st["snComponentIdentifier"] for st in component_types[component_map[pageDict['componentType']]]['types']}

        mod_types = [ct['code'] for ct in component_types[component_map['MODULE']]['types']]
        infra.SelectBox(pageDict,'modType', mod_types, 'Select a module componentType')
        infra.Radio(pageDict, 'ORIENTATION', ["Normal", "Inverted"], "Select an orientation")


        st.write("The FECHIP_VERSION is taken directly from the components below")

        # Bare Module
        st.write("### Bare module selection")


        # Default to the bare SN from the previous bare assembly page
        if "assembleBare" in dir(st.session_state):
            pageDictBare = st.session_state.__getattribute__("assembleBare")
            pageDict["bareCode"] = pageDictBare.get("BARE_SN", "")

        infra.TextBox(pageDict,'bareCode',"Input bare module ID (SN)")

        check = get_component(st.session_state, pageDict, "bare", "BARE_MODULE", "bare")

        if check:
            bareChipVer = [s["value"] for s in pageDict["bareComp"]["properties"] if s["code"] == "FECHIP_VERSION"][0]
            pageDict["BARE_CHIP_VERSION"] = CHIP_TYPE.get(bareChipVer, None)
            pageDict["BARE_TYPE"] = pageDict["bareComp"]["type"]["code"]
            st.write(":white_check_mark: Found good {} {} bare module".format(bareChipVer, pageDict["BARE_TYPE"]))

        # PCB
        st.write("### PCB (Hybrid) selection")
        infra.TextBox(pageDict,'pcbCode',"Input PCB ID (SN)")

        check = get_component(st.session_state, pageDict, "pcb", "PCB", "pcb")

        if check:
            pcbChipVer = [s["value"] for s in pageDict["pcbComp"]["properties"] if s["code"] == "FECHIP_VERSION"][0]
            pageDict["PCB_CHIP_VERSION"] = CHIP_TYPE.get(pcbChipVer, None)
            pageDict["PCB_TYPE"] = pageDict["pcbComp"]["type"]["code"]

            if pageDict["BARE_CHIP_VERSION"] != pageDict["PCB_CHIP_VERSION"]:
                st.write(":exclamation: Inconsistent chip versions for bare module {} vs hybrid {}".format(pageDict["BARE_CHIP_VERSION"], pageDict["PCB_CHIP_VERSION"]))
            else:
                st.write(":white_check_mark: Found good " + pcbChipVer + " " + pageDict["PCB_TYPE"] + " pcb")


        # Carrier
        st.write("### Carrier selection (optional)")
        infra.TextBox(pageDict,'carrierCode',"Input Carrier ID (SN)")

        check = get_component(st.session_state, pageDict, "carrier", "MODULE_CARRIER", "carrier")

        if check:
            st.write(":white_check_mark: Found good carrier")


        if st.button("Register module and Assemble"):
            template = st.session_state.myClient.get( 'generateComponentTypeDtoSample',
                                      json={'project':st.session_state.Authenticate['proj']['code'], 'code': "MODULE", 'requiredOnly':True})

            sn = "20U{}{}{}".format(MODULE_SN_PREFIX[pageDict["modType"]], pageDict["BARE_CHIP_VERSION"], pageDict["pcbComp"]["serialNumber"][-6:])

            mod_info = st.session_state.myClient.post( "registerComponent",
                                   json={**template, "institution" : st.session_state.Authenticate['inst']['code'],
                                    "type" : pageDict["modType"], "subproject" : "PG", "serialNumber" : sn,
                                    'properties': {'ORIENTATION' : pageDict["ORIENTATION"]=="Normal", 'FECHIP_VERSION': pageDict["BARE_CHIP_VERSION"]}})

            mod_sn = mod_info["component"]["serialNumber"]
            st.write(f"Registered module: {mod_sn}")

            st.session_state.myClient.post('assembleComponent', json={ "parent" : mod_sn, "child" : pageDict["bareComp"]["serialNumber"]})
            st.write("Attached Bare module: {}".format(pageDict["bareComp"]["serialNumber"]))

            st.session_state.myClient.post('assembleComponent', json={ "parent" : mod_sn, "child" : pageDict["pcbComp"]["serialNumber"]})
            st.write("Attached PCB: {}".format(pageDict["pcbComp"]["serialNumber"]))

            if "carrierComp" in pageDict:
                st.session_state.myClient.post('assembleComponent', json={"parent" : mod_sn, "child" : pageDict["carrierComp"]["serialNumber"]})
                st.write("Attached Carrier: {}".format(pageDict["carrierComp"]["serialNumber"]))
            else:
                st.write("No carrier attached")

            st.balloons()
