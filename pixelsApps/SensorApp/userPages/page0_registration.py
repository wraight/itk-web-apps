### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import os
from io import StringIO
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### CV script
from . import analyseCV_data

#####################
### useful functions
#####################
def SelectCheck(k,v):
    val=None
    if "img" in k.lower(): # for images (identified by name for the moment)
        val=st.file_uploader(k+" (image):", type=["png", "jpg", "jpeg"]) #bytesio for binary uploads
    try: # for bools formatted as strings by schema
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    except AttributeError:
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    try: # avoid bool trouble
        if val.isnumeric(): # for integers formatted as strings by schema
            val=int(val)
    except AttributeError:
        pass
    return val

def ColorCells(s, df, colName, flip=False):
    thisRow = pd.Series(data=False, index=s.index)
    colours=['red','blue','green','orange','purple''yellow','pink','lightblue','lightgreen']*3
    names=list(df[colName].unique())
    if flip:
        return ['background-color: %s ; color: %s'% ('white',colours[names.index(s[colName])])]*len(df.columns)
    else:
        return ['background-color: %s ; color: %s'% (colours[names.index(s[colName])],'black')]*len(df.columns)

def EditJson(inJson):
    for k,v in inJson.items():
        if type(v)==type({}):
            st.write("**"+str(k)+"**")
            for l,w in v.items():
                inJson[k][l]=SelectCheck(l,w)
        else:
            inJson[k]=SelectCheck(k,v)
    return inJson


infoList=["  * upload _csv_ CV file",
        "  * review test schema",
        "   * reset if required",
        "   * edit if required",
        "  * upload test schema",
        "   * delete test upload if required"]
#####################
### main part
#####################

class Page0(Page):
    def __init__(self):
        super().__init__("Registration", ":microscope: Register Sensors", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="SENSOR_WAFER"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        ### hidden changer
        if st.session_state.debug:
            st.write("**DEBUG** Hidden changer")
            infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
            if pageDict['toggleChanger']:
                infra.TextBox(pageDict,'componentType',"Enter componentType code:")
                infra.TextBox(pageDict,'project',"Enter project _code_:")


        # get test schema
        if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['componentType']):
            pageDict['origSchema'] = st.session_state.myClient.get('generateComponentTypeDtoSample', json={'project':pageDict['project'], 'code':pageDict['componentType'], 'requiredOnly':True})

        pageDict['compTypeObj']=st.session_state.myClient.get('getComponentTypeByCode',json={'project':st.session_state.Authenticate['proj']['code'], 'code':pageDict['componentType']})
        mySubProjs=[x['code'] for x in pageDict['compTypeObj']['subprojects']]
        myCompTypes=[x['code'] for x in pageDict['compTypeObj']['types']]

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])

        # reset test schema if required
        if "compSchema" not in pageDict.keys() or st.button("reset schema"):
            pageDict['compSchema']=pageDict['origSchema'].copy()
            #state.compSchema['component']=state.myComp['code']
            pageDict['compSchema']['institution']=st.session_state.Authenticate['inst']['code']

        # list widgets to update
        for k,v in pageDict['compSchema'].items():
            #st.write(k,":",type(v))
            if type(v)==type({}):
                for l,w in pageDict['compSchema'][k].items():
                    pageDict['compSchema'][k][l]=SelectCheck(l,w)
            else:
                if k=="subproject":
                    try:
                        pageDict['compSchema'][k]=st.selectbox(k+" :", list(mySubProjs), index=list(mySubProjs).index(pageDict['compSchema'][k]))
                    except:
                        pageDict['compSchema'][k]=st.selectbox(k+" :", list(mySubProjs) )
                    continue
                elif k=="type":
                    try:
                        pageDict['compSchema'][k]=st.selectbox(k+" :", list(myCompTypes), index=list(myCompTypes).index(pageDict['compSchema'][k]))
                    except:
                        pageDict['compSchema'][k]=st.selectbox(k+" :", list(myCompTypes) )
                    continue
                else:
                    pageDict['compSchema'][k]=SelectCheck(k,v)
