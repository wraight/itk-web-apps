# Strips SENSOR webApp

Anachronistic tools for Pixels Sensors.

---

## Pages

### CVTestUpload
  * upload SENSOR_TILE CV test manually, based on PDB schema

### IVTestUpload
  * upload SENSOR_TILE IV test manually, based on PDB schema
