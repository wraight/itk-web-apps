### standard
import streamlit as st
from core.Page import Page
### custom
import os
import pandas as pd
import ast
import csv
import json
import plotly.graph_objects as go
from io import BytesIO
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
from .SharedCode import to_excel
from .SharedCode import DFsFromExcel

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _xlsx_ or xls formatted file",
        " * review retrieved data & visualisation",
        " * (optional) add pass/fail - default pass",
        " * upload test schemas to PDB",
        " * delete tests from PDB if required"]
#####################
### main part
#####################

class Page6(Page):
    def __init__(self):
        super().__init__("GP Registration", ":microscope: GelPak Registration", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        chnx.DefaultDictChunk(pageDict,{'componentType':"GELPACK",'project':"P"})

        if "compTypeObj" not in pageDict:
            pageDict['compTypeObj']=st.session_state.myClient.get('getComponentTypeByCode',json={'project':pageDict['project'], 'code':pageDict['componentType']})
        subProjs=[x['code'] for x in pageDict['compTypeObj']['subprojects']]
        compTypes=[x['code'] for x in pageDict['compTypeObj']['types']]

        # get test schema
        if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['componentType']):
            pageDict['origSchema'] = st.session_state.myClient.get('generateComponentTypeDtoSample', json={'project':pageDict['project'], 'code':pageDict['componentType'], 'requiredOnly' :True})

        stTrx.DebugOutput("Original *schema*",pageDict['origSchema'])

        pageDict['file']= st.file_uploader("Upload data file", type=["xlsx","xls"])
        stTrx.DebugOutput("Input file",pageDict['file'])


        sheetName='Einzelprojektübersicht'
        if pageDict['file'] is None:
            st.write("No data file set")
            filePath=os.path.realpath(__file__)
            exampleFileName="BARE_MODULE_vendor_example_old.xlsx"
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
                st.write(os.listdir(filePath[:filePath.rfind('/')]))
            df_test=pd.read_excel(filePath[:filePath.rfind('/')]+"/"+exampleFileName, sheet_name=sheetName)
            df_xlsx = to_excel(df_test,sheetName)
            #st.dataframe(df_test)
            st.download_button(label="Download example", data=df_xlsx, file_name=exampleFileName)
            #st.write(pageDict.keys()-['file'])
            for k in pageDict.keys()-['file']:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            st.stop()


        st.write("## Read Data")
        df_head, df_data= DFsFromExcel(pageDict['file'], sheetName)
        st.dataframe(df_data)

        ### input stats
        # get number hybrids === number sensors
        st.write("### Preliminary stats")
        st.write("unique GP ids:",len(df_data['GP_id'].unique()))
        #st.write("number unique chips:",len(df_data['Projekt-_/_Run-Nr'].unique()))

        infra.SelectBox(pageDict,'subType',compTypes,"Select component subType (for "+pageDict['componentType']+")")
        infra.SelectBox(pageDict,'subProj',subProjs,"Select component sub-project (for "+pageDict['project']+")")

        if "compSchemas" not in pageDict.keys() or st.button("Re-read data"):
            pageDict['compSchemas']=[]
            for sn in df_data['GP_id'].unique():
                st.write("Working on *",sn,"*")
                compSchema=pageDict['origSchema'].copy()
                compSchema['institution']=st.session_state.Authenticate['inst']['code']
                compSchema['subproject']=pageDict['subProj']
                compSchema['type']=pageDict['subType']
                compSchema['serialNumber']=sn
                pageDict['compSchemas'].append(compSchema)

        st.write("### Compiled testSchemas")
        st.dataframe(pd.DataFrame(pageDict['compSchemas']))
        stTrx.DebugOutput("compSchemas: ",pageDict['compSchemas'])

        if st.button("Check GELPACK serialNumbers"):
            pageDict['GPList']=[]
            try:
                #pageDict['GPList'].append(st.session_state.myClient.get('getComponent', json={'component':rl['comb']['SN']} ))
                pageDict['GPList']=st.session_state.myClient.get('getComponentBulk', json={'component':[cs['serialNumber'] for cs in pageDict['compSchemas']]})
                if type(pageDict['GPList'])!=type([]):
                    pageDict['GPList']=pageDict['GPList'].data
            except itkX.BadRequest as b:
                st.write("### :no_entry_sign: get component **Unsuccessful** for",rl['comb']['SN'])
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

        if "GPList" in pageDict.keys():
            # st.write(pageDict['GPList'])
            try:
                df_GPList=pd.json_normalize(pageDict['GPList'], sep = "_")
                colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','currentLocation_code','assembled']
                st.dataframe(df_GPList[colz])
            except KeyError:
                st.write("No matching registered components found in PDB.")

        if st.button("Register Components"):
            pageDict['regList']=[]
            for cs in pageDict['compSchemas']:
                st.write(cs)
                st.write("Working on *",cs['serialNumber'],"*")
                ### upload data
                try:
                    regVal=st.session_state.myClient.post('registerComponent', json=cs)
                    st.write("### **Successful Registration**:",regVal['component']['serialNumber'])
                    pageDict['regList'].append(regVal['component']['serialNumber'])
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: Update **Unsuccessful**")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                except TypeError:
                    st.write("Don't have return value :(")

        if "regList" not in pageDict.keys():
            st.stop()
        elif len(pageDict['regList'])<1:
            st.stop()
        else:

            st.write("### Registered components:",len(pageDict['regList']))
            for k,rl in enumerate(pageDict['regList']):
                infra.ToggleButton(pageDict,'togReg_'+str(k),"R"+str(k)+": "+rl)
                #st.write("Component:",rl['comb']['SN'])
                if pageDict['togReg_'+str(k)]:
                    if st.button("delete",key=k):
                        try:
                            delVal=st.session_state.myClient.post('deleteComponent', json={'component':rl} )
                            st.write("**Successful Deletion**:",rl)
                            pageDict['regList'].remove(next(item for item in pageDict['regList'] if item == rl))
                        except itkX.BadRequest as b:
                            st.write(":no_entry_sign: deletion **Unsuccessful**")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
