### standard
#from msilib.schema import Component
from pickle import NONE
from re import X
import streamlit as st
from core.Page import Page
### custom
import numpy as np
import os
import pandas as pd
import ast
import csv
import json
import plotly.graph_objects as go
from io import BytesIO
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
from .SharedCode import to_excel
from .SharedCode import DFsFromExcel

#####################
### useful functions
#####################


infoList=[" * Input formatted xlsx",
            " * Extract data",
            "  - Sensor wafer Ids + slots",
            " * Convert Ids -> ASNs",
            " * Check in PDB",
            " * Non dicing sensor wafer data",
            "  -Add comments ",
            " * Dicing sensor wafer data"
            "  -Disassembly of sensor Wafer",
]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("wafer Disassembly", ":microscope: Wafer Disassembly", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["xls","xlsx"])
        stTrx.DebugOutput("Input file:",pageDict['file'])

        sheetName='Sheet1'
        if pageDict['file'] is None:
            st.write("No data file set")
            filePath=os.path.realpath(__file__)
            exampleFileName="Sensor_Wafers_list.xlsx"
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
                st.write(os.listdir(filePath[:filePath.rfind('/')]))
            df_test=pd.read_excel(filePath[:filePath.rfind('/')]+"/"+exampleFileName, sheet_name=sheetName)
            df_xlsx = to_excel(df_test,sheetName)
            st.download_button(label="Download example", data=df_xlsx, file_name=exampleFileName)
            for k in pageDict.keys()-['file']:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            st.stop()


        st.write("## Read Data")
        df_head, df_data= DFsFromExcel(pageDict['file'], sheetName)
        st.dataframe(df_data)

        ### input stats
        st.write("### Preliminary stats")
        st.write("Number of Sensor Wafers:",len(df_data['Alternative_ID'].unique()))


        st.write("---")
        st.write("## Extract Data")

        stTrx.DebugOutput("Columns in data: ",df_data.columns.to_list())


        if "regList" not in pageDict.keys() or st.button("Read Data"):
            # compile FE_WAFER list to find FE_CHIPs later
            pageDict['regList']=[]
            st.write("### looping over data...")
            for sewafer in df_data['Alternative_ID'].unique():
                pageDict['regList'].append({'comb':None,'parts':[]})
                st.write("working on sensor wafer:",sewafer)
                pageDict['regList'][-1]['parts'].append({'compType':"SENSOR_WAFER",'SN':None,'ID':sewafer})

        st.write("### Extracted collections:")
        st.write(pageDict['regList'])

        st.write("### Converted identifiers:")
        if "converted" not in pageDict.keys() or st.button("convert IDs"):
            pageDict['converted']=True
            status_bar_con=st.progress(0.0)
            status_text_con=st.empty()
            result_text_con=st.empty()
            checks_text_con=st.empty()
            listLen=len(pageDict['regList'])
            ### ASN Conversion
            for e,rl in enumerate(pageDict['regList']):
                for part in rl['parts']:
                    status_bar_con.progress(1.0*e/listLen)
                    if part['compType']=="SENSOR_WAFER":
                        status_text_con.text("working on "+part['compType']+": "+part['ID'])
                        ### translate to ASN
                        try:
                            waferSN=st.session_state.myClient.get('getComponent', json={'component':part['ID'],'alternativeIdentifier':True})['serialNumber']
                            ### checks!
                            try:
                                checkVal=pdbTrx.CheckComponent(waferSN, part['compType'], st.session_state.Authenticate['inst']['code'], st.session_state.debug)
                                if "False" in checkVal:
                                    checks_text_con.text(waferSN+" failed checks: "+checkVal)
                                part['SN']=waferSN
                                result_text_con.text("- Found ASN: "+waferSN)
                            except TypeError:
                                checks_text_con.text("cannot check component: "+waferSN)
                                part['SN']=None
                                pass
                        except KeyError:
                            result_text_con.text("- no ASN found.")
                            part['SN']=None
                            pass
                        except itkX.BadRequest as b:
                            st.write("### :no_entry_sign: get component **Unsuccessful** for",part['ID'])
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8])
                            part['SN']=None
        else:
            st.write("Done. Re-run if required")

        st.write("### Check database for identifiers:")
        if "ExtractList" not in pageDict.keys() or st.button("reset list"):
            status_bar_ids=st.progress(0.0) #st.empty()
            status_text_ids=st.empty()
            listLen=len(pageDict['regList'])
            pageDict['ExtractList']=[]
            for e,c in enumerate(pageDict['regList']):
                for d in c['parts']:
                    loopText="checking SN: "+str(d['SN']) #+", "+str(e+1)+" of "+str(listLen)
                    status_text_ids.text(loopText)
                    status_bar_ids.progress(1.0*e/listLen)
                    if d['SN']==None:
                        continue
                    try:
                        pageDict['ExtractList'].append(st.session_state.myClient.get('getComponent', json={'component':d['SN']} ))
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: get component **Unsuccessful** for",d)
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        else:
            st.write("Done. Re-run if required")

        try:
            df_extractList=pd.json_normalize(pageDict['ExtractList'], sep = "_")
        except AttributeError:
            st.write("No matching registered components found in PDB.")
            st.stop()
        st.write("### Components found in Production Database")
        colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','currentLocation_code']
        stTrx.ColourDF(df_extractList[colz],'type_code')

        stTrx.DebugOutput("Augmented collections: ",pageDict['regList'])

        st.write("---")

        
        if df_data.query('Dicing !="YES"').iterrows():
           st.write("### The following sensor wafers have not diced:")
           st.dataframe(df_data[df_data['Dicing']!='YES'])
           for d in enumerate(df_data[df_data['Dicing']!='YES']['Alternative_ID']):
                if df_extractList.query('alternativeIdentifier=="'+str(d)+'"').iterrows():
                    dsn=st.session_state.myClient.get('getComponent', json={'component':d[1],'alternativeIdentifier':True})
                    for n in df_data[df_data['Alternative_ID']==''+d[1]+'']['Comment']:
                       if n!="nan":
                            try:
                                st.session_state.myClient.post('createComponentComment',json={'component':dsn['id'],'comments':[n]})
                            except itkX.BadRequest:
                                st.write(":no_entry_sign: Add Comment **Unsuccessful**")


        st.write("### Wafer Disassembly :")
        if st.button("Disassemble"):
            if df_data.query('Dicing =="YES"').iterrows():
                st.dataframe(df_data[df_data['Dicing']=='YES'])
                for k in enumerate(df_data[df_data['Dicing']=='YES']['Alternative_ID']):
                    if df_extractList.query('alternativeIdentifier=="'+str(k)+'"').iterrows():
                        ksn=st.session_state.myClient.get('getComponent', json={'component':k[1],'alternativeIdentifier':True})
                        for m in df_data[df_data['Alternative_ID']==''+k[1]+'']['Comment']:
                            if m!="nan":
                              try:
                                 st.session_state.myClient.post('createComponentComment',json={'component':ksn['id'],'comments':[m]})
                              except itkX.BadRequest:
                                st.write(":no_entry_sign: Add Comment **Unsuccessful**")
                        if ksn['children']==None:
                           st.write(ksn['serialNumber'],"The sensor wafer was already diced")
                        elif ksn['children']!=None: 
                          for i in ksn['children']:     
                            if i['component']!=None:
                                try:
                                    if i['component']['alternativeIdentifier'].rsplit("-", 2)[1] in ['1','3']:
                                        try:
                                            disVal=st.session_state.myClient.post('disassembleComponent', json={"parent" : ksn['id'],
                                                                         "child" : i['component']['id']} )
                                            st.write("**Successful disassembled**:{} from {}".format(i['component']['alternativeIdentifier'], ksn['alternativeIdentifier']))
                                            continue
                                        except itkX.BadRequest as b:
                                            st.write(":no_entry_sign: disassembly **Unsuccessful**")
                                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8])
                                        except TypeError:
                                            st.write("Cannot disassemble",i['component']['serialNumber'])
                                except ValueError:
                                   return
                            continue
