### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _csv_ pull test file",
        " * review retrieved data & visualisation",
        " * review analysis checks",
        " * review and edit test schema",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("PCB Metrology", ":microscope: Upload PCB Metrology Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="BARE_MODULE"
        if "testCode" not in pageDict.keys():
            pageDict['testCode']="QUAD_BARE_MODULE_METROLOGY"
        if "stage" not in pageDict.keys():
            pageDict['stage']="BAREMODULERECEPTION"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_P.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testCode']+'"').reset_index(drop=True)
        st.write("### componentTypes and stages for testType=="+pageDict['testCode'])
        st.dataframe(df_stageTest)

        infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
        if pageDict['toggleChanger']:
            infra.SelectBox(pageDict,'componentType',df_stageTest['compType'].unique(),"Select componentType code:")
            infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list(),"Select stage code:")


        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])


        selMode=st.radio("Select input file style:",["Glasgow","Argonne"])

        if selMode=="Glasgow":
            st.write("__Using Glasgow style input__")
            ### upload measurement file
            filePath="/".join(os.path.realpath(__file__).split('/')[0:-1])
            exampleFileName="BM_METROLOGY_GL_example.csv"
            df_in=chnx.UploadFileChunk(pageDict,["csv"],filePath+"/"+exampleFileName, None, None, None, -1)

        if selMode=="Argonne":
            st.write("__Using Argonne style input__")

            ### upload measurement file
            filePath="/".join(os.path.realpath(__file__).split('/')[0:-1])
            exampleFileName="BM_METROLOGY_ANL_example.txt"
            df_in=chnx.UploadFileChunk(pageDict,["txt"],filePath+"/"+exampleFileName, None, None, '|', -1)


        st.write("## Read Data")
        st.write(df_in)

        ### TODO add smartscope option and analysis

        # get test schema
        if "testSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testCode']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['testSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testCode']})#, 'requiredOnly':True})
            # hack to coordinate with chnx.RegChunk
        # add defaults
        pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
        stTrx.DebugOutput("Original *schema*:",pageDict['testSchema'])

        ### wipe results
        stTrx.SetDictValsNone(pageDict['testSchema']['results'])

        if selMode=="Glasgow":
            ### update json
            # header
            st.write("Read serial number in file")
            snStr=df_in.query('a.str.contains("Serial")', engine='python' )['b'].values[0]
            pageDict['testSchema']['component']=pdbTrx.GetSNFromString(pageDict, snStr)
            dateStr=df_in.query('a.str.contains("Date")', engine='python' )['b'].values[0]
            pageDict['testSchema']['date']=datetime.strptime(dateStr, "%d/%m/%Y %H:%M").strftime("%Y-%m-%dT%H:%MZ")
            # properties
            pageDict['testSchema']['properties']['ANALYSIS_VERSION']="webApp"
            # instrument
            if df_in.query('a.str.contains("perator")', engine='python' ).empty:
                pageDict['testSchema']['properties']['OPERATOR']="Unknown"
            else:
                pageDict['testSchema']['properties']['OPERATOR']=df_in.query('a.str.contains("perator")', engine='python' )['b'].to_list()[0]
            # operator
            if df_in.query('a.str.contains("nstrument")', engine='python' ).empty:
                pageDict['testSchema']['properties']['INSTRUMENT']="Unknown"
            else:
                pageDict['testSchema']['properties']['INSTRUMENT']=df_in.query('a.str.contains("nstrument")', engine='python' )['b'].to_list()[0]
            # results
            pageDict['testSchema']['results']['SENSOR_X']=df_in.query('a.str.contains("X")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['SENSOR_Y']=df_in.query('a.str.contains("Y")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['SENSOR_THICKNESS']=df_in.query('a.str.contains("GA")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['SENSOR_THICKNESS_STD_DEVIATION']=df_in.query('a.str.contains("GA")', engine='python' )['b'].astype(float).std()['b'].to_list()[0]
            # results
            pageDict['testSchema']['results']['FECHIPS_X']=df_in.query('a.str.contains("X")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['FECHIPS_Y']=df_in.query('a.str.contains("Y")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['FECHIP_THICKNESS']=df_in.query('a.str.contains("GA")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['FECHIP_THICKNESS_STD_DEVIATION']=df_in.query('a.str.contains("GA")', engine='python' )['b'].astype(float).std()
            # results
            pageDict['testSchema']['results']['BARE_MODULE_THICKNESS']=df_in.query('a.str.contains("X")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['BARE_MODULE_THICKNESS_STD_DEVIATION']=df_in.query('a.str.contains("Y")', engine='python' )['b'].astype(float).mean()

        if selMode=="Argonne":
            ### update json
            # header
            st.write("Read serial number from file name")
            pageDict['testSchema']['component']=pdbTrx.GetSNFromString(pageDict,pageDict['file'].name)
            dateStr=df_in.query('a.str.contains("DATE")', engine='python' )['b'].values[0]
            dateStr+=" "+df_in.query('a.str.contains("TIME")', engine='python' )['b'].values[0]
            pageDict['testSchema']['date']=datetime.strptime(dateStr, "%m:%d:%y %H:%M:%S").strftime("%Y-%m-%dT%H:%MZ")
            # properties
            pageDict['testSchema']['properties']['ANALYSIS_VERSION']="webApp "+chnx.GetCommitSHA()
            # instrument
            if df_in.query('a.str.contains("perator")', engine='python' ).empty:
                pageDict['testSchema']['properties']['OPERATOR']="Unknown"
            else:
                pageDict['testSchema']['properties']['OPERATOR']=df_in.query('a.str.contains("perator")', engine='python' )['b'].to_list()[0]
            # operator
            if df_in.query('a.str.contains("nstrument")', engine='python' ).empty:
                pageDict['testSchema']['properties']['INSTRUMENT']="Unknown"
            else:
                pageDict['testSchema']['properties']['INSTRUMENT']=df_in.query('a.str.contains("nstrument")', engine='python' )['b'].to_list()[0]
            # results
            {
            "SENSOR_X": -1,
            "SENSOR_Y": -1,
            "SENSOR_THICKNESS": -1,
            "SENSOR_THICKNESS_STD_DEVIATION": -1,
            "FECHIPS_X": -1,
            "FECHIPS_Y": -1,
            "FECHIP_THICKNESS": -1,
            "FECHIP_THICKNESS_STD_DEVIATION": -1,
            "BARE_MODULE_THICKNESS": -1,
            "BARE_MODULE_THICKNESS_STD_DEVIATION": -1
            }


        ### analysis
        # X-Y envelop
        st.write(f"__Analyse X-Y Dimensions...__")
        st.write("🚧 under construction 🚧")



        st.stop()

        
        ### summy for the moment - should remove from testType structure and omit later
        pageDict['testSchema']['results']['DIAMETER_DOWEL_HOLE_A']=-999
        pageDict['testSchema']['results']['WIDTH_DOWEL_SLOT_B']=-999


        ### Review and edit
        chnx.ReviewAndEditChunk(pageDict,'testSchema')

        # st.write("### :construction: Page under construction :construction:")
        # st.stop()

        # upload(!) test schema: change get --> post
        chnx.RegChunk(pageDict, "TestRun", "testSchema", "file")
