### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import plotly.graph_objects as go
from datetime import datetime
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx

#####################
### useful functions
#####################

def GetCompsOfType(df_list,compType):
    df_out=df_list.query('componentType_code=="'+compType+'"').sort_values(by=['serialNumber'])
    st.write(compType,"found at institution:",len(df_out))
    return df_out

infoList=[  " * Get OEC_FOAM_BLOCKSs and OEC_CARBON_FOAM_TRAPEZOIDS_HRs at institute",
            " * For each OEC_CARBON_FOAM_TRAPEZOIDS_HRs select OEC_FOAM_BLOCKS",
            " * Assemble OEC_FOAM_BLOCKSs to OEC_CARBON_FOAM_TRAPEZOIDS_HRs (1:1)",
            "  - Disassemble optional"]
#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("CFT Assembly", ":microscope: Carbon Foam Tiles Assembly", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ### select components at institute
        #compTypes=["BARE_MODULE","GELPACK"]
        compTypes=["OEC_CARBON_FOAM_TRAPEZOIDS_HR","OEC_FOAM_BLOCKS"]
        st.write("## Get components at institute")
        if st.button("reset list") or "compList" not in list(pageDict.keys()): # check list
            st.write("**Getting** "+", ".join(compTypes)+" components for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            pageDict['compList']=st.session_state.myClient.get('listComponents',json={'currentLocation':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'], 'componentType':compTypes })
            if type(pageDict['compList'])==type([]):
                pageDict['compList']=pageDict['compList'].data
        else:
            st.write("**Got** "+", ".join(compTypes)+" components for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            st.write("(colour--> _",'componentType_code',"_)")
        df_compList=pd.json_normalize(pageDict['compList'], sep = "_")
        colz=['serialNumber','componentType_name','componentType_code','assembled']

        if df_compList.empty:
            st.write("No components found")
            st.stop()

        ### make all columns strings
        for c in colz:
            df_compList[c]=df_compList[c].astype(str)

        infra.Radio(pageDict,'removeAss',['all','only assembled','not assembled'],"Keep components in list?")
        if pageDict['removeAss']=="not assembled":
            df_compList=df_compList.query('assembled=="False"')
        if pageDict['removeAss']=="only assembled":
            df_compList=df_compList.query('assembled=="True"')

        try:
            st.dataframe(df_compList[colz].sort_values(by=['serialNumber']).style.apply(stTrx.ColourCells, df=df_compList[colz], colName='componentType_code', flip=True, axis=1))
        except KeyError:
            st.write("No componentTypes found:",",".join(compTypes))
            st.stop()

        st.write("---")
        st.write("## Select Foam Blocks & CF Trapezoids")

        #occupancyList=[{'type':"singles",'count':9},{'type':"quads",'count':4}]
        #infra.SelectBox(pageDict, 'occupancy', occupancyList, 'Select type of Bar Modules','type')

        # df_blocks=GetCompsOfType(df_compList,"GELPACK")
        # df_CFTs=GetCompsOfType(df_compList,"BARE_MODULE")
        df_blocks=GetCompsOfType(df_compList,"OEC_FOAM_BLOCKS")
        df_CFTs=GetCompsOfType(df_compList,"OEC_CARBON_FOAM_TRAPEZOIDS_HR")

        #list of dictionaries
        #[{'comb':{'SN':,'compType':},'parts':[{'SN':,'compType':}]}]

        #st.write("### Up to "+str(pageDict['occupancy']['count'])+" components per GelPack")
        if "CFTAss" not in pageDict.keys() or st.button("reset CFT dictionary"):
           pageDict['CFTAss']=[]
           for tc in [x for x in pageDict.keys() if "togComp_" in x]:
               pageDict[tc]=False

        for i,row in df_CFTs.iterrows():
            sn=str(row['serialNumber'])
            altID=str(row['alternativeIdentifier'])
            infra.ToggleButton(pageDict,'togComp_'+sn,f"{i}: {sn} ({altID})")
            if pageDict['togComp_'+sn]:
                selBlock=st.selectbox("Foam block:",df_blocks['serialNumber'].to_list(), key=sn+'_blocks')
                if sn in [x['comb']['SN'] for x in pageDict['CFTAss']]:
                    ind=[x['comb']['SN'] for x in pageDict['CFTAss']].index(sn)
                    if pageDict['CFTAss'][ind]['parts'][0]['SN']==selBlock:
                        st.write("Block already selected _this_ CFT")
                    else:
                        if selBlock in [y['SN'] for x in pageDict['CFTAss'] for y in x['parts']]:
                            st.write("Block already selected for CFT. Choose again")
                        else:
                            pageDict['CFTAss'][ind]['parts']=[{'SN':selBlock,'compType':"OEC_FOAM_BLOCKS"}]
                else:
                    if selBlock in [y['SN'] for x in pageDict['CFTAss'] for y in x['parts']]:
                        st.write("Block already selected for CFT. Choose again")
                    else:
                        pageDict['CFTAss'].append({'comb':{'SN':sn,'compType':"OEC_CARBON_FOAM_TRAPEZOIDS_HR"},'parts':[{'SN':selBlock,'compType':"OEC_FOAM_BLOCKS"}]})
            ### remove if exists
            else:
                if sn in [x['comb']['SN'] for x in pageDict['CFTAss']]:
                    pageDict['CFTAss'].remove(next(item for item in pageDict['CFTAss'] if item['comb']['SN'] == sn))

        st.write("selected components:",len(pageDict['CFTAss']))
        stTrx.DebugOutput("selected components: ",pageDict['CFTAss'])

        st.dataframe([{'CFT':x['comb']['SN'],'block':x['parts'][0]['SN']} for x in pageDict['CFTAss']])

        st.write("---")

        ### ASSEMBLE PARTS
        if st.button("Assemble"):
            for ba in pageDict['CFTAss']:
                # default dictionary assembly flag to false
                if "assembly" not in ba.keys():
                    ba['assembly']=False
                ### assemble function
                pdbTrx.AssembleComponent(ba,True)

        myAsss=[]
        try:
            myAsss=[x['assembly'] for x in pageDict['CFTAss'] if x['assembly']==True]
        except KeyError:
            pass

        st.write("### Assembled CFT components:",len(myAsss))
        if len(myAsss)==0:
            st.stop()

        for k,ba in enumerate(pageDict['CFTAss']):
            if ba['assembly']==False: continue
            infra.ToggleButton(pageDict,'togAss_'+str(k),"A"+str(k)+": "+ba['comb']['SN'])
            #st.write("Component:",ba['comb']['SN'])
            if pageDict['togAss_'+str(k)]:
                combComp=st.session_state.myClient.get('getComponent', json={"component" : ba['comb']['SN']} )
                st.write("Children:",[x['component']['serialNumber'] for x in combComp['children'] if x['component']!=None])
                stTrx.DebugOutput("Children details: ",combComp['children'])

                positions=[]
                for x in combComp['children']:
                    if x['component']==None: continue
                    positions.append({'x':int(x['order'])/2,'y':int(x['order'])%2,'text':x['component']['serialNumber']})
                stTrx.VisOrientation(pd.DataFrame(positions))

                if st.button("disassemble",key=k+1000):
                    pdbTrx.DisassembleComponent(combComp)
