### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import copy
import json
from pathlib import Path
import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
#####################

def DeleteEOSLink(myClient, objType, objCode, attachCode):
    try:
        delObj=myClient.post("delete"+objType[:1].upper()+objType[1:]+"Attachment", json={objType: objCode, "code": attachCode})
        st.write(f"Successful deletion: {delObj}")
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: deleting **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks


infoList=[" * Visual Inspection Uploads",
        " * Integrated test registration and file attachment"]
#####################
### main part
#####################

class Page9(Page):
    def __init__(self):
        super().__init__("Visual Inspection", "📷 Visual Inspection Uploads", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        st.write("#")
        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULE"
        if "testType" not in pageDict.keys():
            pageDict['testType']="VISUAL_INSPECTION"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        st.write("## Register Visual Inspection Test")

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_P.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_vis=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testType']+'"').reset_index(drop=True)
        df_stages=df_vis.query('compType=="'+pageDict['componentType']+'"').sort_values(by=['stageOrder']).reset_index(drop=True)

        if st.checkbox("See list of stages with Visual Inspections?"):
            st.write(f"#### Stages with Visual Inspection for {pageDict['componentType']}")

            if st.checkbox("Change componentType?"):
                infra.SelectBox(pageDict,'componentType',df_vis['compType'].unique(),"Select componentType code:")
                df_stages=df_vis.query('compType=="'+pageDict['componentType']+'"').sort_values(by=['stageOrder']).reset_index(drop=True)
            # st.write("#### Stages for testType=="+pageDict['testType'])
            st.write(df_stages)

        sel_stage=st.selectbox('Select stage',sorted(df_stages['stage'].unique()))
        pageDict['stage']=sel_stage

        # st.stop()

        #################
        ### Register test
        #################

        # get test schema
        if "origSchema" not in pageDict.keys() or "testSchema" not in pageDict.keys() or pageDict['origSchema']['testType']!=pageDict['testType'] or st.button("Reset Schema: "+pageDict['testType']+"@"+sel_stage+" for "+pageDict['componentType']):
            pageDict['origSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType'], 'requiredOnly':True})
            pageDict['fullSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType'], 'requiredOnly':False})
            pageDict['testSchema']=copy.deepcopy(pageDict['origSchema'])
        pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
        dateStr=datetime.datetime.now().strftime("%Y-%m-%dT%H:%MZ")
        pageDict['testSchema']['date']=pdbTrx.TimeStampConverter(dateStr, "%Y-%m-%dT%H:%MZ")

        # show schema
        stTrx.DebugOutput("Original *schema*:",pageDict['origSchema'])
        if st.checkbox("Show PDB schema?",value=False):
            if "only" in st.radio('See parameters:',["only required","include options"]):
                st.write(pageDict['origSchema'])
            else:
                st.write(pageDict['fullSchema'])

        ### upload schema settings
        st.write("---")
        st.write("### Upload Test Settings")

        sel_enter=st.radio("Test data entry method:",['manual selection','pre-formatted CSV','pre-formatted json'])

        ### manual entry
        if "manual" in sel_enter.lower():
            st.write("__Manual entry__")
            
            chnx.SelectComponentChunk(pageDict)
            testSchema= chnx.SelectStructure(pageDict['origSchema'], pageDict['fullSchema'], pageDict['testSchema'], offerCSV=False)
            testSchema['component']=pageDict['comp']['serialNumber']

        ### Upload JSON
        elif "json" in sel_enter.lower():
            st.write("__Json entry__")

            st.write("A pre-formatted _json_ can be uploaded if available.")
            ### upload measurement file
            filePath="/".join(os.path.realpath(__file__).split('/')[0:-1])
            exampleFileName="VI_example.json"
            with open(filePath+"/"+exampleFileName) as f:
                jsonSchema = json.load(f)
            inJson = chnx.UploadJSONChunk(pageDict, exampleDict=jsonSchema, exampleName="VI_example.json", ignoreSelect=True)
            testSchema = inJson
            # st.write(inJson)
            
        ### Upload csv
        elif "csv" in sel_enter.lower():
            st.write("__CSV entry__")

            st.write("A pre-formatted _CSV_ can be uploaded if available.")
            ### upload measurement file
            filePath="/".join(os.path.realpath(__file__).split('/')[0:-1])
            exampleFileName="VI_example.csv"
            df_input = chnx.UploadFileChunk(pageDict, ['csv'], filePath+"/"+exampleFileName)
            if type(df_input)==type(pd.DataFrame()):
                # pageDict['testSchema'] = df_input
                colz=['component','testType','institution','date','runNumber','passed','problems']
                testSchema = df_input[colz].to_dict(orient='records')[0]
                testSchema['results']={}
                testSchema['properties']={}
                for col in df_input.columns:
                    if col in colz:
                        continue
                    if "key" not in col:
                        continue
                    jKey=df_input[col].values[0]
                    jVal=df_input[col.replace('key','value')].values[0]
                    if "result" in col and "key" in col:
                        testSchema['results'][jKey]=jVal
                    if "property" in col and "key" in col:
                        testSchema['properties'][jKey]=jVal
            else:
                st.stop()

        else:
            st.write("Select option")
            st.stop()

        pageDict['testSchema']=testSchema
        ### check component set and offer stage check
        if "20U" not in testSchema['component']:
            st.write("Component not recognised")
            st.stop()
        
        if st.checkbox("Check component uploads?"):
            chnx.StageCheck(pageDict)

        # st.write(pageDict['testSchema'])

        #################
        ### Upload Test
        #################

        ### upload schema to PDB
        st.write("---")
        ### Review and edit
        chnx.ReviewAndEditChunk(pageDict,'testSchema')

        # upload(!)
        chnx.RegChunk(pageDict, "TestRun", "testSchema")

        if "upVal" not in pageDict.keys():
            st.stop()
        
        st.write("Upload response:")
        st.json(pageDict['upVal'], expanded=False)
        if not st.checkbox("Attach file to test?", value=False):
            st.stop()


        #################
        ### Input file
        #################

        st.write("---")
        st.write("### Upload Attachment")

        if "uploadList" in pageDict.keys():
            st.write(f"Files for upload: {len(pageDict['uploadList'])}")
        else:
            st.write("Add files for upload.")
            pageDict['uploadList']=[]

        if st.button("Clear list?"):
            st.write("List Cleared")
            pageDict['uploadList']=[]
        
        imageExts=['jpg','jpeg','png','tif','tiff']
        fileExts=['dat','sta','csv']
        uploaded_file = st.file_uploader("Upload image file", type=imageExts+fileExts )

        if uploaded_file is not None: 
            st.write("Got file "+uploaded_file.name)
            # pageDict['file']=uploaded_file
        # else:
        #     st.write("Upload file")
        #     st.stop()
    
            if any(ie in uploaded_file.type for ie in imageExts):
                st.write(" - recognise image")
                if st.checkbox("See image?"):
                    # st.write("details:",pageDict['file'])
                    # st.image(pageDict['file'])
                    st.write("details:",uploaded_file)
                    st.image(uploaded_file)
            else:
                if st.checkbox("See file?"):
                    # st.write("details:",pageDict['file'])
                    # st.write(pageDict['file'].read())
                    st.write("details:",uploaded_file)
                    st.write(uploaded_file)

            if st.button("Add to upload list"):
                # pageDict['uploadList'].append(pageDict['file'])
                pageDict['uploadList'].append(uploaded_file)
                st.write("Adding file to list.")
                st.write(f" - total files now: {len(pageDict['uploadList'])}")

            if len(pageDict['uploadList'])<1:
                st.write("Add files for upload.")
                st.stop()
            else:
                if st.checkbox("See upload list?"):
                    st.write(f"Files for upload {len(pageDict['uploadList'])}:")
                    for e,upFile in enumerate(pageDict['uploadList'],1):
                        st.write(f"{e}. {upFile.name}")
                        if st.button("Remove file", key=f"file_{e}"):
                            pageDict['uploadList'].remove(upFile)
                            st.write("Removed file. Uncheck list to update")

            st.write("---")
            st.write("### Upload Details")

            if len(pageDict['uploadList'])>1:
                st.write("For multiple file uploads a tar file is recommended.")
                # if st.checkbox("Use tar file?",value=True):
                #     if "tarFile" not in pageDict.keys() or st.button("generate tar file"):
                #         st.write("🚧 __Under Construction__ 🚧")
                #         st.write("Please unselect")
                #         st.stop()

            if "data_json" not in pageDict.keys():
                pageDict['data_json'] = {
                    "title": f"VI image attachment",
                    "description": "from webApp "+datetime.datetime.now().strftime("%Y-%m-%dT%H:%MZ"),
                    "url": "dummy",
                    "type": "file",
                    "testRun": pageDict['upVal']['testRun']['id']
                }

            st.write("#### meta-data")
            ### review and edit
            chnx.ReviewAndEditChunk(pageDict,'data_json', False)

            if st.button("Upload Image"):
                st.write("Uploading files:",len(pageDict['uploadList']))

                # if tar file used 
                if "tarFile" in pageDict.keys():
                    st.write(f"__Upload file: {tar_file_name}__")
                    files = {"data": itkdb.utils.get_file_components({"data": open(tar_file_name, 'rb')})}
                
                    # do upload
                    try:  
                        response = st.session_state.myClient.post("createTestRunAttachment", data=pageDict['data_json'], files=files)
                        pageDict['imgVal']=response
                        st.write(f"{pageDict['imgVal']}")
                        st.balloons()
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: File Upload **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                
                ### non-tar file
                else:
                    for upFile in pageDict['uploadList']:         
                        st.write(f"Upload file: {upFile.name}")
                        files = {"data": itkdb.utils.get_file_components({"data": upFile})}
                        pageDict['data_json']['title']=upFile.name
                        # do upload
                        try:   
                            response = st.session_state.myClient.post("createTestRunAttachment", data=pageDict['data_json'], files=files)
                            pageDict['imgVal']=response
                            st.write(f"{pageDict['imgVal']}")
                            st.balloons()
                        except itkX.BadRequest as b:
                            st.write("### :no_entry_sign: File Upload **Unsuccessful**")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

            if "imgVal" in pageDict.keys():
                if st.checkbox("Delete last attachment?"):
                    st.write(f"Double check: delete attachment")
                    if st.button("Delete!"):
                        DeleteEOSLink(st.session_state.myClient, "testRun", pageDict['upVal']['testRun']['id'], pdbTrx.FindKey(pageDict['imgVal'],'code'))
                        del pageDict['imgVal']