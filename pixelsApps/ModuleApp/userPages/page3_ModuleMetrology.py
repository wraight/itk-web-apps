### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _csv_ pull test file",
        " * review retrieved data & visualisation",
        " * review analysis checks",
        " * review and edit test schema",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Module Metrology", ":microscope: Upload Module Metrology Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        st.write("#")
        st.write("## Input file")
        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULE"
        if "testCode" not in pageDict.keys():
            pageDict['testCode']="QUAD_MODULE_METROLOGY"
        if "stage" not in pageDict.keys():
            pageDict['stage']="MODULE/ASSEMBLY"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_P.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testCode']+'"').reset_index(drop=True)
        st.write("### componentTypes and stages for testType=="+pageDict['testCode'])
        st.dataframe(df_stageTest)

        st.write("**NB Currently only _QUAD_ modules supported**")

        infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
        if pageDict['toggleChanger']:
            infra.SelectBox(pageDict,'componentType',df_stageTest['compType'].unique(),"Select componentType code:")
            infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list(),"Select stage code:")

        if st.checkbox("See link to Drawing"):
            st.markdown("Thick uncoated module [drawing](https://edms.cern.ch/ui/file/2363543/10/AT2-0000020505_THICK_UNCOATED_MODULE_v11.PDF)")

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])


        selMode=st.radio("Select input file style:",["Glasgow","Argonne"])

        if selMode=="Glasgow":
            st.write("__Using Glasgow style input__")
            ### upload measurement file
            filePath="/".join(os.path.realpath(__file__).split('/')[0:-1])
            exampleFileName="MODULE_METROLOGY_GL_example.csv"
            df_in=chnx.UploadFileChunk(pageDict,["csv"],filePath+"/"+exampleFileName, None, None, None, -1)

        if selMode=="Argonne":
            st.write("__Using Argonne style input__")

            ### upload measurement file
            filePath="/".join(os.path.realpath(__file__).split('/')[0:-1])
            exampleFileName="MODULE_METROLOGY_ANL_example.txt"
            df_in=chnx.UploadFileChunk(pageDict,["txt"],filePath+"/"+exampleFileName, None, None, '|')


        st.write("## Read Data")
        st.write(df_in)

        st.write("Checking \"Notes\"")
        hacks=stTrx.CheckForHacks(df_in, "Notes")

        ### TODO add smartscope option and analysis

        # get test schema
        if "testSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testCode']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['testSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testCode']})#, 'requiredOnly':True})
            # hack to coordinate with chnx.RegChunk
        # add defaults
        pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
        stTrx.DebugOutput("Original *schema*:",pageDict['testSchema'])

        ### wipe results
        stTrx.SetDictValsNone(pageDict['testSchema']['results'])


        if selMode=="Glasgow":
            ### update json
            # header
            st.write("Read serial number in file")
            snStr=df_in.query('a.str.contains("Serial")', engine='python' )['b'].values[0]
            pageDict['testSchema']['component']=pdbTrx.GetSNFromString(pageDict, snStr)
            dateStr=df_in.query('a.str.contains("Date")', engine='python' )['b'].values[0]
            pageDict['testSchema']['date']=datetime.strptime(dateStr, "%d/%m/%Y %H:%M").strftime("%Y-%m-%dT%H:%MZ")
            # properties
            pageDict['testSchema']['properties']['ANALYSIS_VERSION']="webApp "+chnx.GetCommitSHA()
            # instrument
            if df_in.query('a.str.contains("perator")', engine='python' ).empty:
                pageDict['testSchema']['properties']['OPERATOR']="Unknown"
            else:
                pageDict['testSchema']['properties']['OPERATOR']=df_in.query('a.str.contains("perator")', engine='python' )['b'].to_list()[0]
            # operator
            if df_in.query('a.str.contains("nstrument")', engine='python' ).empty:
                pageDict['testSchema']['properties']['INSTRUMENT']="Unknown"
            else:
                pageDict['testSchema']['properties']['INSTRUMENT']=df_in.query('a.str.contains("nstrument")', engine='python' )['b'].to_list()[0]
            # results
            pageDict['testSchema']['results']['DISTANCE_PCB_BARE_MODULE_TOP_LEFT']=[df_in.query('a.str.contains("TL-X")', engine='python' )['b'].astype(float).values[0]]
            pageDict['testSchema']['results']['DISTANCE_PCB_BARE_MODULE_TOP_LEFT'].append(df_in.query('a.str.contains("TL-Y")', engine='python' )['b'].astype(float).values[0])
            pageDict['testSchema']['results']['DISTANCE_PCB_BARE_MODULE_BOTTOM_RIGHT']=[df_in.query('a.str.contains("BR-X")', engine='python' )['b'].astype(float).values[0]]
            pageDict['testSchema']['results']['DISTANCE_PCB_BARE_MODULE_BOTTOM_RIGHT'].append(df_in.query('a.str.contains("BR-Y")', engine='python' )['b'].astype(float).values[0])
            pageDict['testSchema']['results']['ANGLE_PCB_BM']=df_in.query('a.str.contains("ngle")', engine='python' )['b'].astype(float).to_list()[0] # provided by user (i.e. no formaula!)
            pageDict['testSchema']['results']['AVERAGE_THICKNESS']=[] 
            pageDict['testSchema']['results']['STD_DEVIATION_THICKNESS']=[]
            for ga in ["GA1","GA2","GA3","GA4"]:
                queryStr='a.str.contains("'+ga+'")'
                pageDict['testSchema']['results']['AVERAGE_THICKNESS'].append( df_in.query(queryStr, engine='python' )['b'].astype(float).mean() )
                pageDict['testSchema']['results']['STD_DEVIATION_THICKNESS'].append( df_in.query(queryStr, engine='python' )['b'].astype(float).std() )
            pageDict['testSchema']['results']['THICKNESS_VARIATION_PICKUP_AREA']=df_in.query('a.str.contains("GA")', engine='python' )['b'].astype(float).std()
            pageDict['testSchema']['results']['HV_CAPACITOR_THICKNESS']=df_in.query('a.str.contains("HV")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['THICKNESS_INCLUDING_POWER_CONNECTOR']=df_in.query('a.str.contains("FTM")', engine='python' )['b'].astype(float).mean()

        if selMode=="Argonne":
            st.write("🚧 under construction 🚧")


        if hacks!=None and len(hacks)>1:
            st.write("### Some quick manipulations 🛠")
            scale=1
            if "scale" in [h.lower() for h in hacks]:
                st.write("🧑‍🏭 Scaling values by 1000 (mm ➡ µm)")
                scale=1000
                for k,v in pageDict['testSchema']['results'].items():
                    if "float" in str(type(v)) or "int" in str(type(v)):
                        ### skip angle
                        if k=="ANGLE_PCB_BM":
                            continue
                        st.write(f" - scaling {k}")
                        pageDict['testSchema']['results'][k]*=scale
                    elif type(v)==type([]):
                        for e,r in enumerate(v):
                            if "float" in str(type(r)) or "int" in str(type(r)):
                                st.write(f" - scaling {k} element {e}")
                                pageDict['testSchema']['results'][k][e]*=scale
                    else:
                        pass
        
            if "offset" in [h.lower() for h in hacks]:
                st.write("🧑‍🏭 Translating TL, BR coordinates by 7µm in _x_ and 100µm in _y_")
                for pos in ['DISTANCE_PCB_BARE_MODULE_TOP_LEFT', 'DISTANCE_PCB_BARE_MODULE_BOTTOM_RIGHT']:
                    st.write(f" - move {pos}")
                    pageDict['testSchema']['results'][pos][0]+=7
                    pageDict['testSchema']['results'][pos][1]+=100
                

        if -999 in pageDict['testSchema']['results'].values():
            st.write("Analysis code is not yet complete. Please contact maintainer.")
            st.stop()

# outer system module	
# dimension(mm)	  +ve	  -ve
# TL-X	
# 2.212	0.1	0.1
# TL-Y	
# 0.65	0.1	0.1
# BR-X	
# 2.212	0.1	0.1
# BR-Y	
# 0.65	0.1	0.1
# Thickness	
# 0.616	0.155	0.105
# FTM thickness	1.936	0.295	0.105
# HV-cap thickness	2.266	0.315	0.255

# Inner system module	
# TL-X	
# 2.212	0.1	0.1
# TL-Y	
# 0.65	0.1	0.1
# BR-X	
# 2.212	0.1	0.1
# BR-Y	
# 0.65	0.1	0.1
# Thickness	
# 0.566	0.155	0.1
# FTM thickness	1.886	0.295	0.105
# HV-cap thickness	2.216	0.315	0.255


        ### Review and edit
        chnx.ReviewAndEditChunk(pageDict,'testSchema')

        # st.write("### :construction: Page under construction :construction:")
        # st.stop()

        # upload(!) test schema: change get --> post
        chnx.RegChunk(pageDict, "TestRun", "testSchema", "file")
