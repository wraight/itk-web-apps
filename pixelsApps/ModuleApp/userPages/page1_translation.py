### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import copy
import altair as alt
import numpy as np
import os
from io import StringIO
import io
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################

### get ASN for FE_CHIP
def GetFE_SN(compList,waferID,pos,prefix="20UPGFC"):
#     feComp=st.session_state.myClient.get('getComponent', json={'component':waferID,'alternativeIdentifier':True})
#     waferSN=feComp['serialNumber']
    waferObj=next((item for item in compList if item['alternativeIdentifier'] == waferID), None)

    if waferObj==None:
        #st.write("no ASN found for",waferID)
        return None
    waferSN=waferObj['serialNumber']
    st.write("Wafer ASN:",waferSN)
    if st.session_state.debug:
        st.write('serialNumber:',waferSN,"-->",waferSN[-3:])
        st.write(int(waferSN[-3:]),"-->",hex(int(waferSN[-3:])))
    combStr=str(hex(int(waferSN[-3:])))[-3:]+pos.replace('-','')
    if st.session_state.debug:
        st.write(hex(int(waferSN[-3:])),"+",pos,"-->",combStr)
    finalStr=prefix+str(int(combStr, 16)).zfill(7)
    if st.session_state.debug:
        st.write(combStr,"-->",int(combStr, 16),"-->",finalStr)
    return finalStr

### Sensor selections 
### copied form https://edms.cern.ch/ui/file/2649105/1/Module_and_subcomponents_serial_numbers_v2_docx_cpdf.pdf
def GetCodeTable(codeStr):
    
    if "vendor" in codeStr.lower():
        return [{"code": "V1","value": "Advacam"},
                {"code": "V2","value": "HLL"},
                {"code": "V3","value": "FBK, planar"},
                {"code": "V4","value": "HPK"},
                {"code": "V5","value": "LFoundry"},
                {"code": "V6","value": "MICRON"},
                {"code": "V7","value": "CNM"},
                {"code": "V8","value": "FBK, 3D"},
                {"code": "V9","value": "SINTEF"},
                {"code": "","value": "Dummy"} ]
    elif "type" in codeStr.lower():
        return [{"code": "0","value": "RD53A (test structure)"},
                {"code": "1","value": "Single"},
                {"code": "2","value": "Halfmoon"},
                {"code": "3","value": "Quad"},
                {"code": "4","value": "Planar diode (test structure)"},
                {"code": "5","value": "Strip (test structure)"},
                {"code": "6","value": "Mini-sensor (test structure)"},
                {"code": "7","value": "Inter pixel capacitance (test structure)"},
                {"code": "8","value": "Biasing test structure (test structure)"},
                {"code": "9","value": "3D diode (test structure)"} ]
    elif "prod" in codeStr.lower():
        return [{"code": "0","value": "RD53A"},
                {"code": "1","value": "ITkpix_v1 (RD53B ATLAS)"}]
    else:
        st.write("❗ don't understand code table?!")
        return None

def MakeAltId():

    st.write("Please follow the serial number [document](https://edms.cern.ch/ui/file/2649105/1/Module_serial_numbers_v2_docx_cpdf.pdf).")
    st.write(" - In particular p.10-12")

    st.write("**alternative identifier: VX-Y-WWWWWW-D-NNN**")
    st.write("For example: micron (VX=V6), production (Y=1), quads (D=3)")
    st.write("3528-1-C --> V6-1-352801-3-003")

    ### make selections
    # vendor
    selVend=st.selectbox(f'Select vendor:',[item['value'] for item in GetCodeTable("vendor")])
    ### match value-->code in list
    matchVend=next((item['code'] for item in GetCodeTable("vendor") if item['value'] == selVend), None)
    # production
    selProd=st.selectbox(f'Select production:',[item['value'] for item in GetCodeTable("prod")])
    ### match value-->code in list
    matchProd=next((item['code'] for item in GetCodeTable("prod") if item['value'] == selProd), None)
    # type
    selType=st.selectbox(f'Select type:',[item['value'] for item in GetCodeTable("type")])
    ### match value-->code in list
    matchType=next((item['code'] for item in GetCodeTable("type") if item['value'] == selType), None)

    return matchVend, matchProd, matchType

infoList=["Translating identifires to Atlas serial number",
        " * select FE, sensor or PCB",
        " * input identifier to match to ASN"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Translation", ":microscope: Translate identifiers to ASN", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        st.write("#")
        st.write("## Type of Translation")

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULE"
        #if "project" not in pageDict.keys():
        pageDict['project']=st.session_state.Authenticate['proj']['code']

        # selection of subprojects and types
        infra.Radio(pageDict,'source',['Front End Tile: search for tile by wafer and position','Front End Wafer: search for modules made from FE wafer','Sensor: generate tile serialNumber/alternativeIdentifier from wafer and position','PCB: get component info. from alternativeIdentifier'],"Translation source?")

        pageDict['source']=pageDict['source'].split(':')[0]
        st.write("### ",pageDict['source'],"...")

        ##################
        ### SENSOR part
        ##################
        if "sensor" in pageDict['source'].lower():

            infra.Radio(pageDict,'sensASN',['Yep','Nope'],"Do you know the Alternative Identifier?")

            if pageDict['sensASN']=="Yep":
                # pageDict.pop('senObj', None)
                st.text(" - using alternativeIdentifier")
                altID=st.text_input("input alternativeIdentifier (e.g. V6-1-352801-3-001):")
                if "altID" not in pageDict.keys() or pageDict['altID']!=altID:
                    pageDict['altID']=altID

            else:
                # pageDict.pop('senObj', None)
                st.text(" - build alternativeIdentifier...")

                ### get vendor and type
                sensor_vendor, sensor_production, sensor_type = MakeAltId()
                rawID=st.text_input("input sensor identifier (e.g. 3528-1-C):")

                if st.button("generate alternative identifier"):
                    try:
                        sensA=rawID.split('-')[0]
                        sensB=rawID.split('-')[1].zfill(2)
                        sensC="NNN" #str(int('0x'+rawID.split('-')[2],16)).zfill(3)
                        altID=f"{sensor_vendor}-{sensor_production}-{sensA}{sensB}-{sensor_type}-{sensC}"
                        
                        if "altID" not in pageDict.keys() or pageDict['altID']!=altID:
                            pageDict['altID']=altID

                    except IndexError:
                        st.write(f"Do not understand input: {rawID}. Please check.")
                        st.stop()

            if "altID" not in pageDict.keys():
                st.write("please define alternative identifier")
                st.stop()

            if "NNN" in pageDict['altID']:
                st.write("Sorry cannot calculate last three digits at the moment (too many vendor maps).")
                nnn=st.text_input("Please input last three digits:",placeholder="NNN")
                if nnn:
                    # st.write("append with:",nnn)
                    pageDict['altID']=pageDict['altID'][0:-3]+str(nnn)
            try:
                st.write("Using alternative idenfier:",pageDict['altID'])
            except KeyError:
                st.write("Please define alternative identifier")
                st.stop()
            
            if st.button("check ASN"):
                ### translate to ASN
                compObj=st.session_state.myClient.get('getComponent', json={'component': pageDict['altID'],'alternativeIdentifier':True})
                if compObj==None:
                    st.write("- no ASN found (key).")
                else:
                    pageDict['senObj']=compObj

            if "senObj" in pageDict.keys():
                st.write("Sensor ASN:",pageDict['senObj']['serialNumber'])

                infra.ToggleButton(pageDict,'togSenObj',"Show full information?")
                if pageDict['togSenObj']:
                    st.write('Sensor:',pageDict['senObj'])


        ##################
        ### FE part
        ##################
        elif "front end" in pageDict['source'].lower():

            ### find modules form wafer
            if "wafer" in pageDict['source'].lower():

                waferID=st.text_input("Input wafer serial number")
                if len(waferID)<1:
                    st.stop()
                st.write(f"wafer SN: {waferID}")
                # convert decimal to hexadecimal
                dec_num=waferID[7::]
                hex_num=hex(int(dec_num)).split('x')[-1]
                # print(f"- dec: {dec_num} --> hex: {hex_num}")
                # extract B & WW value
                b_val=hex_num[0]
                ww_val=hex_num[1::]
                st.write(f"- extracted values: B: {b_val}, WW: {ww_val}")

                ### get FE tile range
                # min value
                min_hex=hex_num+"00"
                min_dec=int("0x"+min_hex, 16)
                # (f"min: {min_dec} --> {'20UPGFE'+str(min_dec).zfill(7)}")
                # max value
                max_hex=hex_num+"FF"
                max_dec=int("0x"+max_hex, 16)
                # print(f"max: {max_dec} --> {'20UPGFE'+str(max_dec).zfill(7)}")

                ### make component list
                compList=['20UPGFC'+str(x).zfill(7) for x in range(min_dec, max_dec+1, 1)]
                st.write(f"Range of components ({len(compList)}): \n - min: {'20UPGFE'+str(min_dec).zfill(7)} to max: {'20UPGFE'+str(max_dec).zfill(7)}")
                
                ### get component info
                # print("\n##############\n### Front End Chip PDB Information\n##############")
                st.write("__Front End Chip PDB Information__")
                if "retFEs" not in pageDict.keys() or st.button("Re-retrieve FE components"):
                    pageDict['retFEs']=st.session_state.myClient.get('getComponentBulk',json={'component':compList})
                st.write(f"Front Ends retrieved from PDB: {len(pageDict['retFEs'])}")
                ### make dataframe for processing
                df_fes=pd.DataFrame(pageDict['retFEs'])
                # get codes from dictionaries
                for col in df_fes.columns:
                    df_fes[col]=df_fes[col].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
                # explode lists of parent (one row per parent)
                df_fes=df_fes.explode('parents')
                # get parent: compoent type & serialNumber
                df_fes['parent_componentType']=df_fes['parents'].apply(lambda x: x['componentType']['code'] if type(x)==type({}) and "componentType" in x.keys() else x)
                df_fes['parents']=df_fes['parents'].apply(lambda x: x['component'] if type(x)==type({}) and "component" in x.keys() else x)
                df_fes['parent_serialNumber']=df_fes['parents'].apply(lambda x: x['serialNumber'] if type(x)==type({}) and "serialNumber" in x.keys() else x)
                # ignore parents with missing information
                df_fes['parent_serialNumber']=df_fes['parent_serialNumber'].astype(str)
                df_fes=df_fes.query('parent_serialNumber!=None & parent_serialNumber!="None"')
                df_fes=df_fes.query('parent_componentType=="BARE_MODULE"').reset_index(drop=True)
                # show results
                st.write(f"- Remaining FEs with BM parent: {len(df_fes.index)}")
                if st.checkbox("See FE component list?"):
                    st.write(df_fes[['serialNumber','componentType','parent_componentType','parent_serialNumber']])

                ### get bare module info
                # print("\n##############\n### Bare Module PDB Information\n##############")
                st.write("__Front End Chip PDB Information__")
                if "retBMs" not in pageDict.keys() or st.button("Re-retrieve BM components"):
                    pageDict['retBMs']=st.session_state.myClient.get('getComponentBulk',json={'component':list(df_fes['parent_serialNumber'].unique())})
                st.write(f"Bare modules retrieved from PDB: {len(pageDict['retBMs'])}")
                ### get bare module parents
                df_bms=pd.DataFrame(pageDict['retBMs'])
                # get codes from dictionaries
                for col in df_bms.columns:
                    df_bms[col]=df_bms[col].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
                # explode lists of parent (one row per parent)
                df_bms=df_bms.explode('parents')
                # get parent: compoent type & serialNumber
                df_bms['parent_componentType']=df_bms['parents'].apply(lambda x: x['componentType']['code'] if type(x)==type({}) and "componentType" in x.keys() else x)
                df_bms['parents']=df_bms['parents'].apply(lambda x: x['component'] if type(x)==type({}) and "component" in x.keys() else x)
                df_bms['parent_serialNumber']=df_bms['parents'].apply(lambda x: x['serialNumber'] if type(x)==type({}) and "serialNumber" in x.keys() else x)
                # ignore parents with missing information
                df_bms['parent_serialNumber']=df_bms['parent_serialNumber'].astype(str)
                # df_bms=df_bms.query('parent_serialNumber!=None & parent_serialNumber!="None"').reset_index(drop=True)
                # df_bms=df_bms.query('parent_componentType=="MODULE"').reset_index(drop=True)
                # show results
                st.write(f"- Remaining BMs with MODULE parent: {len(df_bms.index)}")
                if st.checkbox("See BM component list?"):
                    st.write(df_bms[['serialNumber','componentType','parent_componentType','parent_serialNumber']])

                ### match FEs to MODULE]
                # print(f"\n##############\n### matched FEs to Modules for {waferSN}\n##############")
                st.write("__match FEs to MODULE__")
                df_fes['grandparent_serialNumber']=df_fes['parent_serialNumber'].apply(lambda x: df_bms.query(f'serialNumber=="{x}"')['parent_serialNumber'].values[0])
                df_fes['grandparent_componentType']=df_fes['parent_serialNumber'].apply(lambda x: df_bms.query(f'serialNumber=="{x}"')['parent_componentType'].values[0])
                st.write(df_fes[['componentType','serialNumber','parent_componentType','parent_serialNumber','grandparent_componentType','grandparent_serialNumber']])
                st.download_button(label="Write csv", data=df_fes[['componentType','serialNumber','parent_componentType','parent_serialNumber','grandparent_componentType','grandparent_serialNumber']].to_csv(),file_name=f"module_family_from_{waferID}.csv")


            ### find FE tile
            if "tile" in pageDict['source'].lower():
                if "feCompList" not in pageDict:
                    st.write("grabbing FE component list...")

                    ### get indexing
                    total=st.session_state.myClient.get('listComponents', json={'project':"P",'componentType':"FE_WAFER",'pageInfo':{ 'pageSize': 1 }}).total
                    pageSize=50
                    count= int(total/pageSize)
                    if total%pageSize>0:
                        count=count+1
                    
                    ### make list of altIDs and SNs
                    st.write("Retrieving FE info.")
                    my_bar = st.progress(0)
                    pageDict['feCompList']=[]
                    for pi in range(0,count,1):
                        # st.write(f"loop:{pi+1}/{count}")
                        my_bar.progress( float(pi+1)/count)
                        compList=st.session_state.myClient.get('listComponents', json={'project':"P",'componentType':"FE_WAFER",'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize }})
                        # st.write(f'length: {len(compList.data)}')
                        pageDict['feCompList'].extend([{'alternativeIdentifier':comp['alternativeIdentifier'], 'serialNumber':comp['serialNumber']} for comp in compList.data])
                    else:
                        st.write("Retrieval complete")
                else:
                    st.write("got FE component list...")

                if st.session_state.debug:
                    infra.ToggleButton(pageDict,'togFEs',"Show FEs information?")
                    if pageDict['togFEs']:
                        st.dataframe(pd.DataFrame(pageDict['feCompList']))

                if st.session_state.debug:
                    infra.TextBox(pageDict,'FEprefix',"Enter FE prefix:")
                if "FEprefix" not in pageDict.keys():
                    pageDict['FEprefix']="20UPGFC"

                waferID=st.text_input("input wafer ID (e.g. N6W780.01-08D4, N4KX64-13B3):")
                posID=st.text_input("input tile position (e.g. C9):")

                if st.button("check ASN"):
                    pageDict['feSN']=GetFE_SN(pageDict['feCompList'],waferID.upper(),posID,pageDict['FEprefix'])
                    if pageDict['feSN']==None:
                        st.write("- no ASN found.")

                if "feSN" in pageDict.keys():
                    st.write("Front End ASN:",pageDict['feSN'])
                    infra.ToggleButton(pageDict,'togFeObj',"Show full information?")
                    if pageDict['togFeObj']:
                        pageDict['feObj']=st.session_state.myClient.get('getComponent', json={'component':pageDict['feSN']})
                        st.write('FE:',pageDict['feObj'])
                        st.write('with parent wafer:', next((item for item in pageDict['feCompList'] if item['alternativeIdentifier'] == waferID), None) )


        ##################
        ### PCB part
        ##################
        elif "pcb" in pageDict['source'].lower():
            
            altID=st.text_input("input PCB alternative Identifier:")

            if st.button("Get component"):
                pageDict['pcbObj']=st.session_state.myClient.get('getComponent', json={'component':altID, 'alternativeIdentifier':True})

            if "pcbObj" in pageDict.keys():
                try:
                    st.write("Component ASN:",pageDict['pcbObj']['serialNumber'])

                    if st.checkbox("See full object?"):
                        st.write(pageDict['pcbObj'])
                except KeyError:
                    st.write("ASN not found. Please check inputs")
                except TypeError:
                    st.write("ASN not found. Please check inputs")

        else:
            st.write("Please select a source")
