
#######
# cropping
#######
import streamlit as st
import cv2
import matplotlib.pyplot as plt
import numpy as np
import argparse 

#------ ------ ------ ------
# This code helps crop and adjust images for perspective - an alternative to using Gimp
# based on:
# https://stackoverflow.com/questions/22656698/perspective-correction-in-opencv-using-python
#------ ------ ------ ------

#######
# image analysis
#######

import numpy as np
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import argparse 

#------ ------ ------ ------
# This code is intended for calculating glue coverage of flex+glass dummy assembly
# 2021 initial code - Silke Möbius (with input from Tobias Bisanz)
# 2022 polishing, added green screen function - Yusong Tian
# 2023 added README.md - Yusong Tian, Hua Ye
# 2023 argparse stuff - Kenny Wraight
#------ ------ ------ ------

#######
# cropping
#######

def unwarp(img, src, testing):
    ### original dimenstions for plotting
    ho, wo = img.shape[:2]
    ### new dimensions for cropping
    maxX=int(max([s[0] for s in src]))
    minX=int(min([s[0] for s in src]))
    maxY=int(max([s[1] for s in src]))
    minY=int(min([s[1] for s in src]))
    # print(f"Dimensions (x,y): {minX}:{maxX}, {minY}:{maxY}")
    img_crop=img[minY:maxY, minX:maxX]

    # cropped extents
    wc=maxX-minX
    hc=maxY-minY
    # print(f"Extents (w,h): {wc}, {hc}")
    # transposed coordinates after cropping
    srcC=np.float32([(s[0]-minX,s[1]-minY) for s in src])
    dstC=np.float32([(0,0),(wc,0),(0,hc),(wc,hc)])

    # use cv2.getPerspectiveTransform() to get M, the transform matrix, and Minv, the inverse
    M = cv2.getPerspectiveTransform(srcC, dstC)
    # use cv2.warpPerspective() to warp your image to a top-down view
    warped = cv2.warpPerspective(img_crop, M, (wc, hc), flags=cv2.INTER_LINEAR)

    # testing modes
    if testing:
        st.write("Test mode: view and edit")
        f, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8))
        f.subplots_adjust(hspace=.2, wspace=.05)
        ax1.imshow(img)
        x = [src[0][0], src[2][0], src[3][0], src[1][0], src[0][0]]
        y = [src[0][1], src[2][1], src[3][1], src[1][1], src[0][1]]
        ax1.plot(x, y, color='red', alpha=1.0, linewidth=5, solid_capstyle='round', zorder=2)
        ax1.set_ylim([ho, 0])
        ax1.set_xlim([0, wo])
        ax1.set_title('Original Image', fontsize=30)
        ax1.grid()
        ax2.imshow(cv2.flip(warped, 1))
        ax2.set_title('Unwarped Image', fontsize=30)
        # plt.show()
        st.pyplot(plt.gcf())
        return None, None
    else:
        st.write("Non-Test mode: returning image")
        return warped, M


#######
# image analysis
#######

def binarize(image_to_transform, threshold, corrOpt):
    # now, lets convert that image to a single greyscale image using convert()
    output_image=image_to_transform.convert("L")
    # the threshold value is usually provided as a number between 0 and 255, which
    # is the number of bits in a byte.
    # the algorithm for the binarization is pretty simple, go through every pixel in the
    # image and, if it's greater than the threshold, turn it all the way up (255), and
    # if it's lower than the threshold, turn it all the way down (0).
    # so lets write this in code. First, we need to iterate over all of the pixels in the
    # image we want to work with
    # We also want to know the percentage that is black
    whitepixel = 0
    blackpixel = 0
    leftcut = 0.0#0.09 
    rightcut = 0.0#0.17 #0.16 for with vac picture
    uppercut = 0.0#0.12 #0.26 #0.17 values for larger area
    lowercut = 0.0#0.12#0.27 #0.25 for with vac picture, 0.19 values for larger area
    width, height=output_image.size
    #st.write "width, height: ",width, height
    num1=int(round(leftcut*width))
    num2=int(round((1-rightcut)*width))
    num3=int(round(uppercut*height))
    num4=int(round((1-lowercut)*height))
    for x in range(num1, num2):
        for y in range(num3, num4):
            # for the given pixel at w,h, lets check its value against the threshold
            if output_image.getpixel((x,y))< threshold: #note that the first parameter is actually a tuple object
                # lets set this to zero
                blackpixel += 1
                output_image.putpixel( (x,y), 0 )
            else:
                # otherwise lets set this to 255
                whitepixel += 1
                output_image.putpixel( (x,y), 255 )
    percentage_black = float(blackpixel)/float((blackpixel+whitepixel))*100
    pct_corrected=percentage_black
    if "corr" in corrOpt:
        pct_corrected=correctToPCBsize(percentage_black)
    comment=f"threshold {threshold} (%black, #black, #white): {(percentage_black, blackpixel, whitepixel)} corrected: {pct_corrected}"
    st.write(comment)
    #font = ImageFont.truetype("arial.ttf", 15, encoding="unic")
    #ImageDraw.Draw(output_image).text((100, 80),"Glue coverage = "+str(round(percentage_black))+'%',font=font,fill=(0,0,255,255))
    #now we just return the new image
    return output_image, comment



def analyseImage_greenScreen(image_to_transform, threshold, corrOpt):
    st.write(threshold)
    output_image=image_to_transform.convert("RGB")
    whitepixel = 0
    blackpixel = 0
    greenpixel = 0
    leftcut = 0.0#0.09 
    rightcut = 0.0#0.17 #0.16 for with vac picture
    uppercut = 0.0#0.12 #0.26 #0.17 values for larger area
    lowercut = 0.0#0.12#0.27 #0.25 for with vac picture, 0.19 values for larger area
    width, height=output_image.size
    #st.write "width, height: ",width, height
    num1=int(round(leftcut*width))
    num2=int(round((1-rightcut)*width))
    num3=int(round(uppercut*height))
    num4=int(round((1-lowercut)*height))
    #st.write "thePixel (5,5):",(output_image.getpixel((5,5)))#note that the first parameter is actually a tuple object

    for x in range(num1, num2):
        for y in range(num3, num4):
            r=(output_image.getpixel((x,y)))[0]
            g=(output_image.getpixel((x,y)))[1]
            b=(output_image.getpixel((x,y)))[2]
            if r<5 and g>=148 and g<=158 and b<5:
                greenpixel += 1
            else:
                gray=0.2989 * float(r) + 0.5870 * float(g) + 0.1140 * float(b) #convert to gray scale, function was found online
                #st.write "gray:",gray,"rgb:",r,g,b
                if gray<threshold:
                    # set the rgb vlues to 0 (black)
                    blackpixel += 1
                    output_image.putpixel((x,y),(0,0,0))
                else:
                    # otherwise set it to 255 (white)
                    whitepixel += 1
                    output_image.putpixel((x,y),(255,255,255))
    comment=f"threshold {threshold}, black, white, green: {blackpixel}, {whitepixel}, {greenpixel}"
    st.write(comment)
    if blackpixel+whitepixel!=0:
        percentage_black = float(blackpixel)/float((blackpixel+whitepixel))*100
        st.write("black%, blackpixel, whitepixel:",percentage_black, blackpixel, whitepixel)
        pct_corrected=percentage_black
        if "corr" in corrOpt and "black" in corrOpt:
            pct_corrected=correctToPCBsize(percentage_black)
        appComment=f"black% corrected: {pct_corrected}"
        st.write(appComment)
        comment=comment+"\n"+appComment
    if blackpixel+whitepixel+greenpixel!=0:
        percentage_green = float(greenpixel)/float((blackpixel+whitepixel+greenpixel))*100
        pct_green_corrected=percentage_green
        if "corr" in corrOpt and "green" in corrOpt:
            pct_green_corrected=correctToPCBsize(percentage_green)
        appComment=f"green%, green_corrected, greenpixel: {percentage_green}, {pct_green_corrected} ,{greenpixel}, total pixel: {blackpixel+whitepixel+greenpixel}"
        st.write(appComment)
        comment=comment+"\n"+appComment
    #now we just return the new image
    return output_image, comment


def analyseImage(img, threshold, option="", corrOpt="correct"):
    
    # img = Image.open(inputFolder+'/'+imgName) #, cv2.IMREAD_GRAYSCALE)
    # st.write(imgName,"width, height:",img.size,"threshold:",threshold)
    img= Image.fromarray(img)
    st.write("width, height:",img.size,"threshold:",threshold)
    #st.write(img.mode)
    #grayimg = img.convert('L')
    #grayimg.show()
    #st.write(grayimg.mode)
    #font = ImageFont.truetype(<font-file>, <font-size>)
    #font = ImageFont.truetype("sans-serif.ttf", 16)
    imList=[]
    if "green" not in option:
        for i in range(threshold[0],threshold[1],threshold[2]):
            retIm, retCom = binarize(img, i,corrOpt)
            imList.append({'image':retIm, 'comment':retCom})
            #binarize(img, threshold, 'saveshow') #130#140 for picture with vacuum
    else:
        for i in range(threshold[0],threshold[1],threshold[2]):
            retIm, retCom = analyseImage_greenScreen(img, i,corrOpt)
            imList.append({'image':retIm, 'comment':retCom})
    return imList


def correctToPCBsize(perc):
    newPerc=perc/(39.6*40.6)*(41*42)
    return newPerc


def calcCoverage(glueMass,thickness):
    glueThickness=sum(thickness)/len(thickness)
    coverage=glueMass/1.05/(glueThickness*0.001)/(39.6*40.6)
    st.write (glueMass,"glue thickness, coverage:",glueThickness, coverage)

