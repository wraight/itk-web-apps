### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import copy
from datetime import datetime,date,time
from pathlib import Path
import altair as alt
import numpy as np
import os
from io import StringIO
import io
from PIL import Image
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################
### Select test parameters by user input
def SelectTestParameters(pageDict, noQC=False):
    ### option to drop non-required information
    dropNonReq=False
    if st.checkbox("Drop all non-required parameters?"):
        dropNonReq=True

    ### don't allow setting of pass/fail
    # noQC=False

    ### get testType info
    if "testInfo" not in pageDict.keys():
        pageDict['testInfo']=st.session_state.myClient.get('getTestTypeByCode', json={'code':pageDict['testSchema']['testType'], 'componentType':pageDict['componentType'], 'project':pageDict['project'], 'required':False})

    ### Fill test content
    # header parameters
    st.write("#### Header Parameters")
    st.write(f"__component__")
    pageDict['testSchema']['component']= st.text_input("Serial number (or PDB code):", value=pageDict['testSchema']['component'])
    st.write(f"__institution__")
    pageDict['testSchema']['institution']= st.text_input("Institution code:", value=pageDict['testSchema']['institution'])
    st.write(f"__runNumber__")
    pageDict['testSchema']['runNumber']= st.text_input("Index of test:")
    st.write(f"__date__")
    test_dt= datetime.strptime(pageDict['testSchema']['date'], "%Y-%m-%dT%H:%M:%S.%fZ")
    test_date= st.date_input("Date of test:", value=test_dt.date())
    test_time= st.time_input("Time of test:", value=test_dt.time())
    pageDict['testSchema']['date']=test_date.strftime("%Y-%m-%d")+"T"+test_time.strftime("%H:%M:%S")+".000Z"
    st.write(f" - date: {pageDict['testSchema']['date']}")
    if noQC==False:
        st.write(f"__passed__")
        pageDict['testSchema']['passed']=st.radio("QC passed?", options=[True,False], index=[True,False].index(pageDict['testSchema']['passed']) )
        st.write(f"__problems__")
        pageDict['testSchema']['problems']=st.radio("QC problems?", options=[True,False], index=[True,False].index(pageDict['testSchema']['problems']) )

    # property & results parameters
    for pr,pp in {'properties':'properties','results':'parameters'}.items():
        st.write(f"#### {pr.title()}")
        for k,v in pageDict['testSchema'][pr].items():
            st.write(f"{k}")
            param=next((x for x in pageDict['testInfo'][pp] if x['code'] == k), None)
            if param==None:
                st.write(f"No {pp} found in testType info. for: __{k}__ ({pr})")
                continue

            ### skip required
            if param['required']!=True:
                if dropNonReq or st.checkbox("Drop non-required parameter?", key=f"{k}"):
                    pageDict['testSchema'][pr][k]="Zaphod Beeblebrox"
                    continue
            else:
                st.write("This parameter is _required_")

            titleStr=param['description']
            if titleStr==None or len(titleStr)<1:
                titleStr=param['name']

            # if code table
            if "code" in param['dataType']:
                rendVal=next((x for x in param['codeTable'] if x['code'] == pageDict['testSchema'][pr][k]), None)
                if rendVal!=None:
                    pageDict['testSchema'][pr][k]= st.selectbox(f"{titleStr}:", options=param['codeTable'], format_func=lambda x: x['value'], index=param['codeTable'].index(rendVal))['code']
                else:
                    pageDict['testSchema'][pr][k]= st.selectbox(f"{titleStr}:", options=param['codeTable'], format_func=lambda x: x['value'])['code']
            # if numeric
            elif "int" in param['dataType']:
                if type(pageDict['testSchema'][pr][k])==type(1):
                    pageDict['testSchema'][pr][k]= st.number_input(f"{titleStr}:", key=f"{pr}_{k}", step=1, value=pageDict['testSchema'][pr][k])
                else:
                    pageDict['testSchema'][pr][k]= st.number_input(f"{titleStr}:", key=f"{pr}_{k}", step=1)
            elif "float" in param['dataType']:
                if type(pageDict['testSchema'][pr][k])==type(1.0):
                    pageDict['testSchema'][pr][k]= st.number_input(f"{titleStr}:", key=f"{pr}_{k}", value=pageDict['testSchema'][pr][k])
                else:
                    pageDict['testSchema'][pr][k]= st.number_input(f"{titleStr}:", key=f"{pr}_{k}")
            elif "bool" in param['dataType']:
                if type(pageDict['testSchema'][pr][k])==type(True):
                    pageDict['testSchema'][pr][k]= st.checkbox(f"{titleStr}:", key=f"{pr}_{k}", value=pageDict['testSchema'][pr][k])
                else:
                    pageDict['testSchema'][pr][k]= st.checkbox(f"{titleStr}:", key=f"{pr}_{k}")
            elif "date" in param['dataType']:
                if pageDict['testSchema'][pr][k]==None:
                    pageDict['testSchema'][pr][k]=pageDict['testSchema']['date']
                pr_dt= datetime.strptime(pageDict['testSchema'][pr][k], "%Y-%m-%dT%H:%M:%S.%fZ")
                pr_date= st.date_input("Date of test:", value=pr_dt.date(), key=f"{pr}_{k}_date")
                pr_time= st.time_input("Time of test:", value=pr_dt.time(), key=f"{pr}_{k}_time")
                pageDict['testSchema'][pr][k]=pr_date.strftime("%Y-%m-%d")+"T"+pr_time.strftime("%H:%M:%S")+".000Z"
                st.write(f" - date: {pageDict['testSchema'][pr][k]}")
            # others
            else:
                if pageDict['testSchema'][pr][k]!="Zaphod Beeblebrox" and pageDict['testSchema'][pr][k]!=None:
                    pageDict['testSchema'][pr][k]= st.text_input(f"{titleStr}:", key=f"{pr}_{k}", value=pageDict['testSchema'][pr][k])
                else:
                    pageDict['testSchema'][pr][k]= st.text_input(f"{titleStr}:", key=f"{pr}_{k}")

    return 

### Recursively remove keys with the specified value from a nested dictionary.
def RemoveKeys(inJson, value_to_remove="Zaphod Beeblebrox"):

    if isinstance(inJson, dict):
        return {k: RemoveKeys(v, value_to_remove) for k, v in inJson.items() if v != value_to_remove}
    elif isinstance(inJson, list):
        return [RemoveKeys(i, value_to_remove) for i in inJson]
    else:
        return inJson

### return component children with childTypeCode
def GetChildren(compCode,childTypeCode):
    myComp= st.session_state.myClient.get('getComponent', json={'component':compCode})
    try:
        st.write("found children:",len([x for x in myComp['children'] if x['component']!=None]))
        # childList=[x['component']['code'] for x in myComp['children'] if x['componentType']['code']==childTypeCode and x['component']!=None]
        childList=[x for x in myComp['children'] if x['componentType']['code']==childTypeCode and x['component']!=None]
    except TypeError:
        st.write("type error")
        childList=None
    except KeyError:
        st.write("key error")
        childList=None
    return childList
### return component parent with parentTypeCode
def GetParents(compCode,parentTypeCode):
    myComp= st.session_state.myClient.get('getComponent', json={'component':compCode})
    try:
        st.write("found parents:",len([x for x in myComp['parents'] if x['component']!=None]))
        # parentList=[x['component']['code'] for x in myComp['parents'] if x['componentType']['code']==parentTypeCode and x['component']!=None]
        parentList=[x for x in myComp['parents'] if x['componentType']['code']==parentTypeCode and x['component']!=None]
    except TypeError:
        st.write("type error")
        parentList=None
    except KeyError:
        st.write("key error")
        parentList=None
    return parentList

### return bit dictionary
def bitfield(n):
    return [{str(2**e):int(digit)} for e,digit in enumerate(reversed(format(n, '#06b')[2:]))] # [2:] to chop off the "0b" part

infoList=[" * Input identifier for MODULE, BARE_MODULE, MODULE_CARRIER, PCB",
        " * Get parent MODULE (if necessary)",
        " * Get MODULE ORIENTATION property",
        " * Get FE_CHIP children of BARE_MODULE child",
        " * Get TRIM information from FE_CHIP tests",
        " * Visualise bond padd connections"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Wirebonding", ":microscope: Check Module Front End TRIM values and upload Wirebinding Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        st.write("#")
        st.write("## Select component")

        chnx.SelectComponentChunk(pageDict)

        st.write("---")

        myComp= st.session_state.myClient.get('getComponent', json={'component':pageDict['comp']['code']})


        ########################
        ### Extract data from parents & children
        ########################
        try:
            if myComp['componentType']['code'] not in ['MODULE','BARE_MODULE','MODULE_CARRIER','PCB']:
                st.write("This ain\'t no module. It's a "+myComp['componentType']['code']+". **HCF**")
                st.stop()
        except KeyError:
            st.write("no componentType code key")
            st.stop()
        except TypeError:
            st.write("unexpected object")
            st.stop()

        modChiSNs=None
        oriFlag=True
        modType=None
        if myComp['componentType']['code']=="MODULE":
            parType, chiType= "MODULE", "BARE_MODULE"
            st.write("checking "+parType+" component for "+chiType+" children")
            modType=myComp['type']['code']
            modComp=myComp
            try:
                oriFlag=next((item for item in myComp['properties'] if item['code'] == "ORIENTATION"), None)['value']
                st.write(f"Extracted MODULE ORIENTATION")
            except TypeError:
                st.write("Cannot extract MODULE ORIENTATION")
            modChis=GetChildren(myComp['code'],chiType)
            if modChis==None:
                st.write("No children found")
                st.stop()
        if myComp['componentType']['code'] in ["MODULE_CARRIER","BARE_MODULE","PCB"]:
            parType, chiType= "MODULE", myComp['componentType']['code']
            st.write("checking "+chiType+" component for "+parType+" parents")
            pars=GetParents(myComp['code'],parType)
            if pars==None or len(pars)<1:
                st.write("No parents found")
                st.stop()
            else:
                stTrx.DebugOutput("parents:",pars)
                try:
                    modComp= st.session_state.myClient.get('getComponent', json={'component':pars[0]['component']['code']})
                    oriFlag=next((item for item in modComp['properties'] if item['code'] == "ORIENTATION"), None)['value']
                    modType=modComp['type']['code']
                    st.write(f"Extracted MODULE ORIENTATION: {oriFlag}")
                except TypeError:
                    st.write(f"Cannot extract Orientation for {pars[0]['component']['code']}")
                st.write("checking MODULE for BARE_MODULE")
                modChis=GetChildren(pars[0]['component']['code'],"BARE_MODULE")
                if modChis==None:
                    st.write("No children found")
                    st.stop()

        if modChis==None:
            st.write(f"no BARE_MODULEs found. Check input component ({myComp['componentType']['code']})")
            st.stop()
        modChiSNs=[x['component']['code'] for x in modChis]
        stTrx.DebugOutput("found BARE_MODULEs:",modChiSNs)

        st.write("### MODULE Summary")
        st.write(f"__Serial number: {modComp['serialNumber']}__")
        st.write(f"__module type: {modType}__")
        st.write(f"__Extracted MODULE ORIENTATION: {oriFlag}__")

        ########################
        ### Quad version
        ########################
        if "QUAD" in modType:
            st.write("Quad analysis")

            bmSN=None
            if len(modChiSNs)!=1:
                st.write("Unexpected number of BARE_MODULEs. Check components")
                st.write(modChiSNs)
            else:
                bmSN=modChiSNs[0]
            st.write("found BARE_MODULE:",bmSN)

            if bmSN==None:
                st.stop()

            ########################
            ### get FE_CHIP children
            ########################
            bmComp=st.session_state.myClient.get('getComponent', json={'component':bmSN})

            feSNs=[x['component']['code'] for x in bmComp['children'] if x['componentType']['code']=="FE_CHIP" and x['component']!=None]
            stTrx.DebugOutput("Child codes",feSNs)
            if feSNs==None:
                st.write("No FE_CHIP children found")
                st.stop()

            childMap={c['component']['code']:str(c['order']) for c in bmComp['children'] if c['componentType']['code']=="FE_CHIP" and c['component']!=None}
            # normal orientation <-- oriFlag=True
            nameMap={'0':"GA1",'1':"GA2",'2':"GA3",'3':"GA4"}
            ### (single) alternative = 180º rotation <-- oriFlag=False
            if oriFlag==False:
                nameMap={'0':"GA3",'1':"GA4",'2':"GA1",'3':"GA2"}

            st.write("Get FE_CHIP data")
            feComps=st.session_state.myClient.get('getComponentBulk', json={'component':feSNs})
            ### optional check
            if st.checkbox("See retrieved quad FE information?"):
                st.write("### Child Components")
                st.write("retrieved FE_CHIP data:",len(feComps))
                st.dataframe(feComps)
            else:
                st.write(" - retrieved FE_CHIP data:",len(feComps))

        ########################
        ### Triplet version
        ########################
        elif "TRIPLET" in modType:
            st.write("Triplet analysis")

            bmSNs=None
            if len(modChiSNs)!=3:
                st.write("Unexpected number of BARE_MODULEs. Check components")
                st.write(modChiSNs)
            else:
                bmSNs=modChiSNs
            # st.write("found BARE_MODULEs:",bmSNs)

            if bmSNs==None:
                st.stop()

            childMap={c['component']['code']:str(c['order']) for c in modComp['children'] if c['component']!=None}

            nameMap={'0':"Position1",'1':"Position2",'2':"Position3",'3':"Position4"}
            if len(list(childMap.keys()))<1:
                print("No children found")
            for c in modComp['children']:
                if c['componentType']['code']!="BARE_MODULE":
                    continue
                cc=c['component']['code']

            bmComps=st.session_state.myClient.get('getComponentBulk', json={'component':bmSNs})
            if st.session_state.debug:
                st.write("Found BARE_MODULEs")
                st.dataframe(pd.DataFrame(bmComps))

            chiType="FE_CHIP"
            feComps=st.session_state.myClient.get('getComponentBulk', json={'component':[y['component']['code'] for x in bmComps for y in x['children'] if y['component']!=None and y['componentType']['code']==chiType]})
            febmMap={x['code']:y['component']['code'] for x in feComps for y in x['parents'] if y['componentType']['code']=="BARE_MODULE"}
            ### optional check
            if st.checkbox("See retrieved triplet FE information?"):
                st.write("### Child Components")
                st.write("retrieved FE_CHIP data:",len(feComps))
                st.dataframe(feComps)
            else:
                st.write(" - retrieved FE_CHIP data:",len(feComps))

        else:
            st.write("### Module type not recognised:",modType)
            st.stop()

        ########################
        ### Get Trim info.
        ########################
        testIDs=[]
        for fe in feComps:
            try:
                #st.write("test count:",len(fe['tests']))
                for fet in fe['tests']:
                    #st.write(fet['code'])
                    for fetr in fet['testRuns']:
                        # try:
                        testIDs.append({'serialNumber':fe['serialNumber'],'code':fet['code'],'id':fetr['id']})
                        # except KeyError:
                        #     testIDs[fet['code']]=[fetr['id']]
            except KeyError:
                st.write("key error")


        st.write("### FE_CHIP Tests")
        df_feIDs=pd.DataFrame(testIDs)
        # st.dataframe(df_feIDs)
        if "code" not in df_feIDs.columns:
            st.write("❔ No test data found. Please check inputs.")
            st.stop()
        feTestRuns=st.session_state.myClient.get('getTestRunBulk', json={'testRun':df_feIDs.query('code=="FECHIP_TEST"')['id'].to_list()})
        ### optional check
        if st.checkbox("See retrieved test information?"):
            st.write("### Test data")
            st.write("retrieved test data:",len(feTestRuns))
            st.dataframe(feTestRuns)
        else:
            st.write(" - retrieved testRuns:",len(feTestRuns))
        # st.dataframe(pd.DataFrame(feTestRuns))

        ### formatting data
        df_testRuns=pd.DataFrame(feTestRuns)
        df_testRuns=df_testRuns.query('state=="ready"')[['components','institution','date','properties','results','passed']]

        st.write(f" - extract testRun components (normal orientaion flag: {oriFlag})")
        df_testRuns=df_testRuns.explode('components')
        for k in ['serialNumber','alternativeIdentifier', 'code']:
            df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[k])
        st.write("   - map chips to module positions")
        if "QUAD" in modType:
            df_testRuns['nickName']=df_testRuns['code'].apply(lambda x: nameMap[childMap[x]])
        elif "TRIPLET" in modType:
            df_testRuns['nickName']=df_testRuns['code'].apply(lambda x: nameMap[childMap[febmMap[x]]])
        else:
            df_testRuns['nickName']=None

        df_testRuns=df_testRuns.explode('results')
        df_testRuns['IREF_TRIM']=df_testRuns['results'].apply(lambda x: x['value'] if x['code']=="IREF_TRIM" else None)
        df_testRuns=df_testRuns[df_testRuns['IREF_TRIM'].notna()]
        df_testRuns['IREF_bit']=df_testRuns['IREF_TRIM'].apply(lambda x: bitfield(int(x)))
        df_testRuns['IREF_bin']=df_testRuns['IREF_bit'].apply(lambda x: [{k:1} if v==0 else {k:0} for b in x for k,v in b.items()])

        ########################
        ### visualise data
        ########################
        st.write("### Visualise")
        if st.checkbox("Calming mountain view?"):
            filePath=os.path.realpath(__file__)
            image = Image.open(filePath[:filePath.rfind('/')]+'/vista.jpg')
            st.image(image, caption='Not bond pads')

        bars = alt.Chart(df_testRuns).mark_bar().encode(
                    y=alt.Y('nickName:N', title="FE position"),
                    x=alt.X('IREF_TRIM:Q', scale=alt.Scale(domain=[0, 20])),
                    color=alt.Color('passed:N'),
                    tooltip=['serialNumber:N','nickName:N','IREF_TRIM:Q','passed:N']
                ).properties(
                    width=600,
                    title="FE_CHIP IREF_TRIM values for MODULE: "+myComp['serialNumber']
                )
        text = alt.Chart(df_testRuns).mark_text(
                fontSize=17,
                font='utopia-std, serif',
                dx=10,
                align='left'
            ).encode(
                y=alt.Y('nickName:N'),
                x=alt.X('IREF_TRIM:Q'),
                color=alt.Color('passed:N'),
                text=alt.Text("IREF_TRIM")
            )
        st.altair_chart(bars+text)

        bondMap={'1':"IREF_TRIM0",'2':"IREF_TRIM1",'4':"IREF_TRIM2",'8':"IREF_TRIM3"}
        for nn in sorted(df_testRuns['nickName'].unique()):
            df_nn=df_testRuns.query('nickName=="'+nn+'"')
            df_nn=df_nn.explode('IREF_bin')
            df_nn['IREF_bin_key']=df_nn['IREF_bin'].apply(lambda x: bondMap[(list(x.keys())[0])])
            df_nn['IREF_bin_val']=df_nn['IREF_bin'].apply(lambda x: list(x.values())[0])
            maxVal=3
            df_nn['long']=df_nn['IREF_bin_val'].apply(lambda x: [int(x)*0.5,int(x)*maxVal])
            df_nn=df_nn.explode('long')

        #     st.dataframe(df_nn)

            bar=alt.Chart(df_nn).mark_bar().encode(
                            x=alt.X('IREF_bin_key:N', sort=None, title=None, axis=alt.Axis(labelAngle=-45)),
                            y=alt.Y('IREF_bin_val:Q', axis=alt.Axis(ticks=False, domain=False, labels=False, values=[0,1]), scale=alt.Scale(domain=[0,maxVal]), title=None),
        #                     opacity=alt.Opacity('IREF_bin_val:N')
                        ).properties(
                            width=600,
                            height=200,
                            title="IREF_TRIM="+str(int(df_nn['IREF_TRIM'].values[0]))+" map for "+df_nn['nickName'].values[0]+", "+df_nn['serialNumber'].values[0]+" (passed="+str(df_nn['passed'].values[0])+")"
                        )
            long=alt.Chart(df_nn).mark_line(height=5, color='red').encode(
                        x=alt.X('IREF_bin_key:N', sort=None, title=None),
                        y=alt.Y('long:Q', axis=alt.Axis(ticks=False, domain=False, labels=False, values=[0,1]), scale=alt.Scale(domain=[0,maxVal]), title=None),
                        detail=alt.Detail('IREF_bin_key')
                    )
            text=alt.Chart(pd.DataFrame([{'text':"50",'y':"IREF_TRIM3",'x':0.25}])).mark_text(color='orange', size=15).encode(
                        x=alt.X('y:N', sort=None, title=None),
                        y=alt.Y('x:Q', axis=alt.Axis(ticks=False, domain=False, labels=False, values=[0,1]), scale=alt.Scale(domain=[0,maxVal]), title=None),
                        text=alt.Text('text:N')
                    )
            st.altair_chart(bar+long+text)

        if not st.checkbox("Upload Wirebonding Information?"):
            st.stop()

        ### upload wirebonding information
        st.write("---")
        st.write("### Upload Wirebonding Information")

        ### select mode
        sel_mode= st.radio("Select mode:", ["pre-formatted json", "manual upload"])   

        ### pre-formatted json
        if "json" in sel_mode.lower():
            st.write("#### Upload using pre-formatted json")

            ## drag and drop method
            pageDict['file']= st.file_uploader("Upload data file", type=["json"])
            if st.session_state.debug: 
                st.write(pageDict['file'])

            ### offer example if no file
            if pageDict['file'] is None:
                ### delete existing info (defined below)
                try:
                    del pageDict['regList']
                except KeyError:
                    pass
                st.write("No data file set")
                filePath=os.path.realpath(__file__)
                exampleFileName="wirebonding_example.json"
                if st.session_state.debug:
                    st.write("looking in:",filePath[:filePath.rfind('/')])
                    st.write(os.listdir(filePath[:filePath.rfind('/')]))
                st.download_button(label="Download example", data=Path(filePath[:filePath.rfind('/')]+"/"+exampleFileName).read_text(), file_name=exampleFileName)
                st.stop()

            ### read in formatted json file
            if "json" in pageDict['file'].type:
                st.write("_json_ file recognised ")
                data=json.loads(StringIO(pageDict['file'].getvalue().decode("utf-8")).read())
            ### unknown file type
            else:
                st.write(f"_{pageDict['file'].type}_ file unrecognised. Please use _json_.")
                st.stop()

            ### add gleaned information to test schema
            # header
            pageDict['testSchema']=data

        ### manual upload
        elif "manual" in sel_mode.lower():
            st.write("#### Manual Upload")

            # set up test info.
            if "componentType" not in pageDict.keys():
                pageDict['componentType']="MODULE"
            if "code" not in pageDict.keys():
                pageDict['code']="WIREBONDING"
            if "stage" not in pageDict.keys():
                pageDict['stage']="MODULE/WIREBONDING"
            if "project" not in pageDict.keys():
                pageDict['project']="P"
                # get test schema (all parameters)
            if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['code']+"@"+pageDict['stage']):
                pageDict['origSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['code'], 'requiredOnly':False})
                pageDict['testSchema']=copy.deepcopy(pageDict['origSchema'])
                # pre-fill some values
                pageDict['testSchema']['component']=pageDict['comp']['serialNumber']
                pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
                pageDict['testSchema']['properties']['MACHINE']="H&K BJ820"
            
            if st.session_state.debug:
                if "origSchema" in pageDict.keys():
                    st.write("**DEBUG** Original *schema*")
                    st.write(pageDict['origSchema'])

            SelectTestParameters(pageDict)

        ### no choice
        else:
            st.write("No mode selected")
            st.stop()
                
        ### remove flagged parameters
        pageDict['testSchema']=RemoveKeys(pageDict['testSchema']) 
        # st.write(pageDict['testSchema'])
        

       ### show/edit data for upload
        st.write("---")
        st.write("### Review test Schema")
        infra.ToggleButton(pageDict,'toggleEdit','Edit Schema?')
        if pageDict['toggleEdit']:
            st.write("### Edit Schema")
            pageDict['testSchema']=EditJson(pageDict['testSchema'])
        else:
            st.write(pageDict['testSchema'])

        infra.ToggleButton(pageDict,'toggleText','Convert values to text?')
        if pageDict['toggleText']:
            pageDict['testSchema'] = json.loads(json.dumps(pageDict['testSchema']), parse_int=str)
            pageDict['testSchema']['results'] = json.loads(json.dumps(pageDict['testSchema']['results']), parse_float=str)
        if st.session_state.debug:
            st.write("### **DEBUG** (final!) Test Schema")
            st.write(pageDict['testSchema'])

        st.write("---")
        st.write("## Test Registration")

        # # check existing component tests
        # chnx.StageCheck(pageDict)

        # upload(!) test schema: change get --> post
        chnx.RegChunk(pageDict, "TestRun", "testSchema")

