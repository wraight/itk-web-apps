### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from pathlib import Path
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
from  .FlatnessCode import GetGleanList
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _csv_ pull test file",
        " * review retrieved data & visualisation",
        " * review analysis checks",
        " * review and edit test schema",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page9(Page):
    def __init__(self):
        super().__init__("Module Flatness", ":microscope: Upload Module Flatness Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        st.write("#")
        st.write("## Input file")
        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULE"
        if "testType" not in pageDict.keys():
            pageDict['testType']="FLATNESS"
        if "stage" not in pageDict.keys():
            pageDict['stage']="MODULE/ASSEMBLY"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_P.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testType']+'"').reset_index(drop=True)
        st.write("### componentTypes and stages for testType=="+pageDict['testType'])
        st.dataframe(df_stageTest)

        infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
        if pageDict['toggleChanger']:
            infra.SelectBox(pageDict,'componentType',df_stageTest['compType'].unique(),"Select componentType code:")
            infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list(),"Select stage code:")

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])



        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["txt","dat"])
    

        stTrx.DebugOutput("Uploaded file:",pageDict['file'])

        if pageDict['file'] is None:
            st.write("No data file set. Please input file")
            filePath=os.path.realpath(__file__)
            exampleFileName="FlatnessDataExample.txt"
            stTrx.DebugOutput("looking for file in:",filePath[:filePath.rfind('/')])
            st.download_button(label="Download example", data=Path(filePath[:filePath.rfind('/')]+"/"+exampleFileName).read_text(),file_name=exampleFileName)
            pageDict['currentFileName']=None
            st.stop()
        if "currentFileName" not in pageDict.keys() or pageDict['currentFileName']!=pageDict['file'].name:
            ### tidy up
            keepList=['componentType', 'testType', 'stage', 'project', 'file']
            loseList=[key for key in pageDict.keys() if key not in keepList]
            for k in loseList:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            ### read in new stuff
            headerList, dataList = GetGleanList(pageDict['file'], pageDict['componentType'])
            # make DFs
            pageDict['df_header'] = pd.DataFrame(headerList, columns=list('abcdef'))
            pageDict['df_header']=pageDict['df_header'].dropna(axis=1, how='all')
            pageDict['df_header']['a']=pageDict['df_header']['a'].str.lower()
            pageDict['df_data'] = pd.DataFrame(dataList, columns=list('abcdef'))
            pageDict['df_data']=pageDict['df_data'].dropna(axis=1, how='all')
            pageDict['df_data']=pageDict['df_data'].rename(columns={'a':"x",'b':"y",'c':"z"})
            pageDict['df_data']=pageDict['df_data'][['x','y','z']]
            # cache to check for input changes next time
            pageDict['currentFileName']=pageDict['file'].name

        st.write("---")

        st.write("## Gleaned Data")

        st.write("### header info.")
        st.dataframe(pageDict['df_header'])
        st.write("### data info.")
        st.dataframe(pageDict['df_data'])

        st.write("**Flatness Visualisation**")
        st.altair_chart(
            alt.Chart(pageDict['df_data']).mark_rect().encode(
                x='x:Q',
                y='y:Q',
                color='z:Q',
                tooltip=['x:Q','y:Q','z:Q']
            ).properties(width=600).interactive()
        )

        # get test schema
        if "testSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testType']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['testSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType']})#, 'requiredOnly':True})
            # hack to coordinate with chnx.RegChunk
        stTrx.DebugOutput("Original *schema*:",pageDict['testSchema'])

        ### set header
        st.write("Reading header info. from file")
        headMap={'component':"serial number", 'institution': "institute"}
        for k,v in headMap.items():
            try:
                pageDict['testSchema'][k]=pageDict['df_header'].query(f'a=="{v}"')['b'].values[0]
                st.write(f" - got {k}")
            except:
                pageDict['testSchema'][k]=None
                st.write(f" - Problem finding **{v}** for {k} data. Please check file or input below") 
        else:
            st.write(" - done")

        ### set properties 
        st.write("Reading properties info. from file")
        propMap={'ANALYSIS_VERSION':"analysis version", 'MEASUREMENT_DATE': "measurement date", 
                 'OPERATOR_IDENTITY':"operator", 'TOOL_TECHNIQUE': "tool technique",
                 'TEMP':"temperature", 'HUM': "humidity"}
        for k,v in propMap.items():
            try:
                pageDict['testSchema']['properties'][k]=pageDict['df_header'].query(f'a=="{v}"')['b'].values[0]
                st.write(f" - got {k}")
            except: 
                pageDict['testSchema']['properties'][k]=None
                st.write(f"Problem finding **{v}** for {k} data. Please check file or input below") 
        else:
            st.write(" - done")
        if pageDict['testSchema']['properties']['ANALYSIS_VERSION'] in [None,"Unknown","UNKNOWN"]:
            pageDict['testSchema']['properties']['ANALYSIS_VERSION']="webApp "+chnx.GetCommitSHA()
        else:
            pageDict['testSchema']['properties']['ANALYSIS_VERSION']+=", webApp "+chnx.GetCommitSHA()
        # ### wipe results
        # stTrx.SetDictValsNone(pageDict['testSchema']['results'])

        # results
        st.write("Reading/calculating results info. from file")
        pageDict['testSchema']['results']['BACKSIDE_FLATNESS']=pageDict['df_data']['z'].astype(float).std()
        st.write(f" - std of data points: {pageDict['testSchema']['results']['BACKSIDE_FLATNESS']}")
        pageDict['testSchema']['results']['FLATNESS_MEASUREMENT_POINTS']=[[row['x'],row['y'],row['z']] for i,row in pageDict['df_data'].iterrows()]
        st.write(f" - size of coordiniate list: {len(pageDict['testSchema']['results']['FLATNESS_MEASUREMENT_POINTS'])}")
        st.write(" - done")

        ### Review and edit
        chnx.ReviewAndEditChunk(pageDict,'testSchema')

        # st.write("### :construction: Page under construction :construction:")
        # st.stop()

        # upload(!) test schema: change get --> post
        chnx.RegChunk(pageDict, "TestRun", "testSchema", "file")
