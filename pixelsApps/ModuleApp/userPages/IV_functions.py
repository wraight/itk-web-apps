### based on: https://gitlab.cern.ch/huye/itk_pdb_testapp_pix_reception

import numpy as np
import streamlit as st
import pandas as pd
import json
from io import StringIO

###################################
### Read-in Part
###################################

### reading json style input
def ReadJsonIV(fileObj):

    # To convert to a string based IO:
    stringio = StringIO(fileObj.getvalue().decode("utf-8"))
    # read string to json
    data = json.loads(stringio.read())
    if st.session_state.debug:
        st.write("input data",data)
    headerList = {k:v for k,v in data.items() if type(v)!=type({})}
    # st.write(headerList)
    df_header=pd.DataFrame([{'key':k,'value':v} for k,v in headerList.items()])
    df_header['key']=df_header['key'].str.replace(':','')
    df_header.fillna('', inplace=True)
    if not df_header.query('key=="depletion_voltage"').empty:
        st.write("Ignoring Vdep in file. Will query PDB later.")
        df_header=df_header.query('key!="depletion_voltage"')
    # st.write(df_header)
    try:
        propList = {k:v for k,v in data['properties'].items()}
    except KeyError:
        st.write("No _properties_ found:",data.keys())
        findAlt=False
        if "IV_ARRAY" in data.keys():
            st.write("- checking IV_ARRAY for temp. & humid. values")
            if all(r in data['IV_ARRAY'].keys() for r in ['temperature', 'humidity']):
                st.write("- found values :)")
                st.write("- adding to data")
                data['properties']={}
                for k,v in {'temperature':"TEMP", 'humidity':"HUM"}.items():
                    data['properties'][v]=sum(data['IV_ARRAY'][k])/len(data['IV_ARRAY'][k])
                    propList = {k:v for k,v in data['properties'].items()}
                findAlt=True
        if findAlt==False:
            st.write("Cannot find values. Please compare input file to examples.")
            st.stop()
    # st.write(propList)
    df_prop=pd.DataFrame([{'key':k,'value':v} for k,v in propList.items()])
    df_prop=df_prop.transpose().reset_index(drop=True)
    df_prop=df_prop.rename(columns=df_prop.iloc[0]).drop(df_prop.index[0])
    # st.write(df_prop)
    if "IV_ARRAY" in data.keys():
        resList = {k:v for k,v in data['IV_ARRAY'].items()}
    elif "results" in data.keys() and "IV_ARRAY" in data['results'].keys():
        resList = {k:v for k,v in data['results']['IV_ARRAY'].items()}
    else:
        st.write("No IV_ARRAY object found in json. Please check input file.")
        st.stop()
    # st.write(resList)
    df_data=pd.DataFrame([{'key':k,'value':v} for k,v in resList.items()])
    df_data=df_data.transpose().reset_index(drop=True)
    df_data=df_data.rename(columns=df_data.iloc[0]).drop(df_data.index[0])
    df_data.columns = df_data.columns.str.lower()
    df_data=df_data.explode(list(df_data.columns)).reset_index(drop=True)
    # st.write(df_data)
    return df_header, df_prop, df_data


def ReadTextIV(fileObj):
                
    df_file=pd.read_csv(fileObj, names=['a','b','c','d','e','f'], sep='\t')
    st.write(df_file)
    idx=df_file.index[df_file['e'].str.find('T/C')==0][0]
    # st.write("## index of row with data header (inc. T/C)",idx[0])
    ### format non-header information
    df_pr=df_file.copy(deep=True)
    df_pr=df_pr[idx::]
    df_pr=df_pr.rename(columns={df_pr.columns[0]:"time",
                                df_pr.columns[1]:"voltage",
                                df_pr.columns[2]:"current",
                                df_pr.columns[3]:"sigma current",
                                df_pr.columns[4]:"TEMP",
                                df_pr.columns[5]:"HUM"})
    df_pr=df_pr.drop(df_pr.index[0]).reset_index(drop=True)
    # st.write(df_pr)
    ### split properties and measurement
    dataCols=['voltage','current','sigma current','TEMP','HUM']
    df_data=df_pr[dataCols]
    # st.write(df_data)
    print("properties:")
    propCols=['TEMP','HUM']
    df_prop=df_pr[propCols].astype(float)
    df_prop=pd.DataFrame(df_prop.mean()).transpose()
    # st.write(df_prop)
    ### format header information
    df_info=df_file.copy(deep=True)
    df_info=df_info[0:idx].dropna(how='all', axis=1)
    headerList=[]
    # pageDict['prefix']=None
    for i,row in df_info.iterrows():
        for col in df_info.columns:
    #         print(row[col])
            if "20U" in str(row[col]):
                headerList.append({'key':'component', 'value':row[col]})
            elif ":" in str(row[col]) and "_" in str(row[col]):
                headerList.append({'key':'date', 'value':row[col]})
            elif "prefix" in str(row[col]).lower():
                headerList.append({'key':'prefix', 'value':row[col].split(' ')[-1].strip()}) 
            elif "depletion_voltage" in str(row[col]).lower():
                st.write("Ignoring Vdep in file. Will query PDB later.")
    df_header=pd.DataFrame(headerList)

    return df_header, df_prop, df_data


###################################
### Analysis Part
###################################

### Scale factor to normalise for temperature
def scaleF(Tbare, Tmod):
    Eg = 1.22
    k = 8.617333262e-5

    def kelvin(T):
        return 273.15 + T

    return (
        np.power(kelvin(Tmod) / kelvin(Tbare), 2)
        * np.exp(Eg * (Tmod - Tbare) / 2 / k / kelvin(Tmod) / kelvin(Tbare))
        - 1
    )

### Checking if the component is 3D by using YY-identifiers
def Check3D(serialNum):
    is3D = False
    if any(serialNum[6] == x for x in ["G", "H", "I", "J", "V", "W", "0", "1"]):
        is3D = True
    return is3D

### Finding Leakage current and breakdown voltage
def calVbdIlc(serialNum, Vdepl, xdata, ydata):
    Vbd = 0
    Ilc = 0

    is3D= Check3D(serialNum)

    # 3D sensor criterias
    if is3D:
        st.write("Recognise __3D__ device")
        if Vdepl == 0:
            Vdepl = 5
        I_voltage_point = Vdepl + 20
        # break_threshold = Vdepl + 20
        # I_treshold = 2.5  # current in uA

    # Planar sensor criterias
    else:
        st.write("Recognise __planar__ device")
        if Vdepl == 0:
            Vdepl = 50
        I_voltage_point = Vdepl + 50
        # break_threshold = Vdepl + 70
        # I_treshold = 0.75  # current in uA

    # Finding leakage current at threshold voltage
    for idx, V in enumerate(xdata):
        # st.write(f"idx: {idx}")
        # if V<0:
        #     V=V*-1
        if V < Vdepl:
            # st.write(" - less!")
            continue
        elif V <= I_voltage_point:
            # Vlc = V
            Ilc = ydata[idx]

        # Finding breakdown voltage for 3D
        if is3D:
            if ydata[idx] > ydata[idx - 5] * 2 and xdata[idx - 5] > Vdepl:
                Vbd = xdata[idx - 5]
                st.write(f"__3D__ - Ilc: {Ilc}, Vbd: {Vbd}")
                break

        # Finding breakdown voltage for Planar
        else:
            if ydata[idx] > ydata[idx - 1] * 1.2 and xdata[idx - 1] != 0:
                Vbd = V
                st.write(f"__planar__ - Ilc: {Ilc}, Vbd: {Vbd}")
                break
        
    if Vbd == 0:
        Vbd = ">%s" % round(xdata[-1], 1)

    # st.write("return: ",[Vbd, Ilc])
    return Vbd, Ilc

### Get pass/fail for analysis
def GetNewToOldComp(Vnew,Vold,Vmax_old=None):

    st.write("#### __Vdep analysis...__")
    ## 2. check breakdown comparison
    # check old and new values
    oldCheck, newCheck = True, True
    st.write("Check old and new values are meaningful:")
    if type(Vnew)==type("str") or Vnew==-999:
        st.write(" - No _new_ breakdown value for comparison.")
        newCheck=False
    else:
        st.write(f" - found _new_ breakdown value for comparison: {Vnew}")
    if type(Vold)==type("str") or Vold==-999:
        st.write(" - No _old_ breakdown value for comparison.")
        oldCheck=False
    else:
        st.write(f" - found _old_ breakdown value for comparison: {Vold}")
    
    # specified difference permitted
    specDiff=10
    # if old and new Vdep values can be compared
    if oldCheck and newCheck:
        st.write(f"Compare breakdown values, permitted difference: {specDiff}")
        measDiff=abs(Vnew - Vold)
        # fail: >specDiff
        if measDiff > specDiff:
            st.write(f" - BREAKDOWN_VOLTAGE difference too big: {measDiff} ")
            st.write(f"#### :broken_heart: **Analysis failed**")
            return False
        # pass: <specDiff
        else:
            st.write(f" - BREAKDOWN_VOLTAGE difference OK: {measDiff}")
            st.write(f"#### :green_heart: **Analysis passed**")
            return True
    # if both values indicate no Vdep recorded
    elif not oldCheck and not newCheck:
        st.write("Both _old_ and _new_ breakdown values missing ➡ __assume analysis passed__")
        st.write(f"#### :green_heart: **Analysis passed**")
        return True
    # if Vdep only avalable for one
    else:
        # st.write(f"Can't compare values (old,new): {Vold}, {Vnew}")
        ### "temporary" hack for missing sensor breakdown
        if Vold==0 or Vold==-999:
            st.write(f"__Missing data hack__: _old_ value is not meaningful: {Vold} ➡ __assume no breakdown found__")
            if not newCheck:
                st.write(" - Both _old_ and _new_ breakdown values missing ➡ __assume passed__")
                st.write(f"#### :green_heart: **Analysis passed**")
                return True
            else:
                # we have breakdown now but not before
                st.write(f" - Can't compare values (old,new): {Vold}, {Vnew}")
                if Vmax_old==None:
                    st.write(f" - Old Vmax missing ➡ __assume passed__")
                    st.write(f"#### :green_heart: **Analysis passed**")
                    return True
                else:
                    st.write(f" - compare new Vdep to old Vmax...")
                    if Vnew > Vmax_old:
                        st.write(f" - {Vnew} _more_ than {Vmax_old}")
                        st.write(f"#### :green_heart: **Analysis passed**")
                        return True
                    else:
                        st.write(f" - {Vnew} _less_ than {Vmax_old}")
                        st.write(f"#### :broken_heart: **Analysis failed** ")
                        return False
        else:
            if not newCheck:
                # we have (non-zero) old value but no new value
                st.write(f"Can't compare values (old,new): {Vold}, {Vnew} (new missing) ➡ __assume passed__")
                st.write(f"#### :green_heart: **Analysis passed**")
                return True
            else:
                st.write(f"Shouldn't get here with values (old,new): {Vold}, {Vnew}. Please contact Kenny. ➡ __assume passed__")
                st.write(f"#### :green_heart: **Analysis passed**")
                return True

def GetLeakageCheck(sensorType, leakage):

    st.write("#### __Leakage current analysis...__")
    factor=2.5
    area=3.84

    if "quad" in sensorType.lower():
        st.write(f" - quad area used for for type: {sensorType}")
        area=15.5172
    else:
        st.write(f" - non-quad area used for type: {sensorType}")

    st.write(f" - Compare {leakage} to limit: {area*factor} µA/cm**2 (factor: {factor})")
    if leakage<area*factor:
        st.write(f"#### :green_heart: **Analysis passed**")
        return True
    else:
        st.write(f"#### :broken_heart: **Analysis failed** ")
        return False


def GetVmaxCheck(Vmax, Vdep, Vbk):
    
    st.write("#### __Vmax analysis...__")
    VmaxCheck=True
    if Vmax < Vdep + 70:
        st.write(f"Vmax ({Vmax}) _less_ than Vdep + 70 ({Vdep+70})")
        st.write(f"### :broken_heart: **Analysis failed** ")
        VmaxCheck=False
    else:
        st.write(f"Vmax ({Vmax}) _more_ than Vdep + 70 ({Vdep+70})")

    # if Vbk < Vmax:
    #     st.write(f"Vbk ({Vbk}) _less_ than Vmax ({Vmax})")
    # else:
    #     st.write(f"Vbk ({Vbk}) _more_ than Vmax ({Vmax})")
    #     st.write(f"### :broken_heart: **Analysis failed** ")
    #     VmaxCheck=False

    if VmaxCheck:
        st.write(f"#### :green_heart: **Analysis passed**")
    else:
        VmaxCheck=False

