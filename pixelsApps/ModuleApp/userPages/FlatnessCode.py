### standard
import streamlit as st
### custom
import os
import pandas as pd
import numpy as np
import ast
import csv
import json
import base64
from datetime import datetime
### PDB stuff
import itkdb.exceptions as itkX
import randomname

#####################
### useful functions
#####################

def GetGleanList(fileObj, compType="MODULE"):
    header=[]
    data=[]
    change = False

    for raw_line in fileObj.readlines():
        #st.write("raw line:",line)
        line=raw_line.decode("ISO-8859-1")
        # st.write(line)
        if not line:
            break

        if change==False:
            if "Position" in line:
                change=True
                continue
            if "#" in line: continue
            appList=[np.NaN for x in range(0,6,1)]
            for i,x in enumerate(line.split(': ')):
                appList[i]=x.strip()
            # header.append([x.strip() for x in line.split(': ')])
            header.append(appList)
        elif change==True:
            if "#" in line: continue
            appList=[np.NaN for x in range(0,6,1)]
            splitArr=[x.strip() for x in line.split(',') if len(x)>0]
            for i,x in enumerate(splitArr):
                # st.write(i,x)
                # st.write([x for x in line.split(' ') if len(x)>0])
                appList[i]=x.strip()
            # data.append([x.strip() for x in line.split('\t')])
            data.append(appList)
        #st.write("Line{}: {}".format(count, line.strip()))
    return header, data

