### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import copy
import altair as alt
from datetime import datetime
import numpy as np
import os
from pathlib import Path
from io import StringIO
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
from . import IV_functions
### local stuff

#####################
### useful functions
#####################

def CheckRelatives(compIDs,proType,relType,lvl=0):
    
#     print(f"THIS THING IS A {type(compIDs)}")
    
    if type(compIDs)==type([]):
#         print("GOT LIST")
#         print(lvl*'\t'+f"Loop: {compIDs}")
        ### loop over IDs
        for compID in compIDs:
            # get component
            compObj= st.session_state.myClient.get('getComponent', json={'component':compID})
            if compObj==None:
                st.write(f"this guy: {compID}")
                continue
            print(lvl*'\t'+f"Checking {compObj['serialNumber']} ({compID})")

            ### check componentType code
            if "code" in compObj['componentType']:
                print(lvl*'\t'+f"-->{compObj['componentType']['code']}")
                # return if match
                if compObj['componentType']['code']==proType:
                    print(lvl*'\t'+f"Found {compObj['componentType']['code']} match: {compObj['serialNumber']} ({lvl})")
                    return compObj
            else:
                print(lvl*'\t'+"No compCode found")

            ### collect relative ids
            relatives=None
            try:
                relatives=[rel['component']['code'] for rel in compObj[relType] if rel['component']!=None]
            except KeyError:
                print(lvl*'\t'+"no relatives (KeyError)")
            except TypeError:
                print(lvl*'\t'+"no relatives (TypeError)")

            # check relatives
            retObj=CheckRelatives(relatives,proType,relType,lvl+1)
            
            if type(retObj)==type({}):
#                 print("GOT DICT")
                return retObj

### nominal values for depletion
def GetNominalDepletion(senType, retVal='Vdep'):
    nomList=[
        {'type':"DUMMY_SENSOR_WAFER", 'Vdep':None, 'Vop':None, 'Vbd':None },
        {'type':"L0_INNER_PIXEL_3D_SENSOR_WAFER_25", 'Vdep':5, 'Vop':25, 'Vbd':25 },
        {'type':"L0_INNER_PIXEL_3D_SENSOR_WAFER_50", 'Vdep':5, 'Vop':25, 'Vbd':25 },
        {'type':"L1_INNER_PIXEL_SENSOR_WAFER", 'Vdep':50, 'Vop':100, 'Vbd':120 },
        {'type':"OUTER_PIXEL_SENSOR_WAFER", 'Vdep':70, 'Vop':120, 'Vbd':140 },
        {'type':"PLANAR_SENSOR_WAFER_100", 'Vdep':50, 'Vop':100, 'Vbd':120 }
    ]
    if senType=="table":
        return nomList
    else:
        return next((item[retVal] for item in nomList if item['type']==senType),None)

infoList=[" * check test type",
        " * upload _dat_ IV file",
        " * review retrieved data & visualisation",
        # " * review analysis checks (from [here](https://gitlab.cern.ch/cambridge-ITK/production_database_scripts/-/blob/sensor_updates/strips/sensors/sensor_tests_interfacing_functions))",
        " * review and edit test schema",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]

#####################
### main part
#####################

# Monika: update below to have new page
class Page7(Page):
    def __init__(self):
        super().__init__("IV Upload", ":microscope: Upload Sensor/Bare Module IV Test", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "testType" not in pageDict.keys():
            pageDict['testType']="IV_MEASURE"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        ######################
        ### selecting stage for ubiquitous test
        ######################
        st.write("#")
        st.write("## Initial Inputs")
        st.write("Some instructions: [indico talk](https://indico.cern.ch/event/1301749/contributions/5474509/attachments/2676279/4641403/Modules_23_6_29.pdf)")

        st.write(f"Uploading testType: {pageDict['testType']}")
        st.write("### Select __componentType__ for upload")
        ### select componentType
        selMode=st.radio("Which type of component?",sorted(["SENSOR_TILE","BARE_MODULE","MODULE"]))
        # set pageDict
        if selMode=="MODULE":
            pageDict['componentType']="MODULE"
        else:
            pageDict['componentType']="SENSOR_TILE"

        st.write(f"### Select __stage__ for upload")
        # get comp schema if not already
        if "compSchema" not in pageDict.keys() or pageDict['compSchema']['code']!=pageDict['componentType']:
            pageDict['compSchema'] = st.session_state.myClient.get('getComponentTypeByCode', json={'project':pageDict['project'], 'code':pageDict['componentType']})
            # st.write(pageDict['compSchema'])
            # get relevant stages with test
            pageDict['stageList']=[stage['code'] for stage in pageDict['compSchema']['stages'] for tt in stage['testTypes'] if type(tt['testType'])==type({}) and "code" in tt['testType'].keys() and tt['testType']['code']==pageDict['testType']]
        selStage=st.selectbox("Select stage code:", sorted(pageDict['stageList']))
        pageDict['stage']=selStage

        st.write(f"Selected upload will go to: {pageDict['componentType']} @ {pageDict['stage']}")
        if selMode=="BARE_MODULE":
            st.write("- BARE_MODULE upload will be offered after SENSOR_TILE upload")

        st.write("---")
        ######################
        ### read data from file
        ######################
        st.write("## Input Data File")
        st.write(f"For {pageDict['componentType']} @ {selStage} (set above)")
        
        ### flag for file name used to decide if things need updated
        if "fileName" not in pageDict.keys():
            pageDict['fileName']="NYS"

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["json",'txt'])
        if st.session_state.debug: 
            st.write(pageDict['file'])

        ### stop if no file
        if pageDict['file'] is None:
            st.write("No data file set")
            filePath=os.path.realpath(__file__)
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
            st.write("_txt_ format to download")
            exampleFileName="example_IV.txt"
            st.download_button(label="Download example", data=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+exampleFileName).to_csv(index=False),file_name=exampleFileName)
            st.write("_json_ format to download")
            exampleFileName="example_IV.json"
            st.download_button(
                "Download example",
                data=Path(filePath[:filePath.rfind('/')]+"/"+exampleFileName).read_text(),
                file_name=exampleFileName,
                mime="application/json")
            pageDict['fileName']="NYS"
            st.stop()
            
        ### update new file
        if pageDict['file'].name!=pageDict['fileName']:
            ### clear existing cache
            if st.session_state.debug: st.write("clearing cache")
            pageKeys=list(pageDict.keys())
            for k in pageKeys:
                if k not in ['project','componentType','testType','stage','origSchema','file']:
                    try:
                        pageDict.pop(k)
                    except KeyError:
                        pass
        if "json" in pageDict['file'].type:
            st.write("Got _json_ file")
            df_header, df_prop, df_data = IV_functions.ReadJsonIV(pageDict['file'])
        elif "text" in pageDict['file'].type:
            st.write("Got _txt_ file")
            df_header, df_prop, df_data = IV_functions.ReadTextIV(pageDict['file'])
        else:
            st.write("🛑 Unknown file type. Please check input file.")
            st.stop()
        pageDict['fileName']=pageDict['file'].name

        ######################
        ### extracting data
        ######################
        st.write("---")
        st.write("## Extract Data")
        st.write("Separating header, properties and measurement data")

        st.write("__header info.__")
        #quick hack for conistency
        if "serialNumber" in df_header['key'].to_list():
            df_header.loc[df_header['key'] == "serialNumber", 'key'] = "component"
        # df_header = pd.DataFrame (headerList, columns=['key','value'])
        df_header.columns=df_header.columns.str.lower()
        for col in df_header.columns:
            df_header[col]=df_header[col].str.replace(' ', '')
        #Monika: easy way to keep track of bare module SN
        orig_SN = df_header.query('key=="component"')['value'].to_list()[0]
        st.dataframe(df_header)

        st.write("__properties info.__")
        # df_prop.columns = df_prop.columns.str.lower()
        df_prop.columns=df_prop.columns.str.lower()
        # for col in df_prop.columns:
        #     df_prop[col]=df_prop[col].str.replace(' ', '')
        for col in df_prop.columns:
            df_prop[col]=df_prop[col].astype(float)
        st.dataframe(df_prop)

        st.write("__measurement info.__")
        df_data.columns=df_data.columns.str.lower()
        for col in df_data.columns:
            try:
                df_data[col]=df_data[col].str.replace(' ', '')
            except AttributeError:
                ### some things are not strings
                pass
        for col in df_data.columns:
            df_data[col]=df_data[col].astype(float)
        st.dataframe(df_data)

        st.write("### Visualisation of input data")
        st.write("No scaling or normalisation here")
        df_plot=df_data[['voltage','current']]
        IV_chart=alt.Chart(df_plot).mark_line().encode(
                x='voltage:Q',
                y='current:Q',
                tooltip=['voltage:Q','current:Q']
        ).properties(width=600, title="Input data").interactive()
        st.altair_chart(IV_chart)

        st.write("---")

        ######################
        ### fill schema with raw data
        ######################

        st.write("## Building Test Schema")
        st.write(f"testRun: {pageDict['testType']} @ {pageDict['stage']} for {pageDict['componentType']}")

        st.write("### Filling "+pageDict['testType']+" schema _raw_ data")
        st.write("Transfer _header_, _properties_, _raw measurements_")

         # get test schema
        if "origSchema" not in pageDict.keys() or pageDict['testType']!=pageDict['origSchema']['testType']:
            pageDict['origSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType'], 'requiredOnly':True})
            #pageDict['origSchema']={'properties':{}, 'results':{}, 'passed':False, 'problems':False}
        
        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])

        ### update from original schema if unknown or on demand
        if "testSchema" not in pageDict.keys() or st.button("re-read input"):
            st.write("Setting up test schema header...")
            # copy original
            pageDict['testSchema']=copy.deepcopy(pageDict['origSchema'])

            ### general settings
            # component i.e. serial number (should be named component after check ~L380)
            st.write(" - component identifiter")
            pageDict['testSchema']['component']=df_header.query('key=="component"')['value'].values[0]

            # institution
            st.write(" - institution (from user info.)")
            # pageDict['testSchema']['institution']= df_header.query('key=="institution"')['value'].values[0]
            pageDict['testSchema']['institution']= st.session_state.Authenticate['inst']['code']
            # date

            ### now to use upload date (rather than measurement date)
            st.write("- using today's date for test date (rather than measurement date)")
            nowStr=datetime.now().strftime("%Y-%m-%dT%H:%MZ")
            pageDict['testSchema']['date']=pdbTrx.TimeStampConverter(nowStr,"%Y-%m-%dT%H:%MZ")

            # runNumber: "webApp_"+now
            # pageDict['testSchema']['runNumber'] = "webApp_"+pdbTrx.TimeStampConverter("now","")
            try:
                pageDict['testSchema']['runNumber'] = df_header.query('key=="runNumber"')['value'].values[0]
            except IndexError:
                pageDict['testSchema']['runNumber']="..."


            ### properties
            st.write("setting up test schema properties...")
            st.write(" - TEMP")
            try:
                pageDict['testSchema']['properties']['TEMP']=df_prop['temp'].to_list()[0]
            except IndexError:
                st.write("   - ⚠ __issue__ check inputs")
                pass
            st.write(" - HUM")
            try:
                pageDict['testSchema']['properties']['HUM']=df_prop['hum'].to_list()[0]
            except IndexError:
                st.write("   - ⚠ __issue__ check inputs")
                pass
            st.write(" - ANALYSIS_VERSION")
            try:
                pageDict['testSchema']['properties']['ANALYSIS_VERSION']="webApp "+chnx.GetCommitSHA()
            except IndexError:
                st.write("   - ⚠ __issue__ check inputs")
                pass
            st.write(" - MEASUREMENT_DATE (from file)")
            ### get date from file
            try:
                dateStr=df_header.query('key=="date"')['value'].values[0]
            except IndexError:
                st.write("   - ⚠ __issue__ check inputs")
            ### convert to PDB format
            try:
                dateStr=pdbTrx.TimeStampConverter(dateStr[0:dateStr.rfind(':')]+'Z',"%Y-%m-%dT%H:%MZ")
            except ValueError:
                ### alternative
                try:
                    dateStr=pdbTrx.TimeStampConverter(dateStr,"%Y-%m-%d_%H:%M")
                except ValueError:
                    ### and again
                    try:
                        dateStr=pdbTrx.TimeStampConverter(dateStr,"%Y-%m-%d %H:%M")
                    except ValueError:
                        ### last try
                        try:
                            dateStr=pdbTrx.TimeStampConverter(dateStr,"%Y-%m-%dT%H:%MZ")
                        except ValueError:
                            st.write("__please edit date format__: '%Y-%m-%dT%H:%MZ'")
                ### upload
                try:
                    pageDict['testSchema']['properties']['MEASUREMENT_DATE']=dateStr
                except ValueError:
                    st.write("   - ⚠ __issue__ check inputs")
                    pass

            ### results: raw data
            st.write("setting up test schema raw measurements...")
            pageDict['testSchema']['results']['IV_ARRAY']={}
            for k in ['voltage', 'current', 'sigma current']:
                try:
                    pageDict['testSchema']['results']['IV_ARRAY'][k]=[abs(x) for x in df_data[k].to_list()]
                except KeyError:
                    st.write(f"__No {k} data found__")
                    if k in ['voltage', 'current']:
                        st.write("Please check input data")
                        st.stop()
            ### scaling
            if "prefix" in df_header['key'].unique():
                prfxStr=df_header.query('key=="prefix"')['value'].values[0]
                scale=1
                try:
                    prfxStr=prfxStr.strip()
                    if prfxStr=="A":
                        scale=1e6
                    elif prfxStr=="mA":
                        scale=1e3
                    elif prfxStr=="uA" or prfxStr=="µA":
                        scale=1
                    elif prfxStr=="nA":
                        scale=1e-3
                    else:
                        st.write("don't understand prefix value")
                except TypeError:
                    st.write("Prefix doesn't seem to be string?!")
                st.write(f"Scaling current by 1/{scale} based on prefix: {prfxStr}")
                for k in ['current', 'sigma current']:
                    try:
                        print(f"- scaling {k}")
                        pageDict['testSchema']['results']['IV_ARRAY'][k]=[abs(x)*scale for x in pageDict['testSchema']['results']['IV_ARRAY'][k]]
                    except KeyError:
                        st.write(f"__No {k} data found__")
                        if k in ['current']:
                            st.write("Please check input data")
                            st.stop()
            else:
                st.write("No _prefix_ value found. Current is not scaled: assume µA")
            for k,v in {'temp':'temperature','hum':'humidity'}.items():
                try:
                    pageDict['testSchema']['results']['IV_ARRAY'][v]=df_data[k].to_list()
                    st.write(f"{k} info. added to IV_ARRAY")
                except KeyError:
                    st.write(f"no {k} info. found")
                    pass
            st.write("__Got data__")

        else:
            st.write("__Got data__")
        st.json(pageDict['testSchema'], expanded=False)

        st.write("---")

        ######################
        ### fill schema derived data
        ######################

        st.write("### Filling "+pageDict['testType']+" schema _derived_ data")
        
        ## match relation
        relOpts=['SENSOR_TILE','BARE_MODULE']
        rel_code=st.radio("Select componentType code of relative for CV & IV comparison:",relOpts)
        
        if "checkCode" not in pageDict.keys()  or pageDict['checkCode']!=rel_code or st.button(f"Check for {rel_code} relative"):
            st.write(f"Check {rel_code} relatives")
            checkObj=CheckRelatives([pageDict['testSchema']['component']],rel_code,'children')
            pageDict['checkCode']=rel_code
            if checkObj==None:
                st.write(f"- no {pageDict['checkCode']} relatives found :(")
                st.stop()
            else:
                st.write(f" - found {pageDict['checkCode']} relative: {checkObj['serialNumber']}")

            relObj= st.session_state.myClient.get('getComponent', json={'component':checkObj['code']})
            if relObj!=None:
                pageDict['relObj']=relObj
        else:
            st.write(f"Found {rel_code} relative: {pageDict['relObj']['serialNumber']}")

        if "relObj" not in pageDict.keys():
            st.write(f"__No {rel_code} object found. Cannot complete analysis__")
            st.stop()

        st.write(f"__Using {pageDict['relObj']['serialNumber']} for CV and IV comparison__")
        stTrx.DebugOutput("Relative object",pageDict['relObj'])

        if "tests" not in pageDict['relObj'].keys() or pageDict['relObj']['tests']==None:
            st.write("No tests found :( Cannot complete analysis. Please try another relative.")
            st.stop()

        ### find CV test object
        # Monika following if does not work as 1st statement becomes true when you first try to run over the bare module to look for Vdep, after the 1st try this goes to true even if Vdep is not found
        #if "Vdep" not in pageDict.keys() or st.button("get Vdep from CV:"):
        if "Vdep" not in pageDict.keys() or pageDict['Vdep']==None or st.button("Get Vdep from CV"):
            testTypeSnippet="CV"
            st.write(f"Trying to retrieve __{testTypeSnippet}__ information for depletion voltage")
            cvTests=[tr for tt in pageDict['relObj']['tests'] for tr in tt['testRuns'] if testTypeSnippet in tt['code'] and "testRuns" in tt.keys()]
            if len(cvTests)<1:
                st.write(f" - No {testTypeSnippet} found in component tests. Please try another or input manually.")
                pageDict['Vdep']=None
            ### make dataframe of testruns
            else:
                df_cv=pd.DataFrame(cvTests)
                ### tidy & order 
                df_cv['date']=pd.to_datetime(df_cv['date'], format='%Y-%m-%dT%H:%M:%S.%fZ')
                df_cv=df_cv.sort_values(by=["date"], ascending=False)
                st.write(f" - matched {testTypeSnippet} testRuns (newset first)",df_cv)
                ### use latest testRun
                cvId=df_cv['id'].to_list()[0]
                st.write(f" - using testRun: {cvId}")
                ### get testRun and extract Vdep value
                cvObj= st.session_state.myClient.get('getTestRun', json={'testRun':cvId})
                if cvObj!=None:
                    st.write(f" - Found test run. Retrieving: **V_FULLDEPL**")
                    pageDict['Vdep']=[res['value'] for res in cvObj['results'] if res['code']=="V_FULLDEPL"][0]
                else:
                    st.write(f" - ⚠ No CV object found for Id: {cvId}")
                    pageDict['Vdep']=None
            ### try sensor wafer
            if pageDict['Vdep']==None:
                st.write("No __CV__ test found. Trying sensor _wafer_...")
                if pageDict['relObj']['alternativeIdentifier']==None:
                    st.write(" - No alternative Identifier. Cannot find associated wafer :(")
                else:
                    altId="-".join(pageDict['relObj']['alternativeIdentifier'].split('-')[0:-2])
                    st.write(f" - Trying to retrieve compoennet information for wafer: {altId}")
                    pageDict['wafObj']= st.session_state.myClient.get('getComponent', json={'component':altId,'alternativeIdentifier':True})
                    # Monika: wrong way round
                    #if pageDict['wafObj']!=None:
                    if pageDict['wafObj']==None:
                        st.write(" - ⚠ Cannot find associated wafer :(")
                    else:
                        st.write(f" - Found associated wafer: {pageDict['wafObj']['serialNumber']}")
                        try:
                            # Monika: following wrong
                            #pageDict['Vdep']=pageDict['wafObj']['DEPLETION_VOLTAGE']
                            pageDict['Vdep']=[res['value'] for res in pageDict['wafObj']['properties'] if res['code']=="DEPLETION_VOLTAGE"][0]
                            st.write(f" - found Vdep: {pageDict['Vdep']}")
                        except KeyError:
                            st.write(" - No __DEPLETION_VOLTAGE__ found in wafer object")
        if pageDict['Vdep']!=None:
            st.write(f"Found Vdep: {pageDict['Vdep']}")

        ### check cvObj found
        if pageDict['Vdep']==None or st.checkbox('Set Vdep manually?'):
            ### nominal values
            if st.checkbox('Check nominal depletion voltages?'):
                st.dataframe(GetNominalDepletion("table"))
            # get nominal using wafer type
            if "wafObj" in pageDict.keys():
                st.write(" - retrieving nominal Vdep for",pageDict['wafObj']['type']['code'])
                pageDict['Vdep']=GetNominalDepletion(pageDict['wafObj']['type']['code'])
                st.write(f" - _nominal_ Vdep: {pageDict['Vdep']}")

            ### set manually
            if pageDict['Vdep']!=None:
                Vdep=st.number_input('Manual input depletion voltage:', value=pageDict['Vdep'])
            else:
                st.write(f" - No _nominal_ Vdep found")
                Vdep=st.number_input('Manual input depletion voltage:')
            
            ### check format and reset analysis flag
            try:
                pageDict['Vdep']=float(Vdep)
                st.write(f"Set Vdep: {pageDict['Vdep']}")
                try:
                    del pageDict['anaCheck']
                except KeyError:
                    pass
            except ValueError:
                st.write(f"Problem converting depletion voltage: {Vdep}. Please input again.")
                st.stop()

        st.write(f"__Using Vdep: {pageDict['Vdep']}__")

        if abs(pageDict['Vdep'])>100 or abs(pageDict['Vdep']<=0):
            st.write(" - ❓ this values seems high/low. Please use manual input if required.")
        else:
            st.write(" - Value does not seem crazy (no guarantees!)")

        # temporary hack while data-structure is updated
        # Monika: the parameter is called BREAKDOWN_VOLTAGE not BREAKDOWN_VOLTAGE_VALUE (did not want to update existing name)
        voltage_key="BREAKDOWN_VOLTAGE"
        if "BREAKDOWN_VOLTAGE" in pageDict['testSchema']['results'].keys():
            voltage_key="BREAKDOWN_VOLTAGE"

        st.write("---")

        #########################
        ### run analysis
        #########################

        if "anaCheck" not in pageDict.keys() or pageDict['Vdep']!=pageDict['Vdep_cache'] or st.button("Analyse raw data"):
            
            # calculate breakdown voltage (Vdb) and leakage current (Ilc)
            st.write(f"Using Vdep: {pageDict['Vdep']} @ {pageDict['testSchema']['properties']['TEMP']} ºC")
            pageDict['Vdep_cache']=pageDict['Vdep']
            Vbd, Ilc = IV_functions.calVbdIlc(
                pageDict['testSchema']["component"], pageDict['Vdep'], pageDict['testSchema']['results']['IV_ARRAY']['voltage'], pageDict['testSchema']['results']['IV_ARRAY']['current']
            )
            st.write(f"Derived values (based on [link](https://gitlab.cern.ch/huye/itk_pdb_testapp_pix_reception)):")
            st.write(f" - Break down voltage [V]: {Vbd}")
            st.write(f" - Leakage current [μA]: {Ilc}")
            if type(Vbd)==type("str") and Vbd.startswith('>'):
                Vbd=-999

            try:
                pageDict['testSchema']['results'][voltage_key]=float(Vbd)
            except ValueError:
                pageDict['testSchema']['results'][voltage_key]=Vbd
            try:
                pageDict['testSchema']['results']['LEAK_CURRENT']=float(Ilc)
            except ValueError:
                pageDict['testSchema']['results']['LEAK_CURRENT']=Ilc

            ### normalising plot
            df_plot=pd.DataFrame({'V':pageDict['testSchema']['results']['IV_ARRAY']['voltage'],
                                  'I':pageDict['testSchema']['results']['IV_ARRAY']['current'],
                                  'I_sig':pageDict['testSchema']['results']['IV_ARRAY']['sigma current']})
            df_plot['scale']="scaled"
            df_plot['scale_value']=1
            # ### ensure +ve values
            # df_plot['V']=df_plot['V'].apply(lambda x: x*-1 if x<0 else x)
            scale = 1
            temperature = pageDict['testSchema']['properties']['TEMP']
            humidity = pageDict['testSchema']['properties']['HUM']
            current_err = 0
            ### looping to normalise values to temperature & humidity
            for i in range(len(pageDict['testSchema']['results']['IV_ARRAY']['voltage'])):
                # check results for temperature
                try:
                    # if temperature taken for each I-V pair
                    if len(pageDict['testSchema']['results']['IV_ARRAY']['voltage']) == len(pageDict['testSchema']['results']['TEMPERATURE']):
                        temperature = pageDict['testSchema']['results']['TEMPERATURE'][i]
                    # if single value
                    elif len(pageDict['testSchema']['results']['TEMPERATURE']) == 1:
                        temperature = pageDict['testSchema']['results']['TEMPERATURE'][0]
                    # if other multiple
                    elif len(pageDict['testSchema']['results']['TEMPERATURE']) > 0:
                        temperature = np.nanmean(pageDict['testSchema']['results']['TEMPERATURE'])
                # check properties for temperature
                except KeyError:
                    # st.write("Temperature missing from results")
                    pass

                scale = IV_functions.scaleF(temperature, 20) + 1

                # check results for humidity
                try:
                    # if humidity taken for each I-V pair
                    if len(pageDict['testSchema']['results']['IV_ARRAY']['voltage']) == len(pageDict['testSchema']['results']['HUMIDITY']):
                        humidity = pageDict['testSchema']['results']['HUMIDITY'][i]
                    # if single value
                    elif len(pageDict['testSchema']['results']['HUMIDITY']) == 1:
                        humidity = pageDict['testSchema']['results']['HUMIDITY'][0]
                    # if other multiple
                    elif len(pageDict['testSchema']['results']['HUMIDITY']) > 0:
                        humidity = np.nanmean(pageDict['testSchema']['results']['HUMIDITY'])
                # check properties for temperature
                except KeyError:
                    # st.write("Humidity missing from results")
                    pass

                if len(pageDict['testSchema']['results']['IV_ARRAY']['voltage']) == len(pageDict['testSchema']['results']['IV_ARRAY']['sigma current']):
                    current_err = pageDict['testSchema']['results']['IV_ARRAY']['sigma current'][i]


                new_row = {'V':pageDict['testSchema']['results']['IV_ARRAY']['voltage'][i],'I':scale * pageDict['testSchema']['results']['IV_ARRAY']['current'][i], 'I_sig':scale * current_err, 'scale':"normalised", 'scale_value':scale}
                df_plot= df_plot.append(new_row, ignore_index=True)

            df_plot=df_plot.reset_index(drop=True)
            st.write("__normalised data__",df_plot)
            norm_chart=alt.Chart(df_plot).mark_line().encode(
                    x='V:Q',
                    y='I:Q',
                    color='scale:N',
                    tooltip=['V:Q','I:Q','scale:N']
            ).properties(width=600, title="Normalised data").interactive()
            st.altair_chart(norm_chart)
            pageDict['anaCheck']=True
        else:
            if "anaCheck" in pageDict.keys():
                st.write(f"Derived values (based on [link](https://gitlab.cern.ch/huye/itk_pdb_testapp_pix_reception)):")
                st.write(f" - Breakdown voltage [V]: {pageDict['testSchema']['results']['BREAKDOWN_VOLTAGE']}")
                st.write(f" - Leakage current [μA]: {pageDict['testSchema']['results']['LEAK_CURRENT']}")


        ######################
        ### DO ANALYSIS - pass/fail (comparison with previous)
        ######################
        st.write("---")
        st.write("### Compare with previous results")
        st.write("EDMS documentation: [link](https://edms.cern.ch/ui/#!master/navigator/document?D:100982225:100982225:subDocs)")

        testTypeSnippet="IV"
        ivTests=[tr for tt in pageDict['relObj']['tests'] for tr in tt['testRuns'] if testTypeSnippet in tt['code'] and "testRuns" in tt.keys()]
        if len(ivTests)<1:
            st.write(f"No {testTypeSnippet} found in component tests. Please try another relative.")
            st.stop()
        ### make dataframe of testruns
        df_iv=pd.DataFrame(ivTests)
        ### remove deleted
        df_iv=df_iv.query('state=="ready"').reset_index(drop=True)
        ### tidy & order 
        df_iv['date']=pd.to_datetime(df_iv['date'], format='%Y-%m-%dT%H:%M:%S.%fZ')
        df_iv=df_iv.sort_values(by=["date"], ascending=False).reset_index(drop=True)
        st.write("__Select previous test for comparison__")
        st.write(f"matched {testTypeSnippet} testRuns (newest first)",df_iv)
        ### by default use latest but allow other selection
        if "testIndex" not in pageDict.keys():
            pageDict['testIndex']=0
        if st.checkbox('select other testRun?'):
            pageDict['testIndex']=sel_index=st.selectbox("Select testRun by index:",df_iv.index.to_list())
            # st.write(df_iv.iloc[sel_index])
        ### get testRun id and notify to check
        ivId=df_iv.iloc[pageDict['testIndex']]['id']
        st.write(f"Using {pageDict['testIndex']}th testRun, id: {ivId}")
        ### get if unknown or on-demand or change
        if "ivObj" not in pageDict.keys() or st.button("compare to testRun") or pageDict['ivObj']['id']!=ivId:
            ivObj= st.session_state.myClient.get('getTestRun', json={'testRun':ivId})
            if ivObj!=None:
                pageDict['ivObj']=ivObj
            else:
                st.write("No test object found :(")
                st.stop()
        stTrx.DebugOutput("IV object",pageDict['ivObj'])
        ### make comparison table
        if len([res for res in pageDict['ivObj']['results'] if "LINK_TO_" in res['code']])>0:
            st.write("This is not an IV test, only a link. Please select another test/component.")
            st.stop()

        # st.write("### Analysis to be implemented.")
        # st.stop()

        ### old values
        # Monika: remove _VALUE 
        #if "BREAKDOWN_VOLTAGE_VALUE" in [res['code'] for res in pageDict['ivObj']['results']]:
        if "BREAKDOWN_VOLTAGE" in [res['code'] for res in pageDict['ivObj']['results']]:
            old_Vbd=[res for res in pageDict['ivObj']['results'] if res['code']=="BREAKDOWN_VOLTAGE"][0]['value']
        else:
            old_Vbd=[res for res in pageDict['ivObj']['results'] if res['code']=="BREAKDOWN_VOLTAGE"][0]['value']
        old_Ilc=[res for res in pageDict['ivObj']['results'] if res['code']=="LEAK_CURRENT"][0]['value']
        # st.write([old_Vbd,old_Ilc])

        ### new values
        new_Vbd=pageDict['testSchema']['results'][voltage_key]
        new_Ilc=pageDict['testSchema']['results']["LEAK_CURRENT"]

        ### max values
        new_Vmax=pageDict['testSchema']['results']['IV_ARRAY']['voltage'][-1]
        try:
            # try MAXIMUM_VOLTAGE first
            old_Vmax=next((item['value'] for item in pageDict['ivObj']['results'] if item['code']=="MAXIMUM_VOLTAGE" ), None )
            # if none there, get last in VOLTAGE array
            if old_Vmax==None:
                old_Vmax=next((item['value']['voltage'][-1] for item in pageDict['ivObj']['results'] if item['code']=="IV_ARRAY" ), None )
        except KeyError:
            old_Vmax=None
        except TypeError:
            old_Vmax=None


        df_comp=pd.DataFrame([
            {'name':"BREAKDOWN_VOLTAGE", 'new':new_Vbd, 'old':old_Vbd},
            {'name':"LEAK_CURRENT", 'new':new_Ilc, 'old':old_Ilc},
            {'name':"MAX_VOLTAGE", 'new':new_Vmax, 'old':old_Vmax}
        ])
        df_comp['diff'] = df_comp.apply(lambda x: abs(x['new']-x['old']) if type(x['new'])==type(1.1) and type(x['old'])==type(1.1) else str(type(x['new']))+"-"+str(type(x['old'])), axis=1)

        st.write("__IV comparison__:",df_comp)

        ## 1. check innate breakdown 
        # no Vbk found
        # Monika: changed to float earlier
        #if type(pageDict['testSchema']['results'][voltage_key])==type("str"):
        if pageDict['testSchema']['results'][voltage_key]==-999:
            # flag NO breakdown
            pageDict['testSchema']['results']['NO_BREAKDOWN_VOLTAGE_OBSERVED'] = True
            pageDict['testSchema']['results'][voltage_key] = -999
        # Vbk found
        else:
            # flag breakdown
            pageDict['testSchema']['results']['NO_BREAKDOWN_VOLTAGE_OBSERVED'] = False
            # leave BREAKDOWN_VOLTAGE as is
        
        # max measured VOLTAGE - last entry in array
        pageDict['testSchema']['results']['MAXIMUM_VOLTAGE'] = pageDict['testSchema']['results']['IV_ARRAY']['voltage'][-1]

        ## check breakdown comparison

        # Monika supress if no breakdown is found for both old/new
        # if new_Vbd!=-999 or old_Vbd!=-999:
        # - Kenny run for all
        pageDict['testSchema']['passed'] = IV_functions.GetNewToOldComp(new_Vbd,old_Vbd,old_Vmax)
        ## check maximum voltage (fail is False)
        if IV_functions.GetVmaxCheck(pageDict['testSchema']['results']['IV_ARRAY']['voltage'][-1], pageDict['Vdep'], pageDict['testSchema']['results'][voltage_key])==False:
            pageDict['testSchema']['passed'] = False
        ## check leakage current (fail is False)
        # get sensor object
        if selMode!="MODULE":
            if "relObj" not in pageDict.keys():
                st.write("No related sensor object found. Cannot check leakage current per area.")
                compObj= st.session_state.myClient.get('getComponent', json={'component':pageDict['testSchema']['code']})
                if IV_functions.GetLeakageCheck(compObj['type']['code'], new_Ilc)==False:
                    pageDict['testSchema']['passed'] = False
            else:
                st.write(f"Using sensor type: {pageDict['relObj']['type']['code']}")
                if IV_functions.GetLeakageCheck(pageDict['relObj']['type']['code'], new_Ilc)==False:
                    pageDict['testSchema']['passed'] = False


        st.write("---")

        #########################
        ### Uploading
        #########################

        st.write("### Test Destination")
        ### check child componentType
        bmObj=None
        # if selected mode is NOT module then the test should go to sensor
        if selMode!="MODULE":
            # if related object is sensor tile then send test data to child component
            if pageDict['relObj']['componentType']['code']=="SENSOR_TILE":
                # announce associated component identifier
                st.write(f"Use SENSOR_TILE serialNumber ({pageDict['relObj']['serialNumber']}) for test upload")
                # get bare module
                bmObj= st.session_state.myClient.get('getComponent', json={'component':pageDict['testSchema']['component']})
                # if bare module not found, set to None
                if bmObj['componentType']['code']!="BARE_MODULE":
                    bmObj=None
                # update testschema component to releated object (sensor)
                pageDict['testSchema']['component']=pageDict['relObj']['serialNumber']
            # if not sensor tile
            else:
                # announce component identifier
                st.write("No SENSOR_TILE component found to set as test component")
                st.write(f" - Continue with input {pageDict['testSchema']['component']}")

        ### double-check relationship
        if selMode=="BARE_MODULE": 
            st.write("Checking PDB relationship:")
            destComp=st.session_state.myClient.get('getComponent', json={'component':pageDict['testSchema']['component']})
            st.write(f" - Destination: {destComp['serialNumber']} ({destComp['componentType']['code']})")
            # st.write(destComp)
            parComp=[x for x in destComp['parents'] if x['componentType']['code']=="BARE_MODULE"][0]
            # st.write(parComp)
            st.write(f" - Parent: {parComp['component']['serialNumber']} ({parComp['componentType']['code']})")
            # st.write(df_header)
            checkId=df_header.query('key=="component"')['value'].values[0]
            if parComp['component']['serialNumber']!=checkId:
                st.write(f" - __this does not match input identifier: {checkId}__ ❌")
                st.write(f"   Please check input data and PDB relations")
                st.stop()
            else:
                st.write(f" - __this matches input identifier: {checkId}__ ✔")


        ######################
        ### Review and edit
        ######################
        
        if st.checkbox("Review & Edit sensor tile information?"):
            st.write("## Review and edit schema")
            infra.ToggleButton(pageDict,'toggleEdit','Edit Schema?')
            if pageDict['toggleEdit']:
                st.write("### Edit Schema")
                pageDict['testSchema']=pdbTrx.EditJson(pageDict['testSchema'])
            else:
                st.write("### Review Schema")
                st.write(pageDict['testSchema'])

            infra.ToggleButton(pageDict,'toggleText','Convert all values to text?')
            if pageDict['toggleText']:
                pageDict['testSchema'] = json.loads(json.dumps(pageDict['testSchema']), parse_int=str)
                pageDict['testSchema']['results'] = json.loads(json.dumps(pageDict['testSchema']['results']), parse_float=str)
            if st.session_state.debug:
                st.write("### **DEBUG** (final!) Test Schema")
                st.write(pageDict['testSchema'])

        ######################
        ### Registration
        ######################
        st.write("---")
        st.write("## Test Registration")

        if st.checkbox("Stage Coherency Check"):
            st.write("__Untick box to return to upload__")
            pdbTrx.CheckStageCoherence(st.session_state.myClient, pageDict['testSchema']['component'], False)
            # pageDict['stCheck']=None

        # upload(!) test schema: change get --> post
        chnx.RegChunk(pageDict, "TestRun", "testSchema")
        ### store SENSOR upVal before replaced by BM
        # if "upVal_SENSOR" not in pageDict.keys():
        #     pageDict['upVal_SENSOR']=copy.deepcopy(pageDict['upVal'])
            
        ######################
        ### Bare Module appendix
        ######################
        
        if selMode!="BARE_MODULE":
            st.stop()

        #Monika now prepare upload of test under bare modules (if SN is from a bare module)
        if "upVal" in pageDict.keys() and orig_SN:
            
            origObj= st.session_state.myClient.get('getComponent', json={'component': orig_SN})
            
            st.info(f"__Sensor IV uploaded.\nPlease upload complimentary test to related Bare Module__ {orig_SN}")

            st.write("---") 
            bmCompCode="BARE_MODULE"
            bmTestCode="BARE_MODULE_SENSOR_IV"
            st.write(f"### Select Stage for {bmCompCode} {bmTestCode} upload")
            # get comp schema if not already
            if "bmCompSchema" not in pageDict.keys():
                pageDict['bmCompSchema'] = st.session_state.myClient.get('getComponentTypeByCode', json={'project':pageDict['project'], 'code':bmCompCode})
            # st.write(pageDict['compSchema'])
            # get relevant stages with test
            stageList=[stage['code'] for stage in pageDict['bmCompSchema']['stages'] for tt in stage['testTypes'] if type(tt['testType'])==type({}) and "code" in tt['testType'].keys() and tt['testType']['code']==bmTestCode]
            pageDict['stage']=st.selectbox("Select stage code:",stageList,index=stageList.index(pageDict['stage']))

            ### 
            # if origObj['componentType']['code']=='BARE_MODULE':
            if selMode=="BARE_MODULE":
                st.write('prepare test upload for summary test on bare module',orig_SN)
                #if st.button("add link to parent"):
                otherTestName="BARE_MODULE_SENSOR_IV"
                if "bmTestSchema" not in pageDict.keys():
                    pageDict['bmTestSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':'P', 'componentType':origObj['componentType']['code'], 'code':otherTestName, 'requiredOnly':True})
                ### set values from SENSOR_TILE test
                pageDict['bmTestSchema']['component']=orig_SN
                for k in ['institution','runNumber','date','passed','problems']:
                    pageDict['bmTestSchema'][k]=pageDict['testSchema'][k]
                for p in ['MEASUREMENT_DATE']:
                    pageDict['bmTestSchema']['properties'][p]=pageDict['testSchema']['properties'][p]
                ### define broken chips
                # pageDict['bmTestSchema']['results']['NON_WORKING_FE_CHIPS']=[]
                # if st.checkbox("Identify problem chips (if required)"):
                #     st.write("Select __NON__ working Front Ends (using 0-3 positions)")
                #     # 0-3 is used
                #     for i in range(0,4,1):
                #         feVal=st.checkbox(f"FE chip {i} NOT working")
                #         if feVal and i not in pageDict['bmTestSchema']['results']['NON_WORKING_FE_CHIPS']:
                #             pageDict['bmTestSchema']['results']['NON_WORKING_FE_CHIPS'].append(i)
                #         if feVal==False and i in pageDict['bmTestSchema']['results']['NON_WORKING_FE_CHIPS']:
                #             pageDict['bmTestSchema']['results']['NON_WORKING_FE_CHIPS'].remove(i)
                # st.write("Identified non-working chips:")
                # st.write(pageDict['bmTestSchema']['results']['NON_WORKING_FE_CHIPS'])
                ### update link id if last test uploaded (upVal) was for sensor (we have sensor compType id from above!)
                if pageDict['upVal']['componentTestRun']['componentType']==pageDict['compSchema']['id']:
                    try:
                        st.write("Adding reference to sensor testRun id:",pageDict['upVal']['testRun']['id'])
                        pageDict['bmTestSchema']['results']['LINK_TO_SENSOR_IV_TEST']= pageDict['upVal']['testRun']['id'] #f"https://itkpd-test.unicorncollege.cz/testRunView?id={sensortest['id']}"
                    except KeyError:
                        pass
                    except TypeError:
                        pass
                    if pageDict['bmTestSchema']['results']['LINK_TO_SENSOR_IV_TEST']==None:
                        st.write("No sensor testRun reference attached.")
                        st.write(pageDict['upVal'])
                
                ### upload
                if st.checkbox("Review & Edit bare module information?"):
                    st.write("## Review and edit schema")
                    if st.checkbox('Edit Schema?', key="BM"):
                        st.write("### Edit Schema")
                        pageDict['bmTestSchema']=pdbTrx.EditJson(pageDict['bmTestSchema'])
                    else:
                        st.write("### Review Schema")
                        st.write(pageDict['bmTestSchema'])

                # upload(!) test schema: change get --> post
                chnx.RegChunk(pageDict, "TestRun", "bmTestSchema")

            st.write("---")

            if st.checkbox("Optional check BARE_MODULE link present?"):
                st.write(f"Checking {orig_SN} for BARE_MODULE_SENSOR_IV (BARE_MODULE) testTypes with matching IV_MEASURE (SENSOR_TILE) link {pageDict['bmTestSchema']['results']['LINK_TO_SENSOR_IV_TEST']}")
                if pdbTrx.CheckTestDataMatch(st.session_state.myClient, pageDict['bmTestSchema']['results']['LINK_TO_SENSOR_IV_TEST'], orig_SN, "BARE_MODULE_SENSOR_IV", "LINK_TO_SENSOR_IV_TEST", "id"):
                    st.write("__test link is present__ ✔")
                else:
                    st.write("__test link not found__ ❌")

