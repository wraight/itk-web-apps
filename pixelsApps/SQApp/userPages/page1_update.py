### standard
import streamlit as st
import streamlit.components.v1 as components
from core.Page import Page
### custom
import pandas as pd
import datetime
import ast
import csv
import json
import copy
import altair as alt
import numpy as np
import os
from io import StringIO
import io
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################
infoList=[" * check component sub-type",
        " * upload component schemas to PDB",
        " * delete component from PDB if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Update SQ", ":microscope: Update Site Qualification", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULESQ"
        if "project" not in pageDict.keys():
            ## set by default
            pageDict['project']="P"
            pageDict['subProject']="PG"
        # if pageDict['project']!=st.session_state.Authenticate['proj']['code']:
            # pageDict['project']=st.session_state.Authenticate['proj']['code']
            # pageDict['subProject']= next((item['code'] for item in st.session_state.Authenticate['proj']['subprojects'] if "general" in item['name']),None)
        if "institution" not in pageDict.keys():
            pageDict['institution']=st.session_state.Authenticate['inst']['code']
        if pageDict['institution']!=st.session_state.Authenticate['inst']['code']:
            pageDict['institution']=st.session_state.Authenticate['inst']['code']
        
        ### current user
        st.write("## Current User:")
        st.write("### name:",st.session_state.Authenticate['user']['firstName'],st.session_state.Authenticate['user']['lastName'])
        st.write("### institute:",st.session_state.Authenticate['inst']['code'])

        st.write("Some instruction on using the app can be found [here](https://indico.cern.ch/event/1258215/contributions/5285054/attachments/2598717/4487552/WP13_23_2_23_KGW.pdf)")

        # select other institute than user's
        infra.ToggleButton(pageDict,'tog_other',"Use other institute")
        if pageDict['tog_other']:
            infra.SelectBox(pageDict,'institution',[x['code'] for x in st.session_state.Authenticate['instList']],"Select institution:")
        
        st.write("---")

        ### Current information
        st.write("## Current Site Qualification for",pageDict['institution'])
        st.write("Stages and tests are detailed here: [googleDoc](https://docs.google.com/spreadsheets/d/1mCRBw6FqDFyPDkdJYjiWVeOllYERfKDbsypDy-LK7Uk)")
        # get site qualification object (should be exactly one per site)
        if "sqObj" not in pageDict.keys() or st.button("re-check component") or pageDict['sqObj']['institution']['code']!=pageDict['institution']:

            sqObjs = st.session_state.myClient.get('listComponents', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'institution':pageDict['institution'] })
            if type(sqObjs)!=type([]):
                sqObjs=sqObjs.data
            # st.write(pageDict['sqObj'])

            if len(sqObjs)>1:
                st.write(":exclamation: Found more than one?!")
                st.write([sqo['serialNumber'] for sqo in sqObjs])
                st.write(f"Will continue with first {sqObjs[0]['serialNumber']}!")
            elif len(sqObjs)<1:
                st.write(f":exclamation: No SQ component found for {pageDict['institution']}!")
                infra.ToggleButton(pageDict,'tog_reg',"Register SQ component?")
                if pageDict['tog_reg']:
                    pageDict['compSchema']={
                        'project': pageDict['project'], 
                        'subproject': pageDict['subProject'], 
                        'institution': pageDict['institution'], 
                        'componentType': pageDict['componentType'], 
                        'type': "MSQ_OBJECT", 
                        'properties': None
                            }
                    st.write(pageDict['compSchema'])
                    chnx.RegChunk(pageDict, 'Component', 'compSchema')
                    st.write("Please re-check component after registration")
                ### remove existing object if exists (multi-user case)
                else:
                    pageDict.pop('sqObj', None)
                st.stop()
            else:
                stTrx.DebugOutput(f"Got the one {sqObjs[0]['serialNumber']} :)")
            
            pageDict['sqObj']=st.session_state.myClient.get('getComponent',json={'component':sqObjs[0]['serialNumber'] })
        
        # full info. option
        infra.ToggleButton(pageDict,'tog_full',"See full component info")
        if pageDict['tog_full']:
            st.write(pageDict['sqObj'])
        

        ### Make stage order list (if not defined)
        if "stageOrderList" not in pageDict.keys():
            compTypeInfo=st.session_state.myClient.get('getComponentTypeByCode', json={'project':pageDict['project'],'code':pageDict['componentType']})
            pageDict['stageOrderList'] = [x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]

            # get stage-test groups by breaking heirarchy
            df_compTypeInfo=pd.json_normalize(compTypeInfo)
            df_compTypeInfo=df_compTypeInfo.explode('stages')
            df_compTypeInfo=df_compTypeInfo.rename(columns={"id": "compType_id", "code": "compType_code", "name": "compType_name"})
            df_compTypeInfo=pd.concat([df_compTypeInfo.drop(columns="stages"),df_compTypeInfo['stages'].apply(pd.Series)],axis=1)
            df_compTypeInfo=df_compTypeInfo.explode('testTypes')
            df_compTypeInfo=df_compTypeInfo.rename(columns={"id": "stage_id", "code": "stage_code", "name": "stage_name", "order": "stage_order"})
            df_compTypeInfo=pd.concat([df_compTypeInfo.drop(columns="testTypes"),df_compTypeInfo['testTypes'].apply(pd.Series)],axis=1)
            df_compTypeInfo=pd.concat([df_compTypeInfo.drop(columns="testType"),df_compTypeInfo['testType'].apply(pd.Series)],axis=1)
            df_compTypeInfo=df_compTypeInfo.rename(columns={"id": "testType_id", "code": "testType_code", "name": "testType_name", "order": "testType_order"})
            pageDict['df_cti']=df_compTypeInfo
        
        if st.session_state.debug:
            infra.ToggleButton(pageDict,'tog_cti',"See component test info.")
            if pageDict['tog_cti']:
                st.write(pageDict['df_cti'])

        ### Stage history
        # option to show
        infra.ToggleButton(pageDict,'tog_stageHist',"See stage history")
        if pageDict['tog_stageHist']:
            st.write("### Stage History")

            stTrx.DebugOutput("### stage-test dataframe",pageDict['df_cti'])

            try:
                df_stages=pd.DataFrame(pageDict['sqObj']['stages'])

                ### formatting
                df_stages=df_stages[['code','name','dateTime']]
                # necessary hack to deal with strange formatting (datetime.datetime instead of pandas._libs.tslibs.timestamps.Timestamp)
                # df_stages['stage_dateTime']= df_stages['stage_dateTime'].apply(lambda x: str(x).split(':')[0])
                df_stages['dateTime']= pd.to_datetime(df_stages['dateTime'],format='%Y-%m-%dT%H:%M:%S.%fZ',errors='coerce')
                df_stages.sort_values(by=['dateTime'], inplace=True)
                df_stages=df_stages.reset_index(drop=True)
                # display df
                
                infra.ToggleButton(pageDict,'tog_stageInfo',"See stage history list")
                if pageDict['tog_stageInfo']:
                    st.dataframe(df_stages)

                stageChart=alt.Chart(df_stages).mark_line(point=True).encode(
                    x=alt.X('dateTime:T',axis = alt.Axis(title = "Date", format = ("%d %b %Y"))),
                #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                    y=alt.Y('code:N',title="Stage",sort=pageDict['stageOrderList']),
                    order='dateTime',
                    tooltip=['dateTime:T','code:N']
                ).configure_point(size=60).properties(width=800, height=400, title="Stage History").interactive()
                st.altair_chart(stageChart)

            except KeyError:
                st.write("no _stages_ key found")
            except TypeError:
                st.write("no stages found")
            # stTrx.ColourDF(df_stages,'stageCode')


        st.write("### Test History")
        try:
            df_test=pd.DataFrame(pageDict['sqObj']['tests'])
            # st.dataframe(df_test)
            df_test=df_test.explode('testRuns')
            # df_test=df_test['testRuns'].apply(pd.Series)
            df_test=df_test.rename(columns={"id": "testType_id", "code": "testType_code", "name": "testType_name"})
            df_test=pd.concat([df_test.drop(columns="testRuns"),df_test['testRuns'].apply(pd.Series)],axis=1)
            
            ### formatting
            df_test=df_test[['testType_code','testType_name','state','runNumber','passed','problems','date']]
            # necessary hack to deal with strange formatting (datetime.datetime instead of pandas._libs.tslibs.timestamps.Timestamp)
            df_test['date']= pd.to_datetime(df_test['date'],format='%Y-%m-%dT%H:%M:%S.%fZ',errors='coerce')
            df_test.sort_values(by=['date'], inplace=True)
            df_test=df_test.reset_index(drop=True)
            df_test['stage']=df_test['testType_code'].apply(lambda x:  pageDict['df_cti'].query('testType_code=="'+x+'"')['stage_code'].values[0] )
            # display df
            # stTrx.ColourDF(df_test,'testCode')

            infra.ToggleButton(pageDict,'tog_testInfo',"See test history list")
            if pageDict['tog_testInfo']:
                st.write("All tests uploaded to SQ component")
                infra.ToggleButton(pageDict,'removeDel',"Ignore deleted tests?")
                if pageDict['removeDel']:
                    df_test=df_test.query('state=="ready"')
                st.dataframe(df_test)

            testChart=alt.Chart(df_test).mark_circle(size=60).encode(
                        x=alt.X('date:T',axis = alt.Axis(title = "Date", format = ("%d %b %Y"))),
                    #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                        y=alt.Y('stage:N',title="stage",sort=pageDict['stageOrderList']),
                        color=alt.Color('testType_code:N'),
                        shape=alt.Shape('passed:N'),
                        tooltip=['date:T','stage:N','testType_code:N','passed:N']
                        ).properties(width=800, height=400, title="Test History").interactive()
            st.altair_chart(testChart)

            def GetStatus(x,df):
                if df.query('testType_code=="'+x+'"').empty:
                    return "No"
                else:
                    return "Yes"

            ### global review grid
            pageDict['df_cti']['present']=pageDict['df_cti']['testType_code'].apply(lambda x: GetStatus(x,df_test) )
        
        except KeyError:
            st.write("no _tests_ key found")
            ### hack to plot check grid
            pageDict['df_cti']['present']="No"
        except TypeError:
            st.write("no tests found")
        
        
        circ=alt.Chart(pageDict['df_cti']).mark_circle(yOffset=40, size=150).encode(
            x=alt.X('stage_code:N', title=None, sort=pageDict['stageOrderList'], axis=alt.Axis(labelAngle=0)),
            y=alt.Y('testType_order:Q', axis=None),
            color=alt.Color('present:N',title="Test Uploaded?"),
            tooltip=['stage_code:N','testType_code:N','present:N']
        )
        rect=alt.Chart(pageDict['df_cti']).mark_rect(filled=False).encode(
            x=alt.X('stage_code:N', sort=pageDict['stageOrderList']),
            y=alt.Y('testType_order:Q')
        )
        text=alt.Chart(pageDict['df_cti']).mark_text(yOffset=25).encode(
            x=alt.X('stage_code:N', sort=pageDict['stageOrderList']),
            y=alt.Y('testType_order:Q'),
            text=alt.Text('testType_code:N')
        )
        testCombPlot=(circ+rect+text).properties(width=800, height=500, title={
            "text": ["Test Review Status"]
                }).interactive()
        st.altair_chart(testCombPlot)




        ### table including status with selection of comment
        # select latest by default

        ### Select SQ information to upload
        st.write("---")
        st.write("## Select SQ test")

        st.write("### stages and testTypes for "+pageDict['componentType'])
       
        # select stage
        infra.SelectBox(pageDict,'stageCode',pageDict['df_cti'].sort_values(by='stage_order')['stage_code'].unique(),"Select stage code:")
        # select test
        infra.SelectBox(pageDict,'testCode',pageDict['df_cti'].query('stage_code=="'+pageDict['stageCode']+'"').sort_values(by='testType_order')['testType_code'].unique(),"Select testType code (in "+pageDict['stageCode']+"):")

        # ### check if schema has changed
        # try:
        #     if pageDict['testSchema']['testType']!=pageDict['testCode']:
        #         st.write("__Please Reset Schema__")
        # except KeyError:
        #     pass

        # get test schema
        if "testSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testCode']+"@"+pageDict['stageCode']+" for "+pageDict['componentType']) or pageDict['testSchema']['testType']!=pageDict['testCode']:
            pageDict['testSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testCode']})#, 'requiredOnly':True})
            testTypeObj = st.session_state.myClient.get('getTestTypeByCode', json={'project':pageDict['project'],'componentType':pageDict['componentType'],'code':pageDict['testCode']})
            pageDict['df_paras']= pd.DataFrame(testTypeObj['parameters'])
            pageDict['df_props']= pd.DataFrame(testTypeObj['properties'])
            # hack to coordinate with chnx.RegChunk
            pageDict['stage']=pageDict['stageCode']
        # add defaults
        pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
        dateStr=datetime.datetime.now().strftime("%Y-%m-%dT%H:%MZ")
        pageDict['testSchema']['date']=pdbTrx.TimeStampConverter(dateStr, "%Y-%m-%dT%H:%MZ")
        pageDict['testSchema']['component']=pageDict['sqObj']['serialNumber']
        stTrx.DebugOutput("Original *schema*:",pageDict['testSchema'])

        # st.write("Pre-update testSchema",pageDict['testSchema'])


        st.write("---")
        st.write("## Update SQ test")
        # ### Upload JSON
        # exampleFileName = pageDict["testCode"] + "_example.json"
        # df_input = chnx.UploadJSONChunk(pageDict, exampleName=exampleFileName, exampleDict=pageDict['testSchema'])
        # if df_input:
        #     pageDict['testSchema'] = df_input

        st.write("### Update Properties")
        try:
            if len(pageDict['testSchema']['properties'])<1:
                st.write("No _properties_ to update")
            else:
                for k,v in pageDict['testSchema']['properties'].items():
                    if pageDict['df_props'].query('code=="'+k+'"').empty:
                        st.write("no matching code found:",k)
                        continue
                    paraDict=pageDict['df_props'].query('code=="'+k+'"').reset_index().to_dict()
                    paraDict={k:v[0] for k,v in paraDict.items()}
                    # if paraDict['required']:
                    #     st.write(" - required!")
                    st.write("Input __"+paraDict['name']+"__ -",paraDict['description'])
                    stTrx.DebugOutput("( "+paraDict['dataType']+" , "+str(paraDict['valueType'])+" )")
                    ### input value
                    # if paraDict['code']=="SQ_STATUS":
                    #     st.write(paraDict)
                    pageDict['testSchema']['properties'][k]=pdbTrx.InputType(paraDict,v)
        except KeyError:
            st.write("No _properties_ key to update")

        st.write("### Update Results")
        if len(pageDict['testSchema']['results'])<1:
            st.write("No _results_ to update")
        else:
            for k,v in pageDict['testSchema']['results'].items():
                if pageDict['df_paras'].query('code=="'+k+'"').empty:
                    st.write("no matching code found:",k)
                    continue
                paraDict=pageDict['df_paras'].query('code=="'+k+'"').reset_index().to_dict()
                paraDict={k:v[0] for k,v in paraDict.items()}
                # if paraDict['required']:
                #     st.write(" - required!")
                st.write("Input __"+paraDict['name']+"__ -",paraDict['description'])
                stTrx.DebugOutput("( "+paraDict['dataType']+" , "+str(paraDict['valueType'])+" )")
                ### input value
                pageDict['testSchema']['results'][k]=pdbTrx.InputType(paraDict,v)

        # ### Review and edit

        st.write("### Review Upload Schema")
        st.write(pageDict['testSchema'])
        # chnx.ReviewAndEditChunk(pageDict,'testSchema')

        ### no more file attachments
        # infra.ToggleButton(pageDict,'tog_file',"Add file?")
        # if pageDict['tog_file']:
        #     st.write("### File Attachment")
        #     st.write("Please limit file size < 1 Mb")

        #     ### make dict for file object and name
        #     if "files" not in pageDict.keys():
        #         pageDict['files']=[]

        #     ### make array for upload 
        #     fileObj = st.file_uploader("Add file", type=["csv","pdf","png","jpg","ppt","pptx"])

        #     ## make sure file uploaded
        #     if fileObj==None:
        #         st.write("Please upload file")
        #         st.stop()

        #     fileName = st.text_input("File name (for reviwer reference):",fileObj.name)

        #     if st.button("Add file for upload?"):
        #         pageDict['files'].append({'obj':fileObj, 'name':fileName})
        #         st.success(f"file: {fileName} added for upload")
        #     ### TO_DO: edit RegChunk to upload based on file object, dictionary or list (of dictionaries)

        # if "files" in pageDict.keys():
        #     st.write(f"test data has {len(pageDict['files'])} selected for attachment.")
        #     infra.ToggleButton(pageDict,'tog_remove',"remove item from list?")
        #     if pageDict['tog_remove']:
        #         remVal=st.selectbox('Select item to remove:',[x['name'] for x in pageDict['files']])
        #         if st.button("remove "+remVal):
        #             dictItem=next((item for item in pageDict['files'] if item['name']==remVal),None)
        #             if dictItem==None:
        #                 st.write("no matching item found")
        #             else:
        #                 del pageDict['files'][pageDict['files'].index(dictItem)]
        #                 st.write("done. uncheck box")

        st.write("---")

        st.write("### Upload to PDB")
        st.write("Add comment after registration if required")

        ### no more file attachments
        # upload(!)
        # if pageDict['tog_file']:
        #     chnx.RegChunk(pageDict, "TestRun", "testSchema", "files")
        # else:
        chnx.RegChunk(pageDict, "TestRun", "testSchema")


