### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

def GetBarcode(sn):
    CODEOBJECT = barcode.get_barcode_class("code128")
    obj=CODEOBJECT(sn, writer=ImageWriter())
    return obj.render()


infoList=["  * select *component* or *testRun*",
        "  * input *component*/*testTypeRun* id"]
#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("Quick Check", ":fast_forward: Quick Component/TestRun/Shipment Check", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # select component or test
        infra.Radio(pageDict,'checkSel',["component", "testRun", "shipping"],"What to check:")

        # input id
        inputID=st.text_input("Identifier:",value="")

        if inputID!="":
            try:
                if inputID not in pageDict['codes']:
                    pageDict['codes'].append(inputID)
            except KeyError:
                pageDict['codes']=[inputID]

        if st.button("Clear List"):
            pageDict['codes']=[]

        st.write("codes to find:")
        try:
            st.write(pageDict['codes'])
        except KeyError:
            st.write("not yet set")

        if st.button("Get!"):
            pageDict['retVals']=[]
            for c in pageDict['codes']:

                if pageDict['checkSel']=="component":
                    retInfo=st.session_state.myClient.get('getComponent', json={'component':c})

                if pageDict['checkSel']=="testRun":
                    retInfo=st.session_state.myClient.get('getTestRun', json={'testRun':c})

                if pageDict['checkSel']=="shipping":
                    retInfo=st.session_state.myClient.get('getShipment', json={'shipment':c})

                try:
                    if retInfo!=None:
                        st.write("### :white_check_mark: Found **"+c+"**!")
                        pageDict['retVals'].append(retInfo)
                        if st.session_state.debug:
                            st.write("Found!",retInfo)

                    # ### delete option if found
                    # if st.button("Delete?"):
                    #     if pageDict['checkSel']=="component":
                    #         pageDict['delInfo']=st.session_state.myClient.post('deleteComponent', json={'component':pageDict['code']})
                    #
                    #     if pageDict['checkSel']=="testRun":
                    #         pageDict['delInfo']=st.session_state.myClient.post('deleteTestRun', json={'testRun':pageDict['code']})
                    #
                    #     if pageDict['checkSel']=="shipping":
                    #         pageDict['delInfo']=st.session_state.myClient.post('deleteShipment', json={'shipment':pageDict['code']})
                    #
                    #     try:
                    #         if pageDict['delInfo']!=None:
                    #             st.write("### :white_check_mark: Found!")
                    #             st.write("Deleted!",pageDict['delInfo'])
                    #         else:
                    #             st.write("### :no_entry_sign: delete unsuccessful")
                    #     except KeyError:
                    #         pass

                    else:
                        st.write("### :no_entry_sign: **"+c+"** search unsuccessful")
                except KeyError:
                    pass

        if 'retVals' not in pageDict.keys():
            st.stop()

        if len(pageDict['retVals'])<1:
            st.write("no PDB information found")
            st.stop()

        st.write("PDB information")
        df_compList=pd.json_normalize(pageDict['retVals'], sep = "_")
        if pageDict['checkSel']!="component":
            st.dataframe(df_compList)
        else:
            #Get standard info. Set (a la Unicorn): SN, altID, compType, subType, home inst, curLoc, shipping dest
            std_list=['serialNumber','alternativeIdentifier','componentType_name','type_name','institution_name','currentLocation_name']
            st.dataframe(df_compList[std_list].drop_duplicates())
            for sn in df_compList['serialNumber'].values:
                if st.button("Get barcode: "+str(sn)):
                    st.image(GetBarcode(sn))

            st.write("More info?..")
            if st.session_state.debug:
                st.write("**DEBUG** full return list")
                st.write(df_compList)

            otherList=[x for x in list(df_compList.columns) if x not in std_list]
            infra.MultiSelect(pageDict,'moreCols',otherList,"select columns")

            st.dataframe(df_compList[['serialNumber']+pageDict['moreCols']].drop_duplicates())

        if st.checkbox('delete some?'):
            pageDict['delInfo']=[]
            for index, row in df_compList.iterrows():
                if st.button(row['serialNumber']+" ("+row['componentType_name']+")"):
                    if pageDict['checkSel']=="component":
                        delInfo=st.session_state.myClient.post('deleteComponent', json={'component':row['serialNumber']})

                    if pageDict['checkSel']=="testRun":
                        delInfo=st.session_state.myClient.post('deleteTestRun', json={'testRun':row['serialNumber']})

                    if pageDict['checkSel']=="shipping":
                        delInfo=st.session_state.myClient.post('deleteShipment', json={'shipment':row['serialNumber']})

                    try:
                        if delInfo!=None:
                            st.write("### :white_check_mark: deleted "+row['serialNumber']+"!")
                            pageDict['delInfos'].append(delInfo)
                        else:
                            st.write("### :no_entry_sign: **"+row['serialNumber']+"** delete unsuccessful")
                    except KeyError:
                            pass
            if st.session_state.debug:
                st.write("**DEBUG** deletion information")
                st.write(pageDict['delInfos'])
