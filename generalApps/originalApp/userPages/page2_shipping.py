### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

infoList=["  * **Updating** shipment:",
        "    - input shipment ID",
        "    - select status",
        "    - update",
        "  * **Creating** shipment:",
        "    - select *sender* and *recipient*",
        "    - select shipment type",
        "    - add componentIDs (*csv* file)",
        "    - review components and create"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Shipping", ":ship: Shipping", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # select shipping type
        st.write("### Select action")
        infra.Radio(pageDict,'action',["Create shipment", "Update shipment"],"Select action:")

        if pageDict['action']=="Update shipment":
            # update existing shipment
            st.write("### Update **existing** shipment *status*")
            # input id
            infra.TextBox(pageDict,'existId',"Shipment id:")
            if st.button("Get!"):
                pageDict['existInfo']=st.session_state.myClient.get('getShipment', json={'shipment':pageDict['existId']})
            try:
                if pageDict['existInfo']!=None:
                    st.write("### :white_check_mark: Found!")
                    st.write("Found! \""+pageDict['existInfo']['name']+"\"")
                    infra.ToggleButton(pageDict,'fullShip',"See full shipment information")
                    if pageDict['fullShip']:
                        st.write("Full shipment detail:")
                        st.write(pageDict['existInfo'])
                    # select shipping status
                    pageDict['status']=pageDict['existInfo']['status']
                    infra.Radio(pageDict,'status',["prepared", "inTransit", "delivered", "deliveredIncomplete", "deliveredWithDamage", "undelivered"],"Select shipment *status*:")
                    if st.button("Update!"):
                        #pageDict['updateInfo']=st.session_state.myClient.post('updateShipment', json={'shipment':pageDict['existId'],'status':pageDict['status']})
                        pageDict['updateInfo']=st.session_state.myClient.post('setShipmentStatus', json={'shipment':pageDict['existId'],'status':pageDict['status']})
                        try:
                            if pageDict['updateInfo']!=None:
                                st.write("### :white_check_mark: Found!")
                                st.write("Updated!",pageDict['updateInfo'])
                            else:
                                st.write("### :no_entry_sign: update unsuccessful")
                        except KeyError:
                            pass
                else:
                    st.write("### :no_entry_sign: search unsuccessful")
            except KeyError:
                pass

        if pageDict['action']=="Create shipment":
            ###
            # select institution codes
            st.write("### Create **new** shipment")
            st.write("### Select *Sender* & *Recipient* Institutions")
            #st.write(st.session_state.Authenticate['inst']['name'])
            if  st.button("reset sender") or "sender" not in pageDict.keys():
                pageDict['sender']=st.session_state.Authenticate['inst']
            if st.session_state.debug:
                st.write("**DEBUG** Full *institution* list")
                st.write(st.session_state.Authenticate['instList'])
            #st.write(pageDict['sender']['name'])
            infra.SelectBox(pageDict,'sender',st.session_state.Authenticate['instList'],'Select a **sender** Institution','name')
            infra.SelectBox(pageDict,'recipient',st.session_state.Authenticate['instList'],'Select a **recipient** Institution','name')

            # select shipping type
            st.write("### Select shipping *type*")
            shipOpts=[{'name':"domestic",'code':"domestic"},{'name':"Intra-continental (within a continent)",'code':"intraContinental"},{'name':"Inter-continental (between continents)",'code':"continental"}]
            infra.Radio(pageDict,'type',[so['name'] for so in shipOpts],"Select shipment *type*:")

            # upload components
            st.write("Upload *component codes* (ASNs/codes) from **csv** file")
            st.write("Follow data format:")
            df_example=pd.DataFrame([{'serialNumber':'20Uxxyynnnnnnn'},{'serialNumber':'20Uxxyynnnnnnn'},{'serialNumber':'20Uxxyynnnnnnn'}])
            st.write(df_example.style.hide_index())
            pageDict['file']= st.file_uploader("Upload a file", type=["csv"])
            if st.session_state.debug: st.write(pageDict['file'])

            #dataSel = GetData(join(state.selectDir, state.selectFile))
            if pageDict['file'] is not None:
                pageDict['file'].seek(0)
                df_input=pd.read_csv(pageDict['file'])
                if "serialNumber" not in df_input.columns:
                    st.write("adding serialNumber header") # reset input file reader
                    pageDict['file'].seek(0)
                    df_input=pd.read_csv(pageDict['file'], header=None)
                    df_input=df_input.rename(columns={0: "serialNumber"})
                    #st.write(df_input)
                myComps=st.session_state.myClient.get('getComponentBulk', json={'component':list(df_input['serialNumber'].values)})
                #st.write(myComps)
                st.write("length of myComps:",len(myComps))
                pageDict['df_data']=pd.DataFrame([{'serialNumber':x['serialNumber'],'code':x['code'],'compCode':x['componentType']['code'],'curLoc':x['currentLocation']['code']} for x in myComps])
                st.dataframe(pageDict['df_data'])
                st.write("length of dataframe:",len(pageDict['df_data']))
                #st.write(list(pageDict['df_data']['code'].values))
            else:
                st.write("No data file set")
                st.stop()

            infra.TextBox(pageDict,'shipName',"Name of shipment:")

            shipObj={'name':pageDict['shipName'],'sender':pageDict['sender']['code'],'recipient':pageDict['recipient']['code'],'type':next(item for item in shipOpts if item['name'] == pageDict['type'])['code'],'shipmentItems':list(pageDict['df_data']['code'].values)}

            infra.ToggleButton(pageDict,'moreData',"Add tracking information")
            if pageDict['moreData']:
                infra.TextBox(pageDict,'trackNum',"Tracking number:")
                infra.TextBox(pageDict,'shipService',"Shipping service:")
                shipObj['trackingNumber']=pageDict['trackNum']
                shipObj['shippingService']=pageDict['shipService']

            if st.session_state.debug:
                st.write("shipping schema for upload...")
                st.write(shipObj)

            # access database
            if st.button("Create Shipping!"):
                ### create shipment
                try:
                    pageDict['retVal']=st.session_state.myClient.post('createShipment', json=shipObj)
                    st.write(pageDict['retVal'])
                    st.write("### **Successful shipment created** for:",pageDict['retVal']['name'])
                    st.balloons()
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: Shipment setting **Unsuccessful**")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

            if 'retVal' in pageDict.keys():
                ### update shipment state
                st.write("Set shipment ("+pageDict['retVal']['name']+") to *inTransit*")
                if st.button("Set to inTransit"):
                    try:
                        pageDict['upVal']=st.session_state.myClient.post('setShipmentStatus', json={'shipment':pageDict['retVal']['id'],'status':"inTransit"})
                        st.write("### **Successful Update**:",pageDict['upVal']['name'])
                        st.balloons()
                        st.write(pageDict['upVal'])
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: Update **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                try:
                    st.write("last successful _upload_:",pageDict['retVal']['name'])
                    if st.button("Delete testrun"):
                        ### delete data
                        try:
                            pageDict['delVal'] = st.session_state.myClient.post('deleteShipment', json={'shipment':pageDict['retVal']['id']})
                            st.write("### **Successful Deletion**:",pageDict['delVal']['name'])
                            st.balloons()
                            st.write(pageDict['delVal'])
                        except itkX.BadRequest as b:
                            st.write("### :no_entry_sign: Deletion unsuccessful")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                except KeyError:
                    pass
            if 'delVal' in pageDict.keys():
                try:
                    st.write("last successful _deletion_:",pageDict['delVal']['name'])
                except KeyError:
                    pass
