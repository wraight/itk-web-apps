### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import os
import sys
cwd=os.path.dirname(os.path.realpath(__file__))
sys.path.insert(1, cwd)
import batchScript as btch

#####################
### useful functions
#####################
def SelectCheck(k,v):
    val=None
    if "img" in k.lower(): # for images (identified by name for the moment)
        val=st.file_uploader(k+" (image):", type=["png", "jpg", "jpeg"]) #bytesio for binary uploads
    try: # for bools formatted as strings by schema
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
    except AttributeError:
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
    try: # avoid bool trouble
        if val.isnumeric(): # for integers formatted as strings by schema
            val=int(val)
    except AttributeError:
        pass
    return val


infoList=["  * select *institution*",
        "  * select *project*",
        "  * get list of *componentTypes*",
        "  * select *componentType*",
        "  * select *stage*",
        "  * select *test*",
        "  * select *component* to update",
        "  * get *testRunResult* schema json (dictionary)",
        "  * edit *properties* and *parameters*",
        "  * review and upload",
        "  * get *testRunResults* (if upload successful)"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("testRuns", ":arrow_up: Upload Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # get list of componentTypes
        st.write("### List of *ComponentTypes* for",st.session_state.Authenticate['inst']['code'],"("+st.session_state.Authenticate['proj']['code']+")")
        # Check available component names and codes
        if st.button("reset list") or "compTypeList" not in list(pageDict.keys()): # check list
            st.write("Get list")
            pageDict['compTypeList']=st.session_state.myClient.get('listComponents',json={'institution':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] })
            if type(pageDict['compTypeList'])!=type([]):
                pageDict['compTypeList']=pageDict['compTypeList'].data
        else:
            st.write("Got compTypeList")
        if len(pageDict['compTypeList'])<1:
            st.write("No data found. Check *Institution* and *Project* settings")
            st.stop()
        st.write("compTypeList size: "+str(len(pageDict['compTypeList'])))
        df_compType=pd.json_normalize(pageDict['compTypeList'], sep = "_")
        df_compType=df_compType[['componentType_name','componentType_code','state']].drop_duplicates()
        st.dataframe(df_compType)

        #st.write(df_compType.columns)
        #st.write(df_compType.query('componentType_name=="Module"'))
        infra.SelectBoxDf(pageDict,'compType',df_compType,'Select a componentType', 'componentType_code')
        if st.session_state.debug:
            st.write("**DEBUG** *componentType* id")
            st.write(pageDict['compType'])

        pageDict['compTypeObj']=st.session_state.myClient.get('getComponentTypeByCode',json={'project':st.session_state.Authenticate['proj']['code'], 'code':pageDict['compType']['componentType_code'].values[0]})
        mySubProjs=[x['code'] for x in pageDict['compTypeObj']['subprojects']]
        myCompTypes=[x['code'] for x in pageDict['compTypeObj']['types']]

        ### Select how many registrations
        st.write("### Select *how many* registrations")
        infra.Radio(pageDict,'multOpt',["single", "multiple"],"How many registrations?:")

        ### SINGLE registration
        if pageDict['multOpt']=="single":
            st.write("**SINGLE** registration")

            # get list of componentType stages
            st.write("### Select *Component Stage*")
            if st.session_state.debug:
                st.write("**DEBUG** *componentType* stages")
                st.write(compType['stages'])
            infra.SelectBox(pageDict,'stage',pageDict['compTypeObj']['stages'],'Select a stage','name')
            try:
                testMap=[{"name":t['testType']['name'], "code":t['testType']['code']} for t in pageDict['stage']['testTypes']]
            except KeyError:
                st.write("No tests for this stage")
                st.stop()

            df_tests=pd.DataFrame(testMap)
            st.dataframe(df_tests)
            
            # select test
            st.write("### Select *Stage Test*")
            infra.SelectBox(pageDict,'test',testMap,'Select a test','name')
            testType=st.session_state.myClient.get('getTestTypeByCode', json={'componentType':pageDict['compType']['componentType_code'].values[0],'project':st.session_state.Authenticate['proj']['code'],'code':pageDict['test']['code']})
            paraMap=[{"name":p['name'], "code":p['code'], "valueType":p['valueType'], "dataType":p['dataType']} for p in testType['parameters']]
            propMap=[{"name":p['name'], "code":p['code'], "valueType":p['valueType'], "dataType":p['dataType']} for p in testType['properties']]
            if st.session_state.debug:
                st.write("**DEBUG** *testType*")
                st.write(testType)
            df_paras=pd.DataFrame(paraMap)
            df_props=pd.DataFrame(propMap)
            st.write("Parameters")
            st.dataframe(df_paras)
            st.write("Properties")
            st.dataframe(df_props)

            # get test schema
            st.write("### Component Schema")
            infra.Radio(pageDict,'reqSet',["required only", "all"],"Schema setting:")
            reqSet={'required only':True,'all':False}[pageDict['reqSet']]

            testSchema =  st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':st.session_state.Authenticate['proj']['code'], 'componentType':pageDict['compType']['componentType_code'].values[0], 'code':pageDict['test']['code'], 'requiredOnly':reqSet})
            # if st.session_state.debug:
            #     st.write("**DEBUG** Original *schema*")
            st.write(testSchema)

            # select component
            compMap = [{"id":w['id'], "code":w['code'], "ASN":w['serialNumber'], "altID":w['alternativeIdentifier'], "state":w['state']} for w in pageDict['compTypeList'] if w['componentType']['code']==pageDict['compType']['componentType_code'].values[0] and w['state']=="ready"]
            if st.session_state.debug:
                st.write("**DEBUG** *component* list")
                st.dataframe(pd.DataFrame(compMap))
            infra.SelectBox(pageDict,'comp',compMap,'Select a component','ASN')

            with st.beta_container():
                st.write("Check states...")
                st.write(" * Component:",pageDict['comp']['ASN'],", state:",pageDict['comp']['state'])
                st.write(" * ComponentType:",pageDict['compType']['componentType_name'].values[0],", state:",str(df_compType.query('componentType_name=="'+pageDict['compType']['componentType_name'].values[0]+'"')['state'].values[0]))
                st.write(" * TestType:",testType['code'],", state:",testType['state'])
                if pageDict['comp']['state']=="ready" and str(df_compType.query('componentType_name=="'+pageDict['compType']['componentType_name'].values[0]+'"')['state'].values[0])=="ready" and testType['state']=="active":
                    st.write(":thumbsup: Test upload possible")
                else:
                    st.write(":no_entry_sign: Cannot upload while state is not active/ready")
                    st.stop()

            # get set up schema for selected component, test, institution
            try:
                if st.session_state.debug:
                    st.write("**DEBUG** *testSchema* keys...")
                    try:
                        st.write("new testSchema:",pageDict['testSchema']['results'].keys())
                    except:
                        pass
                    st.write("original testSchema:",testSchema['results'].keys())
                if set(pageDict['testSchema']['results'].keys())!=set(testSchema['results'].keys()):
                    pageDict['testSchema']=testSchema.copy()
                    pageDict['testSchema']['component']=pageDict['comp']['code']
                    pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
            except:
                pageDict['testSchema']=testSchema.copy()
                pageDict['testSchema']['component']=pageDict['comp']['code']
                pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']

            # reset test schema if required
            if st.button("reset schema"):
                pageDict['testSchema']=testSchema.copy()
                pageDict['testSchema']['component']=pageDict['comp']['code']
                pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']

            # list widgets to update
            for k,v in pageDict['testSchema'].items():
                #st.write(k,":",type(v))
                if type(v)==type({}): # for dictionaries
                    for l,w in pageDict['testSchema'][k].items():
                        pageDict['testSchema'][k][l]=SelectCheck(l,w)
                else:
                    if k=="subproject":
                        try:
                            pageDict['testSchema'][k]=st.selectbox(k, list(mySubProjs), index=list(mySubProjs).index(pageDict['testSchema'][k]))
                        except:
                            pageDict['testSchema'][k]=st.selectbox(k, list(mySubProjs) )
                        continue
                    elif k=="type":
                        try:
                            pageDict['testSchema'][k]=st.selectbox(k, list(myCompTypes), index=list(myCompTypes).index(pageDict['testSchema'][k]))
                        except:
                            pageDict['testSchema'][k]=st.selectbox(k, list(myCompTypes) )
                        continue
                    else:
                        pageDict['testSchema'][k]=SelectCheck(k,v)

            # final review of test schema
            st.write("### Test Schema")
            ### convert integers
            infra.ToggleButton(pageDict,'convert',"Convert integers")
            if st.session_state.debug:
                st.write("**DEBUG** check convert")
                st.write(pageDict['convert'])
            if pageDict['convert']:
                try:
                    for k,v in pageDict['compSchema']['properties'].items():
                        try:
                            pageDict['compSchema']['properties'][k]=int(v)
                        except ValueError:
                            pass
                except KeyError:
                    st.write("no properties to check")
            ### review final schema
            st.write(pageDict['testSchema'])

            # upload(!) test schema: change DbAccess(get) --> DbUpdate(post)
            if st.button("Upload Test"):
                #st.write(pageDict['testSchema'])
                pageDict['retInfo']=st.session_state.myClient.post('uploadTestRunResults', json=pageDict['testSchema'])
                try:
                    if pageDict['retInfo']!=None:
                        st.write("### :white_check_mark: Successful upload:",pageDict['retInfo']['testRun']['date'])
                    else:
                        st.write("### :no_entry_sign: Update unsuccessful")
                except KeyError:
                    pass

            if st.session_state.debug:
                st.write("**DEBUG** Full update return")
                try:
                    st.write("### Full upload return from ("+pageDict['retInfo']['testRun']['date']+")")
                except KeyError:
                    pass

            # see test data
            st.write("### Last uploaded test data")
            try:
                st.write("### Test uploaded from ("+pageDict['retInfo']['testRun']['date']+")")
                testInfo=st.session_state.myClient.get('getTestRun', json={'testRun':pageDict['retInfo']['testRun']['id']})
                st.write(testInfo)
            except KeyError:
                st.write("No upload data")

        ### MULTIPLE registrations
        if pageDict['multOpt']=="multiple":
            st.write("**MULTIPLE** registrations")

            # select subProject
            st.write("### Select *subProject*")
            if st.session_state.debug:
                st.write("**DEBUG** *subProject* list (name:code)")
                st.write([{x['name']:x['code']} for x in pageDict['compTypeObj']['subprojects']])
            infra.SelectBox(pageDict,'subProj',pageDict['compTypeObj']['subprojects'],'Select a sub-project','code')

            # select subType
            st.write("### Select *componentType type*")
            if st.session_state.debug:
                st.write("**DEBUG** *subType* list (name:code)")
                myCompTypes=[x['code'] for x in pageDict['compTypeObj']['types']]
                st.write([{x['name']:x['code']} for x in pageDict['compTypeObj']['types']])
            infra.SelectBox(pageDict,'subType',pageDict['compTypeObj']['types'],'Select a sub-type','code')

            ## get data file
            st.write("### Upload *Test data*")
            st.write("Follow data format:")
            df_example=pd.DataFrame([{'serialNumber':'20Uxxyynnnnnnn','date':'dd/mm/yyyy HH:MM','TEST_NAMEX:KEY_NAMEX':'value1','TEST_NAMEX:KEY_NAMEY':'value2','TEST_NAMEY:KEY_NAMEZ':'value3'}])
            st.write(df_example.style.hide_index())
            pageDict['file']= st.file_uploader("Upload a file", type=["csv"])
            if st.session_state.debug: st.write(pageDict['file'])

            #dataSel = GetData(join(state.selectDir, st.session_state.selectFile))
            if pageDict['file'] is not None:
                pageDict['file'].seek(0)
                pageDict['df_data']=pd.read_csv(pageDict['file'])
                st.dataframe(pageDict['df_data'])
                st.write("length of dataframe:",len(pageDict['df_data']))
            else:
                st.write("No data file set")
                st.stop()

            # check column names are of format
            goodCols=True
            for col in list(pageDict['df_data'].columns):
                if ":" not in col and "serialnumber" not in col.lower() and "date" not in col.lower():
                    goodCols=False
                    st.write("Column name *",col,"* is not well formatted. Please revise and re-upload.")
            # stop is off format
            if goodCols==False:
                st.write("Columns format fail")
                st.stop()
            else:
                st.write("Columns format pass")

            # select if registration required
            st.write("### Registration required?")
            infra.Radio(pageDict, 'regReq', ["yes", "no"], "Registration required?:")

            if pageDict['regReq']=="yes":
                st.write("**Schema required!**")
                pageDict['compSchema']=st.session_state.myClient.get('generateComponentTypeDtoSample', json={'project':st.session_state.Authenticate['proj']['code'], 'code':pageDict['compType']['componentType_code'].values[0], 'requiredOnly':True})
                if pageDict['compSchema']==None:
                    st.write("problem retrieving schema")
                    st.stop()
                pageDict['compSchema']['subproject']=pageDict['subProj']['code']
                pageDict['compSchema']['institution']=st.session_state.Authenticate['inst']['code']
                pageDict['compSchema']['type']=pageDict['subType']['code']
                st.write("Please input required properties")
                for k,v in pageDict['compSchema']['properties'].items():
                    pageDict['compSchema']['properties'][k]=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
                st.write("Review schema...")
                ### convert integers
                infra.ToggleButton(pageDict,'convert',"Convert integers")
                if st.session_state.debug:
                    st.write("**DEBUG** check convert")
                    st.write(pageDict['convert'])
                if pageDict['convert']:
                    try:
                        for k,v in pageDict['compSchema']['properties'].items():
                            try:
                                pageDict['compSchema']['properties'][k]=int(v)
                            except ValueError:
                                pass
                    except KeyError:
                        st.write("no properties to check")
                ### review final schema
                st.write(pageDict['compSchema'])
            else:
                st.write("**Will NOT register!**")
                pageDict['compSchema']=None

            # run batch upload
            if st.button("Run PDB update"):
                btch.MultiTestUpload(pageDict, st.session_st.session_state.Authenticate, st.session_state.myClient, st.session_state.debug, pageDict['regReq'])
