### standard
import streamlit as st
import streamlit.components.v1 as components
from core.Page import Page
### custom
import pandas as pd
import datetime
import ast
import csv
import json
import copy
import altair as alt
import numpy as np
import os
from io import StringIO
import io
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################
infoList=[" * check component sub-type",
        " * upload component schemas to PDB",
        " * delete component from PDB if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Coffee Upload", ":coffee: Upload Coffee Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="COFFEE_COMP"
        if "project" not in pageDict.keys():
            pageDict['project']=st.session_state.Authenticate['proj']['code']
            pageDict['subProject']= next((item['code'] for item in st.session_state.Authenticate['proj']['subprojects'] if "general" in item['name']),None)
        if "institution" not in pageDict.keys():
            pageDict['institution']=st.session_state.Authenticate['inst']['code']
        
        ### current user
        st.write("## Current User:")
        userName=st.session_state.Authenticate['user']['firstName']+"_"+st.session_state.Authenticate['user']['lastName']
        st.write("### name:",userName)
        st.write("### institute (project):",st.session_state.Authenticate['user']['institutions'][0]['code'],"("+pageDict['project']+")")

        st.write("---")

        ### Current information
        st.write("## Current Coffee Components for",userName)

        # get site qualification object (should be exactly one per user)
        if "ccObj" not in pageDict.keys() or st.button("re-check component"):
            ccObjs_inst = st.session_state.myClient.get('listComponents', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'institution':pageDict['institution']})
            if type(ccObjs_inst)!=type([]):
                ccObjs_inst=ccObjs_inst.data

            st.dataframe(ccObjs_inst)
            # st.stop()

            ccObjs=[cco for cco in ccObjs_inst if cco['alternativeIdentifier']==userName]
            # st.write(pageDict['ccObj'])

            if len(ccObjs)>1:
                st.write(":exclamation: Found more than one?!")
                st.write([sqo['serialNumber'] for sqo in ccObjs])
                st.write(f"Will continue with first {ccObjs[0]['serialNumber']}!")
            elif len(ccObjs)<1:
                st.write(f":exclamation: No coffee components found for {userName}!")
                infra.ToggleButton(pageDict,'tog_reg',"Register coffee component?")
                if pageDict['tog_reg']:
                    pageDict['compSchema']={
                        'project': pageDict['project'], 
                        'subproject': pageDict['subProject'], 
                        'institution': pageDict['institution'], 
                        'componentType': "COFFEE_COMP", 
                        'type': "COMMON", 
                        'properties': { 
                        'USER_NAME': userName, 
                                }
                            }
                    st.write(pageDict['compSchema'])
                    chnx.RegChunk(pageDict, 'Component', 'compSchema')
                    st.write("### re-check component after registration")
                st.stop()
            else:
                st.write(f"Got the one {ccObjs[0]['serialNumber']} :)")
                pageDict['ccObj']=st.session_state.myClient.get('getComponent', json={'component':ccObjs[0]['serialNumber'] })
        
        # full info. option
        infra.ToggleButton(pageDict,'tog_full',"See full component info")
        if pageDict['tog_full']:
            st.write(pageDict['ccObj'])
        

        ### Make stage order list (if not defined)
        if "stageOrderList" not in pageDict.keys():
            compTypeInfo=st.session_state.myClient.get('getComponentTypeByCode', json={'project':pageDict['project'],'code':pageDict['componentType']})
            pageDict['stageOrderList'] = [x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]

            # get stage-test groups by breaking heirarchy
            df_compTypeInfo=pd.json_normalize(compTypeInfo)
            df_compTypeInfo=df_compTypeInfo.explode('stages')
            df_compTypeInfo=df_compTypeInfo.rename(columns={"id": "compType_id", "code": "compType_code", "name": "compType_name"})
            df_compTypeInfo=pd.concat([df_compTypeInfo.drop(columns="stages"),df_compTypeInfo['stages'].apply(pd.Series)],axis=1)
            df_compTypeInfo=df_compTypeInfo.explode('testTypes')
            df_compTypeInfo=df_compTypeInfo.rename(columns={"id": "stage_id", "code": "stage_code", "name": "stage_name", "order": "stage_order"})
            df_compTypeInfo=pd.concat([df_compTypeInfo.drop(columns="testTypes"),df_compTypeInfo['testTypes'].apply(pd.Series)],axis=1)
            df_compTypeInfo=pd.concat([df_compTypeInfo.drop(columns="testType"),df_compTypeInfo['testType'].apply(pd.Series)],axis=1)
            df_compTypeInfo=df_compTypeInfo.rename(columns={"id": "testType_id", "code": "testType_code", "name": "testType_name", "order": "testType_order"})
            pageDict['df_cti']=df_compTypeInfo
        

        ### Stage history
        # option to show
        infra.ToggleButton(pageDict,'tog_stageHist',"See stage history")
        if pageDict['tog_stageHist']:
            st.write("### Stage History")

            stTrx.DebugOutput("### stage-test dataframe",pageDict['df_cti'])

            try:
                df_stages=pd.DataFrame(pageDict['ccObj']['stages'])

                ### formatting
                df_stages=df_stages[['code','name','dateTime']]
                # necessary hack to deal with strange formatting (datetime.datetime instead of pandas._libs.tslibs.timestamps.Timestamp)
                # df_stages['stage_dateTime']= df_stages['stage_dateTime'].apply(lambda x: str(x).split(':')[0])
                df_stages['dateTime']= pd.to_datetime(df_stages['dateTime'],format='%Y-%m-%dT%H:%M:%S.%fZ',errors='coerce')
                df_stages.sort_values(by=['dateTime'], inplace=True)
                df_stages=df_stages.reset_index(drop=True)
                # display df
                
                infra.ToggleButton(pageDict,'tog_stageInfo',"See stage history list")
                if pageDict['tog_stageInfo']:
                    st.dataframe(df_stages)

                stageChart=alt.Chart(df_stages).mark_line(point=True).encode(
                    x=alt.X('dateTime:T',axis = alt.Axis(title = "Date", format = ("%d %b %Y"))),
                #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                    y=alt.Y('code:N',title="Stage",sort=pageDict['stageOrderList']),
                    order='dateTime',
                    tooltip=['dateTime:T','code:N']
                ).configure_point(size=60).properties(width=800, height=400, title="Stage History").interactive()
                st.altair_chart(stageChart)

            except KeyError:
                st.write("no _stages_ key found")
            except TypeError:
                st.write("no stages found")
            # stTrx.ColourDF(df_stages,'stageCode')


        st.write("### Test History")
        testRunIDs=None
        try:
            df_test=pd.DataFrame(pageDict['ccObj']['tests'])
            # st.dataframe(df_test)
            df_test=df_test.explode('testRuns')
            # df_test=df_test['testRuns'].apply(pd.Series)
            df_test=df_test.rename(columns={"id": "testType_id", "code": "testType_code", "name": "testType_name"})
            df_test=pd.concat([df_test.drop(columns="testRuns"),df_test['testRuns'].apply(pd.Series)],axis=1)
            
            ### formatting
            testRunIDs=df_test['id'].to_list()
            df_test=df_test[['testType_code','testType_name','state','runNumber','passed','problems','date']]
            # necessary hack to deal with strange formatting (datetime.datetime instead of pandas._libs.tslibs.timestamps.Timestamp)
            df_test['date']= pd.to_datetime(df_test['date'],format='%Y-%m-%dT%H:%M:%S.%fZ',errors='coerce')
            df_test.sort_values(by=['date'], inplace=True)
            df_test=df_test.reset_index(drop=True)
            df_test['stage']=df_test['testType_code'].apply(lambda x:  pageDict['df_cti'].query('testType_code=="'+x+'"')['stage_code'].values[0] )
            # display df
            # stTrx.ColourDF(df_test,'testCode')

            infra.ToggleButton(pageDict,'tog_testInfo',"See test history list")
            if pageDict['tog_testInfo']:
                st.dataframe(df_test)

            testChart=alt.Chart(df_test).mark_circle(size=60).encode(
                        x=alt.X('date:T',axis = alt.Axis(title = "Date", format = ("%d %b %Y"))),
                    #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                        y=alt.Y('stage:N',title="stage",sort=pageDict['stageOrderList']),
                        color=alt.Color('testType_code:N'),
                        shape=alt.Shape('passed:N'),
                        tooltip=['date:T','stage:N','testType_code:N','passed:N']
                        ).properties(width=800, height=400, title="Test History").interactive()
            st.altair_chart(testChart)

        except KeyError:
            st.write("no _tests_ key found")
        except TypeError:
            st.write("no tests found")

        locationList=None
        if testRunIDs!=None:
            ### test runs table including status with selection of comments
            testRuns = st.session_state.myClient.get('getTestRunBulk', json={'testRun':testRunIDs})
            df_testRuns=pd.DataFrame(testRuns)

            ### formatting
            df_testRuns=df_testRuns[['testType','properties','results','comments']]
            df_testRuns['testType']=df_testRuns['testType'].apply(lambda x: x['code'])
            # properties
            df_props=df_testRuns.explode('properties')
            df_props['prop_code']=df_props['properties'].apply(lambda x: x['code'])
            df_props['prop_value']=df_props['properties'].apply(lambda x: x['value'])
            df_props=df_props.drop(columns=['properties'])
            # st.write(df_props)
            locationList=df_props.query('prop_code=="LOCATION"')['prop_value'].to_list()
            locationList=list(set(locationList))
            # st.write(locationList)
            ### select bar chart
            infra.SelectBox(pageDict,'propCode',df_props['prop_code'].unique(),"Select property code:")
            pc=pageDict['propCode']
            propChart=alt.Chart(df_props.query('prop_code=="'+pc+'"')).mark_bar().encode(
                        x=alt.X('count():Q',title="count"),
                        y=alt.Y('prop_value:N',title=None),#,title=pc),
                        color=alt.Color('prop_value:N'),
                        tooltip=['prop_value:N','count():Q']
                        ).properties(width=800, height=400, title=pc).interactive()
            st.altair_chart(propChart)
            # properties
            df_res=df_testRuns.explode('results')
            df_res['res_code']=df_res['results'].apply(lambda x: x['code'])
            df_res['res_value']=df_res['results'].apply(lambda x: x['value'])
            df_res=df_res.drop(columns=['results'])
            # st.write(df_res)
            ### select bar chart
            infra.SelectBox(pageDict,'resCode',df_res['res_code'].unique(),"Select result code:")
            rc=pageDict['resCode']
            resChart=alt.Chart(df_res.query('res_code=="'+rc+'"')).mark_bar().encode(
                        x=alt.X('res_value:Q', title=None, bin=alt.Bin(step=0.5)),#,title=rc),
                        y=alt.Y('count():Q',title="count"),
                        tooltip=['res_value:Q','count():Q']
                        ).properties(width=800, height=400, title=rc).interactive()
            st.altair_chart(resChart)

        # select latest by default

        ### Select information to upload
        st.write("---")
        st.write("## Select Coffee Component test")

        st.write("### stages and testTypes for "+pageDict['componentType'])
       
        # select stage
        infra.SelectBox(pageDict,'stageCode',pageDict['df_cti'].sort_values(by='stage_order')['stage_code'].unique(),"Select stage code:")
        # select test
        infra.SelectBox(pageDict,'testCode',pageDict['df_cti'].query('stage_code=="'+pageDict['stageCode']+'"').sort_values(by='testType_order')['testType_code'].unique(),"Select testType code (in "+pageDict['stageCode']+"):")

        ### check if schema has changed
        try:
            if pageDict['testSchema']['testType']!=pageDict['testCode']:
                st.write("__Please Reset Schema__")
        except KeyError:
            pass

        if pageDict['testCode']!=pageDict['testCode']:
            st.write("No tests for",pageDict['stageCode'])
            st.stop()
        # get test schema
        if "testSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testCode']+"@"+pageDict['stageCode']+" for "+pageDict['componentType']):
            pageDict['testSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testCode']})#, 'requiredOnly':True})
            testTypeObj = st.session_state.myClient.get('getTestTypeByCode', json={'project':pageDict['project'],'componentType':pageDict['componentType'],'code':pageDict['testCode']})
            pageDict['df_paras']= pd.DataFrame(testTypeObj['parameters'])
            pageDict['df_props']= pd.DataFrame(testTypeObj['properties'])
            # hack to coordinate with chnx.RegChunk
            pageDict['stage']=pageDict['stageCode']
        # add defaults
        pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
        nowStr=datetime.datetime.now()
        st.write("Using today's date:",nowStr)
        infra.ToggleButton(pageDict,'tog_date',"Input other date?")
        if pageDict['tog_date']:
            d = st.date_input("When?",datetime.date.today())
            dateStr=d.strftime("%Y-%m-%dT%H:%MZ")
        else:
            dateStr=nowStr.strftime("%Y-%m-%dT%H:%MZ")
        pageDict['testSchema']['date']=pdbTrx.TimeStampConverter(dateStr, "%Y-%m-%dT%H:%MZ")
        pageDict['testSchema']['component']=pageDict['ccObj']['serialNumber']
        stTrx.DebugOutput("Original *schema*:",pageDict['testSchema'])


        st.write("---")
        st.write("## Update Coffee Club test")

        st.write("### Update Properties")
        ### get existing info from tests
        # default drop down
        # option to add new thing
        try:
            if len(pageDict['testSchema']['properties'])<1:
                st.write("No _properties_ to update")
            else:
                for k,v in pageDict['testSchema']['properties'].items():
                    if pageDict['df_props'].query('code=="'+k+'"').empty:
                        st.write("no matching code found:",k)
                        continue
                    paraDict=pageDict['df_props'].query('code=="'+k+'"').reset_index().to_dict()
                    paraDict={k:v[0] for k,v in paraDict.items()}

                    st.write("Input __"+paraDict['name']+"__ -",paraDict['description'])
                    stTrx.DebugOutput("( "+paraDict['dataType']+" , "+str(paraDict['valueType'])+" )")

                    if k=="LOCATION":
                        infra.ToggleButton(pageDict,'tog_newLoc',"New Location?")
                        if pageDict['tog_newLoc']:
                            pageDict['testSchema']['properties'][k]=pdbTrx.InputType(paraDict,"new location")
                        else:
                            if testRunIDs==None:
                                st.write("No previous locations found. Please add one.")
                            else:
                                pageDict['testSchema']['properties'][k]=st.selectbox(k+" :",locationList)
                    elif k=="TASTER":
                        pageDict['testSchema']['properties'][k]=pdbTrx.InputType(paraDict,userName)
                    else:   
                        pageDict['testSchema']['properties'][k]=pdbTrx.InputType(paraDict,v)
                    #pageDict['testSchema']['properties'][k]=st.text_input(k+" :", value=str(v), max_chars=None, type='default')

        except TypeError:
            st.write("No _properties_ key to update")

        st.write("### Update Results")
        try:
            if len(pageDict['testSchema']['results'])<1:
                st.write("No _results_ to update")
            else:
                for k,v in pageDict['testSchema']['results'].items():
                    if pageDict['df_paras'].query('code=="'+k+'"').empty:
                        st.write("no matching code found:",k)
                        continue
                    paraDict=pageDict['df_paras'].query('code=="'+k+'"').reset_index().to_dict()
                    paraDict={k:v[0] for k,v in paraDict.items()}
                    # if paraDict['required']:
                    #     st.write(" - required!")
                    st.write("Input __"+paraDict['name']+"__ -",paraDict['description'])
                    stTrx.DebugOutput("( "+paraDict['dataType']+" , "+str(paraDict['valueType'])+" )")
                    ### input value
                    st.write(k,v)
                    if pageDict['testSchema']['results'][k]>10 or pageDict['testSchema']['results'][k]<0:
                        pageDict['testSchema']['results'][k]=st.slider(k+" :", 0.0, 10.0, value=0.0, step=0.5)
                    else:
                        pageDict['testSchema']['results'][k]=st.slider(k+" :", 0.0, 10.0, value=v, step=0.5)

        except KeyError:
            st.write("No _results_ key to update")
        # ### Review and edit

        st.write("### Review Upload Schema")
        st.write(pageDict['testSchema'])
        # chnx.ReviewAndEditChunk(pageDict,'testSchema')

        st.write("---")

        st.write("### Upload to PDB")
        st.write("Add comment after registration if required")

        # upload(!)
        chnx.RegChunk(pageDict, "TestRun", "testSchema")


