### standard
import streamlit as st
import streamlit.components.v1 as components
from core.Page import Page
### custom
import pandas as pd
import datetime
import ast
import csv
import json
import copy
import altair as alt
import numpy as np
import os
from io import StringIO
import io
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################
infoList=[" * check component sub-type",
        " * upload component schemas to PDB",
        " * delete component from PDB if required"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Local Stats", ":coffee: :bar_chart: Review Coffee Statistics", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="COFFEE_COMP"
        if "project" not in pageDict.keys():
            pageDict['project']=st.session_state.Authenticate['proj']['code']
            pageDict['subProject']= next((item['code'] for item in st.session_state.Authenticate['proj']['subprojects'] if "general" in item['name']),None)
            # pageDict['project']="S"
            # pageDict['subProject']="SG"
        if "institution" not in pageDict.keys():
            pageDict['institution']=st.session_state.Authenticate['inst']['code']
        
        ### current user
        st.write("## Current User:")
        userName=st.session_state.Authenticate['user']['firstName']+"_"+st.session_state.Authenticate['user']['lastName']
        st.write("### name:",userName)
        st.write("### institute (project):",st.session_state.Authenticate['user']['institutions'][0]['code'],"("+pageDict['project']+")")

        st.write("---")

        ### Current information
        st.write("## Current Coffee Components from",pageDict['institution'])

        # get site qualification object (should be exactly one per user)
        if "ccObjs_inst" not in pageDict.keys() or st.button("re-check components"):
            ccObjs_inst = st.session_state.myClient.get('listComponents', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'institution':pageDict['institution']})
            if type(ccObjs_inst)!=type([]): 
                ccObjs_inst=ccObjs_inst.data
            
            st.dataframe(ccObjs_inst)
            # st.stop()

            if len(ccObjs_inst)<1:
                st.write(f":exclamation: No coffee components found for {pageDict['institution']}!")
                st.stop()
            else:
                st.write(f"Found {len(ccObjs_inst)} coffee components")
                pageDict['ccObjs_inst']=st.session_state.myClient.get('getComponentBulk', json={'component':[cc['serialNumber'] for cc in ccObjs_inst] })
        
        # full info. option
        infra.ToggleButton(pageDict,'tog_full',"See full component info")
        if pageDict['tog_full']:
            st.write(pageDict['ccObjs_inst'])
        

        ### Make stage order list (if not defined)
        if "stageOrderList" not in pageDict.keys() or 1==1:
            compTypeInfo=st.session_state.myClient.get('getComponentTypeByCode', json={'project':pageDict['project'],'code':pageDict['componentType']})
            pageDict['stageOrderList'] = [x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]

            # get stage-test groups by breaking heirarchy
            df_compTypeInfo=pd.json_normalize(compTypeInfo)
            df_compTypeInfo=df_compTypeInfo.explode('stages')
            df_compTypeInfo=df_compTypeInfo.rename(columns={"id": "compType_id", "code": "compType_code", "name": "compType_name"})
            df_compTypeInfo=pd.concat([df_compTypeInfo.drop(columns="stages"),df_compTypeInfo['stages'].apply(pd.Series)],axis=1)
            df_compTypeInfo=df_compTypeInfo.explode('testTypes')
            df_compTypeInfo=df_compTypeInfo.rename(columns={"id": "stage_id", "code": "stage_code", "name": "stage_name", "order": "stage_order"})
            df_compTypeInfo=pd.concat([df_compTypeInfo.drop(columns="testTypes"),df_compTypeInfo['testTypes'].apply(pd.Series)],axis=1)
            for c in ['code','name']:
                df_compTypeInfo["testType_"+c]=df_compTypeInfo['testType'].apply(lambda x: x[c] if type(x)==type({}) and c in x.keys() else x)
            # df_compTypeInfo=pd.concat([df_compTypeInfo.drop(columns="testType"),df_compTypeInfo['testType'].apply(pd.Series)],axis=1)
            # df_compTypeInfo=df_compTypeInfo.rename(columns={"id": "testType_id", "code": "testType_code", "name": "testType_name", "order": "testType_order"})
            # st.write(df_compTypeInfo.drop(columns=["testType"]))
            pageDict['df_cti']=df_compTypeInfo.drop(columns=["testType"]).reset_index(drop=True)
   

        ### Stage history
        # option to show
        infra.ToggleButton(pageDict,'tog_stageHist',"See stage history")
        if pageDict['tog_stageHist']:
            st.write("### Stage History")

            stTrx.DebugOutput("### stage-test dataframe",pageDict['df_cti'])

            try:
                df_stages=pd.DataFrame(pageDict['ccObjs_inst'])
                df_stages=df_stages.explode('stages')
                for c in ['code','name','dateTime']:
                    df_stages[c]=df_stages['stages'].apply(lambda x: x[c])
                ### formatting
                df_stages=df_stages[['alternativeIdentifier','code','name','dateTime']]
                st.write(df_stages)

                # necessary hack to deal with strange formatting (datetime.datetime instead of pandas._libs.tslibs.timestamps.Timestamp)
                # df_stages['stage_dateTime']= df_stages['stage_dateTime'].apply(lambda x: str(x).split(':')[0])
                df_stages['dateTime']= pd.to_datetime(df_stages['dateTime'],format='%Y-%m-%dT%H:%M:%S.%fZ',errors='coerce')
                df_stages.sort_values(by=['dateTime'], inplace=True)
                df_stages=df_stages.reset_index(drop=True)
                # display df
                
                infra.ToggleButton(pageDict,'tog_stageInfo',"See stage history list")
                if pageDict['tog_stageInfo']:
                    st.dataframe(df_stages)

                stageChart=alt.Chart(df_stages).mark_line(point=True).encode(
                    x=alt.X('dateTime:T',axis = alt.Axis(title = "Date", format = ("%d %b %Y"))),
                #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                    y=alt.Y('code:N',title="Stage",sort=pageDict['stageOrderList']),
                    order='dateTime',
                    tooltip=['dateTime:T','code:N']
                ).configure_point(size=60).properties(width=800, height=400, title="Stage History").interactive()
                st.altair_chart(stageChart)

            except KeyError:
                st.write("no _stages_ key found")
            except TypeError:
                st.write("no stages found")
            # stTrx.ColourDF(df_stages,'stageCode')


        st.write("### Test History")

        try:
            df_tests=pd.DataFrame(pageDict['ccObjs_inst'])
            df_tests=df_tests.explode('tests')
            for c in ['code','name','testRuns']:
                df_tests[c]=df_tests['tests'].apply(lambda x: x[c] if type(x)==type({}) and c in x.keys() else x)

            ### formatting
            df_tests=df_tests[['alternativeIdentifier','code','name','testRuns']]
            df_tests=df_tests.explode('testRuns')
            for c in ['id','date','passed']:
                df_tests[c]=df_tests['testRuns'].apply(lambda x: x[c] if type(x)==type({}) and c in x.keys() else x)
            df_tests=df_tests.drop(columns=["testRuns"])
            # st.write(df_tests)
            testRunIDs=df_tests.dropna()['id'].to_list()

            # necessary hack to deal with strange formatting (datetime.datetime instead of pandas._libs.tslibs.timestamps.Timestamp)
            df_tests['date']= pd.to_datetime(df_tests['date'],format='%Y-%m-%dT%H:%M:%S.%fZ',errors='coerce')
            df_tests.sort_values(by=['date'], inplace=True)
            df_tests=df_tests.reset_index(drop=True)

            # st.write(pageDict['df_cti'])

            df_tests['stage']=df_tests['code'].apply(lambda x:  pageDict['df_cti'].query('testType_code=="'+x+'"')['stage_code'].values[0] if type(x)==type("") else x)
            # display df
            # stTrx.ColourDF(df_test,'testCode')

            st.write(df_tests)

            infra.ToggleButton(pageDict,'tog_testInfo',"See test history list")
            if pageDict['tog_testInfo']:
                st.dataframe(df_tests)

            testChart=alt.Chart(df_tests).mark_circle(size=60).encode(
                        x=alt.X('date:T',axis = alt.Axis(title = "Date", format = ("%d %b %Y"))),
                    #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                        y=alt.Y('stage:N',title="stage",sort=pageDict['stageOrderList']),
                        color=alt.Color('code:N'),
                        shape=alt.Shape('passed:N'),
                        tooltip=['date:T','stage:N','code:N','passed:N']
                        ).properties(width=800, height=400, title="Test History").interactive()
            st.altair_chart(testChart)

            userChart=alt.Chart(df_tests).mark_bar().encode(
                        x=alt.X('alternativeIdentifier:N',title="user", axis=alt.Axis(labelAngle=0)),
                        y=alt.Y('count():Q',title="count"),
                        color=alt.Color('alternativeIdentifier:N'),
                        tooltip=['alternativeIdentifier:N','count():Q']
                        ).properties(width=800, height=400, title="User uploads").interactive()
            st.altair_chart(userChart)

        except KeyError:
            st.write("no _tests_ key found")
        except TypeError:
            st.write("no tests found")

        st.write("---")


        ### test runs table including status with selection of comments
        testRuns = st.session_state.myClient.get('getTestRunBulk', json={'testRun':testRunIDs})
        df_testRuns=pd.DataFrame(testRuns)
        df_testRuns=df_testRuns.explode('components')
        for c in ['alternativeIdentifier']:
            df_testRuns[c]=df_testRuns['components'].apply(lambda x: x[c])
        df_testRuns=df_testRuns.drop(columns=['components'])
        st.write(df_testRuns)

        ### formatting
        df_testRuns=df_testRuns[['testType','properties','results','comments','alternativeIdentifier']]
        df_testRuns['testType']=df_testRuns['testType'].apply(lambda x: x['code'])
        # properties
        df_props=df_testRuns.explode('properties')
        df_props['prop_code']=df_props['properties'].apply(lambda x: x['code'])
        df_props['prop_value']=df_props['properties'].apply(lambda x: x['value'])
        df_props=df_props.drop(columns=['properties'])
        # st.write(df_props)
        locationList=df_props.query('prop_code=="LOCATION"')['prop_value'].to_list()
        locationList=list(set(locationList))
        # st.write(locationList)
        ### select bar chart
        infra.SelectBox(pageDict,'propCode',df_props['prop_code'].unique(),"Select property code:")
        pc=pageDict['propCode']
        propChart=alt.Chart(df_props.query('prop_code=="'+pc+'"')).mark_bar().encode(
                    x=alt.X('count():Q',title="count"),
                    y=alt.Y('prop_value:N',title=None),#,title=pc),
                    color=alt.Color('alternativeIdentifier:N'),
                    tooltip=['prop_value:N','count():Q','alternativeIdentifier:N']
                    ).properties(width=800, height=400, title=pc).interactive()
        st.altair_chart(propChart)
        # properties
        df_res=df_testRuns.explode('results')
        df_res['res_code']=df_res['results'].apply(lambda x: x['code'])
        df_res['res_value']=df_res['results'].apply(lambda x: x['value'])
        df_res=df_res.drop(columns=['results'])
        # st.write(df_res)
        ### select bar chart
        infra.SelectBox(pageDict,'resCode',df_res['res_code'].unique(),"Select result code:")
        rc=pageDict['resCode']
        resChart=alt.Chart(df_res.query('res_code=="'+rc+'"')).mark_bar().encode(
                    x=alt.X('res_value:Q', title=None, bin=alt.Bin(step=0.5)),#,title=rc),
                    y=alt.Y('count():Q',title="count"),
                    color=alt.Color('alternativeIdentifier:N'),
                    tooltip=['res_value:Q','count():Q','alternativeIdentifier:N']
                    ).properties(width=800, height=400, title=rc).interactive()
        st.altair_chart(resChart)

