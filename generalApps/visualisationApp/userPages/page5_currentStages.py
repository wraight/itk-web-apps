### standard
import streamlit as st
from core.Page import Page
### custom
from datetime import datetime
import pandas as pd
import plotly.graph_objects as go
import altair as alt
import ast
import csv
import numpy as np
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX


#####################
### useful functions
#####################

infoList=["  * Get grouped list of componentTypes at selected institutes (takes time)",
        "  * **other time** Upload csv",
        "   * choose primary variable",
        "   * plotting"]
#####################
### main part
#####################

class Page5(Page):
    def __init__(self):
        super().__init__("CurrentStage", ":microscope: _Current Stage_ of all components @ institute(s)", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ### shpw list of componentTypes
        compTypeRet=st.session_state.myClient.get('listComponentTypes',json={})
        if type(compTypeRet)!=type([]):
            compTypeRet=compTypeRet.data
        #st.write(compTypeRet)
        if st.session_state.debug:
            st.write("Component type keys...")
            st.write(sorted(compTypeRet[0].keys()))
        compTypeList=[]
        for x in compTypeRet:
            compTypeList.append({'name':x['name'],'code':x['code']})
            if "types" in x.keys() and x['types']!=None:
                compTypeList[-1]['types']=([{'name':y['name'],'code':y['code']} for y in x['types']])
            if "stages" in x.keys() and x['stages']!=None:
                compTypeList[-1]['stages']=([{'name':y['name'],'code':y['code']} for y in x['stages']])
        # compTypeList
        st.write("### List of componentTypes")
        df_compTypeList=pd.DataFrame(compTypeList)
        st.dataframe(df_compTypeList)

        ### select list of institutes (user institute as default)
        if "instList" not in pageDict.keys():
            pageDict['instList']=[st.session_state.Authenticate['inst']['code']]
        st.write("List of institues to query",pageDict['instList'])
        if st.checkbox('Add institute?'):
            instCode=st.text_input('Add institute *code*')
            if instCode not in [x['code'] for x in st.session_state.Authenticate['instList']]:
                st.write(instCode+" code *not* recognised")
            elif instCode in pageDict['instList']:
                st.write(instCode+" already included")
            else:
                pageDict['instList'].append(instCode)
                st.write("code added")
        if st.button("reset list"):
            pageDict['instList']=[st.session_state.Authenticate['inst']['code']]

        ### compile big list
        if st.button("get data list"):# or "compInstList" not in list(pageDict.keys()): # check list
            st.write("Compiling list (this will take _a few_ minutes)...")
            loopList=pageDict['instList']
            dataList=[]
            badList=[]
            cols=['componentType_code','currentLocation_code','state', 'stages']
            try:
                del df_total
            except NameError:
                pass

            status_bar=st.progress(0.0) #st.empty()
            status_text=st.empty()
            maxVal=500
            for c,i in enumerate(loopList):
                loopText="inst: "+i+", "+str(c+1)+" of "+str(len(loopList))
                status_text.text(loopText)
                status_bar.progress(1.0*c/len(loopList))
                #st.write("inst: "+i['code'])
                pgndx=0
                while pgndx>-1:
                    # this should not be a list, this should be paginated
                    compList=st.session_state.myClient.get('listComponents', json={'project':st.session_state.Authenticate['proj']['code'], 'currentLocation':i, 'pageInfo':{'pageSize':maxVal,'pageIndex':pgndx}})
                    st.write("comp batch",str(pgndx)+":",len(compList.data),"/",compList.page_info['total'],"..",compList.data[0]['componentType']['code'])
                    df_instComps=pd.json_normalize(compList.data, sep = "_")
                    if df_instComps.empty:
                        badList.append(i)
                        continue
                    df_instComps['sum']=1
                    try:
                        df_total=df_total.append(df_instComps, ignore_index=True)
                    except NameError:
                        df_total=df_instComps.copy()
                    if compList.total>(pgndx+1)*maxVal:
                        pgndx+=1
                    else:
                        pgndx=0
                        break
            else:
                status_text.text("looping complete")
                status_bar.progress(1.0)
            pageDict['compInstList']=df_total#.groupby(["currentLocation_code","componentType_code","state"]).sum().reset_index()

        ### check data
        if "compInstList" not in list(pageDict.keys()):
            st.write("No data collected")
            st.stop()

        ### show grouped data
        cols=['componentType_code','currentLocation_code','type_code','state']
        st.dataframe(pageDict['compInstList'][cols+['sum']].groupby(cols).sum().reset_index())

        ### filter data
        st.write("### filtering...")
        filterList=["TEST","test","20USC","William","SVEN","Michael","uun"]
        if st.session_state.debug:
            st.write("filtering:",filterList)
        for x in filterList:
            df_filt=pageDict['compInstList'][pageDict['compInstList']['componentType_code'].str.contains(x)][cols]
            if not df_filt.empty:
                st.write("check:",x)
                st.write(df_filt)
            pageDict['compInstList']=pageDict['compInstList'][~pageDict['compInstList']['componentType_code'].str.contains(x)]
        else:
            st.write("filtering done")

        ### manipulate data
        pageDict['compInstList']['compStage']=pageDict['compInstList']['componentType_code']+":"+pageDict['compInstList']['currentStage_code']

        ### plot data
        st.write("### Plotting")
        myType=st.radio("y-axis",('linear','log','symlog'))
        #st.dataframe(pageDict['compInstList'][cols+['compStage']])
        stageChart=alt.Chart(pageDict['compInstList'][cols+['currentStage_code','compStage','sum']]).mark_bar().encode(
            x=alt.X('componentType_code'),
            y=alt.Y('sum(sum):Q',scale=alt.Scale(type=myType)), #'sum(sum)',
            color='compStage',
            row='currentLocation_code',
            tooltip=['componentType_code', 'currentStage_code', 'sum(sum)']
        ).configure_legend(labelLimit= 0, symbolLimit=0).properties(width=800)

        st.altair_chart(stageChart)
