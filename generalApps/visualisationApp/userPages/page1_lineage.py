### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################
def GetComponentType(proj,code):
    #Stave ID: 5ad8850c09d7a800067a4f60
    component=st.session_state.myClient.get('getComponentTypeByCode',json={'project':proj,'code':code})
    return component

def GetChildren(graphDict, proj, children_dict, parent_type, parent_node, generation, includeAllType=False):

    #if ask to include All Type
    if includeAllType:
        try:
            #append all-type's children
            children_list = np.concatenate((children_dict[parent_type], children_dict["*"]), axis=0)
        #no all-type
        except KeyError:
            children_list = children_dict[parent_type]

    #exclude All Type
    else:
        children_list = children_dict[parent_type]

    #start the first generation of children
    generation+=1 # self.generation<=self.r here

    #enumerate all children
    for ind,child in enumerate(children_list):

        child_name = child["name"]
        child_code = child["code"]
        try:
            childType_name = child["type"]["name"]
        except (TypeError, KeyError):
            childType_name = "NoType"
        try:
            childType_code = child["type"]["code"]
        except (TypeError, KeyError):
            childType_code = "NoType"
        try:
            child_state = child["state"]
        except (TypeError, KeyError):
            child_state = "NoType"

        #check children in the right generation
        # print("gen:",generation)
        # print("childName:",children_name)
        # print("childType:",children_type)

        #save nodes names
        child_node = "G"+str(generation)+child_name+childType_code.strip()+str(ind)

        # print("childNode:",children_node)

        graphDict['label'].append(child_name+'\n ('+childType_name+')')
        graphDict['code'].append(childType_code)
        graphDict['state'].append(child_state)
        graphDict['parentNode'].append(parent_node)
        graphDict['childNode'].append(child_node)

        #if generation hasn't reached user's input resursive
        if generation < 10 and childType_code != "NoType":

            #find component type in database]
            try:
                this_child = GetComponentType(proj,child_code)
            except TypeError:
                #print("TypeError with",children_type)
                continue

            #if children has children, recursively run getChildren till the recursive level user asked for
            if "children" in this_child.keys() and this_child["children"] != None:

                #children become the parent
                GetChildren(graphDict, proj, this_child["children"], childType_code, child_node, generation)

            #if this children has no children, move on to the next children
            else:
                continue

        #if children type is None, move on to the next children
        elif childType_code == " ":
            continue

def CreateFamilyTree(proj,code,compType):
    component=GetComponentType(proj,code)

    type_name="NYS"
    for ct in component["types"]:
        if ct["code"] == compType:
            type_name = ct["name"]

    #Head node
    head='H'
    generation = 0
    #graph.node('H', component['name'] + ' (' + type_name + ')',color="red")

    graphDict={'label':[component['name'] + ' (' + type_name + ')'],
                'state': ["None"],
                'code': ["None"],
                'parentNode':["None"],
                'childNode':[head]
              }

    GetChildren(graphDict, proj, component["children"], compType, head, generation)

    return graphDict #graph.view()


infoList=["  * input componentType",
        "   * Get Family Tree (sankey plot)",
        "   * Based on code by Jiayi Chen (Brandeis University) jennyz@brandeis.edu"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Lineage", ":microscope: Plotted ComponentType Relationships", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ### componentType code
        #infra.SelectBox(pageDict,'compType',compTypeRet,'Select componentType','name')
        infra.TextBox(pageDict,'compCode','Input ('+st.session_state.Authenticate['proj']['name']+') componentType *code*')

        if len(pageDict['compCode'])<1:
            st.stop()

        if "compType" not in pageDict.keys() or st.button("Get compType"):
            pageDict['compType']=st.session_state.myClient.get('getComponentTypeByCode',json={'project':st.session_state.Authenticate['proj']['code'],'code':pageDict['compCode']})

        if st.session_state.debug:
            st.write("**DEBUG** Selected componentType")
            st.write(pageDict['compType'])

        ### sub-type
        #subTypeList=list(df_compTypeList.query('name=="'+pageDict['compType']['code']+'"')['types'].values)
        try:
            subTypeList=list(pageDict['compType']['types'])
        except TypeError:
            st.write("no componentType _types_ found.")
            st.stop()
        infra.SelectBox(pageDict,'subType',subTypeList,'Select componentType type','name')
        if st.session_state.debug:
            st.write("**DEBUG** Selected componentType type")
            st.write(pageDict['subType'])

        ### get lineage info.
        if st.button("get lineage!"):
            pageDict['lineage']=CreateFamilyTree(st.session_state.Authenticate['proj']['code'],pageDict['compType']['code'],pageDict['subType']['code'])

        try:
            st.dataframe(pd.DataFrame(pageDict['lineage']))
        except KeyError:
            st.stop()

        infra.ToggleButton(pageDict,'noObs','Remove "obsolete" labels?')

        ### plotting
        src=[]
        trg=[]
        val=[]
        for i,x in enumerate(pageDict['lineage']['label']):
            if pageDict['noObs']==True and "obsolete" in x.lower():
                continue
            #print("label:",x,"("+str(i)+")")
            par=pageDict['lineage']['parentNode'][i]
            if par=="None":
                src.append(0)
                trg.append(0)
                val.append(0)
                continue
            p=pageDict['lineage']['childNode'].index(par)
            #print("parent:",par,"("+str(p)+")")
            src.append(i)
            trg.append(p)
            val.append(1)

        if st.session_state.debug:
            st.dataframe({'source':src,'target':trg,'value':val})

        st.write("### Plotting")
        fig = go.Figure(data=[go.Sankey(
                                    node = dict(
                                      pad = 15,
                                      thickness = 20,
                                      line = dict(color = "black", width = 0.5),
                                      label = pageDict['lineage']['label'],
                                      color = "blue"
                                    ),
                                    link = dict(
                                      source = src, # indices correspond to labels, eg A1, A2, A1, B1, ...
                                      target = trg,
                                      value = val
                                  ))])

        fig.update_layout(title_text=pageDict['compType']['code']+"("+st.session_state.Authenticate['proj']['code']+") lineage", font_size=10)
        st.plotly_chart(fig)
