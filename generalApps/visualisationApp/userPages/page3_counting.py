### standard
import streamlit as st
from core.Page import Page
### custom
from datetime import datetime
import pandas as pd
import plotly.graph_objects as go
import altair as alt
import ast
import csv
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX


#####################
### useful functions
#####################

infoList=["  * **first time** Get grouped list of componentTypes at institutes (takes time)",
        "  - download compiled list as csv",
        "  * **other time** Upload csv",
        "   * choose primary variable",
        "   * plotting"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("Counting", ":microscope: Count stuff", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        compTypeRet=st.session_state.myClient.get('listComponentTypes',json={})
        if type(compTypeRet)!=type([]):
            compTypeRet=compTypeRet.data
        #st.write(compTypeRet)
        if st.session_state.debug:
            st.write("Component type keys...")
            st.write(sorted(compTypeRet[0].keys()))
        compTypeList=[]
        for x in compTypeRet:
            compTypeList.append({'name':x['name'],'code':x['code']})
            if "types" in x.keys() and x['types']!=None:
                compTypeList[-1]['types']=([{'name':y['name'],'code':y['code']} for y in x['types']])
            if "stages" in x.keys() and x['stages']!=None:
                compTypeList[-1]['stages']=([{'name':y['name'],'code':y['code']} for y in x['stages']])
        # compTypeList
        df_compTypeList=pd.DataFrame(compTypeList)
        st.dataframe(df_compTypeList)

        ### compile big list
        if st.button("get list"):# or "compInstList" not in list(pageDict.keys()): # check list
            st.write("Compiling list (this will take _a few_ minutes)...")
            loopList=st.session_state.Authenticate['instList']
            dataList=[]
            badList=[]
            cols=['componentType_code','institution_code','type_code','state']
            try:
                del df_total
            except NameError:
                pass

            status_bar=st.progress(0.0) #st.empty()
            status_text=st.empty()
            maxVal=500
            for c,i in enumerate(loopList):
                loopText="inst: "+i['code']+", "+str(c+1)+" of "+str(len(loopList))
                status_text.text(loopText)
                status_bar.progress(1.0*c/len(loopList))
                #st.write("inst: "+i['code'])
                count=0
                clTotal=1
                while count<clTotal:
                    #st.write("count:",count,", clTotal:",clTotal," maxVal:",maxVal)
                    # this should not be a list, this should be paginated
                    compList=st.session_state.myClient.get('listComponents', json={'project':st.session_state.Authenticate['proj']['code'], 'institution':i['code'], 'pageInfo':{'pageSize':maxVal,'pageIndex':int(count/maxVal)}})
                    status_text.text(loopText+" ... "+",".join([k+":"+str(v) for k,v in compList.page_info.items()]))
                    if count==0:
                        clTotal=compList.total
                    if clTotal<1:
                        badList.append(i)
                        continue
                    #st.write(str(int(count/maxVal))+":",len(compList.data),"..",compList.data[0]['componentType']['code'])
                    df_instComps=pd.json_normalize(compList.data, sep = "_")
                    df_instComps['sum']=1
                    df_data=df_instComps[cols+['sum']].groupby(cols).sum().reset_index()
                    count+=maxVal
                    try:
                        df_total=df_total.append(df_data, ignore_index=True)
                    except NameError:
                        df_total=df_data.copy()
            pageDict['compInstList']=df_total.groupby(["institution_code","componentType_code","type_code","state"]).sum().reset_index()


        upVal=st.checkbox('Upload file')
        if upVal:
            st.write("Upload previously downloaded _csv_ file")
            uploaded_file = st.file_uploader(label="Upload csv file",type=["csv"])
            if uploaded_file is not None:
                pageDict['compInstList'] = pd.read_csv(uploaded_file)

        if "compInstList" not in list(pageDict.keys()):
            st.write("No data collected")
            st.stop()

        # try:
        #     st.write("Ungrouped data")
        #     st.dataframe(df_total)
        # except UnboundLocalError:
        #     pass
        st.write("### Grouped data")
        st.write(pageDict['compInstList'])

        now = datetime.now() # current date and time
        st.download_button(label="Download list", data=pageDict['compInstList'].to_csv(index=False),file_name="listedData_"+st.session_state.Authenticate['proj']['name']+"_"+now.strftime("%Y_%m_%d")+".csv")



        st.write("### Plotting")
        filterList=["TEST","test","20USC","William","SVEN","Michael","uun"]
        st.write("filterList (componentTypeCodes):",filterList)
        if st.button("See filtered data?"):
            for x in filterList:
                st.write("### check:",x)
                st.write(pageDict['compInstList'][pageDict['compInstList']['componentType_code'].str.contains(x)])

        ### plot primary (coloured by secondary)
        priVar=st.selectbox("Primary variable", ["institution_code","componentType_code"])
        secVar=[x for x in ["institution_code","componentType_code"] if x!=priVar][0]

        st.write("Counting",priVar,"(coloured by",secVar+")")
        # group by componentType and institute
        df_plot=pageDict['compInstList'].groupby(["institution_code","componentType_code"]).sum().reset_index()
        # drop test componentTypes
        for x in filterList:
            df_plot=df_plot[~df_plot['componentType_code'].str.contains(x)]
        priChart=alt.Chart(df_plot).mark_bar().encode(
            y=priVar,
            x=alt.X('sum(sum):Q',scale=alt.Scale(type='symlog')), #'sum(sum)',
            color=secVar,
            tooltip=['componentType_code', 'institution_code', 'sum(sum)']
        ).configure_legend(labelLimit= 0, symbolLimit=0).properties(width=800)
        st.altair_chart(priChart)

        ### particular info.

        partic=st.selectbox("Particular "+priVar, list(df_plot[priVar].unique()))

        partChart=alt.Chart(df_plot.query(priVar+'=="'+partic+'"')).mark_bar().encode(
            y=secVar,
            x=alt.X('sum(sum):Q'), #'sum(sum)',
            color=secVar,
            tooltip=['componentType_code', 'institution_code', 'sum(sum)']
        ).configure_legend(labelLimit= 0, symbolLimit=0).properties(width=800)
        st.altair_chart(partChart)

        st.write(pageDict['compInstList'].query(priVar+'=="'+partic+'"'))

        # for pv in df_plot[priVar].unique():
        #     st.write("####",pv)
        #     st.write(df_plot.query('priVar=="'+pv+'"'))
        