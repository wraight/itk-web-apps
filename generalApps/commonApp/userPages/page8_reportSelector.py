### standard
import streamlit as st
from core.Page import Page
import os
from io import BytesIO
from io import StringIO
### custom
import requests
from bs4 import BeautifulSoup
import pandas as pd
### analyses
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx


projURLs = {
    "strips" : "https://wraight.web.cern.ch/strips-reports/",
    "pixels" : "https://wraight.web.cern.ch/pixels-reports/",
    "common_electronics" : "https://wraight.web.cern.ch/common_electronics-reports/",
    "common_mechanics" : "https://wraight.web.cern.ch/common_mechanics-reports/"
}


# Function to fetch reports from the given URL
def fetch_reports(url):
# version 1 of this function:
#    response = requests.get(url)
#    if response.status_code == 200:
#        soup = BeautifulSoup(response.text, 'html.parser')
#        # Assuming the reports are in <li> tags, update as necessary
#        reports = [li.get_text() for li in soup.find_all('li')]
#        return reports
#    else:
#        st.error(f"Error fetching reports from {url}")
#        return []
# version 2 of this function:
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raises an error for bad responses (4xx, 5xx)
        
        soup = BeautifulSoup(response.text, 'html.parser')
        
        # Attempt to find links; adjust tag name and class if necessary
        reports = []
        for tag in soup.find_all(['a', 'li']):  # Try both <a> and <li> tags for flexibility
            link = tag.get('href')
            text = tag.get_text()
            
            # Only add links that look like report links
            #if link and "report" in link.lower():
            reports.append(link if link.startswith('http') else f"{url}{link[2:]}")

        # Debug output
        st.write(f"Fetched {len(reports)} report links from {url}")
        
        return reports

    except requests.exceptions.RequestException as e:
        st.error(f"Error fetching reports from {url}: {e}")
        return []
# version 3 of this function:
#    response = requests.get(url)
#    if response.status_code == 200:
#        soup = BeautifulSoup(response.text, 'html.parser')
#        
#        # Retrieve all links regardless of tag type for inspection
#        all_links = [a['href'] for a in soup.find_all('a', href=True)]
#        
#        # Display all links to confirm which ones are being fetched (this part works)
#        #st.write("All fetched links:")
#        #for link in all_links:
#        #    st.write(link)
#        
#        return all_links  # Return all links for further processing
#    else:
#        st.error(f"Error fetching reports from {url}")
#        return []


class Page8(Page):

    def __init__(self):
        super().__init__("Report Selector", ":microscope: select automated reporting")


    def main(self):
        super().main()

        st.title("Automated Reports Selection")
        st.write("""
        This webpage allows users to select  _Data Integrity_ reports automatically generated for various ITk projects.
        - For Data Integretry landing pages see [here]('https://itk-reporting-repository.docs.cern.ch')
        - For list of reporting tools available see [here]('https://itk.docs.cern.ch/general/PDB_tools/Reporting/')
        """)

        st.write("---")
        st.write("### Selection")
        
    # User project selection
        project = st.selectbox("Select Project", list(projURLs.keys()), format_func=lambda x: " ".join([y.title() for y in x.split('_')]))
    
    # Fetching report links from the selected project's URL
        url = projURLs[project]
        report_links = fetch_reports(url)

    # Extract report types from the links
#        report_types = {link.split('/')[-1].split("_")[0]: link for link in report_links}
        report_types = {
            "CTDB" : {'name':"Component Type Dashboard",'description':"Compare properties for components with same component type",'links':[]},
            "TTDB" : {'name':"Test Type Dashboard",'description':"Compare test properties and results for testRuns with same test type",'links':[]},
            "IIDB" : {'name':"Institution Inventory Dashboard",'description':"Count components at institution",'links':[]},
            "PPDB" : {'name':"Part Population Type Dashboard",'description':"Plots of component current locations and stages",'links':[]},
            "MISC" : {'name':"Miscellaneous reports",'description':"Mix of non-standard reports",'links':[]}
        }
        component_types = set()
        test_types = set()
        institutions = set()

        for link in report_links:
            #st.write(link)
            last_word = link.split('/')[-1].split("_")[0]
            #st.write(last_word)
            first_4 = last_word[:4]
            #st.write(first_4)
            if first_4 in [x for x in report_types.keys() if x!="MISC"]:
                report_types[first_4]['links'].append(link)
            else:
                report_types["MISC"]['links'].append(link)
                
            parts = link.split('/')[-1].replace('.html', '').split('-')
            if first_4 in ["CTDB", "TTDB", "PPDB"]:
                if len(parts) > 2:
                    component_type = parts[2]
                    component_types.add(component_type)
            if first_4 == "TTDB":
                if len(parts) > 3:
                    test_type = parts[3]
                    test_types.add(test_type)
            if first_4 == "IIDB":
                if len(parts) > 2:
                    institution = parts[2]
                    institutions.add(institution)

        # Convert sets to sorted lists for dropdowns
        component_types = ["All"] + sorted(list(component_types))
        test_types = ["All"] + sorted(list(test_types))
        institutions = ["All"] + sorted(list(institutions))

        # sanity check: print count of each type of event, should sum to total printed above
        st.write("__Report Types__")
        df_reps=pd.DataFrame(report_types).transpose()
        df_reps['#links']=df_reps['links'].apply(lambda x: len(x) if type(x)==type([]) else x)
        df_reps=df_reps.reset_index().rename(columns={'index':"code"})
        st.write(df_reps[['name','code','description','#links']])
        st.write(" - For more information on report types see [here]('https://itk-reporting-repository.docs.cern.ch/report_notes/')")
    # User report type selection
        if report_types:
            rep_sel = st.multiselect("Select Report Types", df_reps['code'].unique())
            # st.write(rep_sel)
        
        df_types=df_reps.query('code in @rep_sel')
        # stop if empty
        if df_types.empty:
            st.write("Please select report type")
            st.stop()

        st.write(df_types)
        df_types=df_types.explode("links")
        st.write(f" - {len(df_types.index)} reports found")

        filt_sel=st.radio("Filter method?", ["Lists","Input Text"])

        ### filter by lists
        if "list" in filt_sel.lower():
            st.write("#### Filter by lists")

            if "CTDB" in list(df_types['code'].unique()) or "TTDB" in list(df_types['code'].unique())or "PPDB" in list(df_types['code'].unique()):
                ct_sel = st.multiselect("Select Component Types", component_types)

                # st.write(ct_sel)
                if len(ct_sel)>0:
                    df_types= df_types[ df_types['links'].apply(lambda x: any(map(x.__contains__, ct_sel))) ]
                st.write(f" - {len(df_types.index)} reports remaining")
                if df_types.empty:
                    st.write("No reports matching selection.")
                    st.stop()

            if "TTDB" in list(df_types['code'].unique()):
                tt_sel = st.multiselect("Select Test Types", test_types)

                # st.write(tt_sel)
                if len(tt_sel)>0:
                    df_types= df_types[ df_types['links'].apply(lambda x: any(map(x.__contains__, tt_sel))) ]
                st.write(f" - {len(df_types.index)} reports remaining")
                if df_types.empty:
                    st.write("No reports matching selection.")
                    st.stop()

            if "IIDB" in list(df_types['code'].unique()):
                ii_sel = st.multiselect("Select Institutions", institutions)

                # st.write(ii_sel)
                if len(ii_sel)>0:
                    df_types= df_types[ df_types['links'].apply(lambda x: any(map(x.__contains__, ii_sel))) ]
                st.write(f" - {len(df_types.index)} reports remaining")
                if df_types.empty:
                    st.write("No reports matching selection.")
                    st.stop()
                        
        else:
            st.write("#### Filter by text")
            report_filter = st.text_input("Filter Reports by text")

            st.write(report_filter)
            if len(report_filter)>0:
                df_types= df_types[ df_types['links'].str.contains(report_filter) ]
            st.write(f" - {len(df_types.index)} reports remaining")
            if df_types.empty:
                st.write("No reports matching selection.")
                st.stop()

        st.write("### Found reports")

        for link in df_types['links'].to_list():
            st.write(link)
        # # Get the specific report URLs based on the selected report type
        # #filtered_reports = [link for link in report_links if report_type in link]
        # filtered_reports = report_types[report_type]['links']
        
        # if report_name_filter:
        #     filtered_reports = [report for report in filtered_reports if report_name_filter.lower() in report.lower()]

        # if component_type != "All":
        #     filtered_reports = [report for report in filtered_reports if component_type.lower() in report.lower()]

        # if test_type != "All":
        #     filtered_reports = [report for report in filtered_reports if test_type.lower() in report.lower()]

        # if institution != "All":
        #     filtered_reports = [report for report in filtered_reports if institution.lower() in report.lower()]
        
        #     # Display the filtered reports
        #     st.write("Filtered Reports:")
        #     for report in filtered_reports:
        #         st.write(report)
        
        # else:
        #     st.write("No reports found for the selected project.")
        
    
