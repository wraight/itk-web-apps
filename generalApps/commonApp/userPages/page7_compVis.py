### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
import barcode
import cv2
import numpy as np
from barcode.writer import ImageWriter
from io import BytesIO
from PIL import Image
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.StreamlitTricks as stTrx
import commonCode.PDBTricks as pdbTrx
import commonCode.codeChunks as chnx
import pandas as pd

from  .CommonCode import *

infoList=["  * select *componentType* or *testType*"]

def convert_df(df):
   return df.to_csv(index=False).encode('utf-8')
#####################
### main part
#####################

class Page7(Page):
    def __init__(self):
        super().__init__("Comp/Test Structure", "PDB set-up for Components and Tests ", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        st.write("## Get Component Information")
        ### get comp info
        pageDict['projList']= st.session_state.myClient.get('listProjects', json={})
        if type(pageDict['projList'])!=type([]):
            pageDict['projList']=pageDict['projList'].data
        infra.SelectBox(pageDict,'projSel',pageDict['projList'],'Select project:',"name")

        pageDict['compList']= st.session_state.myClient.get('listComponentTypes', json={'project':pageDict['projSel']['code']})
        if type(pageDict['compList'])!=type([]):
            pageDict['compList']=pageDict['compList'].data
        infra.SelectBox(pageDict,'compSel',pageDict['compList'],'Select component type:',"name")

        infra.Radio(pageDict,'objSel',["componentType","testType"],"Select object to build:")

        if pageDict['objSel']=="componentType":
            pageDict['objInfo']= st.session_state.myClient.get('getComponentTypeByCode', json={'project':pageDict['projSel']['code'], 'code':pageDict['compSel']['code']})
        else:
            pageDict['testList']= st.session_state.myClient.get('listTestTypes', json={'project':pageDict['projSel']['code'], 'componentType':pageDict['compSel']['code']})
            if type(pageDict['testList'])!=type([]):
                pageDict['testList']=pageDict['testList'].data
            infra.SelectBox(pageDict,'testSel',pageDict['testList'],'Select test:',"name")
            st.write(pageDict['testSel']['code'])
            pageDict['objInfo']= st.session_state.myClient.get('getTestTypeByCode', json={'project':pageDict['projSel']['code'], 'componentType':pageDict['compSel']['code'], 'code':pageDict['testSel']['code']})

        infra.ToggleButton(pageDict,'tog_full','See full object info.')
        if pageDict['tog_full']:
            st.write(pageDict['objInfo'])
        
        st.write("---")


        if pageDict['objSel']=="componentType": 
            #userJson=chnx.CustomJson(pageDict,'component',{'project':pageDict['projSel']['code'],'code':pageDict['compSel']['code']},None)
            # my selection by hand, objInfo is json
                        # print types
            st.write("## Information for component type: ", pageDict['objInfo']['name'])
            st.write("code: ", pageDict['objInfo']['code'])
            st.write("project code: ", pageDict['objInfo']['project']['code'])

            if "snAutomatically" not in pageDict['objInfo'].keys() or pageDict['objInfo']['snAutomatically']!=True:
                st.write("__Serial number is not automatically generated__")
            

            if "subprojects" not in pageDict['objInfo'].keys() or pageDict['objInfo']['subprojects']==None:
               st.write("No _subprojects_ information available for object")
            else:
                st.write("## Sub-projects")
                st.write(" - used for XX code in serialNumber")
                df_sp=pd.DataFrame(pageDict['objInfo']['subprojects'])
                st.table(df_sp)
                csv = convert_df(df_sp)
                st.download_button(
                    "Press to Download",
                    csv,
                    "subprojects.csv",
                    "text/csv",
                    key='download-subprojects-csv'
                )  
            
            if "types" not in pageDict['objInfo'].keys() or pageDict['objInfo']['types']==None:
               st.write("No _types_ information available for object")
            else:
                st.write("## Types")
                typename=[]
                typecode=[]
                typeyy=[]
                typexx=[]
                prodtype=[]

                for comp_type in pageDict['objInfo']['types']:
                    typename.append(comp_type['name'])
                    typecode.append(comp_type['code'])
                    if "subprojects" in comp_type.keys():
                        if type(comp_type['subprojects'])==type([]):
                            typexx.append([sp['code'] for sp in comp_type['subprojects']])
                        else: 
                            typexx.append(comp_type['subprojects'])
                    typeyy.append(comp_type['snComponentIdentifier'])
                    typeprod=[]
                    if comp_type['version']['prototype']:
                        typeprod.append("prototype")
                    if comp_type['version']['pre-production']:
                        typeprod.append('pre-production')
                    if comp_type['version']['production']:
                        typeprod.append('production')
                    prodtype.append(typeprod)
                type_table = {
                    "name": typename,
                    "code": typecode,
                    "production types": prodtype,
                    "XX identifier": typexx,
                    "YY identifier": typeyy}
                # st.write(type_table)
                df_types = pd.DataFrame(type_table)
                st.table(df_types)
                csv = convert_df(df_types)
                st.download_button(
                    "Press to Download",
                    csv,
                    "types.csv",
                    "text/csv",
                    key='download-csv'
                )            

            if "properties" not in pageDict['objInfo'].keys() or pageDict['objInfo']['properties']==None:
               st.write("No _properties_ information available for object")
            else:
                st.write("## Properties")
                propname=[]
                propcode=[]
                propdatatype=[]
                propreq=[]
                proppos=[]
                propunique=[]
                for property in pageDict['objInfo']['properties']:
                    propname.append(property['name'])
                    propcode.append(property['code'])
                    propdatatype.append(property['dataType'])
                    reqprop='no'
                    if property['required']: reqprop='yes'
                    propreq.append(reqprop)
                    uniqueprop='no'
                    if property['unique']: uniqueprop='yes'
                    propunique.append(uniqueprop)
                    snposstring=""
                    if property['snPosition']:
                        snposstring= str(property['snPosition'][0])+"-"+str(property['snPosition'][1])
                    proppos.append(snposstring)
                #st.write("name",propname)
                #st.write("code",propcode)
                prop_table = {
                    "name": propname,
                    "code": propcode,
                    "data type": propdatatype,
                    "required?": propreq,
                    "unique?": propunique,
                    "SN digit allocation": proppos}
                df_properties = pd.DataFrame(prop_table)
                st.table(df_properties)
                csv = convert_df(df_properties)

                st.download_button(
                    "Press to Download",
                    csv,
                    "properties.csv",
                    "text/csv",
                    key='download-prop.csv'
                )            

            if "stages" not in pageDict['objInfo'].keys() or pageDict['objInfo']['stages']==None:
               st.write("No _stages_ information available for object")
            else:
                st.write("## Stages and associated tests")
            
                stagename=[]
                stagecode=[]
                stagealt=[]
                testname=[]
                testcode=[]
                for stage in pageDict['objInfo']['stages']:
                    stagename.append(stage['name'])
                    stagecode.append(stage['code'])
                    if stage['alternative']:
                        stagealt.append('yes')
                    else:
                        stagealt.append('no')
                    if stage['testTypes']:
                        itest=0
                        for test in stage['testTypes']:
                            if itest>0:
                                stagename.append("")
                                stagecode.append("")
                                stagealt.append("")
                            testname.append(test['testType']['name'])
                            testcode.append(test['testType']['code'])
                            itest+=1
                    else:
                        testname.append("")
                        testcode.append("")
                stage_table = {
                "name": stagename,
                "code": stagecode,
                #"production type": stageprod,
                "alternative stage?": stagealt,
                "test name": testname,
                "test code": testcode}
                df_stages = pd.DataFrame(stage_table)
                st.table(df_stages)
                csv = convert_df(df_stages)
                st.download_button(
                    "Press to Download",
                    csv,
                    "stages.csv",
                    "text/csv",
                    key='download-stages-csv'
                )            

            if "children" not in pageDict['objInfo'].keys() or pageDict['objInfo']['children']==None:
               st.write("No _children_ information available for object")
            else:
                st.write("## Children")
                if pageDict['objInfo']['children']:
                    parenttype=[]
                    childname=[]
                    childcode=[]
                    childquantity=[]
                    childtype=[]
                    for comp_type in typecode:
                        itype=0
                        try:
                            if pageDict['objInfo']['children'][comp_type]:
                                ichild=0
                                #st.write('no of children ',len(pageDict['objInfo']['children'][type]))
                                for child in pageDict['objInfo']['children'][comp_type]:
                                    ichild+=1
                                    childname.append(child['name'])
                                    childcode.append(child['code'])
                                    if child['type']: 
                                        childtype.append(child['type']['name'])
                                    else:   
                                        childtype.append("all Types")
                                    childquantity.append(child['quantity'])
                                    if itype == 0:
                                        parenttype.append(comp_type)
                                    else:
                                        parenttype.append("")
                                    itype+=1
                            else:
                                childname.append("")
                                childcode.append("")
                                childtype.append("")
                                childquantity.append("")
                                parenttype.append("")
                        except KeyError:
                            st.write("no children defined for type ", comp_type)
                        
                    child_table = {
                    "component type": parenttype,
                    "name": childname,
                    "code": childcode,
                    "type": childtype,
                    "number of children": childquantity}
                    df_children = pd.DataFrame(child_table)
                    st.table(df_children)
                    csv = convert_df(df_children)
                    st.download_button(
                    "Press to Download",
                    csv,
                    "stages.csv",
                    "text/csv",
                    key='download-children-csv'
                    )                   

            if "flags" not in pageDict['objInfo'].keys() or pageDict['objInfo']['flags']==None:
                st.write("No _flags_ information available for object")
            else:
                st.write("## Flags")
                df_flags=pd.DataFrame(pageDict['objInfo']['flags'])
                st.table(df_flags)
                csv = convert_df(df_flags)
                st.download_button(
                    "Press to Download",
                    csv,
                    "flags.csv",
                    "text/csv",
                    key='download-flags-csv'
                )    
          

        else:
            #userJson=chnx.CustomJson(pageDict,'test',{'project':pageDict['projSel']['code'],'componentType':pageDict['compSel']['code'],'code':pageDict['testSel']['code']},None)
            st.write("## Information for test type: ", pageDict['objInfo']['name'])
            st.write("## Properties")
            propname=[]
            propcode=[]
            propdatatype=[]
            propreq=[]
            for property in pageDict['objInfo']['properties']:
                propname.append(property['name'])
                propcode.append(property['code'])
                propdatatype.append(property['dataType'])
                reqprop='no'
                if property['required']: reqprop='yes'
                propreq.append(reqprop)
            #st.write("name",propname)
            #st.write("code",propcode)
            prop_table = {
                "name": propname,
                "code": propcode,
                "data type": propdatatype,
                "required?": propreq}
            df_properties = pd.DataFrame(prop_table)
            st.table(df_properties)
            csv = convert_df(df_properties)
            st.download_button(
                "Press to Download",
                csv,
                "testproperties.csv",
                "text/csv",
                key='download-testprop-csv'
            )            

            st.write("## Parameters")
            paraname=[]
            paracode=[]
            paradatatype=[]
            paravalue=[]
            for parameter in pageDict['objInfo']['parameters']:
                paraname.append(parameter['name'])
                paracode.append(parameter['code'])
                paradatatype.append(parameter['dataType'])
                paravalue.append(parameter['valueType'])
            para_table = {
                "name": paraname,
                "code": paracode,
                "data type": paradatatype,
                "value type": paravalue}
            df_parameters = pd.DataFrame(para_table)
            st.table(df_parameters)
            csv = convert_df(df_parameters)
            st.download_button(
                "Press to Download",
                csv,
                "testparameters.csv",
                "text/csv",
                key='download-testparam-csv'
            )    
        
