### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
#####################

infoList=[  " * Get components at institute",
            " * View componentTypes",
            "  - Select componentTypes",
            "  - View components",
            "  - Remove Assembled optional",
            " * Select Component",
            "  - View stage history",
            " * Compare to componentType",
            "  - View stages",
            "  - select stage",
            "  - View tests",
            "* Optional delete component"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Inventory", ":microscope: Components at Institute", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        if st.checkbox("Quick Component Check?"):
            if 'SCC' not in pageDict.keys():
                pageDict['SCC']={}
            chnx.SelectComponentChunk(pageDict['SCC'])
            stTrx.DebugOutput("SelectComponentChunk info.",pageDict['SCC'])
            try:
                st.dataframe(pd.json_normalize(pageDict['SCC']['comp'], sep = "_"))
            except TypeError:
                st.write("no component info.")
        
        ## Set institution
        st.write("## Institution Information")
        sel_mode=st.radio("Get information for",["Institution", "Cluster"])
        
        if "cluster" in sel_mode.lower():
            st.write("### Check components in __Cluster__")
            if "clusList" not in pageDict.keys():
                pageDict['clusList']=st.session_state.myClient.get('listClusters',json={})
                if type(pageDict['clusList'])!=type([]):
                    pageDict['clusList']=pageDict['clusList'].data
            df_clus=pd.DataFrame(pageDict['clusList'])
            if st.checkbox("Show cluster list?"):
                st.write(df_clus)
            sel_clus=st.selectbox('Select cluster',sorted([x['code'] for x in pageDict['clusList']]))
            st.write(" - selected cluster:",sel_clus)
            pageDict['look_list']=[x['code'] for x in df_clus.query('code=="'+sel_clus+'"')['instituteList'].values[0]]

        elif "inst" in sel_mode.lower():
            st.write("### Check components in __Institute__")
            inst_codes=[x['code'] for x in st.session_state.Authenticate['instList']]
            sel_inst=st.selectbox('Select institute:', sorted(inst_codes), index=sorted(inst_codes).index(st.session_state.Authenticate['inst']['code']) )
            st.write("- selected institute:",sel_inst)
            pageDict['look_list']=[sel_inst]
        
        else:
            st.write("Select Cluster or Institute")
            st.stop()
        
        ### get project (useful for institutions and clusters)
        proj_codes=[x['code'] for x in st.session_state.Authenticate['projList']]   
        sel_proj=st.selectbox('Select project:',proj_codes, index=proj_codes.index(st.session_state.Authenticate['proj']['code']))
        
        if st.checkbox("Get quick list"):
            ### quick count
            if "quickList" not in pageDict.keys() or st.button("reset component list", key="quickList"):
                ### clear cached list if reset
                if "compList" in pageDict.keys():
                    del pageDict['compList']
                ### get quick count
                pageDict['quickList']=[]
                # loop over all institutions
                for inst in pageDict['look_list']:
                    total=st.session_state.myClient.get('listComponents', json={'filterMap':{'currentLocation':inst, 'project':sel_proj}, 'pageInfo':{ 'pageSize':1} })
                    if type(total)==type([]):
                        total=len(total)
                    else:
                        total=total.total
                    st.write(f"Got component number at {inst}: {total}")
                    pageDict['quickList'].append({'inst':inst,'total':total})   
        else:
            st.write("Please select institution(s) and project")
            st.stop()

        ### present quick count
        st.write("### Quick count of all components")
        df_quick=pd.DataFrame(pageDict['quickList'])
        st.write(df_quick)   
        st.write(" - total components:",df_quick['total'].sum())

        ### selection
        if df_quick['total'].sum() > 5000:
            st.write("__This is a lot of components__")
            st.write("Please select a subset of componentTypes (skip irrelevant components)")

            if "compTypeList" not in pageDict.keys() or pageDict['compTypeProj']!=sel_proj:
                pageDict['compTypeList']=st.session_state.myClient.get('listComponentTypes', json={'filterMap':{'project':sel_proj}})
                if type(pageDict['compTypeList'])!=type([]):
                    pageDict['compTypeList']=pageDict['compTypeList'].data
                pageDict['compTypeProj']=sel_proj
            sel_compTypes=st.multiselect('Select componentTypes:',sorted([ct['code'] for ct in pageDict['compTypeList'] if type(ct)==type({}) and "code" in ct.keys()]))
            if len(sel_compTypes)<1:
                # clear cache
                if "compList" in pageDict.keys():
                    del pageDict['compList']
                st.stop()
        else:
            sel_compTypes=None
        
        st.write("## Check componentTypes in PDB")
        
        ### get component data per institution
        # if "compList" not in pageDict.keys() or st.button("reset component list"):
        if st.checkbox("Ready to retrieve full component list"):
            if "compList" not in pageDict.keys() or st.button("reset component list", key="compList"):
                pageDict['compList']=[]
                # loop over all institutions
                for inst in pageDict['look_list']:
                    if sel_compTypes!=None:
                        total=st.session_state.myClient.get('listComponents', json={'filterMap':{'currentLocation':inst, 'project':sel_proj, 'componentType':sel_compTypes}, 'pageInfo':{ 'pageSize':1} })
                        if type(total)==type([]):
                            total=len(total)
                        else:
                            total=total.total
                        st.write(f"Getting component list [{','.join(sel_compTypes)}] for {inst} ({total} components)")
                    else:
                        total=df_quick.query('inst=="'+inst+'"')['total'].values[0]
                        st.write(f"Getting component list for {inst} ({total} components)")
                    pageSize=100
                    count= int(total/pageSize)
                    if total%pageSize>0:
                        count=count+1
                    my_bar = st.progress(0)
                    my_text=st.empty()
                    for pi in range(0,count,1):
                        my_bar.progress( float(pi+1)/count)
                        if sel_compTypes!=None:
                            ret_comps=st.session_state.myClient.get('listComponents', json={'filterMap':{'currentLocation':inst, 'project':sel_proj, 'componentType':sel_compTypes}, 'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize } } )
                        else:
                            ret_comps= st.session_state.myClient.get('listComponents',json={'filterMap':{'currentLocation':inst, 'project':sel_proj }, 'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize } } )
                        if type(ret_comps)!=type([]):
                            ret_comps=ret_comps.data
                        pageDict['compList'].extend( ret_comps )
                else:
                    st.write("**Got** components for institutions")
        else:
            st.stop()

        ### stop if nothing found
        if len(pageDict['compList'])<1:
            st.write("### No components found")
            st.stop()

        # st.write(pageDict['compList'])
        df_compList=pd.json_normalize(pageDict['compList'], sep = "_")
        # df_compTypes=df_compList[['componentType_name','componentType_code']].drop_duplicates().sort_values(by='componentType_name').reset_index(drop=True)
        df_compTypes=df_compList.groupby(by=['componentType_name','componentType_code']).count().reset_index()
        st.write("#### Found components")
        st.write(df_compTypes[['componentType_name','componentType_code','id']].rename(columns={'id':'count'}))
        st.write(" - total components:",len(df_compList.index))

        compTypeList=df_compTypes['componentType_code'].astype(str).unique().tolist()
        if len(compTypeList)>1:
            pageDict['selComps']=st.multiselect('Select componentTypes:', sorted(df_compTypes['componentType_code'].astype(str).unique().tolist()))
        else:
            pageDict['selComps']=compTypeList
        queryStr=""
        for e,c in enumerate(pageDict['selComps']):
            if e>0:
                queryStr+=' | '
            queryStr+='componentType_code=="'+c+'"'
        stTrx.DebugOutput("queryStr",queryStr)

        if len(pageDict['selComps'])<1 and not st.checkbox("Use all components?"):
            st.stop()

        if len(pageDict['selComps'])>0:
            st.write("### List of *"+", ".join(pageDict['selComps'])+"* components")
        else: 
            st.write("### List of *all* components")
        
        df_comps=df_compList.sort_values(by=['componentType_code','type_code','currentStage_code']).reset_index(drop=True)
        if len(pageDict['selComps'])>0:
            df_comps=df_compList.query(queryStr).sort_values(by=['componentType_code','type_code','currentStage_code']).reset_index(drop=True)
        # df_comps=df_compList.query('componentType_code=="'+pageDict['compType']['componentType_code'].values[0]+'"')
        colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','currentLocation_code','assembled','state','cts']

        ### make all columns strings
        for c in colz:
            df_comps[c]=df_comps[c].astype(str)

        # infra.ToggleButton(pageDict,'removeAss',"Remove assembled components from list?")
        # if pageDict['removeAss']:
        if st.checkbox("Remove deleted components?", value=True):
            df_comps=df_comps.query('state=="ready"')
        if st.checkbox("Remove assembled components?", value=True):
            df_comps=df_comps.query('assembled=="False"')

        try:
            stTrx.ColourDF(df_comps[colz],'type_code')
            #st.dataframe(df_comps[colz].style.apply(stTrx.ColourCells, df=df_comps[colz], colName='currentStage_code', flip=True, axis=1))
        except IndexError:
            st.dataframe(df_comps[colz])
        st.write("Total:",len(df_comps.index))
        ### download dataframe
        st.download_button(label="Write csv", data=df_comps[colz].to_csv(index=False),file_name="components_"+st.session_state.Authenticate['inst']['code']+"_"+st.session_state.Authenticate['proj']['code']+".csv")

        st.write("---")

        ### simple stats on components: status, currentStage

        if st.checkbox("See Group Statistics?", value=False)==True:
            # st.stop()

            st.write("## Group Component Statistics")
            # loop over selected componentTypes
            if len(pageDict['selComps'])>0:
                for compType in pageDict['selComps']:
                    st.write(f"#### {compType}")
                    df_compType=df_comps.query(f'componentType_code=="{compType}"').reset_index(drop=True)

                    ### group by type
                    st.write(df_compType.groupby(by='type_code').count().rename(columns={'id':"count"}).reset_index()[['type_code','count']])
                    st.write(f" - total:",len(df_compType.index))

                    ### use properties
                    if st.checkbox("Filter by property?", key="checkbox_"+compType):
                        df_props=df_compType.explode('properties').reset_index(drop=True)
                        for cv in ['code','value']:
                            df_props['prop_'+cv]=df_props['properties'].apply(lambda x: x[cv] if type(x)==dict and cv in x.keys() else x)
                        sel_prop=st.selectbox('Select property',sorted(df_props['prop_code'].unique()), key="selectbox_"+compType) 
                        df_grp=df_props.query('prop_code=="'+sel_prop+'"').groupby(by='prop_value').count().rename(columns={'id':"count"}).reset_index()[['prop_value','count']]
                        st.write(df_grp)
                        st.write(" - total:",df_grp['count'].sum())
                        
                    ### select sub-types
                    if st.checkbox(f"Select {compType} types?", value=True):
                        pageDict['selSubs']=st.multiselect(f'Select {compType} types:', df_compType['type_code'].astype(str).unique().tolist())
                    else:
                        pageDict['selSubs']=df_compType['type_code'].unique()

                    # status info.
                    # for subType in df_compType['type_code'].unique():
                    for subType in pageDict['selSubs']:
                        df_subType=df_compType.query(f'type_code=="{subType}"').reset_index(drop=True)
                        st.write(f"**{subType} Information**: total {len(df_subType.index)}")
                        # st.write(f"__type total {len(df_subType.index)}__")
                        df_subType=df_subType.rename(columns={'id':"count"})

                        st.write(f"**Status** ({'/'.join(['ready','reworked','trashed'])})")
                        st.write(df_subType.groupby(by=["state"])['count'].count())
                        st.write(" - total:",len(df_subType.index))
                        st.write(f"**Current Stage** (excluding unpopulated stages)")
                        st.write(df_subType.groupby(by=["currentStage_code"])['count'].count())
                        st.write(" - total:",len(df_subType.index))
                        st.write(f"**Vintage** (by creation year)") 
                        df_subType['year']=df_subType['cts'].apply(lambda x: x.split('-')[0] if len(x.split('-'))>0 else x )
                        # st.write(df_subType.groupby(by=["year"])['count'].count())
                        df_vint=df_subType.groupby(by=["year"])['count'].count().reset_index()[['year','count']]
                        df_vint=df_vint.transpose()
                        df_vint.columns = df_vint.iloc[0]
                        df_vint = df_vint[1:]
                        st.write(df_vint)
                        st.write(" - total:",len(df_subType.index))

                        ### use properties
                        if st.checkbox("Filter by property?", key="checkbox_"+subType):
                            df_props=df_subType.explode('properties').reset_index(drop=True)
                            for cv in ['code','value']:
                                df_props['prop_'+cv]=df_props['properties'].apply(lambda x: x[cv] if type(x)==dict and cv in x.keys() else x)
                            sel_prop=st.selectbox('Select property',sorted(df_props['prop_code'].unique()), key="selectbox_"+subType) 
                            df_grp=df_props.query('prop_code=="'+sel_prop+'"').groupby(by='prop_value').count().rename(columns={'id':"count"}).reset_index()[['prop_value','count']]
                            st.write(df_grp)
                            st.write(" - total:",df_grp['count'].sum())

            ### if no componentTypes selected --> use all components
            else:
                # status info.
                st.write("**Status**:")
                for compState in ["ready","reworked","trashed"]:
                    st.write("-",compState,":",df_comps.query('state=="'+compState+'"')['serialNumber'].count())
                # currentStage info.
                st.write("**Current Stage**:")
                for compStage in sorted(df_comps['currentStage_code'].unique()):
                    st.write("-",compStage,":",df_comps.query('currentStage_code=="'+compStage+'"')['serialNumber'].count())
                # vintage info.
                st.write(f"**Vintage**:") 
                for cts_year in [str(t) for t in range(2020,2030,1)]:
                    df_year=df_comps.query(f'cts.str.contains("{cts_year}")', engine="python")
                    if not df_year.empty:
                        st.write("-",cts_year,":",df_year['serialNumber'].count())

                ### use properties
                if st.checkbox("Filter by property?"):
                    df_props=df_comps.explode('properties').reset_index(drop=True)
                    for cv in ['code','value']:
                        df_props['prop_'+cv]=df_props['properties'].apply(lambda x: x[cv] if type(x)==dict and cv in x.keys() else x)
                    sel_prop=st.selectbox('Select property',sorted(df_props['prop_code'].unique())) 
                    df_grp=df_props.query('prop_code=="'+sel_prop+'"').groupby(by='prop_value').count().rename(columns={'id':"count"}).reset_index()[['prop_value','count']]
                    st.write(df_grp)
                    st.write(" - total:",df_grp['count'].sum())

            st.write("---")

        ### individual component information    
        if st.checkbox("Get individual component information?", value=False)==True:
            # st.stop()

            st.write("## Individual Component Information")

            # filter components
            sel_compType=st.selectbox('Select componentType:', sorted(df_comps['componentType_code'].unique()))
            sel_subType=st.selectbox('Select sub-Type:', sorted(df_comps.query(f'componentType_code=="{sel_compType}"')['type_code'].unique()))
            df_subs=df_comps.query(f'componentType_code=="{sel_compType}" & type_code=="{sel_subType}"')
            
            # df_subs['properties']=df_subs['properties'].apply(lambda x: { p['code']:p['value'] for p in x if type(p)==type({}) and "code" in p.keys() and "value" in p.keys() } if type(x)==type([]) else x )
            # st.write(df_subs[['serialNumber','alternativeIdentifier','currentStage_code','properties']])
            ### use properties
            if st.checkbox("Filter by property?"):
                df_props=df_subs.explode('properties').reset_index(drop=True)
                for cv in ['code','value']:
                    df_props['prop_'+cv]=df_props['properties'].apply(lambda x: x[cv] if type(x)==dict and cv in x.keys() else x)
                sel_prop=st.selectbox('Select property',sorted(df_props['prop_code'].unique())) 
                df_grp=df_props.query('prop_code=="'+sel_prop+'"').groupby(by='prop_value').count().rename(columns={'id':"count"}).reset_index()[['prop_value','count']]
                st.write(df_grp)
                st.write(" - total:",df_grp['count'].sum())
                sel_value=st.selectbox('Select value',sorted(df_props.query(f'prop_code=="{sel_prop}"')['prop_value'].unique())) 
                df_subs=df_props.query(f'prop_code=="{sel_prop}" & prop_value=="{sel_value}"').reset_index(drop=True)
                st.write(df_subs[colz+['prop_code','prop_value']])
            else:
                st.write(df_subs[colz])
            st.write(" - total:",len(df_subs.index))
            ### select component
            if "compKey" not in pageDict.keys():
                pageDict['compKey']='serialNumber'
            stTrx.DebugOutput("using distinguishing key:",pageDict['compKey'])
            if st.session_state.debug:
                infra.SelectBox(pageDict,'compKey',df_comps.columns,'Select key')
                st.write("unique values:",len(df_subs[pageDict['compKey']].unique()))
                st.write("total length:",len(df_subs))

            infra.SelectBox(pageDict, 'selCompVal', df_subs[pageDict['compKey']].astype(str).unique().tolist(), 'Select component '+pageDict['compKey']+':')

            ### individual data
            st.write("#### Individual Information")
            df_compSel=df_comps.query(pageDict['compKey']+'=="'+pageDict['selCompVal']+'"').reset_index(drop=True)
            
            st.write(df_compSel[colz].transpose().rename(columns={0:"value"})) 

            if df_compSel.shape[0]>1:
                st.write("Multiple entries with "+pageDict['compKey']+":",pageDict['selCompVal'])
                infra.SelectBox(pageDict, 'selCompDate', df_compSel['stateTs'].astype(str).unique().tolist(), 'Select component creation date:')
                pageDict['selCompID']=df_comps.query(pageDict['compKey']+'=="'+pageDict['selCompVal']+'" & stateTs=="'+pageDict['selCompDate']+'"')['id'].values[0]
            else:
                pageDict['selCompID']=df_comps.query(pageDict['compKey']+'=="'+pageDict['selCompVal']+'"')['id'].values[0]

            pageDict['thisComp']=df_comps.query('id=="'+pageDict['selCompID']+'"')

            ### delete component?
            pdbTrx.DeleteCoTeSt(pageDict,'delComp','deleteComponent',pageDict['thisComp']['serialNumber'].values[0])

            stTrx.DebugOutput("full component information",pageDict['thisComp'].to_dict())
            if "thisComp" not in pageDict.keys():
                st.write("Select component")
                st.stop()

            ### component information
            for col in ['stages','properties']:
                st.write(f"#### {col.title()} information")
                df_col=pageDict['thisComp'][['serialNumber',col]]
                df_col=df_col.explode(col).reset_index(drop=True)
                ### split dictionary into columns
                df_concat=pd.concat([df_col.drop([col], axis=1), df_col[col].apply(pd.Series)], axis=1)
                ### order by dateTime if possible
                if "dateTime" in df_concat.columns:
                    df_concat['dateTime']=pd.to_datetime(df_concat['dateTime'])
                    df_concat=df_concat.sort_values(by='dateTime').reset_index(drop=True)
                # stTrx.ColourDF(df_life,'code')
                st.write(df_concat)



            ### checklist
            st.write("#### Test Checklist")
            compVal=st.session_state.myClient.get('getComponent', json={'component':pageDict['thisComp']['serialNumber'].values[0]})
        
            # test uploads
            df_cl=pdbTrx.FillCheckList(compVal)
            if df_cl.empty:
                st.write("Cannot generate Test Upload Summary :(")
                return 
            st.write("Test Upload Summary (green: PASSED, red: FAILED, grey: MISSING)")
            sum_cols=['stage_order','stage_code','test_code','present','passed']
            st.write(df_cl[sum_cols].style.apply(pdbTrx.HighlightTests, axis=1))


            ### Add comparison to archetype componentType stages
            infra.ToggleButton(pageDict,'allStages',"See all "+pageDict['thisComp']['componentType_code'].values[0]+" componentType stages?")
            if pageDict['allStages']:
                compTypeRet=st.session_state.myClient.get('getComponentTypeByCode', json={'project':st.session_state.Authenticate['proj']['code'], 'code':pageDict['thisComp']['componentType_code'].values[0]})
                df_compStages=pd.json_normalize(compTypeRet['stages'], sep = "_")
                st.dataframe(df_compStages)
                infra.SelectBoxDf(pageDict,'stageCode',df_compStages,'Select stage', 'code')
                try:
                    df_stageTests=pd.json_normalize(pageDict['stageCode']['testTypes'].to_list()[0], sep = "_")
                    st.dataframe(df_stageTests[['testType_code','order','nextStage']])
                except TypeError:
                    st.write("No tests found")            
                except KeyError:
                    st.write("No information found")

