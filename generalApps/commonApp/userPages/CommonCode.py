### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
import barcode
import cv2
import numpy as np
from barcode.writer import ImageWriter
from io import BytesIO
from PIL import Image
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.StreamlitTricks as stTrx
import commonCode.PDBTricks as pdbTrx
import commonCode.codeChunks as chnx

### Specific component relations
def GetChildTree(client, SN, relStr=""):
    theTree=[]
    GetRelationsSpecific(client, theTree, SN, 0, relStr, 'children')
    return theTree

def GetParentTree(client, SN, relStr=""):
    theTree=[]
    GetRelationsSpecific(client, theTree, SN, 0, relStr, 'parents')
    return theTree

def GetRelationsSpecific(client, theList, theSN, theGen, theRel, relType="parents"):
    #print("trying:",theSN)
    compReq=client.get('getComponent',{'component':theSN })
    #st.write("compReq:",compReq)
    try:
        theList.append({'gen':theGen, 'serialNumber':compReq['serialNumber'], 'componentType_code':compReq['componentType']['code'], 'relation':theRel, 'cts':compReq['cts']})
    except TypeError:
        pass

    try:
        theList[-1]['num'+relType]=len(compReq[relType])
    except TypeError:
        theList[-1]['num'+relType]=0
        #print("Not this one:", len(compReq['children']))
        return None
    ### if relations...
    gen=999
    if relType=="parents": gen=theGen-1
    if relType=="children": gen=theGen+1
    for c in compReq[relType]:
        try:
            # ignore self-child
            if c['component']['serialNumber']==theSN:
                continue
        except TypeError:
            #print("no serialNumber found for componentType_code:",c["componentType"]["code"])
            #print("comp id:",c["id"])
            theList.append({'gen':gen, 'serialNumber':"unknown", 'componentType_code':c['componentType']['code'], 'relation':theSN, 'cts':c['timestamp']})
            continue
        except KeyError:
            theList.append({'gen':gen, 'serialNumber':"unknown", 'componentType_code':c['componentType']['code'], 'relation':theSN, 'cts':c['timestamp']})
            continue
        GetRelationsSpecific(client, theList, c["component"]["serialNumber"], gen, theSN, relType)

def GetParentsSpecific(client, theList, theSN, theGen, theChild):
    #print("trying:",theSN)
    compReq=client.get('getComponent',{'component':theSN })
    theList.append({'gen':theGen, 'serialNumber':compReq['serialNumber'], 'componentType_code':compReq['componentType']['code'], 'child':theChild, 'cts':compReq['cts']})
    try:
        theList[-1]['numParent']=len(compReq['parents'])
    except TypeError:
        theList[-1]['numParent']=0
        #print("Not this one:", len(compReq['parents']))
        return None
    ### if parents...
    for c in compReq['parents']:
        try:
            if c["component"]["serialNumber"]==theSN:
                continue
            try:
                GetParentsSpecific(theList, c["component"]["serialNumber"], theGen-1, theSN)
            except:
                #print("strike out:",c["component"]["serialNumber"])
                continue
        except TypeError:
            #st.write("no serialNumber found for componentType_code:",c["componentType"]["code"])
            continue
        except KeyError:
            continue

def GetChildrenSpecific(client, theList, theSN, theGen, theParent):
    #print("trying:",theSN)
    compReq=client.get('getComponent',{'component':theSN })
    theList.append({'gen':theGen, 'serialNumber':compReq['serialNumber'], 'componentType_code':compReq['componentType']['code'], 'parent':theParent, 'cts':compReq['cts']})
    try:
        theList[-1]['numChild']=len(compReq['children'])
    except TypeError:
        theList[-1]['numChild']=0
        #print("Not this one:", len(compReq['children']))
        return None
    ### if children...
    for c in compReq['children']:
        try:
            if c["component"]["serialNumber"]==theSN:
                continue
            try:
                GetChildrenSpecific(theList, c["component"]["serialNumber"], theGen+1, theSN)
            except:
                #print("strike out:",c["component"]["serialNumber"])
                continue
        except TypeError:
            #st.write("no serialNumber found for componentType_code:",c["componentType"]["code"])
            continue
        except KeyError:
            continue

### General component relations
def GetComponentType(proj,code):
    #Stave ID: 5ad8850c09d7a800067a4f60
    component=st.session_state.myClient.get('getComponentTypeByCode',json={'project':proj,'code':code})
    return component

def GetChildrenGeneral(graphDict, proj, children_dict, parent_type, parent_node, generation, includeAllType=False):
    #if ask to include All Type
    if includeAllType:
        try:
            #append all-type's children
            children_list = np.concatenate((children_dict[parent_type], children_dict["*"]), axis=0)
        #no all-type
        except KeyError:
            children_list = children_dict[parent_type]
    #exclude All Type
    else:
        try:
            children_list = children_dict[parent_type]
        except TypeError:
            children_list=[]
    #start the first generation of children
    generation+=1 # self.generation<=self.r here
    #enumerate all children
    for ind,children in enumerate(children_list):
        children_name=children["name"]
        children_code = children["code"]
        try:
            children_type=children["type"]["name"]
            children_typeCode = children["type"]["code"]
        except TypeError:
            children_type = "NoType"
            children_typeCode= "NoType"
        #save nodes names
        children_node = "G"+str(generation)+children_name+children_typeCode.strip()+str(ind)
        # print("childNode:",children_node)
        graphDict['label'].append(children_name+'\n ('+children_type+')')
        graphDict['parentNode'].append(parent_node)
        graphDict['childNode'].append(children_node)
        #if generation hasn't reached user's input resursive
        if generation < 10 and children_type != "NoType":
            #find component type in database]
            try:
                this_child = GetComponentType(proj,children_code)
            except TypeError:
                #print("TypeError with",children_type)
                continue
            #if children has children, recursively run getChildren till the recursive level user asked for
            if "children" in this_child.keys() and this_child["children"] != None:
                #children become the parent
                GetChildrenGeneral(graphDict, proj, this_child["children"], children_typeCode, children_node, generation)
            #if this children has no children, move on to the next children
            else:
                continue
        #if children type is None, move on to the next children
        elif children_type == " ":
            continue

def GetRelationsGeneral(proj,code,componentType_code):
    component=GetComponentType(proj,code)
    type_name="NYS"
    for ct in component["types"]:
        if ct["code"] == componentType_code:
            type_name = ct["name"]
    #Head node
    head='H'
    generation = 0
    graphDict={'label':[component['name'] + ' (' + type_name + ')'],
                'parentNode':["None"],
                'childNode':[head]
              }
    GetChildrenGeneral(graphDict, proj, component["children"], componentType_code, head, generation)
    return graphDict

def ViewFile(fn):
    byteImgIO = BytesIO()
    byteImg = Image.open(fn)
    byteImg.save(byteImgIO, "JPEG")
    byteImgIO.seek(0)
    byteImg = byteImgIO.read()

    dataBytesIO = BytesIO(byteImg)
    return dataBytesIO

def MakeBcFile(sn, bcType='code128'):
    CODEOBJECT = barcode.get_barcode_class(bcType)
    fileName="barcode.jpg" # sn+".jpg"
    with open(fileName, 'wb') as f:
        CODEOBJECT(sn, writer=ImageWriter()).write(f)
    return fileName
