### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
import barcode
import cv2
import numpy as np
from barcode.writer import ImageWriter
from io import BytesIO
from PIL import Image
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.StreamlitTricks as stTrx
import commonCode.PDBTricks as pdbTrx
import commonCode.codeChunks as chnx

from  .CommonCode import *

#####################
### useful functions
#####################

# matchMap={'int':int,'integer':int,'float':float,'str':str,'string':str,'bool':bool}

# def FillJson(inJson):

#     outJson={}
#     for k,v in inJson.items():
#         st.write("__"+k+"__")
#         if k in ['project','componentType','institution','testType']:
#             outJson[k]=v
#             st.write("got it:",v)
#             continue
#         if type(v)==type({}):
#             outJson[k]=FillJson(inJson[k])
#         elif type(v)==type([]):
#             val=st.selectbox('Select '+k,v)
#             try:
#                 outJson[k]=val['value']
#                 st.write("use value:",val['value'])
#             except KeyError:
#                 try:
#                     outJson[k]=val['code']
#                     st.write("use code:",val['code'])
#                 except KeyError:
#                     outJson[k]=val
#         else:
#             if type(v)==type(True):
#                 outJson[k]= st.radio('Set '+k, [True, False])
#             else:
#                 val=st.text_input('Input '+k+' ('+v+')',v)
#                 if v=="date":
#                     try:
#                         outJson[k]= pdbTrx.TimeStampConverter(val, "%Y-%m-%dT%H:%MZ")
#                     except ValueError:
#                         st.write("format: YYYY-mm-ddTHH:MMZ")
#                 else:
#                     try:
#                         outJson[k]=matchMap[v](val)
#                     except ValueError:
#                         pass

#     return outJson


# def CustomJson(pageDict, cmdType, comJson, subSet=None):

#     ### check reequiredOnly included
#     if "requiredOnly" not in comJson.keys():
#         comJson['requiredOnly']=False
#     else:
#         if comJson['requiredOnly']:
#             comJson['requiredOnly']=False
#             subSet="required"

#     ### get PDB info.
#     retInfo, retJson = None, None
#     if "component" in cmdType:
#         retInfo=st.session_state.myClient.get('getComponentTypeByCode',json=comJson)
#         retJson=st.session_state.myClient.get('generateComponentTypeDtoSample',json=comJson)
#     elif "test" in cmdType:
#         retInfo=st.session_state.myClient.get('getTestTypeByCode',json=comJson)
#         retJson=st.session_state.myClient.get('generateTestTypeDtoSample',json=comJson)
#     else:
#         st.write(f"Don't undetstand {cmdType}.")
#         st.write(f"Check {comJson}. Try component/type.")
#         return None
    
#     ### build json - top level keys
#     basicJson={ k:None for k in retJson.keys()}
#     basicJson['institution']=st.session_state.Authenticate['inst']['code']
#     ### for component
#     if "component" in cmdType:
#         basicJson['project']=comJson['project']
#         basicJson['componentType']=comJson['code']
#         if "serialNumber" in basicJson.keys():
#             basicJson['serialNumber']="string"
#         ### sub-project
#         # infra.SelectBox(pageDict,'subSel',retInfo['subprojects'],'Select sub-project:',"name")
#         basicJson['subproject']=retInfo['subprojects']
#         ### sub-type
#         # infra.SelectBox(pageDict,'typeSel',retInfo['types'],'Select sub-type:',"name")
#         basicJson['type']=retInfo['types']
#     ### for component
#     elif "test" in cmdType:
#         if "component" in basicJson.keys():
#             basicJson['component']="string"
#         basicJson['testType']=comJson['code']
#         basicJson['runNumber']="string"
#         basicJson['date']="date"
#         basicJson['passed']=True
#         basicJson['problems']=False

#     ### build json - properties (comp & test) and results (test)
#     for pp in ["properties","parameters"]:
#         try:
#             ### get selected keys
#             df_info=pd.DataFrame(retInfo[pp])
#             if subSet!=None:
#                 if type(subSet)==type([]):
#                     ### check user columns in dataframe (splits properties and parameters)
#                     dfCodes=[s for s in subSet if s in df_info['code'].to_list()]
#                     # st.write('code==["'+'","'.join(dfCodes)+'"]')
#                     try:
#                         df_info=df_info.query('code==["'+'","'.join(dfCodes)+'"]')
#                     except KeyError:
#                         st.write("One of the columns is not present.")
#                         st.write("Check columns:",df_info['code'].to_list())
#                 else:
#                     if "req" in subSet:
#                         df_info=df_info.query('required==True')
#             st.write(df_info)
            
#             ### get values
#             rp=None
#             if pp=="parameters":
#                 rp="results"
#             else:
#                 rp=pp

#             basicJson[rp]={}
#             for i,row in df_info.iterrows():
#                 # st.write(f"{row['name']} ({row['code']})")
#                 if "code" in row['dataType']:
#                     # code=st.selectbox(f"{row['name']} ({row['code']})",)
#                     basicJson[rp][row['code']]=row['codeTable']
#                 elif row['dataType'] in matchMap.keys():
#                     basicJson[rp][row['code']]=row['dataType']
#                 else:
#                     basicJson[rp][row['code']]="UNKNOWN"
#         except KeyError: # handle non-results (i.e. component)
#             pass

#     # st.write("returning:",basicJson)
#     return basicJson


infoList=["  * select *componentType* or *testType*"]

#####################
### main part
#####################

class Page5(Page):
    def __init__(self):
        super().__init__("Json builder", ":fast_forward: build json for component / testRun upload", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        st.write("## Get Component Information")
        ### get comp info
        pageDict['projList']= st.session_state.myClient.get('listProjects', json={})
        if type(pageDict['projList'])!=type([]):
            pageDict['projList']=pageDict['projList'].data
        infra.SelectBox(pageDict,'projSel',pageDict['projList'],'Select project:',"name")

        pageDict['compList']= st.session_state.myClient.get('listComponentTypes', json={'project':pageDict['projSel']['code']})
        if type(pageDict['compList'])!=type([]):
            pageDict['compList']=pageDict['compList'].data
        infra.SelectBox(pageDict,'compSel',pageDict['compList'],'Select component type:',"name")

        infra.Radio(pageDict,'objSel',["componentType","testType"],"Select object to build:")

        if pageDict['objSel']=="componentType":
            pageDict['objInfo']= st.session_state.myClient.get('getComponentTypeByCode', json={'project':pageDict['projSel']['code'], 'code':pageDict['compSel']['code']})
        else:
            pageDict['testList']= st.session_state.myClient.get('listTestTypes', json={'project':pageDict['projSel']['code'], 'componentType':pageDict['compSel']['code']})
            if type(pageDict['testList'])!=type([]):
                pageDict['testList']=pageDict['testList'].data
            infra.SelectBox(pageDict,'testSel',pageDict['testList'],'Select test:',"name")
            st.write(pageDict['testSel']['code'])
            pageDict['objInfo']= st.session_state.myClient.get('getTestTypeByCode', json={'project':pageDict['projSel']['code'], 'componentType':pageDict['compSel']['code'], 'code':pageDict['testSel']['code']})

        infra.ToggleButton(pageDict,'tog_full','See full object info.')
        if pageDict['tog_full']:
            st.write(pageDict['objInfo'])
        
        reqMap={"all":None, "required only":'requiredOnly', "sub-set (then selection)": 'subSet'}
        infra.Radio(pageDict,'subSel',[str(r) for r in reqMap.keys()],"Included parameters:")

        if pageDict['subSel']=="all" or pageDict['subSel']=="required only":
            subSet=reqMap[pageDict['subSel']]
        else:
            subSet=[]
            st.write("Select properties (comp & test) and results (test)):")
            for pp in ["properties","parameters"]:
                try:
                    df_info=pd.DataFrame(pageDict['objInfo'][pp])
                    st.write("__"+pp+"__")
                    st.write(df_info)
                    infra.MultiSelect(pageDict,'multi_'+pp,df_info['code'],'Select '+pp+':')
                    subSet.extend(pageDict['multi_'+pp])
                except KeyError:
                    pass

        # st.write(subSet)

        st.write("---")
        st.write("## Build dictionary")
        if pageDict['objSel']=="componentType": 
            userJson=chnx.CustomJson(pageDict,'component',{'project':pageDict['projSel']['code'],'code':pageDict['compSel']['code']},subSet)
        else:
            userJson=chnx.CustomJson(pageDict,'test',{'project':pageDict['projSel']['code'],'componentType':pageDict['compSel']['code'],'code':pageDict['testSel']['code']},subSet)
        infra.ToggleButton(pageDict,'tog_built','See built object info.')
        if pageDict['tog_built']:
            st.write(userJson)

        st.write("---")
        st.write("## Fill dictionary")

        userJson=chnx.FillJson(userJson)

        st.write("__Final Object__")
        st.write(userJson)

        ### for testRun
        # basicTest=st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['projSel']['code'], 'componentType':pageDict['compSel']['code'], 'code':pageDict['compSel']['code'], 'requredOnly':True}
        
        # basicJson['testType']=pageDict['testSel']['code']
        # basicJson['passed']=True
        # basicJson['problems']=False

