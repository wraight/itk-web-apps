### standard
from datetime import datetime, timedelta

### PDB stuff
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
### custom
import pandas as pd
import plotly.graph_objects as go
import streamlit as st
from core.Page import Page
import ast

import commonCode.codeChunks as chnx
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx

#####################
### useful functions
#####################

infoList=[  " * Review shipments",
            "  - Check last 100 days",
            "  - Sender/recipient relevant",
            " * Select shipment",
            "  - Check constituents",
            "  - Update shipment status",
            " * Create shipment",
            "  - select sender & recipient",
            "  - Upload components (ASNs) via formatted csv",
            "  - Send shipment",
            "  - Update status to inTransit"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Shipping", ":microscope: Shipping Information and Creation", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        if st.checkbox("Quick Shipment Check?"):
            if 'SSC' not in pageDict.keys():
                pageDict['SSC']={}
            chnx.SelectShipmentChunk(pageDict['SSC'])
            stTrx.DebugOutput("SelectShipmentChunk info.",pageDict['SSC'])
            try:
                st.dataframe(pd.json_normalize(pageDict['SSC']['ship'], sep = "_"))
            except TypeError:
                st.write("no shipment info.")

        st.write("## Check shipments in PDB")
        if st.button("reset list") or "shipList" not in list(pageDict.keys()): # check list
            st.write("**Getting** list for "+st.session_state.Authenticate['inst']['code'])
            pageDict['shipList']=st.session_state.myClient.get('listShipmentsByInstitution',json={'code':st.session_state.Authenticate['inst']['code']})
            if type(pageDict['shipList'])!=type([]):
                pageDict['shipList']=pageDict['shipList'].data
        else:
            st.write("**Got** list for "+st.session_state.Authenticate['inst']['code'])

        ### nothing found
        if len(pageDict['shipList'])<1:
            st.write("## No shipments found")
            st.stop()

        df_shipList=pd.json_normalize(pageDict['shipList'], sep = "_")
        df_shipList=df_shipList[['name','status','sender_code','recipient_code','stateTs','id']] #.sort_values(by=['status','stateTs'], ascending=False).reset_index(drop=True)

        ### use cts if stateTs problematic (isNull)
        df_shipList['dateCheck']=pd.to_datetime(df_shipList['stateTs'], format='%Y-%m-%dT%H:%M:%S.%fZ', errors='coerce')
        # st.write(df_shipList.query('dateCheck.isnull()', engine='python'))
        df_shipList['dateCheck']=df_shipList.apply(lambda x: x['cts'] if pd.isnull(x['dateCheck']) and type(x)==type({}) and "cts" in x.keys() else x['dateCheck'] , axis=1)
        df_shipList['dateCheck']=pd.to_datetime(df_shipList['dateCheck'], format='%Y-%m-%dT%H:%M:%S.%fZ', errors='coerce')
        # st.write(df_shipList.query('dateCheck.isnull()', engine='python'))

        ### reduce columns
        df_shipList=df_shipList[['name','status','sender_code','recipient_code','dateCheck','id']].sort_values(by=['status','dateCheck'], ascending=False).reset_index(drop=True)

        if 'startTime' not in pageDict.keys():
            st.write("default 100 days ago")
            # then=(datetime.now() - timedelta(days = 100)).date()
            # df_shipList[~(df_shipList['stateTs'] <  datetime.strptime(str(pageDict['startTime']), '%Y-%m-%d') ) ]
            pageDict['startTime']= (datetime.now() - timedelta(days = 100)).date()

        pageDict['startTime']=st.slider('Select date', min_value=df_shipList['dateCheck'].min().date(), value=pageDict['startTime'] ,max_value=df_shipList['dateCheck'].max().date(), format="Y-M-D")

        st.write("Check shipments from:",pd.Timestamp(pageDict['startTime'], tz="UTC") )

        # format datetime
        df_shipList['dateCheck']=df_shipList['dateCheck'].apply(lambda x: pd.Timestamp(x, tz="UTC") if "+" not in str(x) else x)
        # filter using selection
        df_shipList = df_shipList[~(df_shipList['dateCheck'] <  pd.Timestamp(pageDict['startTime'], tz="UTC") ) ].reset_index(drop=True)

        st.write("### Shipment List")

        if st.checkbox('Filter status?'):
            statSel=st.radio("Select shipment *status*:",list(df_shipList['status'].unique()))
            df_shipList=df_shipList.query(f'status=="{statSel}"').reset_index(drop=True)

        stTrx.ColourDF(df_shipList,'status')

        infra.SelectBox(pageDict, 'selShipName', df_shipList['name'].astype(str).unique().tolist(), 'Select shipment name:')

        df_shipListSel=df_shipList.query('name=="'+pageDict['selShipName']+'"')
        st.dataframe(df_shipListSel)
        st.write(f"__Items to plot: {len(df_shipList)}__")

        if df_shipListSel.shape[0]>1:
            st.write("Multiple entries with name:",pageDict['selShipName'])
            infra.SelectBox(pageDict, 'selShipDate', df_shipListSel['dateCheck'].astype(str).unique().tolist(), 'Select shipment date:')
            pageDict['selShipID']=df_shipList.query('name=="'+pageDict['selShipName']+'" & dateCheck=="'+pageDict['selShipDate']+'"')['id'].values[0]
        else:
            pageDict['selShipID']=df_shipList.query('name=="'+pageDict['selShipName']+'"')['id'].values[0]

        stTrx.DebugOutput("selected shipment ID: ",pageDict['selShipID'])

        st.write("**Shipment Components**")
        compShipList=st.session_state.myClient.get('listShipmentItems',json={'shipment':pageDict['selShipID']})
        if type(compShipList)!=type([]):
            compShipList=compShipList.data
        try:
            df_compShipList=pd.json_normalize(compShipList, sep = "_")[['component_serialNumber','component_alternativeIdentifier','component_componentType_code']]
            stTrx.ColourDF(df_compShipList,'component_componentType_code')
        except KeyError:
            st.write("**No components found in shipment**")

        ### update existing shipment
        if "selShipID" in pageDict.keys():
            infra.ToggleButton(pageDict,'shipChange','Update shipment?')
            if pageDict['shipChange']:
                # update existing shipment
                st.write("### Update **existing** shipment *status*")
                # input id
                pageDict['existInfo']=st.session_state.myClient.get('getShipment', json={'shipment':pageDict['selShipID']})
                try:
                    if pageDict['existInfo']!=None:
                        st.write("### :white_check_mark: Found!")
                        st.write("Found! \""+pageDict['existInfo']['name']+"\"")
                        infra.ToggleButton(pageDict,'fullShip',"See full shipment information")
                        if pageDict['fullShip']:
                            st.write("Full shipment detail:")
                            st.write(pageDict['existInfo'])
                        # select shipping status
                        pageDict['status']=pageDict['existInfo']['status']
                        infra.Radio(pageDict,'status',["prepared", "inTransit", "delivered", "deliveredIncomplete", "deliveredWithDamage", "undelivered"],"Select shipment *status*:")
                        
                        ### if delivered is selected
                        if pageDict['status']=="delivered":
                            if "shipping_checklist" not in pageDict.keys():
                                pageDict['shipping_checklist']=st.session_state.myClient.get('loadItkpd', json=None)['data']['shipmentChecklist']
                                #st.write(pageDict['shipping_checklist']['data'].keys())
                                
                            st.write("Set shipmentItems")
                            if "shipmentItems" not in pageDict.keys():
                                pageDict['shipmentItems']=[{'serialNumber':si['serialNumber'],'code':si['code'],'delivered':False,'damage':False} for si in pageDict['existInfo']['shipmentItems']]
                            if st.button("set all _delivered without damage_"):
                                pageDict['shipmentItems']=[{'serialNumber':si['serialNumber'],'code':si['code'],'delivered':True,'damage':False} for si in pageDict['existInfo']['shipmentItems']]
                            # st.write(pageDict['existInfo']['shipmentItems'])
                            infra.ToggleButton(pageDict,'setItems',"Set shipment items?")
                            if pageDict['setItems']:
                                st.write("length of shipmentItems:",len(pageDict['shipmentItems']))
                                for e,sit in enumerate(pageDict['shipmentItems']):
                                    st.write(e,sit['serialNumber']) #,sit['code'],"settings:")
                                    # st.write(si)
                                    sit['delivered']=st.checkbox("delivered",value=sit['delivered'], key=sit['code']+"_delivered") # pdbTrx.SelectCheck('delivered',si['delivered'],si['serialNumber'])
                                    sit['damage']=st.checkbox("damage",value=sit['damage'], key=sit['code']+"_damage") # pdbTrx.SelectCheck('damaged',si['damaged'],si['serialNumber'])
                            else:
                                st.dataframe(pageDict['shipmentItems'])

                            st.write("Set checklist")
                            stTrx.DebugOutput("shipment checklist",pageDict['shipping_checklist'])

                            if "questionList" not in pageDict.keys():
                                pageDict['questionList']=[{'code':q['code'],'text':q['text'],'value':False} if q['type']=="checkbox" else {'code':q['code'],'text':q['text'],'value':None} for q in pageDict['shipping_checklist'][0]['questionList']  ]
                            if st.button("check all _True_"):
                                pageDict['questionList']=[{'code':q['code'],'text':q['text'],'value':True} if q['type']=="checkbox" else {'code':q['code'],'text':q['text'],'value':None} for q in pageDict['shipping_checklist'][0]['questionList'] ]
                            infra.ToggleButton(pageDict,'setChecks',"Check each question?")
                            if pageDict['setChecks']:
                                for k,sq in enumerate(pageDict['questionList']):
                                    # st.write(sq['code']+": "+sq['text'])
                                    # sq['value']=pdbTrx.SelectCheck('value', sq['value'], sq, k+100)

                                    if type(sq['value'])==type(True):
                                        sq['value']=st.radio(sq['code']+": "+sq['text'], [True, False], index=[True, False].index(sq['value']), key=k+100)
                                    else:
                                        sq['value']=st.text_input(sq['code']+": "+sq['text'], value=str(sq['value']), key=k+100)
                                        try:
                                            sq['value']=ast.literal_eval(sq['value']) #int(sq['value'])
                                        except ValueError:
                                            if sq['value']=="None":
                                                sq['value']=None

                            else:
                                st.dataframe(pd.DataFrame(pageDict['questionList']).astype(str))
                            pageDict['checklist']={'type':"shipmentChecklist",'questionList':pageDict['questionList']}
                            
                        
                        if st.button("Update!"):
                            try:

                                if pageDict['status']=="delivered":
                                    pageDict['shipVal']=st.session_state.myClient.post('setShipmentStatus', json={'shipment':pageDict['existInfo']['id'],'status':pageDict['status'],'shipmentItems':pageDict['shipmentItems'],'checklist':pageDict['checklist']})
                                else:
                                    pageDict['shipVal']=st.session_state.myClient.post('setShipmentStatus', json={'shipment':pageDict['existInfo']['id'],'status':pageDict['status']})
                                try:
                                    st.write("### **Successful Update**:",pageDict['shipVal']['name'])
                                except TypeError:
                                    st.write("### **Successful shipment, but don't understand return object")
                                st.balloons()
                                st.write(pageDict['shipVal'])
                            except itkX.BadRequest as b:
                                st.write("### :no_entry_sign: Update **Unsuccessful**")
                                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                        pdbTrx.DeleteCoTeSt(pageDict,'delShip','deleteShipment',pageDict['existInfo']['id'])
                    else:
                        st.write("### :no_entry_sign: search unsuccessful")
                except KeyError:
                    pass


        ### create new shipment
        st.write("---")
        st.write("## Create **new** shipment")
        st.write("### Select *Sender* & *Recipient* Institutions")
        #st.write(st.session_state.Authenticate['inst']['name'])
        if  st.button("reset sender") or "sender" not in pageDict.keys():
            pageDict['sender']=st.session_state.Authenticate['inst']
        stTrx.DebugOutput("Full *institution* list: ",st.session_state.Authenticate['instList'])
        #st.write(pageDict['sender']['name'])
        infra.SelectBox(pageDict,'sender',st.session_state.Authenticate['instList'],'Select a **sender** Institution','name')
        infra.SelectBox(pageDict,'recipient',st.session_state.Authenticate['instList'],'Select a **recipient** Institution','name')

        # select shipping type
        st.write("### Select shipping *type*")
        shipOpts=[{'name':"domestic",'code':"domestic"},{'name':"Intra-continental (within a continent)",'code':"intraContinental"},{'name':"Inter-continental (between continents)",'code':"continental"}]
        infra.Radio(pageDict,'type',[so['name'] for so in shipOpts],"Select shipment *type*:")

        # upload components by file
        infra.ToggleButton(pageDict,'tog_upFile',"Upload components by file")
        if pageDict['tog_upFile']:
            st.write("Upload *component codes* (ASNs/codes) from **csv** file")
            st.write("Follow data format:")
            df_example=pd.DataFrame([{'serialNumber':'20Uxxyynnnnnnn'},{'serialNumber':'20Uxxyynnnnnnn'},{'serialNumber':'20Uxxyynnnnnnn'}])
            st.write(df_example.style.hide_index())
            pageDict['file']= st.file_uploader("Upload a file", type=["csv"])
            stTrx.DebugOutput("file: ",pageDict['file'])

            if pageDict['file'] is not None:
                pageDict['file'].seek(0)
                df_input=pd.read_csv(pageDict['file'])
                if "serialNumber" not in df_input.columns:
                    st.write("adding serialNumber header") # reset input file reader
                    pageDict['file'].seek(0)
                    df_input=pd.read_csv(pageDict['file'], header=None)
                    df_input=df_input.rename(columns={0: "serialNumber"})
                myComps=st.session_state.myClient.get('getComponentBulk', json={'component':list(df_input['serialNumber'].values)})
                st.write("length of myComps:",len(myComps))
                if len(myComps)<1:
                    st.write("No matching components found in PDB")
                    st.stop()
                # pageDict['df_data']=pd.DataFrame([{'serialNumber':x['serialNumber'],'code':x['code'],'compCode':x['componentType']['code'],'curLoc':x['currentLocation']['code']} for x in myComps])
                pageDict['df_data']=pd.json_normalize(myComps, sep = "_")
            else:
                st.write("No data file set")
                st.stop()

        ### select components by hand
        infra.ToggleButton(pageDict,'tog_upSel',"Select components")
        if pageDict['tog_upSel']:
            st.write("### Check componentTypes in PDB")
            if st.button("reset list",key="shipping") or "compList" not in list(pageDict.keys()): # check list
                st.write("**Getting** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
                pageDict['compList']=st.session_state.myClient.get('listComponents',json={'currentLocation':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] })
                if type(pageDict['compList'])!=type([]):
                    pageDict['compList']=pageDict['compList'].data
            else:
                st.write("**Got** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            df_compList=pd.json_normalize(pageDict['compList'], sep = "_")

            df_subComps=pdbTrx.SelectManyDf(pageDict,df_compList,'componentType_code')
            stTrx.Stringify(df_subComps)
            infra.MultiSelect(pageDict,'multiSNs',df_subComps['serialNumber'].to_list(),'Select serialNumbers:')
            if len(pageDict['multiSNs'])<1:
                st.write("No serialNumbers chosen")
                st.stop()
            else:
                stTrx.DebugOutput('Selected serialnumbers: ',pageDict['multiSNs'])
                pageDict['df_data'] = df_subComps[df_subComps['serialNumber'].isin(pageDict['multiSNs'])]

        if "df_data" not in pageDict.keys():
            st.write("No components selected to ship")
            st.stop()
        st.write("Selected components to ship")
        stTrx.DebugOutput("uploaded components",pageDict['df_data'])
        colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','currentLocation_code','assembled','cts']
        df_input=pd.DataFrame(pageDict['df_data'][colz])
        st.write("length of dataframe:",len(df_input.index))

        nonOverlap= set(df_input['serialNumber'].to_list()) ^ set(pageDict['df_data']['serialNumber'].to_list())
        if len(nonOverlap)<1:
            st.write("__All components addeed to shipment__ :)")
        else:
            st.write(f"__The following components could not be added to shipment__.\n - Please check registered status or currentLocation.")
            st.write(list(nonOverlap))


        infra.TextBox(pageDict,'shipName',"Name of shipment:")

        shipObj={'name':pageDict['shipName'],'sender':pageDict['sender']['code'],'recipient':pageDict['recipient']['code'],'type': next(item for item in shipOpts if item['name'] == pageDict['type'])['code'],'shipmentItems':list(pageDict['df_data']['code'].values)}

        infra.ToggleButton(pageDict,'moreData',"Add tracking information")
        if pageDict['moreData']:
            infra.TextBox(pageDict,'trackNum',"Tracking number:")
            infra.TextBox(pageDict,'shipService',"Shipping service:")
            shipObj['trackingNumber']=pageDict['trackNum']
            shipObj['shippingService']=pageDict['shipService']

        stTrx.DebugOutput("Shipping schema for upload...",shipObj)

        # access database
        if st.button("Create Shipping!"):
            ### create shipment
            try:
                pageDict['retVal']=st.session_state.myClient.post('createShipment', json=shipObj)
                try:
                    st.write("### **Successful shipment created** for:",pageDict['retVal']['name'])
                except:
                    st.write("### **Successful shipment, but don't understand return object")
                st.balloons()
                st.write(pageDict['retVal'])
            except itkX.BadRequest as b:
                st.write("### :no_entry_sign: Shipment setting **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

        if 'retVal' in pageDict.keys():
            ### stop if upload return object is empty
            if pageDict['retVal']==None:
                st.stop()
            ### update shipment state
            st.write("Set shipment ("+pageDict['retVal']['name']+") to *inTransit*")
            if st.button("Set to inTransit"):
                try:
                    pageDict['upVal']=st.session_state.myClient.post('setShipmentStatus', json={'shipment':pageDict['retVal']['id'],'status':"inTransit"})
                    st.write("### **Successful Update**:",pageDict['upVal']['name'])
                    st.balloons()
                    st.write(pageDict['upVal'])
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: Update **Unsuccessful**")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
            try:
                st.write("last successful _upload_:",pageDict['retVal']['name'])
                ### delete shipment?
                pdbTrx.DeleteCoTeSt(pageDict,'delShip','deleteShipment',pageDict['retVal']['name'])
            except KeyError:
                pass
        if 'delVal' in pageDict.keys():
            try:
                st.write("last successful _deletion_:",pageDict['delVal']['name'])
            except KeyError:
                st.write("return object has not _name_ key")
            except TypeError:
                st.write("return object not dictionary")
