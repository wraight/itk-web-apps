### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import altair as alt
import cv2
import numpy as np
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.StreamlitTricks as stTrx
import commonCode.PDBTricks as pdbTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
#####################
def SpecialString(row,c,retC):
    if row[c]==True or row[c]=="True":
        try:
            return row[retC]+","+c
        except TypeError:
            return c
    else:
        return row[retC]

def HighlightStage(row):
    ret = ["" for _ in row.index]
    compKey="compCheck"
    colKey="code"
    if row[compKey] == True or str(row[compKey]).lower() == "true":
        ret[row.index.get_loc(colKey)] = "background-color: lightblue"
    elif str(row[compKey]).lower() == "current":
        ret[row.index.get_loc(colKey)] = "background-color: green"
    else:
        pass
    return ret

def GetTT(x):
    try:
        retVal=myClient.get('getTestType', json={'id':x})
        # st.write("-->",retVal['code'])
    except RecursionError:
        pass
    return retVal['code']

def HighlightTest(row):
    ret = ["" for _ in row.index]
    for k,v in row.items():
        if "passed" not in k:
            continue
        if row[k] == True or str(row[k]).lower() == "true":
            ret[row.index.get_loc(k.replace('passed','test'))] = "background-color: green"
        elif row[k] == False or str(row[k]).lower() == "false":
            ret[row.index.get_loc(k.replace('passed','test'))] = "background-color: red"
        else:
            pass
    return ret

infoList=["  * select *component*",
        "  * get stage and test history"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("Stage Check", ":fast_forward: Stage Check", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        st.write("## Select Component")

        chnx.SelectComponentChunk(pageDict)

        if 'comp' not in pageDict.keys():
            st.write("Enter identifier to get component information")
            st.stop()

        stTrx.DebugOutput("ComponentJson:",pageDict['comp'])
        if pageDict['comp']==None:
            st.write("No component information found. Check inputs")
            st.stop()

        st.write("---")

        compInfo= st.session_state.myClient.get('getComponent', json={'component':pageDict['comp']['code']})


        ########################
        ### quick summary
        ########################
        st.write("## Quick Summary")
        st.write("__serialNumber (altID):__",compInfo['serialNumber'],"("+str(compInfo['alternativeIdentifier'])+")")
        st.write("__project:__",compInfo['project']['name'])
        st.write("__componentType (type):__",compInfo['componentType']['code'],"("+compInfo['type']['code']+")")
        st.write("__currentStage:__",compInfo['currentStage']['code'])
        st.write("__origin:__",compInfo['institution']['code'])
        st.write("__currentLocation:__",compInfo['currentLocation']['code'])
        try:
            st.write("__No. stages:__",len(compInfo['stages']))
        except TypeError:
            st.write("__No. stages:__",None)
        try:
            st.write("__No. tests:__",len(compInfo['tests']))
        except TypeError:
            st.write("__No. tests:__",None)

        st.write("---")
        ########################
        ### Extract stage data
        ########################
        st.write("## Stages")

        df_stages=pd.DataFrame(compInfo['stages'])
        df_stages['date']= pd.to_datetime(df_stages['dateTime'],format='%Y-%m-%dT%H:%M:%S.%f')
        df_stages

        stageChart=alt.Chart(df_stages).mark_circle(size=60).encode(
            x=alt.X('date:T',axis = alt.Axis(title = "Date", format = ("%b %Y"))),
        #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
            y=alt.Y('code:N',title="Stage"),
            tooltip=['date:T','code:N']
            ).properties(width=600, height=300, title="Stage Timeline")
        st.altair_chart(stageChart)


        downloadName="StageList"
        try:
            if compInfo['serialNumber']!=None:
                downloadName+="_"+compInfo['serialNumber']
        except:
            pass
        st.download_button(label="Download stage list?", data=df_stages.to_csv(index=False),file_name=downloadName)

        ########################
        ### make stage check list
        ########################
        ### get compType schema
        compTypeInfo=st.session_state.myClient.get('getComponentTypeByCode', json={'project':compInfo['project']['code'],'code':compInfo['componentType']['code']})
        stageOrderList = [x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]


        def SpecialString(row,c,retC):
            if row[c]==True or row[c]=="True":
                try:
                    return row[retC]+","+c
                except TypeError:
                    return c
            else:
                return row[retC]
        ### stage check list
        df_scl=pd.DataFrame(compTypeInfo['stages'])[['code','name','order','final','initial']]
        df_scl['special']=None
        for c in ['final','initial']:
            df_scl['special']=df_scl.apply(lambda row: SpecialString(row,c,"special"), axis=1)
        df_scl=df_scl.drop(columns=['initial','final']).reset_index(drop=True)
        df_scl['compCheck']="False"
        df_scl['compCheck']=df_scl.apply(lambda row: "True" if row['code'] in df_stages['code'].to_list() else "False", axis=1)
        # note current stage
        rowInd=df_scl.index[df_scl['code'] == compInfo['currentStage']['code']].tolist()[0]
        df_scl.at[rowInd,'compCheck']="current"
        df_scl['date']=None
        df_scl['date']=df_scl.apply(lambda row: df_stages.query('code=="'+row['code']+'"')['date'].values[0] if row['code'] in df_stages['code'].to_list() else None, axis=1)
        # df_scl

        # highlight
        df_scl_col=df_scl.drop(columns=['name'],axis=1).style.apply(HighlightStage, axis=1)
        # df_scl_col=df_scl.style.apply(HighlightStage, axis=1).hide(['compCheck','name'])
        # visualise
        st.write("### Stage check list")
        colStr="<font color='blue'>previous stage<font>, <font color='green'>current stage<font>"
        st.markdown(colStr, unsafe_allow_html=True)
        st.table(df_scl_col.hide_columns(subset=['compCheck']))

        st.write("---")

        ########################
        ### Extract test data
        ########################
        st.write("## Tests")
        df_tests=pd.DataFrame(compInfo['tests'])
        try:
            df_tests=df_tests.explode('testRuns')
            for c in ['date','passed']:
                df_tests[c]=df_tests['testRuns'].apply(lambda x: x[c])
            df_tests['institution']=df_tests['testRuns'].apply(lambda x: x['institution']['code'])
            df_tests['runID']=df_tests['testRuns'].apply(lambda x: x['id'])

            formatList=['%Y-%m-%dT%H:%M:%S.%f','%Y-%m-%d','%Y.%m.%d']
            for fl in formatList:
                try:
                    df_tests['date']= pd.to_datetime(df_tests['date'],format=fl)
                    break
                except ValueError:
                    pass

            df_tests=df_tests.drop(columns=['testRuns','id'])
            # display(df_tests.head(20))
        except KeyError:
            st.write("__No test data :(__")
            st.stop()

        ### get filtered testRun data
        runInfoList=st.session_state.myClient.get('getTestRunBulk', json={'testRun':df_tests['runID'].to_list()})
        # st.write(sorted(runInfoList[0].keys()))
        ## organise filtered testRun data
        df_runInfoList=pd.json_normalize(runInfoList, sep = "_")
        df_runInfoList=df_runInfoList.explode('components')
        df_runInfoList['stage']=df_runInfoList['components'].apply(lambda x: x['testedAtStage']['code'])
        df_runInfoList['testCode']=df_runInfoList['id'].apply(lambda x: df_tests.query('runID=="'+x+'"')['code'].values[0])
        # display(df_runInfoList[['id','state','date','passed','problems','stage','testCode']])

        ### stage check list
        df_cti=pd.DataFrame(compTypeInfo['stages'])
        df_tcl_comb=None
        # display(df_cti)
        for stgCode in df_cti['code'].unique():
            # st.write("working on:",stgCode)
            df_tcl=df_cti.query('code=="'+stgCode+'"').copy(deep=True).reset_index(drop=True)
            df_tcl=df_tcl.explode('testTypes').reset_index(drop=True) #.explode('testTypes')
        #     display(df_tcl)
            df_tcl['testCode']=None
            for i,row in df_tcl.iterrows():
        #         print(i,row['testTypes']['testType'])
                try:
                    retVal=st.session_state.myClient.get('getTestType', json={'id':row['testTypes']['testType']['id']})
                    df_tcl.at[i,'testCode']=retVal['code']
                except TypeError:
                    pass
        #     df_tcl=df_tcl['testCode']=df_tcl['testTypes'].apply(lambda x: GetTT(x['testType']) if x!=None else x)
            for c in ['order','receptionTest','receptionTestOnly','nextStage']:
                try:
                    df_tcl[c]=df_tcl['testTypes'].apply(lambda x: x[c])
                except TypeError:
                    df_tcl[c]=None
            df_tcl['special']=None
            for c in ['alternative','final','initial']:
                df_tcl['special']=df_tcl.apply(lambda row: SpecialString(row,c,"special"), axis=1)
            df_tcl=df_tcl.drop(columns=['alternative','final','initial'])
            df_tcl['special2']=None
            for c in ['receptionTest','receptionTestOnly','nextStage']:
                df_tcl['special2']=df_tcl.apply(lambda row: SpecialString(row,c,"special2"), axis=1)
            df_tcl['compCheck']=False
            df_tcl['compCheck']=df_tcl['testCode'].apply(lambda x: True if x in df_runInfoList.query('stage=="'+stgCode+'"')['testCode'].to_list() else False)
            for c in ['passed','date']:
                df_tcl[c]=None
                df_tcl[c]=df_tcl.apply(lambda row: df_runInfoList.query('stage=="'+stgCode+'" & testCode=="'+row['testCode']+'"')[c].values[0] if row['compCheck']==True else None, axis=1)
            df_tcl=df_tcl.drop(columns=['receptionTest','receptionTestOnly','nextStage','testTypes']).reset_index(drop=True)
            try:
                df_tcl_comb=pd.concat([df_tcl_comb,df_tcl])
            except TypeError:
                df_tcl_comb=df_tcl.copy(deep=True)
            #     display(df_tcl)
            # df_tcl=df_tcl[['testCode','order','code','special','special2','compCheck','date','passed']].rename(columns={'code':"stageCode"})
            # df_tcl_col=df_tcl.style.applymap(highlight_match, matchVal=True, color_if_true='green', color_if_false=None, subset=['compCheck'])
            # display(df_tcl_col)

        ### combined test info.
        df_tcl_time=df_tcl_comb.query('compCheck==True').reset_index(drop=True)

        formatList=['%Y-%m-%dT%H:%M:%S.%f','%Y-%m-%d','%Y.%m.%d']
        for fl in formatList:
            try:
                df_tcl_time['date']= pd.to_datetime(df_tcl_time['date'],format=fl)
                break
            except ValueError:
                pass

        df_tcl_time=df_tcl_time[['testCode','order','code','special','special2','compCheck','date','passed']].rename(columns={'code':"stageCode"})
        # df_tcl_time

        testChart=alt.Chart(df_tcl_time).mark_circle(size=60).encode(
            x=alt.X('date:T',axis = alt.Axis(title = "Date", format = ("%b %Y"))),
        #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
            y=alt.Y('stageCode:N',title="Stage", sort=stageOrderList),
            color=alt.Color('testCode:N',title="Test"),
            shape=alt.Shape('passed:N'),
            tooltip=['date:T','stageCode:N','testCode:N','passed:N']
            ).properties(width=800, height=300, title="Test Timeline")
        st.altair_chart(testChart)

        downloadName="testList"
        try:
            if compInfo['serialNumber']!=None:
                downloadName+="_"+compInfo['serialNumber']
        except:
            pass
        st.download_button(label="Download test list?", data=df_tcl_time.to_csv(index=False),file_name=downloadName)

        ########################
        ### make stage check list
        ########################
        sumList=[]
        for i,row in df_tcl_comb.iterrows():
            val=next((item for item in sumList if item['stage']==row['code']),None)
            if val==None:
                sumList.append({'stage':row['code'],"test"+str(row['order']):row['testCode'],"passed"+str(row['order']):row['passed']})
            else:
                sumList[sumList.index(val)]["test"+str(row['order'])]=row['testCode']
                sumList[sumList.index(val)]["passed"+str(row['order'])]=row['passed']

        df_sum=pd.DataFrame(sumList)

        # df_sum=df_sum.dropna(how='all', axis=1, inplace=True)
        df_sum=df_sum.dropna(axis=1, how='all')
        df_sum=df_sum[['stage']+sorted([c for c in df_sum.columns if "test" in c])+sorted([c for c in df_sum.columns if "passed" in c])]
        # display(df_sum)
        df_sum_col=df_sum.style.apply(HighlightTest, axis=1).hide_columns(subset=[c for c in df_sum.columns if "passed" in c])
        st.write("### Test check list")
        colStr="<font color='green'>test passed<font>, <font color='red'>test failed<font>"
        st.markdown(colStr, unsafe_allow_html=True)
        st.table(df_sum_col)

