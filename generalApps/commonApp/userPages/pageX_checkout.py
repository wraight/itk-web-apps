### standard
import datetime
import os
from io import StringIO
import io
import json
import altair as alt
import core.stInfrastructure as infra
### PDB stuff
import numpy as np
### custom
import pandas as pd
import streamlit as st
import itkdb
import itkdb.exceptions as itkX
from core.Page import Page

import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
from commonCode.PDBTricks import TimeStampConverter

### analyses

#####################
### useful functions
#####################

infoList=[" * select selected component",
        " * get status"]
#####################
### main part
#####################

class PageX(Page):
    def __init__(self):
        super().__init__("Checkout", "Check Status of Components", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()
        

        pageDict['project']=st.session_state.Authenticate['proj']['code']

        ### Select mode
        st.write("## Select Components")
        sel_mode=st.radio("Get information for",["Single","Multiple"])
        
        # single identifier
        if "sing" in sel_mode.lower():

            st.write("### Select Single Component")
            chnx.SelectComponentChunk(pageDict)

            df_input= pd.DataFrame([pageDict['comp']])
        
        # multiple identifiers
        elif "multi" in sel_mode.lower():

            st.write("### Input Component Identifiers")

            # review componentTypes and stages from file
            filePath=os.path.realpath(__file__)
            exampleFileName="CheckoutExample.csv"
            df_input=chnx.UploadFileChunk(pageDict,['csv','xls'],filePath[:filePath.rfind('/')]+"/"+exampleFileName,"Sheet1")

        # catch other
        else:
            st.write("Select Cluster or Institute")
            st.stop()

        ## check input option
        if st.checkbox("Check input data?"):
            st.dataframe(df_input)


        ### show information
        st.write("---")
        st.write("## Checkout Information")
        
        st.write(f"### Looping over {len(df_input.index)} components")
        ### loop over components
        for e,sn in enumerate(df_input['serialNumber'].to_list(),1):
            st.write(f"#### {e}: __{sn}__")

            chnx.StageCheck({'dummy':{'component':sn}},"dummy")

