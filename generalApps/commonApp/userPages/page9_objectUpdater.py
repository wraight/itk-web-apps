### standard
import datetime
import os
from io import StringIO
import io
import json
import altair as alt
import core.stInfrastructure as infra
### PDB stuff
import numpy as np
### custom
import pandas as pd
import streamlit as st
import itkdb
import itkdb.exceptions as itkX
from core.Page import Page

import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
from commonCode.PDBTricks import TimeStampConverter

### analyses

#####################
### useful functions
#####################

def InputValue(myObj,myCode):

    objDict= next((item for item in myObj if item['code'] == myCode), None)
    if objDict==None:
        st.write(f"Cannot find code: {myCode}.\nNone returned")
        return None
    st.write(f"Return value for {myCode}")

    if "dataType" not in objDict.keys():
        st.write(f"Cannot assess datatype.\n\t - keys: {objDict.keys()}.\nNone returned")
        return None

    if objDict['dataType'].lower()=="codetable":
        retVal=st.selectbox("Select from code table", objDict['codeTable'], format_func=lambda x: x['name'] if type(x)==type({}) and "name" in x.keys() else x)
        retVal=retVal['code']
    
    elif objDict['dataType'].lower()=="boolean":
        retVal=st.radio("Select binary", [True, False])
    
    elif objDict['dataType'].lower()=="string":
        retVal=st.text_input("Input info.")
    
    elif objDict['dataType'].lower()=="datetime":
        if st.checkbox("Input date manually?"):
            st.write("Please use format: yyyy-mm-ddThh:mm:ssZ")
            retVal=st.text_input("Input date (%Y-%m-%dT%H:%M:%SZ):")
        else:
            retVal=st.date_input("Select date:", datetime.datetime.now().date())
        retVal=TimeStampConverter(retVal.strftime("%Y-%m-%dT00:00:00Z"),"%Y-%m-%dT%H:%M:%SZ") 
        # st.write(f"converted value: {retVal}")
    else:
        retVal= st.number_input("Input value for "+myCode)
        
    return retVal


infoList=[" * select selected component or testRun",
        " * select update type",
        " * select parameter to update",
        " * set value",
        " * update"]
#####################
### main part
#####################

class Page9(Page):
    def __init__(self):
        super().__init__("Object Update", "Update Components and TestRuns", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        pageDict['projectCode']=st.session_state.Authenticate['proj']['code']
        pageDict['institutionCode']=st.session_state.Authenticate['inst']['code']
        
        ##############
        ### select object to edit
        ##############
        st.write("## Get PDB Object")
        
        ##############
        ### select object type: component or testRun
        ##############
        objList=['component', 'testRun']
        objSel=st.radio("select object type:", objList)

        st.write("---")
        st.write(f"### Identify {objSel} object in database to update")
        
        ##############
        ### select identification method
        ##############
        methodList=['input identifier', 'select from lists']
        methodSel=st.radio("select function:", methodList)

        if 'identifier' in methodSel:
            objId=st.text_input(f"Input {objSel} identifier:")
            if type(objId)==type(None) or len(objId)<1: #, value="20USBRT0731003")  
                st.write("Please input identifier")
                st.stop()
            
        elif 'list' in methodSel:
            ### get list of components
            if "compList" not in pageDict.keys() or st.button("reset component list"):
                # later migration to filterMap
                # filterMap={'project':pageDict['projectCode'], 'currentLocation':[pageDict['institutionCode']], "state":"ready"}
                # pageDict['compList']=st.session_state.myClient.get('listComponents', json={'filterMap':filterMap}, True)
                pageDict['compList']=st.session_state.myClient.get('listComponents', json={'project':pageDict['projectCode'], 'currentLocation':pageDict['institutionCode']})
                if type(pageDict['compList'])!=type([]):
                    pageDict['compList']=pageDict['compList'].data
            else:
                st.write(f"Got component list for {pageDict['institutionCode']} ({pageDict['projectCode']})")

            compTypeSel=st.selectbox("Select component type:", set([ct['componentType']['code'] for ct in pageDict['compList'] if type(ct['componentType'])==type({}) and "code" in ct['componentType']]))
            comp_subList=[ct['serialNumber'] for ct in pageDict['compList'] if type(ct)==type({}) if type(ct['componentType'])==type({}) and compTypeSel==ct['componentType']['code'] and ct['serialNumber']!=None]
            st.write(f"- found {len(comp_subList)} components")
            compId=st.selectbox("Select component:", comp_subList )

            if "component" in objSel.lower():
                objId=compId

            ### testRun list
            if "testrun" in objSel.lower():
                ### get component
                if "compObj" not in pageDict.keys() or pageDict['compObj']['serialNumber']!=compId:
                    pageDict['compObj']=st.session_state.myClient.get('getComponent', json={'component': compId, 'noEosToken':False})
                st.write("### Select testRun")
                df_tests=pd.DataFrame(pageDict['compObj']['tests'])
                try:
                    df_tests=df_tests.explode('testRuns').reset_index()
                except KeyError:
                    st.write("No testRuns found")
                    st.stop()
                colNames=['id','date','state']
                for col in colNames:
                    df_tests['testRun_'+col]=df_tests['testRuns'].apply(lambda x: x[col])
                st.write("Tests:",df_tests[['id','code']+['testRun_'+col for col in colNames]])
                objId=st.selectbox("Select testRun (by test_id):", df_tests['testRun_id'].to_list())

        else:
            st.write("select input method")

        ### automatic object update (missing or empty) or on demand
        if "component" in objSel.lower():

            if "retObj" not in pageDict.keys() or st.button(f"reset {objSel}"):
                # st.write("prompt update")
                retObj=st.session_state.myClient.get('getComponent', json={'component': objId, 'noEosToken':False})
                pageDict['retObj']=retObj
            elif type(pageDict['retObj'])==type({}) and "serialNumber" in pageDict['retObj'].keys() and pageDict['retObj']['serialNumber']!=objId:
                # st.write("mismatch update")
                retObj=st.session_state.myClient.get('getComponent', json={'component': objId, 'noEosToken':False})
                pageDict['retObj']=retObj
            elif pageDict['retObj']==None:
                # st.write("None update")
                retObj=st.session_state.myClient.get('getComponent', json={'component': objId, 'noEosToken':False})
                pageDict['retObj']=retObj
            else:
                pass

            ### catch nothing selected
            if "retObj" not in pageDict.keys():
                st.write("Input component info.")
                st.stop()

            ### announce component and offer full info.
            if pageDict['retObj']==None or type(pageDict['retObj'])!=type({}):
                st.write("Returned component doesn't look right :(")
                st.write(" - if _Granting token from EoS failed_ remove broken EOS links and try again")
                st.write(pageDict['retObj'])
                st.stop()

            ### announce component and offer full info.
            st.write(f"__Got {pageDict['retObj']['serialNumber']}__ ({pageDict['retObj']['code']})")
            check_comp=st.checkbox("See full component object")
            if check_comp:
                st.write("Full object")
                st.write(pageDict['retObj'])
                # set cache for tests later
                pageDict['compObj']=pageDict['retObj']

        ### automatic update (missing or empty) or on demand
        if "testrun" in objSel.lower():

            if "retObj" not in pageDict.keys() or st.button(f"reset {objSel}"):
                # st.write("prompt update")
                retObj=st.session_state.myClient.get('getTestRun', json={'testRun': objId, 'noEosToken':False})
                pageDict['retObj']=retObj
            elif type(pageDict['retObj'])==type({}) and "id" in pageDict['retObj'].keys() and pageDict['retObj']['id']!=objId:
                # st.write("mismatch update")
                retObj=st.session_state.myClient.get('getTestRun', json={'testRun': objId, 'noEosToken':False})
                pageDict['retObj']=retObj
            elif pageDict['retObj']==None:
                # st.write("None update")
                retObj=st.session_state.myClient.get('getTestRun', json={'testRun': objId, 'noEosToken':False})
                pageDict['retObj']=retObj
            else:
                pass

            # st.write(df_tests.query(f'testRun_id=="{selTestRun}"'))

        ##############
        ### Get Things to Update
        ##############
        st.write("---")
        st.write("### Object to Update")

        if "upObj" not in pageDict.keys():
            pageDict['upObj']={}

        if st.checkbox("See full object?"):
            st.write(pageDict['retObj'])

        if objSel=="component":
            st.write(f"__Got {pageDict['retObj']['id']}__ ({pageDict['retObj']['componentType']['code']})")
            # set component to update if necessary
            if "code" not in pageDict['upObj'].keys() or pageDict['upObj']['code']!=pageDict['retObj']['code']:
                pageDict['upObj']=pageDict['retObj']
            upDict={'component':pageDict['upObj']['code']}
            upOpts=[["set","Component","Property"],["update","Component","Comment"]]
        
        elif objSel=="testRun":
            st.write(f"__Got {pageDict['retObj']['id']}__ ({pageDict['retObj']['testType']['code']})")
            ### set testRun to update if necessary
            if "id" not in pageDict['upObj'].keys() or pageDict['upObj']['id']!=pageDict['retObj']['id']:
                pageDict['upObj']=pageDict['retObj']
                # pageDict['upObj']=st.session_state.myClient.get('getTestRun', json={'testRun': pageDict['retObj']['id']})
            upDict={'testRun':pageDict['upObj']['id']}
            upOpts=[["set","TestRun","Property"],["set","Test","Run","Parameter"],["update","Test","Run","Comment"],["update","Test","Run","Defect"]]

        st.write(f"{objSel}")
        st.write(pd.DataFrame([pageDict['upObj']]).transpose())

        st.write("---")
        st.write("### Select parameter")
        upSel=st.radio("Select thing to update:", upOpts, format_func=lambda x: " ".join([y.title() for y in x]))

        if "Property" in upSel:
            st.write("__Current Values__")
            df_cur=pd.DataFrame(pageDict['upObj']['properties'])
            if df_cur.empty:
                st.write("No current values")
                st.stop()
            else:
                st.write(df_cur)
            upKey=st.selectbox("Select property to update:", pageDict['upObj']['properties'], format_func=lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
            st.write(f"Current {upKey['code']} value: {upKey['value']}")
            upVal=InputValue(pageDict['upObj']['properties'],upKey['code'])
            upDict['code']=upKey['code']
            upDict['value']=upVal
        elif "Parameter" in upSel:
            st.write("__Current Values__")
            df_cur=pd.DataFrame(pageDict['upObj']['results'])
            if df_cur.empty:
                st.write("No current values")
                st.stop()
            else:
                st.write(df_cur)
            upKey=st.selectbox("Select parameter to update:", pageDict['upObj']['results'], format_func=lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
            st.write(f"Current {upKey['code']} value: {upKey['value']}")
            upVal=InputValue(pageDict['upObj']['results'],upKey['code'])
            upDict['code']=upKey['code']
            upDict['value']=upVal
        elif "Comment" in upSel:
            st.write("__Current Values__")
            df_cur=pd.DataFrame(pageDict['upObj']['comments'])
            if df_cur.empty:
                st.write("No current values")
                st.stop()
            else:
                st.write(df_cur)
            upKey=st.selectbox("Select comment to update:", pageDict['upObj']['comments'], format_func=lambda x: x['comment'] if type(x)==type({}) and "comment" in x.keys() else x)
            upVal=st.text_input('Input comment:',value=upKey['comment'])
            upDict['code']=upKey['code']
            upDict['value']=upVal
        elif "Defect" in upSel:
            st.write("__Current Values__")
            df_cur=pd.DataFrame(pageDict['upObj']['defects'])
            if df_cur.empty:
                st.write("No current values")
                st.stop()
            else:
                st.write(df_cur)
            st.write("Update _defect_ ...")
            st.write("__This needs added__")
        else:
            st.write("Please select")


        #########################
        ### Upload to PDB
        #########################
        cmdStr=''.join(upSel)
        st.write(f"### Ready to {cmdStr}?")
        st.write(upDict)
        if st.button("Update Object"):
            try:
                retObj=st.session_state.myClient.post(cmdStr, json=upDict)
                stTrx.Flourish()
                st.write("### 🎈 Upload **Successful**")
            except itkX.BadRequest as b:
                st.write("### :no_entry_sign: Upload **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks


