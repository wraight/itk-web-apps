### standard
import streamlit as st
import streamlit.components.v1 as components
from core.Page import Page
### custom
import os
import pandas as pd
import cv2
import numpy as np
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
### analyses
import qrcode 

#####################
### useful functions
#####################
def generate(data=None, img_name="qr.png"):
    if data==None:
        st.write("__Please enter text__")
        return None
    img = qrcode.make(data) #generate QRcode 
    img.save(os.getcwd()+"/"+img_name)
    return os.getcwd()+"/"+img_name  

def read_qr(file_obj):
    bytes_data = file_obj.getvalue()
    cv2_img = cv2.imdecode(np.frombuffer(bytes_data, np.uint8), cv2.IMREAD_COLOR)
    detector = cv2.QRCodeDetector()
    data_raw, bbox, straight_qrcode = detector.detectAndDecode(cv2_img)
    return data_raw
    
infoList=[" * check component sub-type",
        " * upload component schemas to PDB",
        " * delete component from PDB if required"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("QR", ":microscope: Embedded visualisations", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ### mode
        st.write("## Select mode")
        mode=st.radio("What to do?",['Generate QR','Read QR'])

        if "generate" in mode.lower():
            st.write("### Generate QR code")

            in_text=st.text_input("Text to encode:")

            st.image(generate(in_text),"Right-click and 'copy image' to download")

        elif "read" in mode.lower():
            st.write("### Read QR code")

            in_file=st.file_uploader('Upload image file', type=['png','jpg','jpeg'])
            
            if in_file==None:
                st.write("__Please upload file__")
            else:
                # st.write(in_file.__dir__())
                st.write(read_qr(in_file)) 


        else:

            st.write("Dunno what to do")



        
