""" cookiecutter itk-web-apps page template """
# standard imports
import ast
import csv

# other imports
import altair as alt
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import streamlit as st

# standard page class
from core.Page import Page

"""
#####################
### useful stuff (page specific)
#####################
"""

testMapDict={'VISUAL_INSPECTION':{'meta':['passed']},'VISUAL_INSPECTION_RECEPTION':{'meta':['passed']},'MODULE_IV_PS_V1':{'defaults':{'valueType':"array",'xKey':"VOLTAGE",'yKey':"CURRENT"}}}

def CheckAndSelect(xKey, pageDict, opts, default=None):
    ### get default
    if xKey not in pageDict.keys():
        try:
            pageDict[xKey]=default
        except KeyError:
            pass
    infra.SelectBox(pageDict, xKey, opts, "Select "+xKey)


infoList=[" * input component ASN",
        " * Get all uploaded tests from all stages",
        " * Get common tests",
        " * Get data --> plots",
        " * Overlay plots (colour by stage)"]
"""
#####################
### main part
#####################
"""


class Page1(Page):
    """page content"""

    def __init__(self):
        """general page structure"""
        super().__init__(
            "singlePlotter",
            ":microscope: :microscope: Single Component Plots",
            infoList,
        )

    def main(self):
        """fill page with content"""
        super().main()

        # reference session state attribute as page dictionary
        pageDict = st.session_state[self.name]

        # check API client exists
        doWork = False
        try:
            if st.session_state.myClient:
                doWork = True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        # gatekeeping: stop if no client
        if not doWork:
            st.stop()

        #
        # Page specific code starts here
        #

        st.write("## Inputs")

        infra.TextBox(pageDict,'inComp',"Enter component identifier")
        infra.Radio(pageDict,'altID',['serialNumber','alternativeIdentifier'],"Type of identifier")

        if pageDict['inComp']==None or len(pageDict['inComp'])<1:
            st.write("Enter identifier to get component information")
            st.stop()

        if 'comp' not in pageDict.keys() or st.button("Get component"):
            compQueryJson={'component':pageDict['inComp'],'type':"SN"}
            if pageDict['altID']=='alternativeIdentifier':
                compQueryJson['type']='altID'
            stTrx.DebugOutput("queryJson:",compQueryJson)

            pageDict['comp']=pdbTrx.GetComponentByIdentifier(compQueryJson['component'],compQueryJson['type'])

        if 'comp' not in pageDict.keys():
            st.write("Enter identifier to get component information")
            st.stop()

        stTrx.DebugOutput("ComponentJson:",pageDict['comp'])
        if pageDict['comp']==None:
            st.write("No component information found. Check inputs")
            st.stop()

        st.write("### Stages")
        st.write("(colour--> _",'code',"_)")
        df_stages=pd.json_normalize(pageDict['comp']['stages'], sep = "_")
        stageColz=['code','name','dateTime','rework','comment']
        st.dataframe(df_stages[stageColz].style.apply(stTrx.ColourCells, df=df_stages[stageColz], colName='code', flip=True, axis=1))

        st.write("### Tests")
        st.write("(colour--> _",'code',"_)")
        df_tests=pd.json_normalize(pageDict['comp']['tests'], sep = "_")
        testColz=['code','name','testRuns','id']
        st.dataframe(df_tests[testColz].style.apply(stTrx.ColourCells, df=df_tests[testColz], colName='code', flip=True, axis=1))

        ### select testType
        infra.SelectBox(pageDict,'testType',df_tests['code'].unique(),"Select a testType:")
        ### get common tests
        df_selTest=df_tests.query('code=="'+pageDict['testType']+'"')
        # st.dataframe(df_selTest)

        lst_col = 'testRuns'
        df_new=pd.DataFrame({
            col:np.repeat(df_selTest[col].values, df_selTest[lst_col].str.len())
            for col in df_selTest.columns.difference([lst_col])
            }).assign(**{lst_col:np.concatenate(df_selTest[lst_col].values)})[df_selTest.columns.tolist()]

        df_newer= pd.json_normalize(df_new[lst_col], sep = "_")

        if st.checkbox("Remove deleted tests form list?"):
            df_newer=df_newer.query('state!="deleted"').reset_index(drop=True)
        st.write("### "+pageDict['testType']+" tests")
        st.write("(colour--> _",'passed',"_)")
        st.dataframe(df_newer.style.apply(stTrx.ColourCells, df=df_newer, colName='passed', flip=True, axis=1))
        st.write(" - found tests:",len(df_newer.index))
        # st.write(df_newer['id'].tolist(), expanded=False)
        try:
            for x in testMapDict[pageDict['testType']]['meta']:
                st.write(df_newer[['cts','runNumber',x]])
        except KeyError:
            st.write("no comparison list for test")

        # get test data
        if "testRunList" not in pageDict.keys() or st.button("Get testRuns"):
            try:
                pageDict['testRunList']=st.session_state.myClient.get('getTestRunBulk', json={'testRun':df_newer['id'].tolist()} )
            except itkX.BadRequest as b:
                st.write("### :no_entry_sign: get component **Unsuccessful** for",d)
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

        ### get axes
        df_data=None
        for e,tr in enumerate(pageDict['testRunList']):
            #df_testData=pd.json_normalize(testRunList, sep = "_")
            ### properties
            # st.write(tr['properties'])#
            df_props=pd.DataFrame(tr['properties'])
            stTrx.Stringify(df_props)
            ### results
            # st.write(tr['results'])#
            df_res=pd.DataFrame(tr['results'])
            stTrx.Stringify(df_res)
            stTrx.DebugOutput("Properties:",df_props)
            stTrx.DebugOutput("Results:",df_props)

            st.write(f"TestRun ({e}):",tr['id'])
            if e==0:
                st.dataframe(df_res[['code','valueType']].style.apply(stTrx.ColourCells, df=df_res[['code','valueType']], colName='valueType', flip=True, axis=1))
                try:
                    CheckAndSelect('valueType', pageDict, df_res['valueType'].unique(), testMapDict[pageDict['testType']]['defaults']['valueType'])
                except KeyError: # no defaults
                    CheckAndSelect('valueType', pageDict, df_res['valueType'].unique())
                df_res_vt=df_res.query('valueType=="'+pageDict['valueType']+'"')
                try:
                    CheckAndSelect('xKey', pageDict, df_res_vt['code'].to_list(), testMapDict[pageDict['testType']]['defaults']['xKey'])
                except KeyError: # no defaults
                    CheckAndSelect('xKey', pageDict, df_res_vt['code'].to_list())
                try:
                    CheckAndSelect('yKey', pageDict, df_res_vt['code'].to_list(), testMapDict[pageDict['testType']]['defaults']['yKey'])
                except KeyError: # no defaults
                    CheckAndSelect('yKey', pageDict, df_res_vt['code'].to_list())
                xVals=df_res.query('code=="'+pageDict['xKey']+'"')['value'].values[0].replace('[','').replace(']','').split(',')
                yVals=df_res.query('code=="'+pageDict['yKey']+'"')['value'].values[0].replace('[','').replace(']','').split(',')
                # st.write(xVals)
                # st.write(yVals)
                df_data=pd.DataFrame({pageDict['xKey']:xVals,pageDict['yKey']:yVals})
                df_data['iteration']=e
                df_data['cts']=tr['cts']
            else:
                # st.write("pre x:",df_res.query('code=="'+pageDict['xKey']+'"')['value'].values[0])
                # st.write("pre y:",df_res.query('code=="'+pageDict['yKey']+'"')['value'].values[0])
                xVals=df_res.query('code=="'+pageDict['xKey']+'"')['value'].values[0].replace('[','').replace(']','').split(',')
                yVals=df_res.query('code=="'+pageDict['yKey']+'"')['value'].values[0].replace('[','').replace(']','').split(',')
                # st.write("xVals:",xVals)
                # st.write("yVals:",yVals)
                if len(xVals)==len(yVals):
                    st.write(" - matching data array lengths :white_check_mark:")
                else:
                    st.write(f" - mismatching data array lengths, x: {len(xVals)}, y: {len(yVals)}")
                    st.write(" - __skipping__")
                    continue
                    # st.stop()
                
                df_temp=pd.DataFrame({pageDict['xKey']:xVals,pageDict['yKey']:yVals})
                df_temp['iteration']=e
                df_temp['cts']=tr['cts']
                #df_data.assign(yKey+str(e)=df_res[pageDict['yKey']].to_list())
                df_data=pd.concat([df_data, df_temp])
                pass
        try:
            if df_data.empty:
                st.write("No further data for testRuns")
                st.stop()
        except AttributeError:
            if df_data==None:
                st.write("No further data for testRuns")
                st.stop()
        st.write("### Data found :white_check_mark:")
        try:
            df_data['cts']=df_data['cts'].astype(str)
        except TypeError:
            pass

        st.write("## Results")
        st.dataframe(df_data)
        #st.dataframe(df_data.style.apply(stTrx.ColourCells, df=df_data, colName='iteration', flip=True, axis=1))
        st.write("## Visualisation")
        st.write("**"+pageDict['yKey']+" vs. "+pageDict['xKey']+"**")
        visChart=alt.Chart(df_data).mark_line().encode(
                x=pageDict['xKey']+':Q',
                y=pageDict['yKey']+':Q',
                color='iteration:N',
                tooltip=[pageDict['xKey']+':Q',pageDict['yKey']+':Q','cts:N','iteration:N']
        ).properties(width=800).interactive()
        st.altair_chart(visChart)
