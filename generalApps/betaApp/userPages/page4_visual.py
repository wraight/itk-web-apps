### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################
def GetResultStat(statArr,stat="ave"):
    try:
        if len(statArr)<1: return 0
    except TypeError:
        return statArr

    if "min" in stat.lower():
        try:
            return min(statArr)
        except TypeError:
            return -101
    elif "max" in stat.lower():
        try:
            return max(statArr)
        except TypeError:
            return -101
    elif "mean" in stat.lower():
        try:
            return float(sum(statArr))/len(statArr)
        except TypeError:
            return -101
    else:
        return -1

def SendReport(instName, repList):
    now = datetime.now() # for publishing
    dateStr = now.strftime("%Y-%m-%d")
    timeStr = now.strftime("%H:%M:%S")
    report = dp.Report(
        dp.Markdown("## PDB Institute report: "+str(instName)),
        dp.Markdown("### Generated: "+dateStr+", "+timeStr),
        *repList,
        dp.Markdown("that's yer lot."), #create a table
    )
    report.publish(headline='PDB Report: '+dateStr+', '+timeStr, name='pdb_report_'+dateStr.replace('-','_')+'__'+timeStr.replace(':','_'))
    return True

infoList=["  * select *institution*",
        "  * select *project*",
        "  * view sunburst plot of available *componentTypes*",
        "  * select *componentType*",
        "  * view sunburst plot of available *componentTypes*",
        "  * select *componentType* from list",
        "  * view *subtypes*",
        "  * select max. number of test per component",
        "  * click *Get test data* component",
        "  * select statistical method (for multiple tests)",
        "  * view #tests per component stage",
        "  * select individual test"]
#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("Visual", ":fireworks: Visual Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # get list of componentTypes
        st.write("### List of *ComponentTypes*")
        # Check available component names and codes
        selCompList=st.session_state.myClient.get('listComponents',json={'institution':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] })
        if type(selCompList)!=type([]):
            selCompList=selCompList.data
        st.json(selCompList, expanded=False)
        compTypeMap=[]
        for w in selCompList:
            if w['state']!="ready": continue
            try:
                compTypeMap.append({"name":w['componentType']['name'], "code":w['componentType']['code'], "ASN":w['serialNumber'], "state":w['state'], "type":w['type']['name'], "proj":st.session_state.Authenticate['proj']['name']})
            except TypeError:
                pass
        df_compType=pd.DataFrame(compTypeMap)
        if st.session_state.debug:
            st.write("All found components")
            st.dataframe(df_compType)
        if df_compType.empty:
            st.write("No componentTypes found")
            st.stop()

        ### sunburst of components
        df_comps_g=pd.DataFrame({'count': pd.DataFrame(compTypeMap).groupby( ['proj','name','type'] ).size() }).reset_index()
        if st.session_state.debug: st.write("data set size:",len(df_comps_g.index))
        # plotly chart
        st.write("All project("+st.session_state.Authenticate['proj']['code']+") components @ "+st.session_state.Authenticate['inst']['name'])
        sun_chart = px.sunburst(df_comps_g, path=['proj','name','type'], values='count',
                      color='count', hover_data=['count'])
        st.plotly_chart(sun_chart)
        # choose componentType from radio
        st.write("**Select componentType**")
        #st.write(df_comps_g)
        pageDict['selComp']=st.radio("componentType: multiplicity", list(zip(df_comps_g['name'].tolist(),df_comps_g['count'].tolist())), format_func=lambda x: "{0}:\t\t{1}".format(x[0],x[1]) )[0]
        st.write("**subTypes**")
        st.dataframe(df_comps_g.query('name=="'+pageDict['selComp']+'"'))


        ### histograms
        #infra.SelectBox(pageDict,'selComp',list(df_comps_g['name'].unique()),'Select a component type')
        if st.session_state.debug: st.write("associated code:",pageDict['selComp'])
        st.write("### Tests available for **",pageDict['selComp'],"** components at **",st.session_state.Authenticate['inst']['code'],"**")

        infra.Slider(pageDict,'limit',1,1000,"max number tests per component")

        if st.button("Get test data!"):
            # set up results df
            df_results = pd.DataFrame(columns=["compSN","compName","testName","testStage","fullResult","statResult"])
            #if st.session_state.debug: st.write(df_results.columns)
            prog_bar=st.progress(0.0)
            prog_text = st.empty()
            count=0
            selCompList_filter=[x for x in compTypeMap if x['name']==pageDict['selComp']]
            for e,sc in enumerate(selCompList_filter,1):
                if st.session_state.debug: sc['code']
                if sc['ASN']!="null":
                    compReq=st.session_state.myClient.get('getComponent', json={'component': sc['ASN']})
                else:
                    st.write("No serial Number found!")
                    continue
                if st.session_state.debug: st.write("#test",len(compReq['tests']))
                try:
                    for t in compReq['tests']:
                        for tr in t['testRuns']:
                            #st.write("this one",tr['id'])
                            testReq=st.session_state.myClient.get('getTestRun', json={'testRun': tr['id']})
                            #if st.session_state.debug: st.write(testReq)
                            for trr in testReq['results']:
                                #st.write(trr['value'])
                                stage="NYS"
                                try:
                                    stage=testReq['components'][0]['testedAtStage']['name']
                                except:
                                    stage="not available"
                                df_temp=pd.DataFrame({"compSN":[sc['ASN']],"compName":[sc['name']],"testName":[testReq['testType']['name']],"testStage":[stage],"fullResult":[trr['value']]})
                                #st.write(df_temp)
                                df_results=df_results.append(df_temp, ignore_index=True)
                                count+=1
                                if count>pageDict['limit']: break
                            if count>pageDict['limit']: break
                        if count>pageDict['limit']: break
                except TypeError:
                    st.write("No tests for:",sc['ASN'],sc)
                prog_bar.progress(1.0*e/len(selCompList_filter))
                prog_text.text("component "+str(e)+" of "+str(len(selCompList_filter)))
            st.write("Data collection complete!")
            pageDict['results']=df_results
        try:
            st.write("total tests:",len(pageDict['results'].index)-1)
        except KeyError:
            st.write("no results defined")
            st.stop()
        if st.session_state.debug:
            st.write(pageDict['results'])

        infra.Radio(pageDict,'statOpt',["mean","min","max"],'Select a statistic option')

        pageDict['results']['statResult']=pageDict['results'].apply(lambda x: GetResultStat(x['fullResult'],pageDict['statOpt']), axis=1)
        if st.session_state.debug:
            st.write(pageDict['results'])

        compTestBar=alt.Chart(pageDict['results']).mark_bar().encode(
        x='testName:O',
        y='count(testName):Q',
        color='compName:N',
        column='testStage:N'
        ).properties(title='Tests per stage', width=200)
        st.altair_chart(compTestBar)

        if st.session_state.debug: st.write(list(pageDict['results']['testName'].unique()))

        st.write("Select individual test")
        infra.SelectBox(pageDict,'selTest',list(pageDict['results']['testName'].unique()),'Select test type name')

        df_test_results=pageDict['results'].query("testName=='"+pageDict['selTest']+"'").reset_index()
        if st.session_state.debug: df_test_results
        st.write(df_test_results.groupby( ['compName','testName','testStage'] ).size())
        try:
            resultsBar=alt.Chart(pageDict['results']).mark_bar().encode(
            x='testStage:O',
            y='statResult:Q'
                ).properties(title='Tests conducted', width=400)
            st.altair_chart(resultsBar)

            ### replace with violin chart when numbners better handled
            paraChart=alt.Chart(df_test_results).transform_window(
                index='count()'
            ).transform_fold(
                df_test_results['testStage'].unique()
            ).mark_circle().encode(
                x=alt.X('testStage:O', axis=alt.Axis(title='testStage')),
                y=alt.Y('statResult:Q', axis=alt.Axis(title=pageDict['selTest']+" ("+pageDict['statOpt']+")")),
                #color='species:N',
                #detail='index:N',
                opacity=alt.value(0.5)
            ).properties(title='Tests data', width=500)
            st.altair_chart(paraChart)
        except TypeError:
            st.write("No tests found")
        except:
            st.write("missing information, e.g. no stage names")
