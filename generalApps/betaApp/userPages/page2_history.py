### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import altair as alt
import ast
import csv
### PDB stuff
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX


#####################
### useful functions
#####################

def ColourCells(s, df, colName, flip=False):
    thisRow = pd.Series(data=False, index=s.index)
    colours=['red','blue','green','orange','purple''yellow','pink','lightblue','lightgreen']*3
    names=list(df[colName].unique())
    if flip:
        return ['background-color: %s ; color: %s'% ('white',colours[names.index(s[colName])])]*len(df.columns)
    else:
        return ['background-color: %s ; color: %s'% (colours[names.index(s[colName])],'black')]*len(df.columns)

def CalcStats(arr,name=""):
    myarr=[float(x) for x in arr]
    st.write("*"+str(name)+"* stats...")
    st.write("  * ave+/-std:",np.mean(myarr),"+/-",np.std(myarr))
    st.write("  * max,min:",np.max(myarr),",",np.min(myarr))

@st.cache
def GetChildTree(client, SN, relStr=""):
    theTree=[]
    GetRelations(client, theTree, SN, 0, relStr, 'children')
    return theTree

@st.cache
def GetParentTree(client, SN, relStr=""):
    theTree=[]
    GetRelations(client, theTree, SN, 0, relStr, 'parents')
    return theTree

def GetRelations(client, theList, theSN, theGen, theRel, relType="parents"):
    #print("trying:",theSN)
    compReq=st.session_state.myClient.get('getComponent', json={'component':theSN })
    theList.append({'gen':theGen, 'serialNumber':compReq['serialNumber'], 'compType':compReq['componentType']['code'], 'relation':theRel, 'cts':compReq['cts']})
    try:
        theList[-1]['num'+relType]=len(compReq[relType])
    except TypeError:
        theList[-1]['num'+relType]=0
        #print("Not this one:", len(compReq['children']))
        return None
    ### if relations...
    gen=999
    if relType=="parents": gen=theGen-1
    if relType=="children": gen=theGen+1
    for c in compReq[relType]:
        try:
            # ignore self-child
            if c['component']['serialNumber']==theSN:
                continue
        except TypeError:
            print("no serialNumber found for compType:",c["componentType"]["code"])
            print("comp id:",c["id"])
            theList.append({'gen':gen, 'serialNumber':"unknown", 'compType':c['componentType']['code'], 'relation':theSN, 'cts':c['timestamp']})
            continue
        GetRelations(client, theList, c["component"]["serialNumber"], gen, theSN, relType)


def GetParents(client, theList, theSN, theGen, theChild):
    #print("trying:",theSN)
    compReq=st.session_state.myClient.get('getComponent', json={'component':theSN })
    theList.append({'gen':theGen, 'serialNumber':compReq['serialNumber'], 'compType':compReq['componentType']['code'], 'child':theChild, 'cts':compReq['cts']})
    try:
        theList[-1]['numParent']=len(compReq['parents'])
    except TypeError:
        theList[-1]['numParent']=0
        #print("Not this one:", len(compReq['parents']))
        return None
    ### if parents...
    # st.write("We got one!")
    # st.write(compReq['parents'])
    for c in compReq['parents']:
        try:
            if c["component"]["serialNumber"]==theSN:
                continue
            try:
                GetParents(theList, c["component"]["serialNumber"], theGen-1, theSN)
            except:
                #print("strike out:",c["component"]["serialNumber"])
                continue
        except TypeError:
            #st.write("no serialNumber found for compType:",c["componentType"]["code"])
            continue

def GetChildren(client, theList, theSN, theGen, theParent):
    #print("trying:",theSN)
    compReq=st.session_state.myClient.get('getComponent', json={'component':theSN })
    theList.append({'gen':theGen, 'serialNumber':compReq['serialNumber'], 'compType':compReq['componentType']['code'], 'parent':theParent, 'cts':compReq['cts']})
    try:
        theList[-1]['numChild']=len(compReq['children'])
    except TypeError:
        theList[-1]['numChild']=0
        #print("Not this one:", len(compReq['children']))
        return None
    ### if children...
    #st.write("We got one!")
    #st.write(compReq['children'])
    for c in compReq['children']:
        try:
            if c["component"]["serialNumber"]==theSN:
                continue
            try:
                GetChildren(theList, c["component"]["serialNumber"], theGen+1, theSN)
            except:
                #print("strike out:",c["component"]["serialNumber"])
                continue
        except TypeError:
            #st.write("no serialNumber found for compType:",c["componentType"]["code"])
            continue

infoList=["1. Input component code",
        "2. Get component history",
        "3. Select feature",
        " * a. Geneology plot (sankey)",
        " * b. Timeline (1D scatter)",
        " * c. Check relation to another component"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("History", ":dizzy: History Plots", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # get list of componentTypes
        st.write("### List of *ComponentTypes* for",st.session_state.Authenticate['inst']['code'],"("+st.session_state.Authenticate['proj']['code']+")")
        # Check available component names and codes
        if st.button("reset list") or "compTypeList" not in list(pageDict.keys()): # check list
            st.write("Get list")
            pageDict['compTypeList']=st.session_state.myClient.get('listComponents',json={'institution':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] })
            if type(pageDict['compTypeList'])!=type([]):
                pageDict['compTypeList']=pageDict['compTypeList'].data
        else:
            st.write("Got compTypeList")
        if len(pageDict['compTypeList'])<1:
            st.write("No data found. Check *Institution* and *Project* settings")
            st.stop()
        st.write("compTypeList size: "+str(len(pageDict['compTypeList'])))
        df_compList=pd.json_normalize(pageDict['compTypeList'], sep = "_")
        df_compType=df_compList[['componentType_name','componentType_code']].drop_duplicates()
        st.dataframe(df_compType)

        infra.SelectBoxDf(pageDict,'compType',df_compType,'Select a componentType', 'componentType_code')
        if st.session_state.debug:
            st.write("**DEBUG** *componentType* id")
            st.write(pageDict['compType'])

        infra.SelectBoxDf(pageDict,'comp',df_compList.query('componentType_code=="'+pageDict['compType']['componentType_code'].values[0]+'"'),'Select a component', 'serialNumber')
        st.write(pageDict['comp'])

        ###################
        # Geneology
        ###################
        st.write("## Get component history")
        if st.button("Get component history"):
            # get primary ancestor
            pageDict['histComp']=st.session_state.myClient.get('getComponent', json={'component': pageDict['comp']['serialNumber'].values[0]})
            parentTree=GetParentTree(st.session_state.myClient, pageDict['histComp']['serialNumber'], "origin")
            #for c in parentTree: print(c)
            ancSN=parentTree[-1]['serialNumber']
            st.write("Primary ancestor:",ancSN)
            # get full tree from primary ancestor
            childTree=GetChildTree(st.session_state.myClient, pageDict['histComp']['serialNumber'], "origin")
            # for c in childTree: print(c)
            genTree=parentTree+childTree
            pageDict['df_hist']=pd.DataFrame(genTree).drop_duplicates(subset='serialNumber', keep="first").sort_values(by ='gen' ).reset_index(drop=True)
            pageDict['df_hist']['gen']=pageDict['df_hist']['gen']-pageDict['df_hist']['gen'].min()
            if st.session_state.debug: st.write("### Full tree")
            if st.session_state.debug: st.dataframe(pageDict['df_hist'])
            st.dataframe(pageDict['df_hist'].style.apply(ColourCells, df=pageDict['df_hist'], colName='gen', flip=True, axis=1))
        else:
            if "df_hist" not in pageDict.keys():
                st.write("**No history found.** Get component history first.")
            else:
                st.write("Got component history for:",pageDict['histComp']['serialNumber'])
                st.dataframe(pageDict['df_hist'].style.apply(ColourCells, df=pageDict['df_hist'], colName='gen', flip=True, axis=1))

        st.write("## Select feature")
        ###################
        # Family tree
        ###################
        st.write("### Geneology plot (sankey)")
        if st.button("Get geneology plot"):
            if 'df_hist' not in pageDict.keys():
                st.write("**No history found.** Get component history first.")
            else:
                # group entries
                ancSN=pageDict['df_hist'].loc[0]['serialNumber']
                compGen=pageDict['df_hist'].query('serialNumber=="'+pageDict['histComp']['serialNumber']+'"')['gen']
                grouping=["relation","compType"]
                df_time_g=pd.DataFrame({'num' : pageDict['df_hist'].groupby( grouping ).size(), 'serialNumber': pageDict['df_hist'].groupby( grouping )['serialNumber'].aggregate(lambda x: list(x))}).reset_index()
                if st.session_state.debug: st.write("### Grouped tree")
                if st.session_state.debug: st.dataframe(df_time_g)
                # plot
                labs=list(df_time_g['compType'].unique())
                if st.session_state.debug: st.write(labs)
                plotDict=[]
                for i, r in df_time_g.iterrows():
                    for j, s in df_time_g.iterrows():
                        if i==j: continue
                        #print(str(i)+str(j))
                        for rc in r['serialNumber']:
                            if rc==s['relation']:
                                #plotDict.append({'from':i, 'to':j, 'value':s['num'], 'ij':str(i)+str(j)})
                                plotDict.append({'from':labs.index(r['compType']), 'to':labs.index(s['compType']), 'value':s['num'], 'ij':str(i)+str(j)})
                                break
                fig = go.Figure(data=[go.Sankey(
                    node = dict(
                      pad = 15,
                      thickness = 20,
                      line = dict(color = "black", width = 0.5),
                      label = list(df_time_g['compType']),
                      color = "blue"
                    ),
                    link = dict(
                      source = [plotDict[p]['from'] for p in range(0,len(plotDict))], # indices correspond to labels, eg A1, A2, A2, B1, ...
                      target = [plotDict[p]['to'] for p in range(0,len(plotDict))],
                      value = [plotDict[p]['value'] for p in range(0,len(plotDict))]
                  ))])
                fig.update_layout(title_text="Child flow for "+str(ancSN), font_size=10)

                st.write("### Geneology:",pageDict['histComp']['serialNumber'])
                st.write("### generation",compGen.values[0],"of",ancSN)
                st.write(fig)

        ###################
        # Timeline
        ###################
        st.write("### Timeline (1D scatter)")
        if st.button("Get timeline plot"):
            if 'df_hist' not in pageDict.keys():
                st.write("**No history found.** Get component history first.")
            else:
                # dfs
                df_time_solo=pageDict['df_hist']
                df_time_solo['cts']=pd.to_datetime(df_time_solo['cts'], format='%Y-%m-%d')
                # plots
                #df_time_solo['cts']=df_time_solo['cts'].dt.strftime("%d/%m/%Y")
                if st.session_state.debug: st.dataframe(df_time_solo)
                points = alt.Chart(df_time_solo).mark_point().encode(
                    x=alt.X('cts:T',
                        scale=alt.Scale(zero=False),
                        axis=alt.Axis(title='CTS', grid=True)),
                    tooltip=['cts','compType','gen']

                ).properties(width=600, height=300)

                text = points.mark_text(
                    align='left',
                    angle=270,
                    baseline='middle',
                    dx=5
                ).encode(
                    text='compType',
                )

                compType=str(df_time_solo.loc[df_time_solo['serialNumber'] == pageDict['comp']['serialNumber'].values[0]]['compType'])
                compCTS=str(df_time_solo.loc[df_time_solo['serialNumber'] == pageDict['comp']['serialNumber'].values[0]]['cts'])
                st.write("### Timeline of:",pageDict['comp']['serialNumber'].values[0],"(",compType,")")
                st.write("DoB:",compCTS)
                st.altair_chart(points + text)

        ###################
        # Related?
        ###################
        st.write("### Check relation to another component")
        nextSN = st.text_input(label='second serialNumber')#, value='20UPGFC0029051')
        if st.button("Check related"):
            if 'df_hist' not in pageDict.keys():
                st.write("**No history found.** Get component history first.")
            else:
                if nextSN==pageDict['comp']['serialNumber'].values[0]:
                    st.write("*This is the same component!*")
                else:
                    #st.write(st.session_state.myClient.get('getComponent', json={'component':nextSN }))
                    nextGen=-1
                    try:
                        nextGen=int(pageDict['df_hist'].loc[pageDict['df_hist']['serialNumber'] == nextSN]['gen'])
                    except TypeError:
                        nextGen=None

                    if nextGen==None:
                        st.write("Components **not** related")
                    else:
                        st.write("Components **are** related...")
                        ancCode=pageDict['df_hist'].loc[0]['serialNumber']
                        compGen=int(pageDict['df_hist'].loc[pageDict['df_hist']['serialNumber'] == pageDict['comp']['serialNumber'].values[0]]['gen'])
                        st.write(" * ",pageDict['comp']['serialNumber'].values[0]," is generation",compGen,"of",ancCode)
                        st.write(" * ",nextSN," is generation",nextGen,"of",ancCode)
