### standard
import streamlit as st
from core.Page import Page
### custom
import os
import pandas as pd
import copy
import json
from io import StringIO
from datetime import datetime,date,time
from pathlib import Path
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################

infoList=[" * select component type",
        " * select component sub-type",
        " * select component sub-project",
        " * get component schema from PDB",
        " * fill required fields",
        " * upload to PDB",
        " * delete component from PDB if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Single Component", ":microscope: Upload Single component to PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        pageDict['project']=st.session_state.Authenticate['proj']['code']

        st.write("## Select Component Type")
        # review componentTypes and stages (from file)
        filePath="/code/commonCode/"
        stTrx.DebugOutput("filePath:",filePath)
        csvFileName="stageTestList_"+pageDict['project']+".csv"
        stTrx.DebugOutput("looking in: "+filePath[:filePath.rfind('/')])
        df_all=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+csvFileName)
        infra.SelectBox(pageDict,'componentType',df_all['compType'].unique(),"Select componentType code:")

        if "componentType" not in pageDict.keys():
            st.write("No componentType selected.")

        # selection of subprojects and types
        pageDict['compTypeObj']=st.session_state.myClient.get('getComponentTypeByCode',json={'project':pageDict['project'], 'code':pageDict['componentType']})
        
        # try:
        #     if "subprojects" in pageDict['compTypeObj'].keys():
        #         subProjs=[x['code'] for x in pageDict['compTypeObj']['subprojects']]
        #         infra.SelectBox(pageDict,'subProj',subProjs,"Select component subType (for "+pageDict['componentType']+")")
        # except TypeError:
        #     st.write(f"Subprojects not found for {pageDict['compTypeObj']['code']}")
        #     pageDict['subProj']==None
        
        # try:
        #     if "types" in pageDict['compTypeObj'].keys():
        #         compTypes=[x['code'] for x in pageDict['compTypeObj']['types']]
        #     infra.SelectBox(pageDict,'subType',compTypes,"Select component type")
        # except TypeError:
        #     st.write(f"Component types not found for {pageDict['compTypeObj']['code']}")
        #     pageDict['subType']==None

        # st.write(pageDict['compSchema'])
        # set upload schema
        if "origSchema" not in pageDict.keys() or pageDict['compSchema']['componentType']!=pageDict['componentType'] or st.button("Reset Schema: "+pageDict['componentType']):
            pageDict['origSchema'] = st.session_state.myClient.get('generateComponentTypeDtoSample', json={'project':pageDict['project'], 'code':pageDict['componentType'], 'requiredOnly' :False})
            pageDict['reqSchema'] = st.session_state.myClient.get('generateComponentTypeDtoSample', json={'project':pageDict['project'], 'code':pageDict['componentType'], 'requiredOnly' :True})
            pageDict['compSchema']=copy.deepcopy(pageDict['origSchema'])
            pageDict['compSchema']['institution']=st.session_state.Authenticate['inst']['code']


        # show schema
        stTrx.DebugOutput("Original *schema*:",pageDict['origSchema'])
        if st.checkbox("Show PDB schema?",value=False):
            if "only" in st.radio('',["only required","include options"]):
                st.write(pageDict['reqSchema'])
            else:
                st.write(pageDict['origSchema']) 

        ### upload schema settings
        st.write("---")
        st.write("### Upload settings")

        #######################
        ### Enter Information
        #######################

        ### select mode
        sel_mode= st.radio("Select mode:", ["pre-formatted json", "manual upload"])  
        
        ### pre-formatted json
        if "json" in sel_mode.lower():
            st.write("#### Upload using pre-formatted json")

            ## drag and drop method
            pageDict['file']= st.file_uploader("Upload data file", type=["json"])
            if st.session_state.debug: 
                st.write(pageDict['file'])

            ### offer example if no file
            if pageDict['file'] is None:
                ### delete existing info (defined below)
                try:
                    del pageDict['regList']
                except KeyError:
                    pass
                st.write("No data file set")
                # filePath=os.path.realpath(__file__)
                # exampleFileName= pageDict["code"] + "_example.json"
                # if st.session_state.debug:
                #     st.write("looking in:",filePath[:filePath.rfind('/')])
                #     st.write(os.listdir(filePath[:filePath.rfind('/')]))
                # st.download_button(label="Download example", data=Path(filePath[:filePath.rfind('/')]+"/"+exampleFileName).read_text(), file_name=exampleFileName)
                st.stop()

            ### read in formatted json file
            if "json" in pageDict['file'].type:
                st.write("_json_ file recognised ")
                data=json.loads(StringIO(pageDict['file'].getvalue().decode("utf-8")).read())
            ### unknown file type
            else:
                st.write(f"_{pageDict['file'].type}_ file unrecognised. Please use _json_.")
                st.stop()

            # ### Upload JSON
            # st.write("A pre-formatted json can be uploaded if available.")
            # exampleFileName = pageDict["code"] + "_example.json"
            # df_input = chnx.UploadJSONChunk(pageDict, exampleName=exampleFileName, exampleDict=pageDict['compSchema'])
            # if df_input:
            #     pageDict['compSchema'] = df_input

            ### add gleaned information to test schema
            # header
            pageDict['compSchema']=data

        ### manual upload
        elif "manual" in sel_mode.lower():
            st.write("#### Manual Upload")

            # manually set parameters
            chnx.SelectObjectParameters(pageDict, 'compSchema')

        ### no choice
        else:
            st.write("No mode selected")
            st.stop()
                
        ### remove flagged parameters
        pageDict['compSchema']= chnx.RemoveKeys(pageDict['compSchema']) 
        # st.write(pageDict['compSchema'])
        

       ### show/edit data for upload
        st.write("---")
        st.write("### Review Component Schema")
        infra.ToggleButton(pageDict,'toggleEdit','Edit Schema?')
        if pageDict['toggleEdit']:
            st.write("### Edit Schema")
            pageDict['compSchema']=EditJson(pageDict['compSchema'])
        else:
            st.write(pageDict['compSchema'])

        infra.ToggleButton(pageDict,'toggleText','Convert values to text?')
        if pageDict['toggleText']:
            pageDict['compSchema'] = json.loads(json.dumps(pageDict['compSchema']), parse_int=str)
            pageDict['compSchema']['results'] = json.loads(json.dumps(pageDict['compSchema']['results']), parse_float=str)
        if st.session_state.debug:
            st.write("### **DEBUG** (final!) Test Schema")
            st.write(pageDict['compSchema'])

        st.write("---")
        st.write("## Component Registration")

        # # check existing component tests
        # chnx.StageCheck(pageDict)

        # upload(!) test schema: change get --> post
        chnx.RegChunk(pageDict, "Component", "compSchema")
