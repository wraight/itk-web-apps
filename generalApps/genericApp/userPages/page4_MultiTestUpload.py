### standard
import os

import altair as alt
import core.stInfrastructure as infra
### custom
import pandas as pd
import streamlit as st
import xlrd
import xlsxwriter
from core.Page import Page

import commonCode.codeChunks as chnx
import commonCode.StreamlitTricks as stTrx

### analyses

#####################
### useful functions
#####################

infoList=[" * register multiple tests",
        " * upload _xlsx_ or xls formatted file",
        " * review retrieved data & visualisation (and pass/fail)",
        " * upload test schemas to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("Multi Test", ":microscope: Upload Multiple tests to PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        pageDict['project']=st.session_state.Authenticate['proj']['code']

        # review componentTypes and stages from file
        filePathList="/code/commonCode/"
        stTrx.DebugOutput("filePathList:",filePathList)
        csvFileName="stageTestList_"+st.session_state.Authenticate['proj']['code']+".csv"
        stTrx.DebugOutput("looking in: "+filePathList[:filePathList.rfind('/')])
        chnx.ToggleChangesChunk(pageDict, 'testrun', filePathList, csvFileName)

        filePath=os.path.realpath(__file__)
        exampleFileName="QCTestExample.csv"
        df_input=chnx.UploadFileChunk(pageDict,['csv','xls'],filePath[:filePath.rfind('/')]+"/"+exampleFileName,"Sheet1")

        st.write("length of df_input:",len(df_input))
        ### read input data
        st.dataframe(df_input)
        # for c in df_input.columns:
        #     st.write(c,":",df_input[c].dtype)

        ### input visualisations
        st.write("### Test data visualisation")
        visChart=alt.Chart(df_input).mark_bar().encode(
                x=alt.X('component:N'),
                y=alt.Y('count(component):Q'),
                color='testType:N',
                tooltip=['testType:N','stage:N','count(component):Q']
        ).properties(
            width=600,
        ).facet(
            facet='componentType:N'
        ).configure_axisX(labelAngle=-45).interactive()
        st.altair_chart(visChart, use_container_width=True)


        ### read input data
        chnx.ReadCSVChunk(df_input, pageDict, 'testRun', 'testSchemas')

        ### check schemas and passes
        if "testSchemas" in pageDict.keys():
            if len(pageDict['testSchemas'])<1:
                st.write("No matching data found :(")
                st.stop()
            st.write(f"**{len(pageDict['testSchemas'])} testRuns compiled**")
            st.write("### Set test passes?")
            infra.ToggleButton(pageDict,'setPasses','Change test pass values?')
            if pageDict['setPasses']:
                for c,ts in enumerate(pageDict['testSchemas']):
                    ts['passed']=st.checkbox(ts['component']+": "+ts['testType']+' passed?', value=ts['passed'], key=c)

        # st.write("### testSchemas",pageDict['testSchemas'])

        st.write("### Compiled testSchemas")

        st.dataframe(pd.DataFrame(pageDict['testSchemas']))

        stTrx.DebugOutput("testSchemas",pageDict['testSchemas'])

        ### registration
        chnx.RegMultiChunk(pageDict, 'registerTestRun', 'testSchemas')
