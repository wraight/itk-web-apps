### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
import io
import xlsxwriter
import xlrd
import openpyxl
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _xlsx_ or xls formatted file",
        " * review retrieved data & visualisation",
        " * (optional) add pass/fail - default pass",
        " * upload test schemas to PDB",
        " * delete tests from PDB if required"]
#####################
### main part
#####################

class Page8(Page):
    def __init__(self):
        super().__init__("Multi Stage Set", ":microscope: Set Stage Multiple components in PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        pageDict['project']=st.session_state.Authenticate['proj']['code']

        # review componentTypes and stages from file
        filePath=os.path.realpath(__file__)
        exampleFileName="QCStageSetExample.csv"
        df_input=chnx.UploadFileChunk(pageDict,['csv','xls'],filePath[:filePath.rfind('/')]+"/"+exampleFileName,"Sheet1")

        ### read input data
        st.dataframe(df_input)

        ### input visualisations
        st.write("### Assembly data visualisation")
        visChart=alt.Chart(df_input).mark_bar().encode(
                x=alt.X('stage:N'),
                y=alt.Y('count(serialNumber):Q'),
                color='stage:N',
                tooltip=['stage:N','count(serialNumber):Q']
        ).configure_axisX(labelAngle=-45).properties(
            width=250,
        ).interactive()
        st.altair_chart(visChart, use_container_width=True)

        ### read input data
        if st.button("Set stages"):
            pageDict['putBackList']=[]
            st.write("Set components")
            listLen=len(df_input)
            status_bar_ids=st.progress(0.0) #st.empty()
            status_text_ids=st.empty()
            status_text_update=st.empty()
            for index, row in df_input.iterrows():
                status_bar_ids.progress((1.0*index+1)/listLen)
                loopText="setting "+row['stage']+" stage for "+row['serialNumber']+", "+str(index+1)+" of "+str(listLen)
                status_text_ids.text(loopText)
                pageDict['putBackList'].append(None)

                try:
                    compVal=st.session_state.myClient.get('getComponent', json={'component':row['serialNumber']})

                    if compVal['currentStage']['code']!=row['stage']:
                        ### set stage first!
                        status_text_update.text(f"- changing to set stage: {row['stage']} (from {compVal['currentStage']['code']})")
                        if pdbTrx.SetCompStage(compVal['code'], row['stage']):
                            pageDict['putBackList'][-1]={compVal['code']:compVal['currentStage']['code']}
                    else:
                        status_text_update.text(f"- already in appropriate stage: {row['stage']}")
                except TypeError:
                    status_text_update.text("- no stage previously exists. Setting...")
                    pdbTrx.SetCompStage(compVal['code'], row['stage'])

        ### return to previous stages
        if "putBackList" in pageDict.keys():
            if st.button(f"Return components to previous stages"):
                status_text_pb=st.empty()
                for pb in pageDict['putBackList']:
                    if pb==None or pb==False:
                        continue
                    for k,v in pb.items():
                        status_text_pb.text(f"- Return {k} in to {v}...")
                        pdbTrx.SetCompStage(k,v)

