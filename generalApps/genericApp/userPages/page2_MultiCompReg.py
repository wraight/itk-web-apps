### standard
import os

import altair as alt
import core.stInfrastructure as infra
### PDB stuff
import itkdb.exceptions as itkX
### custom
import pandas as pd
import streamlit as st
import xlrd
import xlsxwriter
from core.Page import Page

import commonCode.codeChunks as chnx
import commonCode.StreamlitTricks as stTrx

### analyses

#####################
### useful functions
#####################

infoList=[" * register multiple components",
        " * upload _xlsx_ or xls formatted file",
        " * review retrieved data & visualisation",
        " * upload component schemas to PDB",
        " * delete component from PDB if required"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Multi Component", ":microscope: Upload Multiple components to PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        pageDict['project']=st.session_state.Authenticate['proj']['code']

        filePath=os.path.realpath(__file__)
        exampleFileName="QCRegistrationExample.csv"
        df_input=chnx.UploadFileChunk(pageDict,['csv','xls'],filePath[:filePath.rfind('/')]+"/"+exampleFileName,"Sheet1")

        ### check input data
        st.dataframe(df_input)

        ### delete on the fly
        infra.ToggleButton(pageDict,'togDel',"Delete components?")
        if pageDict['togDel']:
            try:
                st.write("Components:",df_input['serialNumber'].tolist())
                if st.button("Confirm deletion?"):
                    for sn in df_input['serialNumber'].tolist():
                        try:
                            delVal= st.session_state.myClient.post('deleteComponent', json={'component':sn})
                            st.write("### **Successful Deletion**:",delVal)
                        except itkX.BadRequest as b:
                            st.write("### :no_entry_sign: Deletion unsuccessful")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                        except TypeError:
                            st.write("Don't have return value :(")
            except KeyError:
                st.write("no _serialNumber_s found")
                pass

        ### input visualisation
        st.write("### Input data visualisation")
        visChart=alt.Chart(df_input).mark_bar().encode(
                x=alt.X('componentType:N'),
                y=alt.Y('count(componentType):Q'),
                color='serialNumber:N',
                tooltip=['componentType:N','count(componentType):Q']
        ).configure_axisX(labelAngle=-45).interactive()
        st.altair_chart(visChart, use_container_width=True)

        ### read input data
        chnx.ReadCSVChunk(df_input, pageDict, 'component', 'compSchemas')

        if "compSchemas" in pageDict.keys():
            if len(pageDict['compSchemas'])<1:
                st.write("No matching data found :(")
                st.stop()
        ### check schemas
        st.write("### Compiled compSchemas")
        st.dataframe(pd.DataFrame(pageDict['compSchemas']))
        stTrx.DebugOutput("compSchemas:",pageDict['compSchemas'])

        ### registration
        chnx.RegMultiChunk(pageDict, 'registerComponent', 'compSchemas')
