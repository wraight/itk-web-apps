### standard
import datetime
import os
from io import StringIO
import io
from PIL import Image
import altair as alt
import core.stInfrastructure as infra
### PDB stuff
import numpy as np
### custom
import pandas as pd
import streamlit as st
import itkdb
import itkdb.exceptions as itkX
from core.Page import Page

import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

from PIL import Image
import magic

### analyses

#####################
### useful functions
#####################

def DeleteEOSLink(myClient, objType, objCode, attachCode):
    try:
        delObj=myClient.post("delete"+objType.title()+"Attachment", json={objType: objCode, "code": attachCode})
        st.write(delObj)
        st.write("__Success!__")
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: deleting **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
    except:
        st.write("### :no_entry_sign: deleting **Unsuccessful**")
        # st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

### upload file function


infoList=[" * check test parameters for componentType at institute(s)",
        " * select componentType at institute",
        " * select stage and testType (from test schema)",
        " * select parameter (currently no arrays supported)",
        " * visualise and download csv"]
#####################
### main part
#####################

class Page13(Page):
    def __init__(self):
        super().__init__("EOS Cleaner", ":microscope: Delete broken links from EOS", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        ##############
        ### check eos
        ##############

        st.write("### ready for EOS?")
        st.write(st.session_state.myClient.use_eos)
        if st.session_state.myClient.use_eos:
            st.success("__ready__")
        else:
            st.info("EOS flag not set. Please use check box in Authenication page and get new token")
            st.stop()

        ##############
        ### credits
        ##############
        st.write("## Credits")
        st.write("The base code for this page was writen by _Ewan Chin Hill_")
        st.write("- code repo. [here](https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts/-/tree/sensor_updates/strips/eosAttachments)")

        st.write("---")
        st.write("## Select delete option")
        selMod=st.radio("Select option:",["Delete using manual input", "Delete using spreadsheet"])

        st.write("---")
        ##############
        ### manual delete
        ##############
        if "manual" in selMod.lower():
            st.write("## Check EOS attachment manually")
            
            selObj=st.radio("What to delete from:",["component", "testRun"])

            objCode=st.text_input("Input object identifier")

            if st.button(f"Get Object {objCode}"):
                pageDict['retObj']=st.session_state.myClient.get('get'+selObj.title(), json={selObj: objCode})

            if "retObj" in pageDict.keys():
                if type(pageDict['retObj'])!=type({}):
                    st.write("return object not readable: ",type(pageDict['retObj']))
                    st.write(" - please check identifier")
                    st.stop()
                df_att=pd.DataFrame(pageDict['retObj']['attachments'])
                if df_att.empty:
                    st.write(f"No attachments found for {objCode}")
                    st.stop()
                st.write("__attachments__")
                st.write(df_att)                
                selAttach=st.selectbox(f'Select attachment code:',df_att['code'].unique())

                if st.button(f"Deleting attachment {selAttach}"):
                    st.write(f"Deleting objID: {pageDict['retObj']['id']}, attachmentCode: {selAttach}")
                    DeleteEOSLink(st.session_state.myClient,selObj,pageDict['retObj']['id'],selAttach)

        ##############
        ### check EOS spreadsheet
        ##############
        elif "spreadsheet" in selMod.lower():

            st.write("## Check EOS testType spreadsheet")
            st.write("Broken EOS links can be checked [here](https://docs.google.com/spreadsheets/d/19oelQVg1u23D8djHl6daoMsC18eXch7FbIsHMXJQFNg)")
            st.write(" - please download file as CSV and upload to webPage _or_ use existing")

            if st.checkbox("Upload file"):
                df_input=chnx.UploadFileChunk(pageDict,['csv'],None,None)
                if df_input.empty:
                    st.write("Please upload _csv_ file")
                    st.stop()
            
            else:
                filePath=os.path.realpath(__file__)
                filePath=filePath[:filePath.rfind('/')]+"/"+"deadDatabasePointersTestEosAttachments_lost_files.csv"
                df_input=pd.read_csv(filePath)

            ##############
            ### Add PDB information 
            ##############
            st.write("---")
        
            if "testTypeMap" not in pageDict.keys() or st.button("re-check additional"): # or st.button("Get PDB info."):
                st.write("_Collecting addition PDB information..._")

                pageDict['testTypeMap']={}
                for tt in df_input['testRunType'].unique():
                    pageDict['testTypeMap'][tt]=None
                    testTypeObj=st.session_state.myClient.get("getTestType", json={'id': tt})
                    pageDict['testTypeMap'][tt]=testTypeObj['code']

                pageDict['testRunMap']={}
                testRunObj=st.session_state.myClient.get("getTestRunBulk", json={'testRun': list(df_input['testID'].unique())})

                for tr in testRunObj:
                    if tr['id'] not in pageDict['testRunMap'].keys():
                        pageDict['testRunMap'][tr['id']]=tr
            # st.write(pageDict['testMap'])

            st.write("_...Got additional information_")
            for x in ['testTypeMap','testRunMap']:
                if st.checkbox(f"Check {x}"):
                    st.dataframe(pageDict[x])
            
            st.write("---")
            #######################
            ### display information
            #######################
            st.write("## See broken link info.")

            df_input['testTypeCode']=df_input['testRunType'].apply(lambda x: pageDict['testTypeMap'][x] if x in pageDict['testTypeMap'].keys() else None)
            df_input['testRunState']=df_input['testID'].apply(lambda x: pageDict['testRunMap'][x]['state'] if x in pageDict['testRunMap'].keys() else None)
            df_input['compSerialNumber']=df_input['testID'].apply(lambda x: pageDict['testRunMap'][x]['components'][0]['serialNumber'] if x in pageDict['testRunMap'].keys() and "serialNumber" in pageDict['testRunMap'][x]['components'][0].keys() else None)
            if st.checkbox("Filter sheet"):
                colVal=st.radio('Select column:',df_input.columns)
                filtVal=st.selectbox(f'Select {colVal}:',df_input[colVal].unique())
                df_filt=df_input.query(f'{colVal}=="{filtVal}"').reset_index(drop=True)
            else:
                df_filt=df_input
            st.write(df_filt)

            st.write("---")
            #######################
            ### delete  infromation
            #######################
            st.write("## Delete attachments")
            st.write("__NB__ only users who made upload can do deletion")

            selDel=st.radio('Select delete option:',["Single attachment","All component attachments","All testType attachments"])
            
            if "single" in selDel.lower():
                st.write("### Delete single attachment")
                selAttach=st.selectbox('select testRun id',df_filt['attachmentCode'])
                st.write(df_filt.query(f'attachmentCode=="{selAttach}"'))
                testRunId=df_filt.query(f'attachmentCode=="{selAttach}"')['testID'].values[0]
                st.write(testRunId)

                if st.button(f"Deleting attachment {selAttach}"):
                    st.write(f"Deleting testID: {testRunId}, attachmentCode: {selAttach}")
                    DeleteEOSLink(st.session_state.myClient,"testRun",testRunId,selAttach)
            
            elif "component" in selDel.lower():
                st.write("### Delete all attachments associated with component")
                selCompSN=st.selectbox('select component serialNumber',df_filt['compSerialNumber'].unique())
                st.write(df_filt.query(f'compSerialNumber=="{selCompSN}"'))
                codes=df_filt.query(f'compSerialNumber=="{selCompSN}"')[['attachmentCode','testID']].to_dict(orient='list')
                # st.write(codes)
                if st.button("Delete links?"):
                    for x,y in zip(codes['testID'],codes['attachmentCode']):
                        st.write(f"Deleting testID: {x}, attachmentCode: {y}")
                        DeleteEOSLink(st.session_state.myClient,"testRun",x,y)

            elif "testtype" in selDel.lower():
                st.write("### Delete all attachments associated with testType")
                selTTC=st.selectbox('select component serialNumber',df_filt['testTypeCode'].unique())
                st.write(df_filt.query(f'testTypeCode=="{selTTC}"'))
                codes=df_filt.query(f'testTypeCode=="{selTTC}"')[['attachmentCode','testID']].to_dict(orient='list')
                # st.write(codes)
                if st.button("Delete links?"):
                    for x,y in zip(codes['testID'],codes['attachmentCode']):
                        st.write(f"Deleting testID: {x}, attachmentCode: {y}")
                        DeleteEOSLink(st.session_state.myClient,"testRun",x,y)

            else:
                st.write("Select deletion option")
