### standard
import datetime
import os
from io import StringIO
import io
import json
import altair as alt
import core.stInfrastructure as infra
### PDB stuff
import numpy as np
### custom
import pandas as pd
import streamlit as st
import itkdb
import itkdb.exceptions as itkX
from core.Page import Page
import tarfile
import randomname
from io import StringIO

import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

### eos stuff
from PIL import Image
from requests.exceptions import ConnectionError


### analyses

#####################
### useful functions
#####################

def DeleteEOSLink(myClient, objType, objCode, attachCode):
    try:
        delObj=myClient.post("delete"+objType[:1].upper()+objType[1:]+"Attachment", json={objType: objCode, "code": attachCode})
        st.write(f"Successful deletion: {delObj}")
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: deleting **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

def MakeUniqueName():
    # random bit
    name=randomname.get_name(adj=('music_theory',), noun=('apex_predators'))
    # with date
    name=name+"_"+datetime.datetime.now().strftime('%Y_%m_%d')
    return name

infoList=[" * check test parameters for componentType at institute(s)",
        " * select componentType at institute",
        " * select stage and testType (from test schema)",
        " * select parameter (currently no arrays supported)",
        " * visualise and download csv"]
#####################
### main part
#####################

class Page12(Page):
    def __init__(self):
        super().__init__("EOS Loader", "🏹 Upload/download files (inc. images) to PDB space on EOS", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        ##############
        ### check eos
        ##############

        st.write("### ready for EOS?")
        st.write(st.session_state.myClient.use_eos)
        if st.session_state.myClient.use_eos:
            st.success("__ready__")
        else:
            st.info("EOS flag not set. Please use check box in Authenication page and get new token")
            st.stop()


        pageDict['projectCode']=st.session_state.Authenticate['proj']['code']
        pageDict['institutionCode']=st.session_state.Authenticate['inst']['code']
        
        ##############
        ### select object to attach image
        ##############
        st.write("## Get PDB Object")
        
        ##############
        ### select object type: component or testRun
        ##############
        objList=['component', 'testRun']
        objSel=st.radio("select object type:", objList)

        st.write("---")


        st.write(f"## Identify {objSel} in database to attach file")
        
        st.write(f"### Select component")
        ##############
        ### select identification method
        ##############
        methodList=['input identifier', 'select from lists']
        methodSel=st.radio("select function:", methodList)

        if 'identifier' in methodSel:
            compId=st.text_input("Input component identifier:")
            if type(compId)==type(None) or len(compId)<1: #, value="20USBRT0731003")  
                st.write("Please input identifier")
                st.stop()
        elif 'list' in methodSel:
            ### get list of components
            if "compList" not in pageDict.keys():
                # later migration to filterMap
                # filterMap={'project':pageDict['projectCode'], 'currentLocation':[pageDict['institutionCode']], "state":"ready"}
                # pageDict['compList']=st.session_state.myClient.get('listComponents', json={'filterMap':filterMap}, True)
                pageDict['compList']=st.session_state.myClient.get('listComponents', json={'project':pageDict['projectCode'], 'currentLocation':pageDict['institutionCode']})
                if type(pageDict['compList'])!=type([]):
                    pageDict['compList']=pageDict['compList'].data

            else:
                st.write(f"Component list for {pageDict['institutionCode']} ({pageDict['projectCode']})")
                if st.button("rest component list"):
                    # later migration to filterMap
                    # filterMap={'project':pageDict['projectCode'], 'currentLocation':[pageDict['institutionCode']]}
                    # pageDict['compList']=st.session_state.myClient.get('listComponents', {'filterMap':filterMap}, True)
                    pageDict['compList']=st.session_state.myClient.get('listComponents', json={'project':pageDict['projectCode'], 'currentLocation':pageDict['institutionCode']})
                    if type(pageDict['compList'])!=type([]):
                        pageDict['compList']=pageDict['compList'].data

            compTypeSel=st.selectbox("Select component type:", set([ct['componentType']['code'] for ct in pageDict['compList'] if type(ct['componentType'])==type({}) and "code" in ct['componentType']]) )
            comp_subList=[ct['serialNumber'] for ct in pageDict['compList'] if type(ct)==type({}) if type(ct['componentType'])==type({}) and compTypeSel==ct['componentType']['code'] and ct['serialNumber']!=None]
            st.write(f"- found {len(comp_subList)} components")
            compId=st.selectbox("Select component:", comp_subList )
        else:
            st.write("select input method")

        ### automatic update (missing or empty) or on demand
        if "retComp" not in pageDict.keys() or st.button("reset component"):
            # st.write("prompt update")
            retComp=st.session_state.myClient.get('getComponent', json={'component': compId, 'noEosToken':False})
            pageDict['retComp']=retComp
            pageDict['retTests']=None
        elif type(pageDict['retComp'])==type({}) and pageDict['retComp']['serialNumber']!=compId:
            # st.write("mismatch update")
            retComp=st.session_state.myClient.get('getComponent', json={'component': compId, 'noEosToken':False})
            pageDict['retComp']=retComp
            pageDict['retTests']=None
        elif pageDict['retComp']==None:
            # st.write("None update")
            retComp=st.session_state.myClient.get('getComponent', json={'component': compId, 'noEosToken':False})
            pageDict['retComp']=retComp
            pageDict['retTests']=None
        else:
            pass

        ### catch nothing selected
        if "retComp" not in pageDict.keys():
            st.write("Input component info.")
            st.stop()

        ### announce component and offer full info.
        if pageDict['retComp']==None or type(pageDict['retComp'])!=type({}):
            st.write("Returned component doesn't look right :(")
            st.write(" - if _Granting token from EoS failed_ remove broken EOS links and try again")
            st.write(pageDict['retComp'])
            st.stop()

        ### announce component and offer full info.
        st.write(f"__Got {pageDict['retComp']['serialNumber']}__ ({pageDict['retComp']['code']})")
        check_comp=st.checkbox("See full component object")
        if check_comp:
            st.write("Full object")
            st.write(pageDict['retComp'])

        if objSel=="testRun":

            ### select test form list
            if "retTests" not in pageDict.keys() or pageDict['retTests']==None:
                retTests=st.session_state.myClient.get('listTestRunsByComponent', json={'filterMap':{'code': pageDict['retComp']['code']}} )
                if type(retTests)!=type([]):
                    retTests=retTests.data
                pageDict['retTests']=retTests
            
            st.write("### Select testRun")
            df_tests=pd.DataFrame(pageDict['retTests'])
            for c in df_tests.columns:
                df_tests[c]=df_tests[c].apply(lambda x: x['code'] if type(x)==type({}) and 'code' in x.keys() else x)

            cols=['id','testType','stage','state','institution','date','runNumber','passed','problems','cts']
            df_tests=df_tests[cols].sort_values('date',ascending=True).reset_index(drop=True)
            if st.checkbox("remove not ready",value=True):
                df_tests=df_tests.query('state=="ready"')
            st.write("Tests:",df_tests)
            selTestRun=st.selectbox("Select testRun (testType @ stage on cts):", options=df_tests.to_dict("records"), format_func=lambda x: f"{x['testType']} @ {x['stage']} on {x['cts']}")
            if type(selTestRun)==type({}) and "id" in selTestRun.keys():
                selTestRun=selTestRun['id']
            else:
                selTestRun=None
            # st.write(selTestRun)


        st.write("---")

        ##############
        ### select mode: upload/retrieve
        ##############
        modeList=['upload file to EOS','retrieve file from EOS']
        modeSel=st.radio("select function:",modeList)
        st.write("---")

        ##############
        ### read in image file
        ##############
        if "upload" in modeSel.lower():

            st.write("## Upload file")

            if "uploadList" in pageDict.keys():
                st.write(f"Files for upload: {len(pageDict['uploadList'])}")
            else:
                st.write("Add files for upload.")
                pageDict['uploadList']=[]

            if st.button("Clear list?"):
                st.write("List Cleared")
                pageDict['uploadList']=[]
            
            imageExts=['jpg','jpeg','png','tif','tiff']
            fileExts=['dat','sta','csv']
            uploaded_file = st.file_uploader("Upload image file", type=imageExts+fileExts )

            if uploaded_file is not None: 
                st.write("Got file "+uploaded_file.name)
                # pageDict['file']=uploaded_file
            # else:
            #     st.write("Upload file")
            #     st.stop()
        
                if any(ie in uploaded_file.type for ie in imageExts):
                    st.write(" - recognise image")
                    if st.checkbox("See image?"):
                        # st.write("details:",pageDict['file'])
                        # st.image(pageDict['file'])
                        st.write("details:",uploaded_file)
                        st.image(uploaded_file)
                else:
                    if st.checkbox("See file?"):
                        # st.write("details:",pageDict['file'])
                        # st.write(pageDict['file'].read())
                        st.write("details:",uploaded_file)
                        st.write(uploaded_file)

                if st.button("Add to upload list"):
                    # pageDict['uploadList'].append(pageDict['file'])
                    pageDict['uploadList'].append(uploaded_file)
                    st.write("Adding file to list.")
                    st.write(f" - total files now: {len(pageDict['uploadList'])}")

            if len(pageDict['uploadList'])<0:
                st.write("Add files for upload.")
                st.stop()
            else:
                if st.checkbox("See upload list?"):
                    st.write(f"Files for upload {len(pageDict['uploadList'])}:")
                    for e,upFile in enumerate(pageDict['uploadList'],1):
                        st.write(f"{e}. {upFile.name}")
                        if st.button("Remove file", key=f"file_{e}"):
                            pageDict['uploadList'].remove(upFile)
                            st.write("Removed file. Uncheck list to update")

            st.write("---")
            st.write("### Upload Details")

            if len(pageDict['uploadList'])>1:
                st.write("For multiple file uploads a tar file is recommended.")
                if st.checkbox("Use tar file?",value=True):
                    if "tarFile" not in pageDict.keys() or st.button("generate tar file"):
                        st.write("🚧 __Under Construction__ 🚧")
                        st.write("Please unselect")
                        st.stop()
                        # st.write(f" - __NB__ tarring {len(pageDict['uploadList'])} files for upload")
                        # # quick unique definition of tar file name
                        # tar_file_name=os.path.realpath(__file__)
                        # tar_file_name=tar_file_name[:tar_file_name.rfind('/')]+"/"+MakeUniqueName()+'.tar.gz'
                        # # Create a tar file and add the files
                        # with tarfile.open(tar_file_name, 'w:gz') as tar:
                        #     for file in pageDict['uploadList']:
                        #         tar.add(file.getvalue()) #, arcname=os.path.basename(file))        
                        # # Update the files dictionary to include the tar file
                        # pageDict['tarFile']=tar_file_name
                        # st.write(f" - tar file ready: {tar_file_name}")

            st.write("#### meta-data")
            descStr=st.text_input("Optional description:",value="from webApp "+datetime.datetime.now().strftime("%Y-%m-%dT%H:%MZ"))

            ### make json for upload
            pageDict['data_json'] = {
                "title": f"a {objSel} image attachment",
                "description": descStr,
                "url": "dummy",
                "type": "file"
            }

            if objSel=="component":
                pageDict['data_json']['component']= pageDict['retComp']['id']
            elif objSel=="testRun":
                pageDict['data_json']['testRun']= selTestRun
            else:
                st.write(f"unknown object: {objSel}")
                st.stop()

            ### review and edit
            chnx.ReviewAndEditChunk(pageDict,'data_json', False)

            # st.write(pageDict['file'].__dir__())

            if st.button("Upload Image"):
                st.write("Uploading files:",len(pageDict['uploadList']))

                # if tar file used 
                if "tarFile" in pageDict.keys():
                    st.write(f"__Upload file: {tar_file_name}__")
                    files = {"data": itkdb.utils.get_file_components({"data": open(tar_file_name, 'rb')})}
                
                    # do upload
                    try:            
                        if objSel=="component":
                            response = st.session_state.myClient.post("createComponentAttachment", data=pageDict['data_json'], files=files)
                        elif objSel=="testRun":
                            response = st.session_state.myClient.post("createTestRunAttachment", data=pageDict['data_json'], files=files)
                        st.write(f"{response}")
                        st.balloons()
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: File Upload **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                
                ### non-tar file
                else:
                    for upFile in pageDict['uploadList']:         
                        st.write(f"Upload file: {upFile.name}")
                        files = {"data": itkdb.utils.get_file_components({"data": upFile})}
                        pageDict['data_json']['title']=upFile.name
                        # do upload
                        try:            
                            if objSel=="component":
                                response = st.session_state.myClient.post("createComponentAttachment", data=pageDict['data_json'], files=files)
                            elif objSel=="testRun":
                                response = st.session_state.myClient.post("createTestRunAttachment", data=pageDict['data_json'], files=files)
                            st.write(f"{response}")
                            st.balloons()
                        except itkX.BadRequest as b:
                            st.write("### :no_entry_sign: File Upload **Unsuccessful**")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks


        ##############
        ### view available images in app
        ##############
        elif "retrieve" in modeSel.lower():
            
            st.write(f"## Check Attachments")

            if objSel=="component":
                st.write(f"__Got {pageDict['retComp']['id']}__ ({pageDict['retComp']['componentType']['code']})")
            
                if st.checkbox("See all attachment info?"):
                    st.write(pageDict['retComp']['attachments'])

                st.write(f"Found _total_ {len(pageDict['retComp']['attachments'])} attachments")
                df_attach=pd.DataFrame(pageDict['retComp']['attachments'])
            
            elif objSel=="testRun":

                if "retTest" not in pageDict.keys() or st.button("reset testRun") or pageDict['retTest']['id']!=selTestRun:
                    pageDict['retTest']=st.session_state.myClient.get('getTestRun', json={'testRun': selTestRun, 'noEosToken':False})

                st.write(f"__Got {pageDict['retTest']['id']}__ ({pageDict['retTest']['testType']['code']})")
                check_testRun=st.checkbox("See full testRun object")
                if check_testRun:
                    st.write("Full object")
                    st.write(pageDict['retTest'])

                if st.checkbox("See all attachment info?"):
                    st.write(pageDict['retTest']['attachments'])

                st.write(f"Found _total_ {len(pageDict['retTest']['attachments'])} attachments")
                df_attach=pd.DataFrame(pageDict['retTest']['attachments'])
            
            if df_attach.empty:
                st.write("No attachments found")
                st.stop()

            st.write(f"### {objSel} attachments")
            ### split by type
            df_eos=df_attach.query('type=="eos"')
            df_non=df_attach.query('type!="eos"')

            ### eos attachments not empty --> i.e. found
            if not df_eos.empty:
                st.write(f"__eos__ attachments ({len(df_eos.index)})",df_eos[['dateTime','title','description','filename','url']])

                # selEos=st.selectbox("Select eos file",df_attach.query('type=="eos"')['dateTime'].to_list())
                selEos=st.selectbox("Select eos file (title , description on dateTime):", options=df_eos.to_dict("records"), format_func=lambda x: f"{x['title']} , {x['description']} on {x['dateTime']}")
                # st.write(selEos)

                selUrl=selEos['url']
                selCode=selEos['code']
                st.write(f"download via link: {selUrl}")

                ##############
                ### select object type: component or testRun
                ##############
                fileTypeList=['image', 'non-image']
                fileTypeSel=st.radio("select file type:", fileTypeList)

                if st.checkbox(f"view {fileTypeSel}"):
                    try:
                        eosFile=st.session_state.myClient.get(selUrl)
                    except ConnectionError:
                        st.write("__Connection issue__ Try un-check and re-check")
                        st.stop()
                    if "non" in fileTypeSel.lower():
                        ### try converting to json
                        try:
                            eosDict = json.loads(eosFile.content.decode("utf-8"))
                            selKey=st.selectbox("Select key",list(eosDict.keys()))
                            st.write(eosDict[selKey])
                        except:
                            try:
                                st.write(eosFile.content.decode("utf-8"))
                            except:
                                st.write("__Reading issue__ Is the data file type correct?")
                    else:
                        try:
                            st.image(eosFile.content)
                        except:
                            st.write("__Reading issue__ Is the data file type correct?")

                if st.checkbox("Delete attachment?"):
                    st.write(f"Double check: delete attachment {selCode}")
                    if st.button("Delete!"):
                        if objSel=="testRun":
                            DeleteEOSLink(st.session_state.myClient, objSel, pageDict['retTest']['id'], selCode)
                        if objSel=="component":
                            DeleteEOSLink(st.session_state.myClient, objSel, pageDict['retComp']['id'], selCode)

            ### announce no eos found
            else:
                st.write("No __eos__ attachments found")

            ### view other attachments (if exist)
            if st.checkbox('see _non_ eos attachments?'):
                if  not df_non.empty:
                    st.write(f"__non eos__ attachments ({len(df_non.index)})",df_non[['dateTime','title','description','filename','contentType']])
                else:
                    st.write("No __non eos__ attachments found")

        else:
            st.write("Select mode")
