### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import copy
import altair as alt
import datetime
import numpy as np
import os
from io import StringIO
import io
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * review retrieved data & visualisation",
        " * upload test schemas to PDB",
        " * delete tests from PDB if required"]
#####################
### main part
#####################

class Page7(Page):
    def __init__(self):
        super().__init__("Single Stage Set", ":microscope: Set Stage Single component in PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        pageDict['project']=st.session_state.Authenticate['proj']['code']
        pageDict['institute']=st.session_state.Authenticate['inst']['code']

        ### get components
        if "compList" not in list(pageDict.keys()) or st.button("reset list"): # check list
            st.write("**Getting** list for "+pageDict['institute']+" ("+pageDict['project']+")")
            pageDict['compList']=st.session_state.myClient.get('listComponents',json={'currentLocation':pageDict['institute'], 'project':pageDict['project'] })
            if type(pageDict['compList'])!=type([]):
                pageDict['compList']=pageDict['compList'].data
        else:
            st.write("**Got** list for "+pageDict['institute']+" ("+pageDict['project']+")")

        ### cehck components found
        if len(pageDict['compList'])<1:
            st.write("__No componentTypes found at institute__")
            st.stop()

        ### make dataframe
        df_compList=pd.json_normalize(pageDict['compList'], sep = "_")

        # review components @ institute
        if st.checkbox(f"Review all {pageDict['institute']}({pageDict['project']})"):
            st.write("## Check componentTypes @ institute in PDB")
            df_compTypes=df_compList[['componentType_name','componentType_code']].drop_duplicates()
            stTrx.DebugOutput("component Types at institute:",df_compTypes)

            infra.MultiSelect(pageDict, 'selComps', df_compTypes['componentType_code'].astype(str).unique().tolist(), 'Select componentTypes:')
            queryStr=""
            for e,c in enumerate(pageDict['selComps']):
                if e>0:
                    queryStr+=' | '
                queryStr+='componentType_code=="'+c+'"'
            stTrx.DebugOutput("queryStr",queryStr)

            st.write("### List of *"+", ".join(pageDict['selComps'])+"* components")
            st.write("(colour--> _",'currentStage_code',"_)")
            if len(pageDict['selComps'])>0:
                df_comps=df_compList.query(queryStr).sort_values(by=['componentType_code','type_code','currentStage_code']).reset_index(drop=True)
            colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','assembled','cts']

            ### make all columns strings (avoid issues)
            for c in colz:
                df_comps[c]=df_comps[c].astype(str)

            infra.ToggleButton(pageDict,'removeAss',"Remove assembled components list?")
            if pageDict['removeAss']:
                df_comps=df_comps.query('assembled=="False"')

            try:
                st.dataframe(df_comps[colz].style.apply(stTrx.ColourCells, df=df_comps[colz], colName='currentStage_code', flip=True, axis=1))
            except IndexError:
                st.dataframe(df_comps[colz])

        # select parent
        st.write(f"## Select component from {pageDict['institute']} inventory")

        if "compKey" not in pageDict.keys():
            pageDict['compKey']="serialNumber"
        if st.session_state.debug:
            st.write("__DEBUG__ select alternative identity key:")
            infra.SelectBox(pageDict,'compKey',df_comps.columns,'Select key')
            st.write("unique values:",len(df_comps[pageDict['compKey']].unique()))
            st.write("total length:",len(df_comps))

        # infra.SelectBox(pageDict, 'compType', df_compList['componentType_code'].astype(str).unique().tolist(), 'Select componentType:')
        # infra.SelectBox(pageDict, 'compId', df_compList.query('componentType_code=="'+pageDict['compType']+'"')[pageDict['compKey']].astype(str).unique().tolist(), 'Select component '+pageDict['compKey']+':')

        pageDict['compType']=st.selectbox('Select componentType:', df_compList['componentType_code'].astype(str).unique().tolist())
        pageDict['compId']=st.selectbox('Select component:', df_compList.query('componentType_code=="'+pageDict['compType']+'"')[pageDict['compKey']].astype(str).unique().tolist())

        # check existing component tests
        st.write("## Stage Check")
        compVal=st.session_state.myClient.get('getComponent', json={'component':pageDict['compId']})
        # st.write("got comp:",compVal)
        df_cl=pdbTrx.FillCheckList(compVal)
        if df_cl.empty:
            st.write("Cannot generate Test Upload Summary :(") 
        else:
            st.write("Test Upload Summary (green: PASSED, red: FAILED, grey: MISSING)")
            sum_cols=['stage_order','stage_code','test_order','test_code','present','passed']
            st.write(df_cl[sum_cols].style.apply(pdbTrx.HighlightTests, axis=1))

        
        st.write("## Set Component Stage")
        st.write(f"__current stage: {compVal['currentStage']['code']}__")

        if "compTypeInfo" not in pageDict.keys() or pageDict['compTypeInfo']['code']!=compVal['componentType']['code']:
            pageDict['compTypeInfo']=st.session_state.myClient.get('getComponentTypeByCode', json={'code':compVal['componentType']['code'],'project':compVal['project']['code']}) 

        infra.SelectBox(pageDict, 'stage', [x['code'] for x in pageDict['compTypeInfo']['stages']], 'Select component stage to set')

        # st.write(df_cl['stage_code'].unique().tolist())

        if "putBack" not in pageDict.keys():
            pageDict['putBack']=False
        
        if st.button("Set Stage"):
            try:
                if compVal['currentStage']['code']!=pageDict['stage']:
                    ### set stage first!
                    st.write(f"Changing to set stage: {pageDict['stage']} (from {compVal['currentStage']['code']})")
                    if pdbTrx.SetCompStage(compVal['code'], pageDict['stage']):
                        pageDict['putBack']=compVal['currentStage']['code']
                else:
                    st.write(f"Already in appropriate stage: {pageDict['stage']}")
            except TypeError:
                st.write("No stage previously exists. Setting...")
                pdbTrx.SetCompStage(compVal['code'], pageDict['stage'])

        ### return to previous stage
        if pageDict['putBack']:
            if st.button(f"Return component to stage {pageDict['putBack']}"):
                pdbTrx.SetCompStage(compVal['code'], pageDict['putBack'])


