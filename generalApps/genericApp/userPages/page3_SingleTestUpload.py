### standard
import streamlit as st
from core.Page import Page
### custom
import os
import pandas as pd
import copy
import json
from io import StringIO
from datetime import datetime,date,time
from pathlib import Path
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################

infoList=[" * select component type",
        " * select component stage",
        " * select component testType",
        " * get testType schema from PDB",
        " * fill required fields",
        " * upload to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("Single Test", ":microscope: Upload Single test to PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        pageDict['project']=st.session_state.Authenticate['proj']['code']

        st.write("## Select Test Type")
        # review componentTypes and stages (from file)
        filePath="/code/commonCode/"
        stTrx.DebugOutput("filePath:",filePath)
        csvFileName="stageTestList_"+pageDict['project']+".csv"
        stTrx.DebugOutput("looking in: "+filePath[:filePath.rfind('/')])
        df_all=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+csvFileName)
        infra.SelectBox(pageDict,'componentType',df_all['compType'].unique(),"Select componentType code:")

        if "componentType" not in pageDict.keys():
            st.write("No componentType selected.")

        # catch empty stages
        if df_all.query('compType=="'+pageDict['componentType']+'"')['stage'].empty:
            st.write(f"No stages found for {pageDict['componentType']}. Cannot proceed")
            st.stop()

        # catch unset stages
        if df_all.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list()[0]!=df_all.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list()[0]:
            st.write(f"No stages found for {pageDict['componentType']}. Cannot proceed")
            st.stop()

        infra.SelectBox(pageDict,'stage',df_all.query('compType=="'+pageDict['componentType']+'"')['stage'].unique(),"Select stage code:")
        ### catch no tests (use self identity check)
        # st.write(df_all.query('compType=="'+pageDict['componentType']+'" & stage=="'+pageDict['stage']+'"')['testType'].to_list()[0])
        if df_all.query('compType=="'+pageDict['componentType']+'" & stage=="'+pageDict['stage']+'"')['testType'].to_list()[0]!=df_all.query('compType=="'+pageDict['componentType']+'" & stage=="'+pageDict['stage']+'"')['testType'].to_list()[0]:
            st.write("No test found @",pageDict['stage'])
            st.stop()
        infra.SelectBox(pageDict,'code',df_all.query('compType=="'+pageDict['componentType']+'" & stage=="'+pageDict['stage']+'"')['testType'].unique(),"Select testType code (in "+pageDict['stage']+"):")

        # st.write(df_all.query('stage=="'+pageDict['stage']+'"'))

        st.write("### stages and testTypes for "+pageDict['componentType'])
        df_stageTest=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+csvFileName).query('compType=="'+pageDict['componentType']+'"').reset_index(drop=True)
        try:
            st.dataframe(df_stageTest[['compType','stage','altStage','testType']].style.apply(stTrx.ColourCells, df=df_stageTest[['compType','stage','altStage','testType']], colName='stage', flip=True, axis=1))
        except IndexError:
            st.dataframe(df_stageTest)

        # get test schema
        if "origSchema" not in pageDict.keys() or pageDict['testSchema']['testType']!=pageDict['code'] or st.button("Reset Schema: "+pageDict['code']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['origSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['code'], 'requiredOnly':False})
            pageDict['reqSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['code'], 'requiredOnly':True})
            pageDict['testSchema']=copy.deepcopy(pageDict['origSchema'])
            pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']

        # show schema
        stTrx.DebugOutput("Original *schema*:",pageDict['origSchema'])
        if st.checkbox("Show PDB schema?",value=False):
            if "only" in st.radio('',["only required","include options"]):
                st.write(pageDict['reqSchema'])
            else:
                st.write(pageDict['origSchema']) 

        #######################
        ### Enter Information
        #######################

        ### select mode
        sel_mode= st.radio("Select mode:", ["pre-formatted json", "manual upload"])  

        ### pre-formatted json
        if "json" in sel_mode.lower():
            st.write("#### Upload using pre-formatted json")

            ## drag and drop method
            pageDict['file']= st.file_uploader("Upload data file", type=["json"])
            if st.session_state.debug: 
                st.write(pageDict['file'])

            ### offer example if no file
            if pageDict['file'] is None:
                ### delete existing info (defined below)
                try:
                    del pageDict['regList']
                except KeyError:
                    pass
                st.write("No data file set")
                # filePath=os.path.realpath(__file__)
                # exampleFileName= pageDict["code"] + "_example.json"
                # if st.session_state.debug:
                #     st.write("looking in:",filePath[:filePath.rfind('/')])
                #     st.write(os.listdir(filePath[:filePath.rfind('/')]))
                # st.download_button(label="Download example", data=Path(filePath[:filePath.rfind('/')]+"/"+exampleFileName).read_text(), file_name=exampleFileName)
                st.stop()

            ### read in formatted json file
            if "json" in pageDict['file'].type:
                st.write("_json_ file recognised ")
                data=json.loads(StringIO(pageDict['file'].getvalue().decode("utf-8")).read())
            ### unknown file type
            else:
                st.write(f"_{pageDict['file'].type}_ file unrecognised. Please use _json_.")
                st.stop()

            # ### Upload JSON
            # st.write("A pre-formatted json can be uploaded if available.")
            # exampleFileName = pageDict["code"] + "_example.json"
            # df_input = chnx.UploadJSONChunk(pageDict, exampleName=exampleFileName, exampleDict=pageDict['testSchema'])
            # if df_input:
            #     pageDict['testSchema'] = df_input

            ### add gleaned information to test schema
            # header
            pageDict['testSchema']=data

        ### manual upload
        elif "manual" in sel_mode.lower():
            st.write("#### Manual Upload")

            # manually set parameters
            chnx.SelectObjectParameters(pageDict, 'testSchema')

        ### no choice
        else:
            st.write("No mode selected")
            st.stop()
                
        ### remove flagged parameters
        pageDict['testSchema']= chnx.RemoveKeys(pageDict['testSchema']) 
        # st.write(pageDict['testSchema'])
        

       ### show/edit data for upload
        st.write("---")
        st.write("### Review Test Schema")
        infra.ToggleButton(pageDict,'toggleEdit','Edit Schema?')
        if pageDict['toggleEdit']:
            st.write("### Edit Schema")
            pageDict['testSchema']=EditJson(pageDict['testSchema'])
        else:
            st.write(pageDict['testSchema'])

        infra.ToggleButton(pageDict,'toggleText','Convert values to text?')
        if pageDict['toggleText']:
            pageDict['testSchema'] = json.loads(json.dumps(pageDict['testSchema']), parse_int=str)
            pageDict['testSchema']['results'] = json.loads(json.dumps(pageDict['testSchema']['results']), parse_float=str)
        if st.session_state.debug:
            st.write("### **DEBUG** (final!) Test Schema")
            st.write(pageDict['testSchema'])

        st.write("---")
        st.write("## Test Registration")

        # # check existing component tests
        # chnx.StageCheck(pageDict)

        # upload(!) test schema: change get --> post
        chnx.RegChunk(pageDict, "TestRun", "testSchema")
