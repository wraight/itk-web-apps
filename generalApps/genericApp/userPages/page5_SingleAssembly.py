### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import copy
import altair as alt
import datetime
import numpy as np
import os
from io import StringIO
import io
### PDB stuff
import itkdb
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * review retrieved data & visualisation",
        " * upload test schemas to PDB",
        " * delete tests from PDB if required"]
#####################
### main part
#####################

class Page5(Page):
    def __init__(self):
        super().__init__("Single Assemble", ":microscope: Assemble Single component in PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        pageDict['project']=st.session_state.Authenticate['proj']['code']
        pageDict['institute']=st.session_state.Authenticate['inst']['code']

        # review components @ institute
        st.write("## Check componentTypes @ institute in PDB")
        if st.button("reset list") or "compList" not in list(pageDict.keys()): # check list
            st.write("**Getting** list for "+pageDict['institute']+" ("+pageDict['project']+")")
            pageDict['compList']=st.session_state.myClient.get('listComponents',json={'currentLocation':pageDict['institute'], 'project':pageDict['project'] })
            if type(pageDict['compList'])!=type([]):
                pageDict['compList']=pageDict['compList'].data
        else:
            st.write("**Got** list for "+pageDict['institute']+" ("+pageDict['project']+")")
        
        ### check components found
        if len(pageDict['compList'])<1:
            st.write("__No componentTypes found at institute__")
            st.stop()

        ### make dataframe
        df_compList=pd.json_normalize(pageDict['compList'], sep = "_")
        df_compTypes=df_compList[['componentType_name','componentType_code']].drop_duplicates()
        stTrx.DebugOutput("component Types at institute:",df_compTypes)

        infra.MultiSelect(pageDict, 'selComps', df_compTypes['componentType_code'].astype(str).unique().tolist(), 'Select componentTypes:')
        queryStr=""
        for e,c in enumerate(pageDict['selComps']):
            if e>0:
                queryStr+=' | '
            queryStr+='componentType_code=="'+c+'"'
        stTrx.DebugOutput("queryStr",queryStr)

        if len(pageDict['selComps'])>0:
            st.write("### List of *"+", ".join(pageDict['selComps'])+"* components")
        else:
            st.write("### List of components")
        st.write("(colour--> _",'currentStage_code',"_)")
        df_comps=df_compList.sort_values(by=['componentType_code','type_code','currentStage_code']).reset_index(drop=True)
        if len(pageDict['selComps'])>0:
            df_comps=df_compList.query(queryStr).sort_values(by=['componentType_code','type_code','currentStage_code']).reset_index(drop=True)
        colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','assembled','cts']

        ### make all columns strings (avoid issues)
        for c in colz:
            df_comps[c]=df_comps[c].astype(str)

        infra.ToggleButton(pageDict,'removeAss',"Remove assembled components list?")
        if pageDict['removeAss']:
            df_comps=df_comps.query('assembled=="False"')

        try:
            st.dataframe(df_comps[colz].style.apply(stTrx.ColourCells, df=df_comps[colz], colName='currentStage_code', flip=True, axis=1))
        except IndexError:
            st.dataframe(df_comps[colz])

        # select parent
        st.write("## Select parent component")

        if "compKey" not in pageDict.keys():
            pageDict['compKey']="serialNumber"
        if st.session_state.debug:
            st.write("__DEBUG__ select alternative identity key:")
            infra.SelectBox(pageDict,'compKey',df_comps.columns,'Select key')
            st.write("unique values:",len(df_comps[pageDict['compKey']].unique()))
            st.write("total length:",len(df_comps))

        infra.SelectBox(pageDict, 'parentType', df_compTypes['componentType_code'].astype(str).unique().tolist(), 'Select parent componentType:')
        infra.SelectBox(pageDict, 'parentComp', df_comps.query('componentType_code=="'+pageDict['parentType']+'"')[pageDict['compKey']].astype(str).unique().tolist(), 'Select parent component '+pageDict['compKey']+':')

        if st.button("Add selected component as Parent?"):
            pageDict['comb']={'SN':pageDict['parentComp'],'compType':pageDict['parentType']}

        if 'comb' not in pageDict.keys():
            st.write("No parent selected")
            st.stop()

        # select children
        st.write("## Select child components")
        if 'children' not in pageDict.keys():
            pageDict['children']=[]
        st.write("### "+str(len(pageDict['children']))+" children selected")
        infra.SelectBox(pageDict, 'childType', df_compTypes['componentType_code'].astype(str).unique().tolist(), 'Select child componentType:')
        infra.MultiSelect(pageDict, 'childComps', df_comps.query('componentType_code=="'+pageDict['childType']+'"')[pageDict['compKey']].astype(str).unique().tolist(), 'Select child components '+pageDict['compKey']+':')

        if st.button("Add selected components as Children?"):
            if "parts" not in pageDict.keys():
                pageDict['parts']=[]
            for x in pageDict['childComps']:
                if x in [p['SN'] for p in pageDict['parts']]:
                    continue
                pageDict['parts'].append({'SN':x,'compType':pageDict['childType']})

        if 'parts' not in pageDict.keys():
            st.write("No children selected")
            st.stop()

        ### add slots
        infra.ToggleButton(pageDict,'addSlots',"Add slots?")

        if pageDict['addSlots']:
            doSlots=False
            try:
                combType=st.session_state.myClient.get('getComponentTypeByCode', json={"project":pageDict['project'], "code":pageDict['comb']['compType']} )
                slots=len(combType['children'])
                doSlots=True
            except KeyError:
                st.write("no children information found for componentType:",pageDict['comb']['compType'])
            # if slots to be done
            if doSlots:
                for part in pageDict['parts']:
                    selPos=st.selectbox(part['SN']+" slot:",[x for x in range(0,slots,1)], key=part['SN']+'_pos')
                    ind=[x['SN'] for x in pageDict['parts']].index(part['SN'])
                    try:
                        if pageDict['parts'][ind]['slot']==selPos:
                            continue
                        else:
                            if selPos in [x['slot'] for x in pageDict['parts']]:
                                st.write("**position already filled** choose again")
                            else:
                                pageDict['parts'][ind]['slot']=selPos
                    except KeyError:
                        pageDict['parts'][ind]['slot']=selPos


        ### review
        st.write("### Selected components")
        st.write("**Parent**")
        st.dataframe([pageDict['comb']])
        st.write("**Children**")
        st.dataframe(pageDict['parts'])

        if st.button("clear children"):
            pageDict['parts']=[]
            st.stop()

        if st.button("Assemble"):
            ### assembly
            if 'slot' in pageDict['parts'][0].keys():
                assRet=chnx.AssembleChunk({'comb':pageDict['comb'],'parts':pageDict['parts']},True)
            else:
                assRet=chnx.AssembleChunk({'comb':pageDict['comb'],'parts':pageDict['parts']})
            pageDict['assembly']=assRet

        if "assembly" not in pageDict.keys():
            stTrx.DebugOutput("no assembly key yet")
            st.stop()
        
        if not pageDict['assembly']:
            st.stop()

        st.write("Assembled component:")
        infra.ToggleButton(pageDict,'togAss',pageDict['comb']['SN'])

        if pageDict['togAss']:
            combComp=st.session_state.myClient.get('getComponent', json={"component" : pageDict['comb']['SN']} )
            st.write("Children:",[x['component']['serialNumber'] for x in combComp['children'] if x['component']!=None])
            stTrx.DebugOutput("Children details: ",combComp['children'])

            if st.button("disassemble?"):
                chnx.DisassembleComponent(combComp)
