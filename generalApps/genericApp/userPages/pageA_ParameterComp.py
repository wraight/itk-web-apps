### standard
import datetime

import altair as alt
import core.stInfrastructure as infra
### PDB stuff
import numpy as np
### custom
import pandas as pd
import streamlit as st
from core.Page import Page

import commonCode.StreamlitTricks as stTrx

### analyses

#####################
### useful functions
#####################

combCollection=[]
def GetIndex(inst,sn):
    global combCollection
    if inst not in set([cc['inst'] for cc in combCollection]): ### no inst found
        combCollection.append({'inst':inst,'SN':sn})
        return 0
    else:
        instCollection=[cc for cc in combCollection if cc['inst']==inst] ### get inst collection
        if sn not in set([ic['SN'] for ic in instCollection]): ### no SN found
            combCollection.append({'inst':inst,'SN':sn})
            return 0
        else:
            SNCollection=[ic for ic in instCollection if ic['SN']==sn]
            combCollection.append({'inst':inst,'SN':sn})
            return len(SNCollection)
    return -1

### align array length for plotting.
### from: https://gitlab.cern.ch/wraight/itk-reports/-/blob/master/notebooks/ParameterConsistency-tutorial.ipynb
def AlignLengths(arr1,arr2, output=False):
    try:
        if len(arr1)>len(arr2):
            arr1=arr1[0:len(arr2)]
            if output:
                st.write("array length adjusted")
        else:
            arr1
            # st.write("array length ok")
    except TypeError:
        st.write("TypeError arr1:",type(arr1))
        st.write("TypeError arr2:",type(arr2))
    except ValueError:
        st.write("ValueError arr1:",arr1)
        st.write("ValueError arr2:",arr2)
    return arr1

### check page dictionary and return true if differences found
def CheckPageDict(pageDict,keyCheck,keyValChecks):
    # check key=keyCheck exists in dictionary
    if keyCheck!=None and keyCheck not in pageDict.keys():
        # alert if changes made
        return True
    else:
        # loop through dictionary to check values unchanged
        kvCheck=False
        for k,v in keyValChecks.items():
            try:
                if pageDict[k]!=v:
                    kvCheck=True
            except KeyError:
                kvCheck=True
        if kvCheck:
            # alert if changes made
            return True
    # no changes made
    return False

### update page dictionary
def UpdatePageDict(pageDict,keyValChecks):
    # loop through dictionary to check values unchanged
    for k,v in keyValChecks.items():
        pageDict[k]=v
        
### define function to check identifier uniqueness
def GetIdCol(df_test, extraCols=None):

    idCols=["serialNumber","alternativeIdentifier"]
    if extraCols!=None:
        idCols=idCols[0]+extraCols+idCols[1::]
    for idc in idCols: # order important!
        # print(f"trying idCol={idc}")
        try:
            if len(df_test[idc].unique())==1 and df_test[idc][0]==None:
                # print(f"{idc} values empty. Switching")
                continue
            else:
                # print(f"{idc} is non-empty")
                return idc
        except KeyError:
            print(f"{idc} key missing")
            pass
    ### if no other id found...
    return "code"

infoList=[" * check test parameters for componentType at institute(s)",
        " * select componentType at institute",
        " * select stage and testType (from test schema)",
        " * select parameter (currently no arrays supported)",
        " * visualise and download csv"]
#####################
### main part
#####################

class Page11(Page):
    def __init__(self):
        super().__init__("ParameterComp", ":microscope: Compare test parameter of components at institute(s)", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        
        # review componentTypes and stages (from file) using projCode from LHS bar
        filePath="/code/commonCode/"
        stTrx.DebugOutput("filePath:",filePath)
        csvFileName="stageTestList_"+st.session_state.Authenticate['proj']['code']+".csv"
        stTrx.DebugOutput("looking in: "+filePath[:filePath.rfind('/')])
        df_all=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+csvFileName)

        ### select component code
        sel_compCode=st.selectbox("Select componentType code:",df_all['compType'].unique())

        # get stages for component
        compStages=df_all.query('compType=="'+sel_compCode+'"')['stage'].dropna().unique()
        stTrx.DebugOutput("stages for component:",compStages)
        ### select stages (if present) 
        if len(compStages)>0:
            # infra.SelectBox(pageDict,'stage',compStages,"Select stage code:")
            sel_stage=st.selectbox("Select stage code:",compStages)
            stageTests=df_all.query('compType=="'+sel_compCode+'" & stage=="'+sel_stage+'"')['testType'].dropna().unique()
            # get tests for stage
            stTrx.DebugOutput("tests for stage:",stageTests)
            ### select test (if present) 
            if len(stageTests)>0:
                # infra.SelectBox(pageDict,'testCode',stageTests,"Select testType code (in "+pageDict['stage']+"):")
                sel_testCode=st.selectbox("Select testType code (in "+sel_stage+"):",stageTests)

        ### show stages and testTypes dataframe for compType
        st.write("### stages and testTypes for "+sel_compCode)
        df_stageTest=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+csvFileName).query('compType=="'+sel_compCode+'"').reset_index(drop=True)
        try:
            st.write(df_stageTest[['compType','stage','altStage','testType']].style.apply(stTrx.ColourCells, df=df_stageTest[['compType','stage','altStage','testType']], colName='stage', flip=False, axis=1))
        except IndexError:
            st.write("Too many varieties to colour\n - proceeding in monochrome")
            st.write(df_stageTest[['compType','stage','altStage','testType']])

        ### get components from PDB
        st.write(f"### Check PDB for componentType:{sel_compCode} @ {st.session_state.Authenticate['inst']['code']}")
        if st.button("reset componentlist") or CheckPageDict(pageDict,"compList",{'instCode':st.session_state.Authenticate['inst']['code'], 'projCode':st.session_state.Authenticate['proj']['code'], 'compCode':sel_compCode}): # check list
            UpdatePageDict(pageDict,{'instCode':st.session_state.Authenticate['inst']['code'], 'projCode':st.session_state.Authenticate['proj']['code'], 'compCode':sel_compCode})
            st.write(f"**Getting** list for {pageDict['instCode']} ({pageDict['projCode']})")
            pageDict['compList']=st.session_state.myClient.get('listComponents',json={'currentLocation':pageDict['instCode'], 'project':pageDict['projCode'], 'componentType':pageDict['compCode'] })
            if type(pageDict['compList'])!=type([]):
                pageDict['compList']=pageDict['compList'].data
        else:
            st.write(f"**Got** list for {pageDict['instCode']} ({pageDict['projCode']})")

        ### check component cache list
        if len(pageDict['compList'])<1:
            st.write(f"No {pageDict['compCode']} components found at {pageDict['instCode']} ({pageDict['projCode']})")
            st.stop()
        if len(compStages)<1:
            st.write("No defined stages")
            st.stop()
        if len(stageTests)<1:
            st.write("No defined tests")
            st.stop()

        ### make dataframe to show components
        df_compList=pd.json_normalize(pageDict['compList'], sep = "_")
        st.write(f"{len(df_compList['serialNumber'].to_list())} {pageDict['compCode']}s found")
        stTrx.Stringify(df_compList)
        ### summarise columns
        st.dataframe(df_compList[['serialNumber','alternativeIdentifier','componentType_code','currentStage_code','code']])


        ### find matching tests
        st.write(f"### Find Matching {sel_testCode} Tests")
        if st.button("re-check matching tests") or CheckPageDict(pageDict,"testCode",{'testCode':sel_testCode}):
            UpdatePageDict(pageDict,{'testCode':sel_testCode})
            ### getComponentsBulk
            foundComps=[x['code'] for x in pageDict['compList']]
            st.write("Search for",len(foundComps),"components for",pageDict['testCode'])
            pageDict['testDict']={}
            for s in [pageDict['testCode']]:
                pageDict['testDict'][s]=[]
            pageDict['matchedTestRuns']=[]

            chunk=100
            for x in range(0,int(np.ceil(len(foundComps)/chunk))):
                st.write(f"component loop {x}: [{x*chunk}:{(x+1)*chunk}]")
                foundList=st.session_state.myClient.get('getComponentBulk',json={'component':foundComps[x*chunk:(x+1)*chunk]})
                df_foundList=pd.DataFrame(foundList)
                # st.dataframe(df_foundList)

                for i,row in df_foundList.iterrows():
                    if len(row['tests'])<1:
                #         st.write("no tests")
                        continue
                    for myTestTypeCode in pageDict['testDict'].keys():
                        compMatchedRuns=pd.DataFrame(row['tests']).query('code=="'+myTestTypeCode+'"').explode('testRuns')['testRuns'].to_list()
                        if len(compMatchedRuns)<1:
                            # st.write("no matching testRuns:",myTestTypeCode)
                            continue
                        if st.session_state.debug:
                            if row['serialNumber']!=None:
                                st.write(f"{row['serialNumber']} has {len(compMatchedRuns)} matched testRuns")
                            else:
                                st.write(f"{row['alternativeIdentifier']} has {len(compMatchedRuns)} matched testRuns")
                        pageDict['testDict'][myTestTypeCode].extend([cmr['id'] for cmr in compMatchedRuns])
                        pageDict['matchedTestRuns'].extend(compMatchedRuns)
                st.write(f"update matched testRuns: {len(pageDict['matchedTestRuns'])}")
            st.write(f"final matched testRuns: {len(pageDict['matchedTestRuns'])}")

            ### also get testSchema for later parameter selection
            pageDict['origSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['projCode'], 'componentType':pageDict['compCode'], 'code':pageDict['testCode'], 'requiredOnly':True})
            ### also reset test run list (induce re-query later)
            pageDict['testRuns']=[]

        ### check matched runs cache list
        if "matchedTestRuns" not in pageDict.keys():
            st.write("Please run: match tests")
            st.stop()
        if len(pageDict['matchedTestRuns'])<1:
            st.write("No matched tests found")
            st.stop()
        else:
            st.write(f"Matched testRuns: {len(pageDict['matchedTestRuns'])}")
            st.dataframe(pd.DataFrame([{'test':k,'count':len(v)} for k,v in pageDict['testDict'].items()]))


        ### retrieve testRuns - wait for prompt as maybe large query
        st.write(f"### Retrieve {pageDict['testCode']} testRuns")
        if st.button("get testRuns") or len(pageDict['testRuns'])<1:
            pageDict['testRuns']=[]

            chunk=100
            for x in range(0,int(np.ceil(len(pageDict['matchedTestRuns'])/chunk))):
                st.write(f"testRun loop {x}: [{x*chunk}:{(x+1)*chunk}]")
                testRunChunk=st.session_state.myClient.get('getTestRunBulk',json={'testRun':[m['id'] for m in pageDict['matchedTestRuns'][x*chunk:(x+1)*chunk]]})
                if type(testRunChunk)!=type([]):
                    testRunChunk=testRunChunk.data
                pageDict['testRuns'].extend(testRunChunk)
                st.write(f"update testRun results: {len(pageDict['testRuns'])}")
            st.write(f"final testRun results: {len(pageDict['testRuns'])}")

        ### check test runs cache list
        if "testRuns" not in pageDict.keys():
            st.write("Please run: get tests")
            st.stop()
        if len(pageDict['testRuns'])<1:
            st.write("No test runs found")
            st.stop()
        else:
            st.write(f"Retrieved testRuns: {len(pageDict['testRuns'])}")


        st.write("### Select test parameters")
        # get test schema
        # if st.button("Reset Schema: "+pageDict['testCode']+"@"+pageDict['stage']+" for "+pageDict['compCode']) or CheckPageDict(pageDict,"origSchema",{'testCode':pageDict['origSchema']['testType']}): # check list
        #     pageDict['origSchema'] = st.session_state.myClient.get('generateTestTypeDtoSample', json={'project':pageDict['projCode'], 'componentType':pageDict['compCode'], 'code':pageDict['testCode'], 'requiredOnly':True})
        infra.ToggleButton(pageDict,'togShowSchema',"Show test schema?")
        if pageDict['togShowSchema']:
            st.write(pageDict['origSchema'])

        singleList=[]
        arrayList=[]
        noneList=[]
        # check schema for type
        for x in ['properties','results']:
            try:
                noneList=noneList+ [k for k in pageDict['origSchema'][x].keys() if pageDict['origSchema'][x][k]==None]
                arrayList=arrayList+ [k for k in pageDict['origSchema'][x].keys() if type(pageDict['origSchema'][x][k])==type([]) or type(pageDict['origSchema'][x][k])==type({})]
                singleList=singleList+ [k for k in pageDict['origSchema'][x].keys() if type(pageDict['origSchema'][x][k])!=type([]) and type(pageDict['origSchema'][x][k])!=type({}) and pageDict['origSchema'][x][k]!=None]
            except KeyError:
                pass
        # check data for remaining "nones"
        if len(noneList)>0:
            # st.write("checking data for type of *NULL*s:",noneList)
            for n in noneList:
                try:
                    propCheck=next((item for item in pageDict['testRuns'][0]['properties'] if item['code']==n), None)
                except TypeError:
                    st.write("__Trouble getting property information__ 😮")
                    st.stop()
                if propCheck!=None:
                    # st.write("property found:",propCheck)
                    if type(propCheck['value'])==type([]) or type(propCheck['value'])==type({}):
                        arrayList.append(n)
                    else:
                        singleList.append(n)
                else:
                    resCheck=next((item for item in pageDict['testRuns'][0]['results'] if item['code']==n), None)
                    if resCheck!=None:
                        # st.write("result found:",resCheck)
                        if type(resCheck['value'])==type([]) or type(resCheck['value'])==type({}):
                            arrayList.append(n)
                        else:
                            singleList.append(n)
                    else:
                        continue
                        # st.write(f"matching {n} key found")

        st.write(pd.DataFrame([{'type':"single",'keys':singleList},{'type':"array",'keys':arrayList},{'type':"none",'keys':noneList}]))
        # st.write("arrays:",arrayList)
        # st.write("singles:",singleList)
        radioTypes=[]
        if len(singleList)>0:
            radioTypes.append("single-like")
        if len(arrayList)>0:
            radioTypes.append("array-like")
            radioTypes.append("comb. array")

        if len(radioTypes)<1:
            st.write("__No parameters to visualise__")
            st.stop()

        # infra.Radio(pageDict,'sel_likeType',["single-like", "array-like", "comb. array"],"Select value type:")
        sel_likeType=st.radio("Select value type:",radioTypes)

        if sel_likeType=="single-like":
            # infra.SelectBox(pageDict,'paraCode',singleList,'Select test parameter:')
            sel_paraCode=st.selectbox('Select test parameter:',singleList)
        elif sel_likeType=="array-like":
            # infra.SelectBox(pageDict,'paraCode',arrayList,'Select test parameter:')
            sel_paraCode=st.selectbox('Select test parameter:',arrayList)
        else:
            # infra.SelectBox(pageDict,'paraCodeX',arrayList,'Select test X parameter:')
            # infra.SelectBox(pageDict,'paraCodeY',arrayList,'Select test Y parameter:')
            sel_paraCodeX=st.selectbox('Select test X parameter:',arrayList)
            sel_paraCodeY=st.selectbox('Select test Y parameter:',arrayList)
            # infra.SelectBox(pageDict,'paraCodeY',[a for a in arrayList if a!=sel_paraCodeX],'Select test Y parameter:')


        ### formatting
        st.write("### Format data")
        # st.write(f"checking {sel_paraCode} ({sel_likeType})")
        df_testRuns=pd.DataFrame(pageDict['testRuns'])
        # st.write(df_testRuns.columns)
        df_testRuns=df_testRuns.query('state=="ready"')[['components','institution','date','properties','results']]
        # df_testRuns['components']=df_testRuns['components'].apply(lambda x: x['serialNumber'])
        df_testRuns['institution']=df_testRuns['institution'].apply(lambda x: x['code'])
        df_testRuns['date']= pd.to_datetime(df_testRuns['date'],format='%Y-%m-%dT%H:%M:%S.%f')

        df_testRuns=df_testRuns.explode('components')
        for col in ['serialNumber','alternativeIdentifier','code']:
            try:
                df_testRuns[col]=df_testRuns['components'].apply(lambda x: x[col])
            except KeyError:
                pass
        df_testRuns['stage']=df_testRuns['components'].apply(lambda x: x['testedAtStage']['code'])

        ### decide identifier column
        idCol=GetIdCol(df_testRuns)

        dfList=[]
        inCols=['properties','results']
        for pr in inCols:
            if df_testRuns[pr].isnull().all():
                st.write(f"{pr} is empty. skipping")
                continue
            df_pr=df_testRuns.explode(pr)
            for k in ['code','value']:
                df_pr[k]=df_pr[pr].apply(lambda x: x[k] if type(x)==type({}) and k in x.keys() else None)
                df_pr['paraCode']=df_pr[pr].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else None)
                df_pr['paraValue']=df_pr[pr].apply(lambda x: x['value'] if type(x)==type({}) and "value" in x.keys() else None)
                df_pr['valueType']=df_pr[pr].apply(lambda x: x['valueType'] if type(x)==type({}) and "valueType" in x.keys() else None)

            df_pr=df_pr.drop(columns=inCols)
            dfList.append(df_pr)
        df_testRuns=pd.concat(dfList).reset_index(drop=True)

        
        if st.session_state.debug:
            st.write("all codes:")
            st.write(df_testRuns[[item for item in df_testRuns.columns if item not in ["properties","results","components"]]])

        # identifier per test
        # df_testRuns=df_testRuns.reset_index(drop=True).reset_index()
        if sel_likeType=="single-like" or sel_likeType=="array-like":
            df_testRuns=df_testRuns.query('paraCode=="'+sel_paraCode+'"')
        else:
            df_testRuns=df_testRuns.query('paraCode=="'+sel_paraCodeX+'" | paraCode=="'+sel_paraCodeY+'"')
        
        try:
            df_testRuns['testIndex']=df_testRuns.apply(lambda row: GetIndex(row['institution'],row[idCol]), axis=1)
        except ValueError:
            st.write(":confused: Difficult parameter. Try another")
            st.stop()

        colSet=[item for item in df_testRuns.columns if item not in ["properties","results","components"]]
        df_testRuns=df_testRuns[colSet]
        # st.write(df_testRuns.columns)

        ### select specific serialNumbers if required
        if st.checkbox("Select specific components?"):
            compIds=st.multiselect("Select components:",df_testRuns[idCol].unique())
            df_testRuns=df_testRuns.query(f'{idCol} in @compIds')

        if df_testRuns.empty:
            st.write("🛑 __No components selected__")
            st.stop()


        # set tool tip info (overlap generic list with df columns)
        toolDict={'date:':"T",'paraValue':"Q",'institution':"N",'alternativeIdentifier':"N",'stage':"N", 'serialNumber':"N"}
        toolList=[f'{k}:{v}' for k,v in toolDict.items() if k in list(df_testRuns.columns)]

        st.write("### Visualisation")

        ### single-like
        if sel_likeType=="single-like":
            df_sub=df_testRuns.query('paraCode=="'+sel_paraCode+'"')

            ### timeline plot
            timeline=alt.Chart(df_sub).mark_point().encode(
                x=alt.X('date:T'),
                y=alt.Y('paraValue:Q', title=sel_paraCode),
                color=alt.Color('institution:N', title="measurement institution"),
                shape=alt.Shape('stage:N'),
                tooltip=toolList
            ).properties(
                width=800,
                title=sel_paraCode+" timeline"
            )#.interactive()
            # st.altair_chart(timeline)

            ### histogram plot
            hist=alt.Chart(df_testRuns.query('paraCode=="'+sel_paraCode+'"')).mark_bar().encode(
                alt.X('paraValue:Q', bin=True, title=sel_paraCode),
                y='count()',
                color=alt.Color('institution:N', title="measurement institution"),
                tooltip=[tl for tl in toolList if "date" not in tl]
            ).properties(
                width=800,
                title=sel_paraCode+" distribution"
            )#.interactive()
            # st.altair_chart(hist)
  
            ### combined plot
            brush = alt.selection(type='interval')
            combHist=alt.vconcat(
                timeline.encode(
                x=alt.X('date:T'),
                y=alt.Y('paraValue:Q', title=sel_paraCode),
                color=alt.Color('institution:N', title="measurement institution"),
                shape=alt.Shape('stage:N'),
                tooltip=toolList
                ).properties(
                    width=600,
                    title=sel_paraCode+" timeline (select points for histogram)"
                ).add_selection(brush),
                hist.encode(
                    x=alt.X('paraValue:Q',
                        bin=alt.Bin(maxbins=30)
                    #     , extent=brush),
                    #     scale=alt.Scale(domain=brush)
                    ),
                    y=alt.Y('count()'),
                    color=alt.Color('institution:N', title="measurement institution"),
                    tooltip=[tl for tl in toolList if "date" not in tl]
                ).properties(
                    width=600,
                    title=sel_paraCode+" distribution (of selected points)"
                ).transform_filter(brush)
            )

            st.altair_chart(combHist)
            st.dataframe(df_sub)
            st.download_button(label="Write csv", data=df_sub.to_csv(index=False),file_name="testResults_"+pageDict['projCode']+"_"+pageDict['compCode']+"_"+pageDict['testCode']+"_"+sel_paraCode+".csv")

        ### array-like
        elif sel_likeType=="array-like":
            df_sub=df_testRuns.query('paraCode=="'+sel_paraCode+'"')
            titleY=sel_paraCode
            # st.write(df_sub['paraValue'].iloc[0])
            if type(df_sub['paraValue'].iloc[0])==type({}):
                st.write("(sigh!) This is a dictionary. Please choose a key:")
                infra.SelectBox(pageDict,'keyCode',list(df_sub['paraValue'].iloc[0].keys()),'Select test parameter:')
                df_sub['paraValue']=df_sub['paraValue'].apply(lambda x: x[pageDict['keyCode']] if type(x)==type({}) and pageDict['keyCode'] in x.keys() else None)
                titleY=pageDict['keyCode']
            df_array=df_sub.explode('paraValue')

            timeline=alt.Chart(df_array).mark_point().encode( #.mark_boxplot(extent='min-max').encode( #.mark_point().encode(
                x=alt.X('date:T'),
                y=alt.Y('paraValue:Q', title=titleY),
                color=alt.Color('institution:N', title="measurement institution"),
                tooltip=toolList
            ).properties(
                width=800,
                title=sel_paraCode+", "+titleY+" timeline"
            )#.interactive()
            # st.altair_chart(timeline)

            hist=alt.Chart(df_array).mark_bar().encode(
                alt.X('paraValue:Q', bin=True, title=titleY),
                y='count()',
                color=alt.Color('institution:N', title="measurement institution"),
                tooltip=[tl for tl in toolList if "date" not in tl]
            ).properties(
                width=800,
                title=sel_paraCode+", "+titleY+" distribution"
            )#.interactive()
            # st.altair_chart(hist)

            ### combined plot
            brush = alt.selection(type='interval')
            combHist=alt.vconcat(
                timeline.encode(
                    x=alt.X('date:T'),
                    y=alt.Y('paraValue:Q', title=titleY),
                    color=alt.Color('institution:N', title="measurement institution"),
                    tooltip=toolList
                ).properties(
                    width=600,
                    title=sel_paraCode+", "+titleY+" timeline (select points for histogram)"
                ).add_selection(brush),
                hist.encode(
                    x=alt.X('paraValue:Q',
                        bin=alt.Bin(maxbins=30)
                    #     , extent=brush),
                    #     scale=alt.Scale(domain=brush)
                    ),
                    y='count()',
                    color=alt.Color('institution:N', title="measurement institution"),
                    tooltip=[tl for tl in toolList if "date" not in tl]
                ).properties(
                    width=600,
                    title=sel_paraCode+", "+titleY+" distribution (of selected points)"
                ).transform_filter(brush)
            )

            st.altair_chart(combHist)
            st.dataframe(df_sub)
            st.download_button(label="Write csv", data=df_sub.to_csv(index=False),file_name="testResults_"+pageDict['projCode']+"_"+pageDict['compCode']+"_"+pageDict['testCode']+"_"+sel_paraCode+".csv")


        ### comb. array
        elif sel_likeType=="comb. array":
            st.write(f"Combined plot: {sel_paraCodeY} vs. {sel_paraCodeX}")
            ### concatonate x & y
            df_x=df_testRuns.query('paraCode=="'+sel_paraCodeX+'"').rename(columns={"paraValue": sel_paraCodeX})
            if type(df_x[sel_paraCodeX].iloc[0])==type({}):
                st.write("(sigh!) This is a dictionary. Please choose a key:")
                infra.SelectBox(pageDict,'keyCodeX',list(df_x[sel_paraCodeX].iloc[0].keys()),'Select test X parameter:')
                df_x[pageDict['keyCodeX']]=df_x[sel_paraCodeX].apply(lambda x: x[pageDict['keyCodeX']] if type(x)==type({}) and pageDict['keyCodeX'] in x.keys() else None)
                sel_paraCodeX=sel_paraCodeX+"->"+pageDict['keyCodeX']
            df_y=df_testRuns.query('paraCode=="'+sel_paraCodeY+'"').rename(columns={"paraValue": sel_paraCodeY})
            if type(df_y[sel_paraCodeY].iloc[0])==type({}):
                st.write("(sigh!) This is a dictionary. Please choose a key:")
                infra.SelectBox(pageDict,'keyCodeY',list(df_y[sel_paraCodeY].iloc[0].keys()),'Select test Y parameter:')
                df_y[pageDict['keyCodeY']]=df_y[sel_paraCodeY].apply(lambda x: x[pageDict['keyCodeY']] if type(x)==type({}) and pageDict['keyCodeY'] in x.keys() else None)
                sel_paraCodeY=sel_paraCodeY+"->"+pageDict['keyCodeY']
            ### fudge to get around dictionary names
            nameX, nameY = sel_paraCodeX, sel_paraCodeY
            if nameX==nameY:
                st.write("Hey! It's the same parameter.")
                st.stop()
            else:
                df_xy = df_x.merge(df_y, how = 'inner', on = [idCol, 'date', 'institution', 'stage'])

            st.write(f"Compile values: {nameY} (Y) vs. {nameX} (X)")
            df_xy=df_xy[[idCol, 'date', 'institution', 'stage', nameX, nameY]]
            st.write("- drop empty values")
            df_xy=df_xy.dropna().reset_index(drop=True)
            # st.dataframe(df_xy)

            ### force arrays to be same size to explode to points (== df rows)
            df_xy[nameX]=df_xy.apply(lambda row: AlignLengths(row[nameX],row[nameY],st.session_state.debug), axis=1)
            df_xy[nameY]=df_xy.apply(lambda row: AlignLengths(row[nameY],row[nameX],st.session_state.debug), axis=1)

            #st.dataframe(df_xy)
            ### explode together
            df_xy=df_xy.explode([nameX,nameY])
            ### absolutise!
            # display(df_xy)
            df_xy[nameX]=df_xy[nameX].astype('float').abs()
            df_xy[nameY]=df_xy[nameY].astype('float').abs()

            combPlot=alt.Chart(df_xy).transform_calculate(
                    comb="datum."+idCol+" + '-' + datum.testIndex"
            ).mark_line(point=True).encode(
                x=alt.X(nameX+':Q'),
                y=alt.Y(nameY+':Q'),
                color=alt.Color('institution:N', title="measurement institution"),
                shape=alt.Shape('stage:N'),
                strokeDash=alt.StrokeDash('comb:N', legend=None),
                #shape=alt.Shape('index:N', scale=None),
                tooltip=[nameX+':Q',nameY+':Q','institution:N',idCol+':N','alternativeIdentifier:N','stage:N']
            ).properties(
                width=800,
                title="bespoke: "+nameY+" vs. "+nameX
            ).interactive()
            st.altair_chart(combPlot)
            st.dataframe(df_xy)
            st.download_button(label="Write csv", data=df_xy.to_csv(index=False),file_name="testResults_"+pageDict['projCode']+"_"+pageDict['compCode']+"_"+pageDict['testCode']+"_"+nameX+"_"+nameY+".csv")

        else:
            st.write(f"### don't recoginise valueType: {pageDict['valueType']} ###")
