# Generic webApp

Non-project specific PDB tools.

---

Intended for generic database entries. No checks applied, e.g. analysis for test pass/fail.

## Pages

### Single Component Registration
  * register a single PCB

### Multi Component Registration
  * register multiple PCBs using formatted data file

### Single Test Upload
  * upload a single test

### Multi Test Upload
  * upload multiple tests using formatted data file

### Single Test Upload
  * assemble single component

### Multi Test Upload
  * assemble multiple components tests using formatted data file

### Test parameter comparison
  * check parameters for components of selected componentType at user institute
