# base image
FROM registry.cern.ch/itk-streamlit/itk-app-base:multi

### set-up packages
USER root
RUN true \ 
    && apt-get update \
    && apt-get install ffmpeg libsm6 libxext6 git -y \
    && rm -rf ./userPages/theme*

### common code 
COPY commonCode/* ./commonCode/
### get latest stage-test info.
RUN true \
    && /bin/bash commonCode/get_stages_and_tests.sh

### copy in apps
COPY pixelsApps/ModuleApp/userPages/* ./userPages/ModuleApp/
COPY pixelsApps/PCBApp/userPages/* ./userPages/PCBApp/
# COPY pixelsApps/SensorApp/userPages/* ./userPages/SensorApp/
# COPY pixelsApps/ringLoadingApp/userPages/* ./userPages/ringLoadingApp/

COPY generalApps/commonApp/userPages/* ./userPages/commonApp/
COPY reportingApps/conorApp/userPages/* ./userPages/reportingApp/
COPY generalApps/genericApp/userPages/* ./userPages/genericApp/
COPY pixelsApps/SQApp/userPages/* ./userPages/SQApp/

COPY generalApps/CoffeeApp/userPages/* ./userPages/CoffeeApp/

### set main info.
COPY mainFiles/mainAppPixels.py ./mainApp.py
# add build date and anouncements
COPY .git/refs/heads/master gitRef.txt
RUN true \   
    # && git ls-remote https://gitlab.cern.ch/wraight/itk-web-apps --short HEAD | awk '{ print $1}' > gitRef.txt \ 
    && sed -i 's/COMMITCODE/'$(cat gitRef.txt | head -c7)'/g' mainApp.py \
    && sed -i 's/BUILDDATE/'$(date +%d-%m-%y)'/g' mainApp.py \
    && sed -i 's/announcement_text=None/announcement_text="Recent changes: New pages for Module Wirebonding (inc. trim check) and Visual Inspection (inc. image upload) _Please contact if problems_."/g' corePages/pageA.py
COPY requirements_apps.txt ./requirements.txt
RUN pip3 install -r requirements.txt

### tmp credentials for running
RUN true \
    && chown -R appuser:appuser /tmp \
    && chmod 777 /tmp

### run non-root user: appuser exists
USER appuser

# start app with match exposed port
CMD ["streamlit", "run", "mainApp.py","--server.port=8501"]
